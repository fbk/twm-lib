# README #

This library is part of The Wiki Machine project (developed in [Fondazione Bruno Kessler](http://www.fbk.eu), Trento, Italy); it provides a set of tools to parse the Wikipedia XML dumps and create indexed data.

It is used in commercial and non-commercial environments, among them [airpedia](http://www.airpedia.org) (a set of open source tools that can be used to automatically create a set of mappings for the [DBpedia project](http://www.dbpedia.org)) and [twm-service](https://bitbucket.org/cgiuliano/twm-service), an application that provides links to Wikipedia entites in texts written in more than 30 languages.