#! /bin/tcsh

setenv CLASSPATH dist/thewikimachine-alone.jar
setenv CLASSPATH ${CLASSPATH}:dist/thewikimachine-lib.jar

setenv CLASSPATH ${CLASSPATH}:properties/

setenv CLASSPATH ${CLASSPATH}:lib/bliki-core-3.0.19.jar
setenv CLASSPATH ${CLASSPATH}:lib/de.tudarmstadt.ukp.wikipedia.parser-0.9.2.jar
setenv CLASSPATH ${CLASSPATH}:lib/jcore-alone.jar