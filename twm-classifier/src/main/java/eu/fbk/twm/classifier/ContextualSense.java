/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.classifier;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/19/13
 * Time: 2:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContextualSense extends Sense {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>ContextualSense</code>.
	 */
	static Logger logger = Logger.getLogger(ContextualSense.class.getName());

	protected double bow;

	protected double ls;

	public static final String UNKNOWN_SENSE_LABEL = "UNKNOWN";

	public ContextualSense() {
		this(UNKNOWN_SENSE_LABEL, 0, 0, 0);
	}

	public ContextualSense(String page, double prior, double bow, double ls) {
		super(page, prior);
		this.bow = bow;
		this.ls = ls;
	}

	public String getPage() {
		return page;
	}

	public double getCombo() {
		return (bow + ls) / 2;
	}

	public double getBow() {
		return bow;
	}

	public double getLs() {
		return ls;
	}

	@Override
	public String toString() {
		return sf.format(prior) + "\t" + sf.format(bow) + "\t" + sf.format(ls) + "\t" + sf.format(getCombo()) + "\t" + page;
	}
}
