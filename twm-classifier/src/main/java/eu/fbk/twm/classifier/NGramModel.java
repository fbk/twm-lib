/*
 * Copyright (2014) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.classifier;

import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/27/13
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class NGramModel {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>NGramModel</code>.
	 */
	static Logger logger = Logger.getLogger(NGramModel.class.getName());

	public static final int DEFAULT_N_GRAM_LENGTH = 3;

	public static final int DEFAULT_N_GRAM_SIZE = 1000000;

	private static Pattern tabPattern = Pattern.compile("\t");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	//todo: remove ...
	Tokenizer tokenizer;

	private Map<String, Integer> indexMap;

	private Map<String, Double> valueMap;

	private int length;
	private int size;

	/**
	 * Constructs a <code>NGramModel</code> object.
	 */
	public NGramModel(File indexFile, File valueFile, File stopwordsFile) throws IOException {
		this(indexFile, valueFile, stopwordsFile, DEFAULT_N_GRAM_LENGTH, DEFAULT_N_GRAM_SIZE);
	}

	public NGramModel(String indexFileName, String valueFileName, String stopwordsFileName) throws IOException {
		this(new File(indexFileName), new File(valueFileName), new File(stopwordsFileName), DEFAULT_N_GRAM_LENGTH, DEFAULT_N_GRAM_SIZE);
	}

	/**
	 * Constructs a <code>NGramModel</code> object.
	 */
	public NGramModel(File indexFile, File valueFile, File stopwordsFile, int length, int size) throws IOException {
		this.length = length;
		this.size = size;
		tokenizer = new HardTokenizer();
		Set<String> stopwords = readStopwords(stopwordsFile);
		indexMap = readIndex(indexFile, stopwords);
		valueMap = readValue(valueFile, stopwords);
	}

	/**
	 * Constructs a <code>NGramModel</code> object.
	 */
	public NGramModel(String indexFileName, String valueFileName, String stopwordsFileName, int length, int size) throws IOException {
		this(new File(indexFileName), new File(valueFileName), new File(stopwordsFileName), length, size);
		//read(indexFile, length, size);
	}

	public Double getValue(String nGram) {
		//logger.debug("NGramModel.add : " + nGram);
		return valueMap.get(nGram);
	}

	public Integer getIndex(String nGram) {
		//logger.debug(nGram + "\t"  +indexMap.get(nGram));
		return indexMap.get(nGram);
	}

	public int size() {
		return size;
	}

	public int getLength() {
		return length;
	}

	public Set<String> readStopwords(File file) throws IOException {
		long begin = System.currentTimeMillis();
		logger.info("reading " + file + "...");
		Set<String> set = new HashSet<String>();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		StringBuilder sb = new StringBuilder();
		String line = null;
		//int count = 0;
		while ((line = lnr.readLine()) != null) {
			set.add(line);
		}
		lnr.close();
		long end = System.currentTimeMillis();
		logger.info(df.format(set.size()) + " stopwords read in " + df.format(end - begin) + " (" + new Date() + ")");

		return set;
	}


	//todo: remove tokenize
	public Map<String, Integer> readIndex(File file, Set<String> stopwords) throws IOException {
		long begin = System.currentTimeMillis();
		logger.info("reading " + file + "...");
		Map<String, Integer> map = new HashMap<String, Integer>();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		StringBuilder sb = new StringBuilder();
		String line = null;
		//int count = 0;
		while ((line = lnr.readLine()) != null) {
			String[] s = tabPattern.split(line);
			if (s.length == 2) {
				//todo: remove tokenizer and add lowercase
				//String form = s[1].toLowerCase();
				//if (!stopwords.contains(s[1].toLowerCase())) {
					//map.put(tokenizer.tokenizedString(s[1]), new Integer(s[0]));
				//logger.debug("'"+s[1]+"'\t"+s[0]);
				map.put(s[1], new Integer(s[0]));
				//}
				///count++;
			}
		}
		lnr.close();
		long end = System.currentTimeMillis();
		logger.info(df.format(map.size()) + " forms/id read in " + df.format(end - begin) + " (" + new Date() + ")");

		return map;
	}

	public Map<String, Double> readValue(File file, Set<String> stopwords) throws IOException {
		long begin = System.currentTimeMillis();
		logger.info("reading " + file + "...");
		Map<String, Double> map = new HashMap<String, Double>();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		StringBuilder sb = new StringBuilder();
		String line = null;
		//read the first line
		int maxDf = 0;
		if ((line = lnr.readLine()) != null) {
			String[] s = tabPattern.split(line);
			if (s.length == 3) {
				maxDf = new Integer(s[0]);
			}
		}

		//int count=0;
		while ((line = lnr.readLine()) != null) {
			String[] s = tabPattern.split(line);
			if (s.length == 3) {
				//todo: add toLowerCase
				//String form = s[2].toLowerCase();
				//if (!stopwords.contains(s[2].toLowerCase())) {
					int df = new Integer(s[0]);
					int lf = new Integer(s[1]);
					if (lf > 2) {
						//todo: use the max df?
						//double idf = log2(Integer.MAX_VALUE / df);
						double idf = log2(maxDf / df);
						map.put(s[2], idf);
						//count++;
					}
				//}
			}
		}
		lnr.close();
		long end = System.currentTimeMillis();
		logger.info(df.format(map.size()) + " forms/idf read in " + df.format(end - begin) + " (" + new Date() + ")");
		return map;
	}

	public static final double LOG2 = Math.log(2);

	public double log2(double d) {
		return Math.log(d) / LOG2;
	}

	public static void main(String[] args) {
		// java com.ml.test.net.HttpServerDemo
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option formIdFileNameOpt = OptionBuilder.withArgName("file").hasArg().withDescription("form id mapping").isRequired().withLongOpt("form-id").create();
			Option ngramFileNameOpt = OptionBuilder.withArgName("file").hasArg().withDescription("form idf mapping").isRequired().withLongOpt("form-idf").create();
			Option stopwordsFileNameOpt = OptionBuilder.withArgName("file").hasArg().withDescription("stopwords").isRequired().withLongOpt("stopwords").create();
			Option nGramLengthOpt = OptionBuilder.withArgName("int").hasArg().withDescription("n-gram length (default is " + DEFAULT_N_GRAM_LENGTH + ")").withLongOpt("ngram-length").create("l");
			Option nGramSizeOpt = OptionBuilder.withArgName("int").hasArg().withDescription("n-gram length (default is " + DEFAULT_N_GRAM_SIZE + ")").withLongOpt("ngram-size").create("s");


			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(formIdFileNameOpt);
			options.addOption(ngramFileNameOpt);
			options.addOption(nGramLengthOpt);
			options.addOption(nGramSizeOpt);
			options.addOption(stopwordsFileNameOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			logger.debug(options);

			logger.debug(line.getOptionValue("output") + "\t" + line.getOptionValue("input") + "\t" + line.getOptionValue("lsm"));


			boolean normalized = false;
			if (line.hasOption("normalized")) {
				normalized = true;
			}

			int nGramSize = DEFAULT_N_GRAM_SIZE;
			if (line.hasOption("ngram-size")) {
				nGramSize = Integer.parseInt(line.getOptionValue("ngram-size"));
			}

			int nGramLength = DEFAULT_N_GRAM_LENGTH;
			if (line.hasOption("ngram-length")) {
				nGramLength = Integer.parseInt(line.getOptionValue("ngram-length"));
			}


			NGramModel nGramModel = new NGramModel(line.getOptionValue("form-id"), line.getOptionValue("form-idf"), line.getOptionValue("stopwords"));


		} catch (ParseException e) {
			// oops, something went wrong
			System.err.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.OneExamplePerSenseExtractor", "\n", options, "\n", true);
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
