package eu.fbk.twm.classifier;

import org.apache.log4j.Logger;

import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/19/13
 * Time: 6:31 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Sense implements Comparable<ContextualSense> {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Sense</code>.
	 */
	static Logger logger = Logger.getLogger(Sense.class.getName());

	DecimalFormat sf = new DecimalFormat("###,###,###,###.000000");

	protected String page;
	protected double prior;

	public static final String UNKNOWN_SENSE_LABEL = "UNKNOWN";

	public Sense() {
		this(UNKNOWN_SENSE_LABEL, 0);
	}

	public Sense(String page, double prior) {
		this.page = page;
		this.prior = prior;
	}

	public String getPage() {
		return page;
	}

	public abstract double getCombo();


	public double getPrior() {
		return prior;
	}

	@Override
	public int compareTo(ContextualSense sense) {
		double diff = getCombo() - sense.getCombo();
		if (diff > 0) {
			return -1;
		}
		else if (diff < 0) {
			return 1;
		}

		return 0;
	}

	@Override
	public String toString() {
		return sf.format(prior) + "\t" + sf.format(getCombo()) + "\t" + page;
	}

}
