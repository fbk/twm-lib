package eu.fbk.twm.utils;

import org.apache.log4j.Logger;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/27/13
 * Time: 11:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class Vocabulary {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Vocabulary</code>.
	 */
	static Logger logger = Logger.getLogger(Vocabulary.class.getName());

	/**
	 * to do.
	 */
	private Map<String, Integer> map;

	private int total;

	/**
	 * Constructs a <code>Vocabulary</code> object.
	 */
	public Vocabulary() {
		map = new HashMap<String, Integer>();
	} // end constructor

	/**
	 * Add a term to the Vocabulary
	 *
	 * @param term the term.
	 */
	public synchronized int get(String term) {
		//logger.debug("Vocabulary.add : " + term);
		Integer index = map.get(term);

		if (index == null) {
			////System.out.print("\"" + term +  "\"\n");
			index = total++;
			map.put(term, index);
		}

		return index;
		//logger.debug("added " + term + " (" + tf + ")");
	} // end add

	public int size() {
		return map.size();
	}
}
