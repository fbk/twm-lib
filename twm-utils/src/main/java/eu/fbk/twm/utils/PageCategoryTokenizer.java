/*
 * Copyright 2012 FBK (http://www.fbk.eu)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PageCategoryTokenizer {

	static Logger logger = Logger.getLogger(PageCategoryTokenizer.class.getName());
	private Pattern checkPattern = Pattern.compile("^[^:]+:(.+)");

	public PageCategoryTokenizer(String inputFile, String outPutFile) throws IOException {
		this(inputFile, outPutFile, null, false);
	}

	public PageCategoryTokenizer(String inputFile, String outPutFile, String swFile) throws IOException {
		this(inputFile, outPutFile, swFile, false);
	}

	public PageCategoryTokenizer(String inputFile, String outputFile, String swFile, boolean matchPattern) throws IOException {
		String line;

		HashSet<String> swlist = new HashSet<String>();
		if (swFile != null) {
			BufferedReader swFileReader = new BufferedReader(new InputStreamReader(new FileInputStream(swFile), "UTF-8"));
			logger.info("Loading stopwords file...");
			while ((line = swFileReader.readLine()) != null) {
				String trimmed = line.trim();
				if (trimmed.length() > 0) {
					swlist.add(trimmed);
				}
			}
		}

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8"));
		Pattern delimiter = Pattern.compile("\\t");

		BufferedReader fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), "UTF-8"));
		String lastPage = "";
		HashSet<String> collected = new HashSet<String>();
		while ((line = fileReader.readLine()) != null) {

			String[] parts = delimiter.split(line);
			if (parts.length < 2) {
				continue;
			}

			String category = parts[1];
			String page = parts[0];

			if (!lastPage.equals(page)) {
				lastPage = page;
				collected.clear();
			}

			if (matchPattern) {
				Matcher templateMatcher = checkPattern.matcher(category);
				if (!templateMatcher.matches()) {
					continue;
				}
				category = templateMatcher.group(1);
			}

			category = category.replaceAll("[(),]", "_");

			String[] categories = category.split("_+");
			List<String> catlist = Arrays.asList(categories);
			HashSet<String> catset = new HashSet<String>(catlist);
			catset.removeAll(swlist);

			for (String thisCat : catset) {
				thisCat = thisCat.trim().toLowerCase();
				if (!collected.contains(thisCat)) {
					if (thisCat.length() > 0) {
						writer.write(page + "\t" + thisCat + "\n");
						collected.add(thisCat);
					}
				}
			}
		}

		writer.close();
	}

	public static void main(String args[]) throws Exception {

		if (args.length != 3) {
			System.err.println("Wrong number of parameters " + args.length);
			System.err.println("Usage: java -mx8g main.java.org.fbk.cit.hlt.moschitti.PageCategoryTokenizer in-category-file out-category-tokens-file stopwords-file");
			System.exit(-1);
		}

		String inputFile = args[0];
		String outputFile = args[1];
		String swFile = args[2];

		new PageCategoryTokenizer(inputFile, outputFile, swFile);
	} // end main

} // end PageCategoryIndexer
