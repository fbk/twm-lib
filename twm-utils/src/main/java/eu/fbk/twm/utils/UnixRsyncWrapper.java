package eu.fbk.twm.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/12/13
 * Time: 3:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class UnixRsyncWrapper {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>UnixRsyncWrapper</code>.
	 */
	static Logger logger = Logger.getLogger(UnixRsyncWrapper.class.getName());

	public static int rsync(String src, String dst) throws Exception {
		long begin = System.currentTimeMillis();
		String[] cmd = new String[]{"/bin/sh",
				"-c", "/usr/bin/rsync -vaz " + src + " " + dst};
		logger.debug(Arrays.toString(cmd));
		Process p = Runtime.getRuntime().exec(cmd);
		logger.info("synchronizing...");
		p.waitFor();
		int exitValue = p.exitValue();

		InputStream is = p.getInputStream();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(is));
		String line;
		StringBuilder sb = new StringBuilder();
		while ((line = lnr.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}
		logger.info(sb.toString());

		logger.info("rsync exit value: " + exitValue);
		if (exitValue != 0) {
			is = p.getErrorStream();
			lnr = new LineNumberReader(new InputStreamReader(is));
			sb = new StringBuilder();
			while ((line = lnr.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
			logger.error(sb.toString());
		}

		long end = System.currentTimeMillis();
		logger.info(src + " synchronized in " + (end - begin) + " ms");

		return exitValue;
	}

	public static void main(String[] args) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		//java -cp dist/thewikimachine.jar eu.fbk.twm.utils.UnixRsyncWrapper
		String src = args[0];
		String dst = args[1];
		UnixRsyncWrapper.rsync(src, dst);

	}
}
