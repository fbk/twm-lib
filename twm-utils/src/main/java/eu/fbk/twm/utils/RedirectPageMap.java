package eu.fbk.twm.utils;

import eu.fbk.twm.utils.PageMap;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 03/07/13
 * Time: 14:00
 * To change this template use File | Settings | File Templates.
 */
public class RedirectPageMap extends PageMap {

	public RedirectPageMap(File redir) throws IOException {
		super(redir);

		for (String page : map.keySet()) {
			int i = 0;
			String originPage = page;
			while (map.get(page) != null && i++ < 10) {
				page = map.get(page);
			}
			map.put(originPage, page);
		}
	}

	public RedirectPageMap(String fileName) throws IOException {
		this(new File(fileName));
	}

	@Override
	public String toString() {
		return map.toString();
	}
}
