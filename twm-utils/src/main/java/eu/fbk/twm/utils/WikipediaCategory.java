/*
 * Copyright (2014) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

import org.apache.log4j.Logger;

/**
 * Created by giuliano on 12/22/14.
 */
public class WikipediaCategory {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaCategory</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaCategory.class.getName());
	private String label;
	private int depth;
	private String path;

	public WikipediaCategory(String label, int depth, String path) {
		this.label = label;
		this.depth = depth;
		this.path = path;
	}

	public WikipediaCategory(String label) {
		this(label, 0, "");
	}

	public String getPath() {
		return path;
	}

	public String getLabel() {
		return label;
	}

	public int getDepth() {
		return depth;
	}

	@Override
	public String toString() {
		return getPath() + ">" + label + ":" + depth;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof WikipediaCategory)) {
			return false;
		}

		WikipediaCategory category = (WikipediaCategory) o;

		if (label != null ? !label.equals(category.label) : category.label != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return label != null ? label.hashCode() : 0;
	}
}
