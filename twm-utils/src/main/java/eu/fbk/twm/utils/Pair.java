package eu.fbk.twm.utils;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 27/05/14
 * Time: 16:52
 * To change this template use File | Settings | File Templates.
 */

public class Pair<L, R> {

	static Logger logger = Logger.getLogger(Pair.class.getName());

	private final L left;
	private final R right;

	public Pair(L left, R right) {
		this.left = left;
		this.right = right;
	}

	public L getLeft() {
		return left;
	}

	public R getRight() {
		return right;
	}

	@Override
	public String toString() {
		return "Pair{" +
				"left=" + left +
				", right=" + right +
				'}';
	}
}
