/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/9/13
 * Time: 8:22 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractParsedPage {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AbstractParsedPage</code>.
	 */
	static Logger logger = Logger.getLogger(AbstractParsedPage.class.getName());

	protected String form;

	protected String page;

	/**
	 * Returns the string capitalized if it is not.
	 */
	protected String normalizePageName(String s) {
		if (s.length() == 0) {
			return s;
		}

		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		}

		return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
	}


	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public boolean hasCompliantForm() {
		if (form.length() == 0) {
			return false;
		}

		return containsLetters();
	}

	public boolean hasCompliantPage() {
		if (page.length() == 0) {
			return false;
		}
		return true;
	}

	public boolean isCompliant() {
		return hasCompliantPage() && hasCompliantForm();
	}

	private boolean containsLetters() {
		for (int i = 0; i < form.length(); i++) {
			if (Character.isLetter(form.charAt(i))) {
				return true;
			}
		}
		return false;
	}
}
