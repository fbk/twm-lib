package eu.fbk.twm.utils;

/**
 * Created by alessio on 13/10/16.
 */

public class Defaults {

    public final static int DEFAULT_NOTIFICATION_POINT = 10000;
    public final static int DEFAULT_THREADS_NUMBER = 1;
    public final static int DEFAULT_NUM_PAGES = Integer.MAX_VALUE;

}
