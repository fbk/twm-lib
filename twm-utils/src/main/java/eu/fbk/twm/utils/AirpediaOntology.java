package eu.fbk.twm.utils;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 04/02/14
 * Time: 16:46
 * To change this template use File | Settings | File Templates.
 */

public class AirpediaOntology extends DBpediaOntology {

	static Logger logger = Logger.getLogger(AirpediaOntology.class.getName());
	Pattern startsWithSpaces = Pattern.compile("^(\\s*)[^\\s]");

	public AirpediaOntology(String ontologyFile) {
		super(ontologyFile);
		addenda(null);
	}

	public AirpediaOntology(String ontologyFile, String addendaFile) {
		super(ontologyFile);
		addenda(addendaFile);
	}

	private void addenda(String addendaFile) {
		nodes.add(new DBpediaOntologyNode("SportsSeason", null));

		if (addendaFile != null) {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(addendaFile), "utf-8"));
				String line;

				int level = 0;
				ArrayList<String> lastClasses = new ArrayList<>();

				while ((line = reader.readLine()) != null) {

					if (line.trim().startsWith("-")) {
						line = line.trim().substring(1).trim();

						logger.trace(String.format("Removing %s", line));
						DBpediaOntologyNode node = getNodeByName(line);
						if (node != null) {
							removeNode(node);
						}
						continue;
					}

					Matcher m = startsWithSpaces.matcher(line);
					if (m.find()) {
						String spaces = m.group(1);
						int n = spaces.length();
						for (int i = 0; i < spaces.length(); i++) {
							char c = spaces.charAt(i);
							if (c == '\t') {
								n += 3;
							}
						}

						line = line.trim();

						level = n / 4;
						for (int i = lastClasses.size(); i <= level; i++) {
							lastClasses.add(null);
						}
						lastClasses.set(level, line);
						String superClass = null;
						if (level > 0) {
							superClass = lastClasses.get(level - 1);
						}

						logger.trace(String.format("%s [%d] [%s]", line, level, superClass));

						DBpediaOntologyNode node = getNodeByName(line);
						if (node != null) {
							node.superClass = superClass;
						}
						else {
							nodes.add(new DBpediaOntologyNode(line, superClass));
						}
					}
				}
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}

		updateTree();
	}

	public static void main(String[] args) {
		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		commandLineWithLogger.addOption(OptionBuilder.withDescription("Ontology file").isRequired().hasArg().withArgName("file").withLongOpt("ontology").create("o"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("Addenda file").hasArg().withArgName("file").withLongOpt("addenda").create("a"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}

		String ontologyFile = commandLine.getOptionValue("ontology");
		String addendaFile = commandLine.getOptionValue("addenda");

		AirpediaOntology ontology = new AirpediaOntology(ontologyFile, addendaFile);
		System.out.println(ontology);
	}
}
