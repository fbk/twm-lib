package eu.fbk.twm.utils;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 31/01/14
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */

public class DBpediaMapping {

	static Logger logger = Logger.getLogger(DBpediaMapping.class.getName());
	private ArrayList<DBpediaMappingCondition> dBpediaMappingConditions = new ArrayList<DBpediaMappingCondition>();
	private String infobox;
	private String mapping;

	public static HashMap<String, HashMap<String, DBpediaMapping>> loadFromFile(String fileName) {
		return loadFromFile(fileName, true);
	}

	public String getMapping() {
		return mapping;
	}

	public String getInfobox() {
		return infobox;
	}

	public ArrayList<DBpediaMappingCondition> getdBpediaMappingConditions() {
		return dBpediaMappingConditions;
	}

	public static HashMap<String, HashMap<String, DBpediaMapping>> loadFromFile(String fileName, boolean lowerCase) {
		HashMap<String, HashMap<String, DBpediaMapping>> ret = new HashMap<String, HashMap<String, DBpediaMapping>>();

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] parts = line.split("\t");
				if (parts.length < 3) {
					continue;
				}

				String lang = parts[0];
				if (ret.get(lang) == null) {
					ret.put(lang, new HashMap<String, DBpediaMapping>());
				}
				try {
					DBpediaMapping m = new DBpediaMapping(parts);
					String infobox = lowerCase ? m.infobox.toLowerCase() : m.infobox;
					ret.get(lang).put(infobox, m);
				} catch (Exception e) {
					logger.warn(e.getMessage());
				}
			}
			reader.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return ret;
	}

	public class DBpediaMappingCondition {
		String operator;
		String property;
		String value;
		String mapping;

		public DBpediaMappingCondition(String operator, String property, String value, String mapping) {
			this.operator = operator;
			this.property = property;
			this.value = value;
			this.mapping = mapping;
		}

		public DBpediaMappingCondition(String[] pars) {
			this(pars[0], pars[1], pars[2], pars[3]);
		}

		@Override
		public String toString() {
			return "DBpediaMappingCondition{" +
					"operator='" + operator + '\'' +
					", property='" + property + '\'' +
					", value='" + value + '\'' +
					", mapping='" + mapping + '\'' +
					'}';
		}
	}

	public DBpediaMapping(String[] parts) throws Exception {

		if (parts.length < 3) {
			throw new Exception("Invalid row");
		}

		// parts[0] is language, ignored
		infobox = parts[1];
		for (int i = 2; i < parts.length; i++) {
			String[] tParts = parts[i].split("\\|");

			if (tParts.length == 1) {
				mapping = parts[i];
				continue;
			}
			if (tParts.length < 4) {
				throw new Exception("Line skipped: " + parts[i]);
			}

			DBpediaMappingCondition condition = new DBpediaMappingCondition(tParts);
			dBpediaMappingConditions.add(condition);
		}
	}

	@Override
	public String toString() {
		return "DBpediaMapping {\n" +
				"\tinfobox='" + infobox + '\'' + ",\n" +
				"\tmapping='" + mapping + '\'' + ",\n" +
				"\tdBpediaMappingConditions=" + dBpediaMappingConditions + ",\n" +
				"}";
	}

	public String applyConditions(HashMap<String, String> parts) {
		for (DBpediaMappingCondition c : dBpediaMappingConditions) {
			switch (c.operator) {
				case "isSet":
					if (parts.containsKey(c.property) && parts.get(c.property).trim().length() > 0) {
						logger.trace(String.format("Condition isSet on %s = %s", c.property, c.value));
						return c.mapping;
					}
					break;
				case "equals":
					if (parts.containsKey(c.property.toLowerCase())) {
						if (parts.get(c.property.toLowerCase()).toLowerCase().equals(c.value.toLowerCase())) {
							logger.trace(String.format("Condition equals on %s = %s", c.property, c.value));
							return c.mapping;
						}
					}
					break;
				case "contains":
					if (parts.containsKey(c.property.toLowerCase())) {
						if (parts.get(c.property.toLowerCase()).toLowerCase().contains(c.value.toLowerCase())) {
							logger.trace(String.format("Condition contains on %s = %s", c.property, c.value));
							return c.mapping;
						}
					}
					break;
				case "otherwise":
					logger.trace("Condition otherwise");
					return c.mapping;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		commandLineWithLogger.addOption(OptionBuilder.withDescription("File name").isRequired().hasArg().withArgName("file").withLongOpt("filename").create("f"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}

		String fileName = commandLine.getOptionValue("f");
		HashMap<String, HashMap<String, DBpediaMapping>> mappings = DBpediaMapping.loadFromFile(fileName);
		for (String lang : mappings.keySet()) {
			System.out.format("*** %s ***\n", lang.toUpperCase());
			for (DBpediaMapping m : mappings.get(lang).values()) {
				System.out.println(m);
			}
		}
	}

}
