/*
 * Copyright (2014) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils.analysis;

import org.apache.log4j.Logger;
import eu.fbk.twm.utils.CharacterTable;

import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/27/13
 * Time: 12:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class NGramExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageNGramExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(NGramExtractor.class.getName());

	DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static NGramExtractor ourInstance;


	private static Pattern tabPattern = Pattern.compile("\t");

	int nGramLength;

	Tokenizer tokenizer;

	public NGramExtractor(int nGramLength) {
		this.nGramLength = nGramLength;
		tokenizer = new HardTokenizer();
	}

	private String tokenizedForm(Token[] tokenArray, int start, int end) {
		StringBuilder sb = new StringBuilder();
		sb.append(tokenArray[start].getForm());
		for (int i = start + 1; i <= end; i++) {
			sb.append(CharacterTable.SPACE);
			sb.append(tokenArray[i].getForm());
		}
		return sb.toString();
	}

	/*
	public List<NGram> extract(Token[] tokenArray, String text) {
		long tbegin = System.nanoTime();
		Token firstToken = null, lastToken = null;
		int m = 0;
		String form = null, tokenizedForm;
		NGram ngram = null;
		int start = 0, end = 0;

		for (int i = 0; i < tokenArray.length; i++) {
			firstToken = tokenArray[i];
			start = firstToken.getStart();
			m = i + nGramLength + 1;
			if (m > tokenArray.length) {
				m = tokenArray.length;
			}
			for (int j = i; j < m; j++) {
				lastToken = tokenArray[j];
				end = lastToken.getEnd();
				//logger.debug(start + "\t" + end);
				form = text.substring(start, end);
				tokenizedForm = tokenizedForm(tokenArray, i, j);

			}
		}

		long tend = System.nanoTime();
		long time = tend - tbegin;
		//logger.info(count + "/" + keywordArray.length + " keywords filtered in " + nf.format(time) + " ns");
		return null;
	}
	*/
	public List<String> extract(String text) {
		return extract(tokenizer.tokenArray(text), text);
	}

	public List<String> extract(Token[] tokenArray, String text) {
		//long tbegin = System.nanoTime();
		Token firstToken = null, lastToken = null;
		int m = 0;
		String form = null, tokenizedForm;
		List<String> list = new ArrayList<String>();
		int start = 0, end = 0;

		for (int i = 0; i < tokenArray.length; i++) {
			firstToken = tokenArray[i];
			start = firstToken.getStart();
			m = i + nGramLength + 1;
			if (m > tokenArray.length) {
				m = tokenArray.length;
			}
			for (int j = i; j < m; j++) {
				lastToken = tokenArray[j];
				end = lastToken.getEnd();
				//logger.debug(start + "\t" + end);
				//form = text.substring(start, end);
				tokenizedForm = tokenizedForm(tokenArray, i, j);
				list.add(tokenizedForm);
			}
		}

		//long tend = System.nanoTime();
		//long time = tend - tbegin;
		//logger.info(count + "/" + keywordArray.length + " keywords filtered in " + nf.format(time) + " ns");
		return list;
	}


}
