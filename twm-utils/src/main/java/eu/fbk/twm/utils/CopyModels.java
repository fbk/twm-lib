package eu.fbk.twm.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/12/13
 * Time: 3:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class CopyModels {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CopyModels</code>.
	 */
	static Logger logger = Logger.getLogger(CopyModels.class.getName());

	public static void main(String[] args) {

		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		//java -cp dist/thewikimachine.jar eu.fbk.twm.utils.CopyModels folder
		String folder = args[0];

		try {
			Map<String, String> resourceMap = GenericFileUtils.searchForFilesInTheSameFolder(folder, "type-index", "page-form-index", "form-page-index", "ngram-index", "page-freq.csv", "form-freq", "cross-lang-index", "ngram.csv", "unigram", "one-example-per-sense-index", "page-file-source-index", "first-name-index", "person-info-index", "airpedia-class-index", "abstract-index", "page-category-index", "category-super-category-index", "page-vector-index", "incoming-outgoing-weighted-index", "cross-lang.csv");
			Iterator<String> it = resourceMap.keySet().iterator();
			for (int i = 0; it.hasNext() ; i++) {
				String s = it.next();
				String v = resourceMap.get(s);
				logger.info(i + "\t" + s + "\t" + v);
				//UnixRsyncWrapper.rsync()

			}
			logger.info(resourceMap);
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
