package eu.fbk.twm.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 23/01/14
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */
public class FakeExtractorParameters extends ExtractorParameters {

	public FakeExtractorParameters(String lang, String baseDir) throws FileNotFoundException {
		this(lang, baseDir, null);
	}

	public FakeExtractorParameters(String lang, String baseDir, String version) throws FileNotFoundException {
		this(lang, baseDir, version, true);
	}

	public FakeExtractorParameters(String lang, String baseDir, String version, boolean checkExistence) throws FileNotFoundException {
		super(lang, baseDir, false);

		this.wikipediaXmlFileName = null;
		this.wikipediaDirName = null;

		if (!baseDir.endsWith(File.separator)) {
			baseDir += File.separator;
		}
		baseDir += lang + File.separator;

		if (version == null) {
			File base = new File(baseDir);
			File[] listOfFiles = base.listFiles();
			if (listOfFiles == null) {
				return;
			}
			Arrays.sort(listOfFiles, Collections.reverseOrder());

			for (File f : listOfFiles) {
				if (f.isDirectory()) {
					this.lang = lang;
					this.locale = new Locale(lang);
					this.version = f.getName();
					setNames(f.getAbsolutePath());
					return;
				}
			}
			throw new FileNotFoundException(String.format("Folder for [%s] not found", lang));
		}
		else {
			baseDir += version + File.separator;
			this.lang = lang;
			this.locale = new Locale(lang);
			this.version = version;
			setNames(baseDir);

			if (checkExistence) {
				File f = new File(baseDir);
				if (!f.isDirectory()) {
					throw new FileNotFoundException(String.format("Folder for [%s, %s] not found", lang, version));
				}
			}
		}
	}
}
