package eu.fbk.twm.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 2/11/13
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class AgreementCalculation {
	public static void main(String[] args) {

		if (args.length < 2) {
			System.exit(0);
		}

		try {
			ArrayList<String> fileNames = new ArrayList<String>();
			HashMap<String, ArrayList<String>> values = new HashMap<String, ArrayList<String>>();

//			ArrayList<String> propNames = new ArrayList<String>();
			HashSet<String> propNamesTmp = new HashSet<String>();
//			int k = propNames.size();
//			System.out.println(k);

			fileNames.addAll(Arrays.asList(args).subList(0, args.length));
			int n = fileNames.size();

			Integer N = null;
			for (String fileName : fileNames) {
				BufferedReader r = new BufferedReader(new FileReader(fileName));
				String line;
				int i = 0;
				while ((line = r.readLine()) != null) {
					if (line.trim().length() > 0) {
						propNamesTmp.add(line.trim());
						i++;
					}
				}
				if (N == null || i > N) {
					N = i;
				}
			}

			int k = propNamesTmp.size();
			double[][] nij = new double[N][k];
			ArrayList<String> propNames = new ArrayList<String>(propNamesTmp);

			for (String fileName : fileNames) {
				values.put(fileName, new ArrayList<String>());
				BufferedReader r = new BufferedReader(new FileReader(fileName));
				String line;
				int i = 0;
				while ((line = r.readLine()) != null) {
					String[] parts = line.split("\\*?\\s+");
					String className = parts[0];
					nij[i][propNames.indexOf(className)]++;

					values.get(fileName).add(className);
					i++;
					if (i >= N) {
						System.out.println(fileName + " " + values.get(fileName).size());
						break;
					}
				}
			}

			// Simple calculation
			if (n == 2) {
				int identical = 0;
				for (int i = 0; i < N; i++) {
					if (values.get(fileNames.get(0)).get(i).equals(values.get(fileNames.get(1)).get(i))) {
						identical++;
					}
				}
				double agree = (1.0 * identical) / N;
				System.out.println("Simple agreement [" + identical + "/" + N + "]: " + agree);
			}

			// Calculation of p
			double[] pj = new double[k];
			for (int j = 0; j < k; j++) {
				int sum = 0;
				for (int i = 0; i < N; i++) {
					sum += nij[i][j];
				}
				pj[j] = 1.0 * sum / (N * n);
			}

			// Calculation of P
			double[] P = new double[N];
			for (int i = 0; i < N; i++) {
				int sum = 0;
				for (int j = 0; j < k; j++) {
					sum += nij[i][j] * (nij[i][j] - 1);
				}
				P[i] = 1.0 * sum / (n * (n - 1));
			}

			double Pline = 0.0;
			for (int i = 0; i < N; i++) {
				Pline += P[i];
			}
			Pline = Pline / N;

			double Peline = 0.0;
			for (int j = 0; j < k; j++) {
				Peline += pj[j] * pj[j];
			}

			double kappa = (Pline - Peline) / (1.0 - Peline);
			System.out.println(kappa);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
