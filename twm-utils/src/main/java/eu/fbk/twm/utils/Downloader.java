package eu.fbk.twm.utils;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 29/01/14
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */

import org.apache.log4j.Logger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

public class Downloader {

	static Logger logger = Logger.getLogger(Downloader.class.getName());

	static void updateProgress(String beforeText, double progressPercentage) {
		System.out.print("\r");

		System.out.print(beforeText);
		System.out.print(" - ");

//		System.out.print("[");
//		int i = 0;
//		for (; i <= (int) (progressPercentage * width); i++) {
//			System.out.print(".");
//		}
//		for (; i < width; i++) {
//			System.out.print(" ");
//		}
//		System.out.print("] ");

		System.out.format("%6.2f", progressPercentage * 100);
		System.out.print(" %");
	}

	public static void Download(String siteURL, String filename) throws Exception {
		try {
			URL url = new URL(siteURL);
			HttpURLConnection connection =
					(HttpURLConnection) url.openConnection();
			long filesize = Long.parseLong(connection.getHeaderField("Content-Length"));
			logger.debug("File size: " + filesize);

			int buffer = 1024;
			if (filesize > 0) {
				buffer *= 1024;
			}

			long totalDataRead = 0;
			java.io.BufferedInputStream in = new java.io.BufferedInputStream(connection.getInputStream());
			java.io.FileOutputStream fos = new java.io.FileOutputStream(filename);
			java.io.BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
			byte[] data = new byte[buffer];
			int i;

			while ((i = in.read(data, 0, buffer)) >= 0) {
				totalDataRead = totalDataRead + i;
				bout.write(data, 0, i);
				double Percent = (1.0 * totalDataRead) / filesize;
				updateProgress((new File(filename)).getName(), Percent);
			}
			System.out.println();

			bout.close();
			in.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public static void main(String[] args) {
		try {
			Download("http://dumps.wikimedia.org/enwiki/20140102/enwiki-20140102-pages-meta-history3.xml-p000053243p000055000.7z", "/tmp/prova.7z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
