/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils.analysis;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/20/13
 * Time: 9:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class SpaceTokenizer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>SpaceTokenizer</code>.
	 */
	static Logger logger = Logger.getLogger(SpaceTokenizer.class.getName());

	private static SpaceTokenizer ourInstance = null;

	public static synchronized SpaceTokenizer getInstance() {
		if (ourInstance == null) {
			ourInstance = new SpaceTokenizer();
		}
		return ourInstance;
	}

	public String[] stringArray(String text) {
		if (text.length() == 0) {
			return new String[0];
		}

		List<String> list = new ArrayList<String>();


		return list.toArray(new String[list.size()]);
	}


	public Token[] tokenArray(String text) //throws Exception
	{
		if (text.length() == 0) {
			return new Token[0];
		}
		List<Token> list = new ArrayList<Token>();

		return list.toArray(new Token[list.size()]);
	}


	public static void main(String argv[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		// java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.analysis.SpaceTokenizer


	}
}
