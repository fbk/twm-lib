package eu.fbk.twm.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/4/13
 * Time: 10:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class StringKernel {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>StringKernel</code>.
	 */
	static Logger logger = Logger.getLogger(StringKernel.class.getName());
	//
	private double lambda;

	//
	private int length;

	public static String join(Collection<String> col, String delim) {
		StringBuilder sb = new StringBuilder();
		Iterator<String> iter = col.iterator();
		if (iter.hasNext()) {
			sb.append(iter.next());
		}
		while (iter.hasNext()) {
			sb.append(delim);
			sb.append(iter.next());
		}
		return sb.toString();
	}

	public static int getLevenshteinDistance(String s, String t) {
		if (s == null || t == null) {
			throw new IllegalArgumentException("Strings must not be null");
		}

      /*
		 The difference between this impl. and the previous is that, rather
         than creating and retaining a matrix of size s.length()+1 by t.length()+1,
         we maintain two single-dimensional arrays of length s.length()+1.  The first, d,
         is the 'current working' distance array that maintains the newest distance cost
         counts as we iterate through the characters of String s.  Each time we increment
         the index of String t we are comparing, d is copied to p, the second int[].  Doing so
         allows us to retain the previous cost counts as required by the algorithm (taking
         the minimum of the cost count to the left, up one, and diagonally up and to the left
         of the current cost count being calculated).  (Note that the arrays aren't really
         copied anymore, just switched...this is clearly much better than cloning an array
         or doing a System.arraycopy() each time  through the outer loop.)

         Effectively, the difference between the two implementations is this one does not
         cause an out of memory condition when calculating the LD over two very large strings.
       */

		int n = s.length(); // length of s
		int m = t.length(); // length of t

		if (n == 0) {
			return m;
		}
		else if (m == 0) {
			return n;
		}

		if (n > m) {
			// swap the input strings to consume less memory
			String tmp = s;
			s = t;
			t = tmp;
			n = m;
			m = t.length();
		}

		int p[] = new int[n + 1]; //'previous' cost array, horizontally
		int d[] = new int[n + 1]; // cost array, horizontally
		int _d[]; //placeholder to assist in swapping p and d

		// indexes into strings s and t
		int i; // iterates through s
		int j; // iterates through t

		char t_j; // jth character of t

		int cost; // cost

		for (i = 0; i <= n; i++) {
			p[i] = i;
		}

		for (j = 1; j <= m; j++) {
			t_j = t.charAt(j - 1);
			d[0] = j;

			for (i = 1; i <= n; i++) {
				cost = s.charAt(i - 1) == t_j ? 0 : 1;
				// minimum of cell to the left+1, to the top+1, diagonally left and up +cost
				d[i] = Math.min(Math.min(d[i - 1] + 1, p[i] + 1), p[i - 1] + cost);
			}

			// copy current distance counts to 'previous row' distance counts
			_d = p;
			p = d;
			d = _d;
		}

		// our last action in the above loop was to switch d and p, so p now
		// actually has the most recent cost counts
		return p[n];
	}


	//
	public StringKernel(double lambda, int length) {
		this.lambda = lambda;
		this.length = length;

//		logger.info("lambda: " + lambda);
//		logger.info("length: " + length);
	} // end constructor

	//
	public double get(String s1, String s2) {
		return k(s1.split(" "), s2.split(" "), length);
	} // end compare

	public double getNormalized(String[] s1, String[] s2) {
		double k0 = k(s1, s2, length);
		double k1 = k(s1, s1, length);
		double k2 = k(s2, s2, length);
		return k0 / Math.sqrt(k1 * k2);
	}

	//
	private double k(String[] s, String[] t, int n) {
//logger.debug("k('" + new String(s) + "', '" + new String(t) + "', " + n + ")");
//logger.debug("k(" + s + ", " + t +")");
		if (Math.min(s.length, t.length) < n) {
//logger.debug("k.return 0");
			return 0;
		}

		String[] p = prefix(s);
		String x = last(s);

//logger.debug("k.p: " + new String(p));
//logger.debug("k.x: " + x);

		return k(p, t, n) + sumk(p, t, x, n - 1);
	} // end k

	//
	private double sumk(String[] s, String[] t, String x, int n) {
//logger.debug("sumk('" + new String(s) + "', '" + new String(t) + "', '" +  x + "', " + n + ")");

		double sum = 0;

		for (int j = 0; j < t.length; j++) {
//logger.debug("sumk.t[" + j + "]='" + t[j] + "' vs. '" + x + "'");

			if (t[j].equals(x)) {
//logger.debug("sumk.t[" + j + "]='" + x + "'");
				sum += kp(s, prefix(t, j), n) * Math.pow(lambda, 2);
//logger.debug("sumk.sum[1.." + j + "]=" + sum);
			}
		}

//logger.debug("sumk.sum: " + sum);
		return sum;
	} // end sumk

	//
	private double kp(String[] s, String[] t, int n) {
//logger.debug("kp('" + new String(s) + "', '" + new String(t) + "', " + n + ")");

		if (n == 0) {
//logger.debug("kp.return 1");
			return 1;
		}

		if (Math.min(s.length, t.length) < n) {
//logger.debug("kp.return 0");
			return 0;
		}

		String[] p = prefix(s);
		String x = last(s);

		return (lambda * kp(p, t, n)) + sumkp(p, t, x, n - 1);
	} // end kp

	//
	private double sumkp(String[] s, String[] t, String x, int n) {
//logger.debug("sumkp('" + new String(s) + "', '" + new String(t) + "', '" +  x + "', " + n + ")");

		double sum = 0;

		for (int j = 0; j < t.length; j++) {
//logger.debug("sumkp.t[" + j + "]=" + x + "' vs. " + x + "'");

			if (t[j].equals(x)) {
//logger.debug("sumk.t[" + j + "]='" + x + "'");
				sum += kp(s, prefix(t, j), n) * Math.pow(lambda, t.length - j + 1);
//logger.debug("sumkp.sum[1.." + j + "]=" + sum);
			}
		}

//logger.debug("sumkp.sum: " + sum);
		return sum;
	} // end sumkp

	//
	private String last(String[] t) {
		return t[t.length - 1];
	} // end last

	//
	private String[] prefix(String[] t) {
		return prefix(t, t.length - 1);
	} // end prefix

	//
	private String[] prefix(String[] s, int j) {
//logger.debug("prefix: '" + new String(s) + "', " + j);

		if (j < 0) {
			return null;
		}

		String[] d = new String[j];

		for (int i = 0; i < j; i++) {
			d[i] = s[i];
		}

		return d;
	} // end prefix

	//
	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 4) {
			System.err.println("java org.itc.irst.tcc.kre.ml.kernels.StringKernel lambda length s t");
			System.exit(-1);
		}

		double lambda = Double.parseDouble(args[0]);
		int length = Integer.parseInt(args[1]);
		String s = args[2];
		String t = args[3];

		StringKernel sk = new StringKernel(lambda, length);
		double k0 = sk.get(s, t);
		double k1 = sk.get(s, s);
		double k2 = sk.get(t, t);
		double k = k0 / Math.sqrt(k1 * k2);
		logger.info("k('" + s + "', '" + t + "')=" + k);
	} // end main

}

