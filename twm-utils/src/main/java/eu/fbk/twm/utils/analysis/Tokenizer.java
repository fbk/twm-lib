package eu.fbk.twm.utils.analysis;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/15/13
 * Time: 2:03 PM
 * To change this templatePageCounter use File | Settings | File Templates.
 */
public interface Tokenizer {
	public abstract Token[] tokenArray(String text);

	public abstract String[] stringArray(String text);

	public abstract String tokenizedString(String text);
}
