/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils.analysis;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.Serializable;


/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/22/12
 * ServiceTime: 5:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class NGram extends Token implements Serializable//, Comparable<NGram>
{
	//
	private static final long serialVersionUID = 1024396602591514749L;

	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>NGram</code>.
	 */
	static Logger logger = Logger.getLogger(NGram.class.getName());

	protected int nGramLength;

	boolean filtered;

	private int startIndex;

	private int endIndex;

	public NGram(int start, int end, int startIndex, int endIndex, String form) {
		super(start, end, form);
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		//this.nGramLength = nGramLength;
		nGramLength = endIndex - startIndex + 1;
		filtered = false;
	} // constructor

	public int getStartIndex() {
		return startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public boolean isFiltered() {
		return filtered;
	}

	public void setFiltered(boolean filtered) {
		this.filtered = filtered;
	}

	public int getNGramLength() {
		return nGramLength;
	}

	public void setNGramLength(int nGramLength) {
		this.nGramLength = nGramLength;
	}

	/**
	 * Returns <code>true</code> if this ngram precedes the specified ngram; <code>false</code> otherwise.
	 *
	 * @param anotherNGram ngram whose relative position with this ngram must be tested.
	 * @return <code>true</code> if this ngram precedes the specified ngram; <code>false</code> otherwise.
	 */
	public boolean precedes(NGram anotherNGram) {
		if (endIndex == anotherNGram.getStartIndex() - 1) {
			return true;
		}
		return false;
	}

	/**
	 * Returns <code>true</code> if this ngram follows the specified ngram; <code>false</code> otherwise.
	 *
	 * @param anotherNGram ngram whose relative position with this ngram must be tested.
	 * @return <code>true</code> if this ngram follows the specified ngram; <code>false</code> otherwise.
	 */
	public boolean follows(NGram anotherNGram) {
		if (startIndex == anotherNGram.getEndIndex() + 1) {
			return true;
		}
		return false;
	}

	/**
	 * Returns <code>true</code> if this ngram is adjacent to the specified ngram; <code>false</code> otherwise.
	 *
	 * @param anotherNGram ngram whose adjacency with this ngram must be tested.
	 * @return <code>true</code> if this ngram adjacent to the specified ngram; <code>false</code> otherwise.
	 */
	public boolean isAdjacentTo(NGram anotherNGram) {
		return precedes(anotherNGram) || follows(anotherNGram);
	}

	public boolean equals(NGram anotherNGram) {
		return start == anotherNGram.getStart() && end == anotherNGram.getEnd() && form.equals(anotherNGram.getForm());
	}

	public boolean equals(Object obj) {
		if (obj instanceof NGram) {
			return equals((NGram) obj);
		}

		return false;
	}

	/*public boolean isCapitalized()
	{
		for (int i = 0; i < ; i++) {

		}
	}*/

	@Override
	public String toString() {
		return nGramLength + "\t" + super.toString();
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);


		if (args.length == 0) {
			logger.info("java com.machinelinking.annotation.tok.NGram");
			System.exit(1);
		}


	}

} // end class NGram


