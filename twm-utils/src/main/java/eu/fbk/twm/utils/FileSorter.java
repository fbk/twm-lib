/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/**
 * This class is an experiment.
 * <p/>
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/19/13
 * Time: 1:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileSorter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>FileSorter</code>.
	 */
	static Logger logger = Logger.getLogger(FileSorter.class.getName());

	private String[][] buffer;

	private static Pattern tabPattern = Pattern.compile("\t");

	private final static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private final static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	private AtomicInteger counter;

	private BlockingQueue<Entry> queue;

	public FileSorter(File in, File out, int size, int col) {
		long begin = System.currentTimeMillis();
		logger.info("sorting " + in + " (" + out + ", " + df.format(size) + ", " + col + " " + new Date() + ")...");
		int step = 0;
		int currentSize;
		int numThreads = 1;
		counter = new AtomicInteger(0);

		queue = new LinkedBlockingQueue<Entry>(1000);
		LineNumberReader reader;
		try {
			reader = new LineNumberReader(new FileReader(in));
			while ((currentSize = read(reader, size, col)) > 0) {
				//File f = File.createTempFile(out.getName(), Integer.toString(step), out.getParentFile());
				File f = new File(out.getParentFile() + File.separator + step + "-prova.csv");
				logger.debug(step + "\t" + buffer.length + " == " + currentSize + "\t" + f);
				//new QuickSort().sort(0, currentSize - 1, col);
				//final int end = currentSize - 1;
				long b = System.nanoTime();

				Thread[] pool = new Thread[numThreads];
				for (int i = 0; i < numThreads; i++) {
					logger.debug("creating thread " + i + "/" + numThreads + "...");
					pool[i] = new QuickSort();
					pool[i].start();
				}

				try {
					queue.put(new Entry(0, currentSize - 1, col));

					for (int i = 0; i < numThreads; i++) {
						logger.debug("joining thread " + pool[i].getName() + "/" + numThreads + "...");
						pool[i].join();
					}

				} catch (InterruptedException e) {
					logger.error(e);
				}

				for (int k = 0; k < currentSize; k++) {
					logger.debug(k + "\t" + buffer[k][col]);
				}

				long e = System.nanoTime();
				logger.debug(df.format(currentSize) + " lines sorted in " + tf.format(e - b) + " ns");
				logger.debug(counter.intValue() + " call to quick sort");
				logger.debug("writing " + f + "\t");
				write(f, currentSize);
				/*stack.push(f);
				if (stack.size() > 5) {
					merge(stack, col);
				} */
				step++;
				System.exit(0);
			}

			//merge(stack, col);
			//File f = stack.pop();
			//logger.info("renaming " + f + " to " + out + "...");
			//f.renameTo(out);
			reader.close();
		} catch (IOException e) {
			logger.error(e);
		}

		long end = System.currentTimeMillis();
		logger.info("process done in " + df.format(end - begin) + " ms " + new Date());
		logger.warn((int) "\n".charAt(0));
		logger.warn((int) "\r".charAt(0));

	}


	class QuickSort extends Thread implements Runnable {
		/*Comparator<T> comparator;

		QuickSort(Comparator<T> comparator)
		{
			this.comparator = comparator;
		}  */

		public void run() {
			for (; ; ) {
				try {
					Entry entry = queue.take();
					int index = partition(entry.getBegin(), entry.getEnd(), entry.getCol());
					logger.debug("IDX " + index + "\t" + entry.getBegin() + "\t" + entry.getEnd());

					if (entry.getBegin() < index - 1) {
						//logger.debug((++count) + "]\t" + begin + "\t" + (index - 1) + "\t" + col);
						//sort(begin, index - 1, col);
						queue.put(new Entry(entry.getBegin(), index - 1, entry.getCol()));
					}
					if (index < entry.getEnd()) {
						//logger.debug((++count) + "}\t" + index + "\t" + end + "\t" + col);
						//sort(index, end, col);
						queue.put(new Entry(index, entry.getEnd(), entry.getCol()));
					}

					if (index >= entry.getEnd()) {
						interrupt();
						return;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
				}
			}


		}

		private void swap(int i, int j) {
			logger.debug("S\t" + i + "\t" + j + "\t" + buffer[i][0] + "\t" + buffer[j][0]);
			String[] tmp = buffer[i];
			buffer[i] = buffer[j];
			buffer[j] = tmp;
		}

		/**
		 * Partition quindi effettua una scansione degli elementi dalla sinistra saltando
		 * quelli più piccoli del pivot e dalla destra saltando quelli più grandi; quindi
		 * scambia gli elementi che arrestano le scansioni e ricomincia.
		 *
		 * @param begin
		 * @param end
		 * @param col
		 * @return
		 */
		public int partition(int begin, int end, int col) {
			logger.debug("P\t" + begin + "\t" + end + "\t" + col);
			int i = begin, j = end;
			String pivot = buffer[(begin + end) / 2][col];
			logger.debug("pivot " + pivot);
			while (i <= j) {
				while (buffer[i][col].compareTo(pivot) < 0) {
					logger.debug("I\t" + i + "\t" + buffer[i][col] + "\t" + pivot);
					i++;
				}
				while (buffer[j][col].compareTo(pivot) > 0) {
					logger.debug("J\t" + j + "\t" + buffer[j][col] + "\t" + pivot);
					j--;
				}
				if (i <= j) {
					swap(i, j);
					i++;
					j--;
				}
			}
			logger.debug("index = " + i);
			for (int k = begin; k < end; k++) {
				logger.debug(k + "\t" + buffer[k][col]);
			}

			return i;
		}
	}

	private void write(File out, int currentSize) throws IOException {
		long begin = System.nanoTime();
		logger.info("writing " + df.format(currentSize) + " lines in " + out + "...");
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(out)));

		for (int i = 0; i < currentSize; i++) {
			if (buffer[i].length > 0) {
				pw.print(buffer[i][0]);
			}
			for (int j = 1; j < buffer[i].length; j++) {
				pw.print(CharacterTable.HORIZONTAL_TABULATION);
				pw.print(buffer[i][j]);
			}
			pw.print(CharacterTable.LINE_FEED);
		}

		pw.flush();
		pw.close();
		long end = System.nanoTime();
		logger.info(df.format(currentSize) + " lines wrote in " + tf.format(end - begin) + " ns " + new Date());
	}

	private int read(LineNumberReader reader, int size, int col) throws IOException {
		long begin = System.nanoTime();
		logger.info("reading " + df.format(size) + " lines starting from " + reader.getLineNumber() + "... " + new Date());
		buffer = new String[size][];
		String line;
		int j;
		for (j = 0; j < size && (line = reader.readLine()) != null; j++) {
			buffer[j] = tabPattern.split(line);
		}

		long end = System.nanoTime();
		logger.info("read " + df.format(j) + " lines in " + tf.format(end - begin) + " ns " + new Date());

		return j;
	}

	class Entry {
		int begin;

		int end;

		int col;

		Entry(int begin, int end, int col) {
			this.begin = begin;
			this.end = end;
			this.col = col;
		}

		public int getCol() {
			return col;
		}

		public void setCol(int col) {
			this.col = col;
		}

		public int getBegin() {
			return begin;
		}

		public void setBegin(int begin) {
			this.begin = begin;
		}

		public int getEnd() {
			return end;
		}

		public void setEnd(int end) {
			this.end = end;
		}
	}

	public static void main(String args[]) {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		File in = new File(args[0]);
		File out = new File(args[1]);
		int size = Integer.parseInt(args[2]);
		int col = Integer.parseInt(args[3]);

		FileSorter fileSorter = new FileSorter(in, out, size, col);

	}
}
