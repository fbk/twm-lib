package eu.fbk.twm.utils;

import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.Map;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 28/01/14
 * Time: 18:23
 * To change this template use File | Settings | File Templates.
 */
public class CheckLangModelFolder extends BaseFolder {

	static Logger logger = Logger.getLogger(CheckLangModelFolder.class.getName());

	public void start(String lang, String version) {

		Map<String, TreeSet<Integer>> versions;
		if (lang == null) {
			versions = getPresentVersions();
		}
		else {
			versions = getPresentVersions(new String[]{lang});
		}

		for (String language : versions.keySet()) {
			FakeExtractorParameters extractorParameters;
			try {
				extractorParameters = new FakeExtractorParameters(language, folders.get("base"), version);
			} catch (Exception e) {
				logger.error(e.getMessage());
				continue;
			}
			logger.info(String.format("Getting folder %s", extractorParameters.getExtractionOutputDirName()));
		}
	}

	public static void main(String[] args) {

		CheckLangModelFolder checkLangModelFolder = new CheckLangModelFolder();

		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		checkLangModelFolder.extendCommandLine(commandLineWithLogger);
		commandLineWithLogger.addOption(
                org.apache.commons.cli.OptionBuilder.withDescription("Language").hasArg().withArgName("iso-code").withLongOpt("language").create("l"));
		commandLineWithLogger.addOption(
                OptionBuilder.withDescription("Version").hasArg().withArgName("version").withLongOpt("wiki-version").create("w"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}

		String lang = commandLine.getOptionValue("language");
		String version = commandLine.getOptionValue("wiki-version");
		checkLangModelFolder.init(commandLine);
		checkLangModelFolder.start(commandLine.getOptionValue("language"), commandLine.getOptionValue("wiki-version"));
	}
}
