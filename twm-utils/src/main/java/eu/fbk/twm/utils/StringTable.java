/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/21/13
 * Time: 8:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class StringTable {
	public final static String EMPTY_STRING = "";

	public final static String QUOTATION_MARK = "\"";

	public final static String APOSTROPHE = "'";

	public final static String LEFT_PARENTHESIS = "(";

	public final static String RIGHT_PARENTHESIS = ")";

	public final static String LOW_LINE = "_";

	public final static String SPACE = " ";

	public final static String NUMBER_SIGN = "#";

	public final static String SOLIDUS = "/";

	public final static String FULL_STOP = ".";

	public final static String VERTICAL_LINE = "|";

	public static final String HORIZONTAL_TABULATION = "\t";

	public static final String LINE_FEED = "\n";

	public static final String FORM_FEED = "\f";

	public static final String CARRIADGE_RETURN = "\r";

	public static final String PLUS_SIGN = "+";


}
