package eu.fbk.twm.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/10/13
 * Time: 5:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class Pippo {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Pippo</code>.
	 */
	static Logger logger = Logger.getLogger(Pippo.class.getName());

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}
		//java -mx10G -cp dist/thewikimachine.jar eu.fbk.twm.utils.Pippo
		PropertyConfigurator.configure(logConfig);
		File oldFile = new File(args[0]); // the one edited
		File newFile = new File(args[1]);
		File outFile = new File(args[2]);

		Properties oldProperties = new Properties();
		oldProperties.load(new BufferedReader(new InputStreamReader(new FileInputStream(oldFile), "UTF-8")));
		logger.debug(oldProperties.size() + " read");
		//Properties newProperties = new Properties();
		//newProperties.load(new BufferedReader(new InputStreamReader(new FileInputStream(newFile), "UTF-8")));

		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8")));

		LineNumberReader lnr = new LineNumberReader(new BufferedReader(new InputStreamReader(new FileInputStream(newFile), "UTF-8")));
		String line = null;
		int a = 0, b = 0;
		while ((line = lnr.readLine()) != null) {
			String s = line.trim();

			String oldValue = oldProperties.getProperty(s);
			if (oldValue != null) {
				pw.print(s);
				pw.print("=");
				pw.print(oldValue);
				a++;
			} else {
				pw.print("#");
				pw.print(s);
				pw.print("=Factotum");
				b++;
			}
			pw.print("\n");
		}
		pw.close();
		logger.debug(a +"\t" + b + "\t" + (a+b));
	}
}
