package eu.fbk.twm.utils;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 05/02/14
 * Time: 11:00
 * To change this template use File | Settings | File Templates.
 */

public class ExtendedProperties extends Properties {

	static Logger logger = Logger.getLogger(ExtendedProperties.class.getName());

	public void loadFromFile(String fileName) throws IOException {

		logger.info("Reading " + fileName + "...");

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.startsWith("#")) {
					continue;
				}
				if (line.length() == 0) {
					continue;
				}

				String[] parts = line.split("=");
				if (parts.length < 1) {
					continue;
				}

				String key = parts[0];
				String[] keyParts = key.split("\t");
				key = keyParts[keyParts.length - 1];

				String value = "";

				//todo: predisporre il multi-label
				if (parts.length >= 2) {
					String[] values = parts[1].split(",");
					for (String v : values) {
						if (v.trim().length() > 0) {
							value = v;
							break;
						}
					}
				}
				this.put(key, value);
			}
			reader.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		logger.debug(this.size() + " mappings read");
	}

	public ExtendedProperties() {
		super();
	}

	public ExtendedProperties(String fileName) throws IOException {
		super();
		loadFromFile(fileName);
	}
}
