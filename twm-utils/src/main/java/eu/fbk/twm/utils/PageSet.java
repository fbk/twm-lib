/*
 * Copyright (2010) Fondazione Bruno Kessler (FBK)
 * 
 * FBK reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced
 * in any form whatsoever except as provided by license
 * without the written consent of FBK.  A license under FBK's
 * rights in the Program may be available directly from FBK.
 */

package eu.fbk.twm.utils;

import java.io.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * This class represent a set of disabiguation pages. The set is
 * read from a filePageCounter with format:
 * <p/>
 * <code>(disambiguation_page \n)+</code>
 *
 * @author Claudio Giuliano
 * @version 1.0
 * @since 1.0
 */
public class PageSet {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageSet</code>.
	 */
	static Logger logger = Logger.getLogger(PageSet.class.getName());

	//
	protected Set<String> set;

	//
	public PageSet() throws IOException {
		set = new HashSet<String>();
	} // end 

	//
	public PageSet(File dis) throws IOException {
		long begin = System.currentTimeMillis(), end = 0;
		logger.info("reading " + dis + "...");
		set = read(dis);
		end = System.currentTimeMillis();
		logger.info(set.size() + " pages read in " + (end - begin) + " ms");
	} // end 

	//
	public int size() {
		return set.size();
	} // end size

	//
	private Set<String> read(File dis) throws IOException {
		Set<String> set = new HashSet<String>();
		if (!dis.exists()) {
			return set;
		}
		//boolean b = dis.getName().contains("url-encoded");
		//logger.info("reading " + dis + "... (URL " + (b ? "encoded" : "decoded") + ")");

		LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(dis), "UTF-8"));

		String line = null;
		int j = 1;

		// read pages
		while ((line = reader.readLine()) != null) {
			if ((j % 100000) == 0) {
				System.out.print(".");
			}

			//if (b)
			//{
			//	set.add(URLDecoder.decode(line.trim().replace(' ', '_')));
			//}
			//else
			//{
			//set.add(line.trim().replace(' ', '_'));
			set.add(line);
			//}

			//set.add(URLDecoder.decode(line.trim()));
			//set.add(line.trim().replace(' ', '_'));
			j++;
		} // end while
		reader.close();

		if (j > 100000) {
			System.out.print("\n");
		}

		return set;
	} // end read

	//
	public boolean contains(String page) {
		return set.contains(page);
	} // end contains

	//
	public static void main(String[] args) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 1) {
			logger.info("java -cp dist/thewikimachine.jar -mx1024M org.fbk.cit.hlt.thewikimachine.xmldump.util.PageSet file");
			System.exit(1);
		}

		File f = new File(args[0]);
		PageSet set = new PageSet(f);
		logger.info(set.size());

	} // end main

} // end PageSet
