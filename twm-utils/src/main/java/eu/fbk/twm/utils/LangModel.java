package eu.fbk.twm.utils;

import org.apache.log4j.Logger;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 05/02/14
 * Time: 11:18
 * To change this template use File | Settings | File Templates.
 */

public abstract class LangModel {

	static Logger logger = Logger.getLogger(LangModel.class.getName());

	public static final int DEFAULT_DEPTH = 10;
	protected int maxDepth;

	protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);
	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");
	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	protected boolean useCategories = false;
	protected boolean useNavs = false;
	protected boolean usePortals = false;
	protected boolean useSuffixes = false;

	protected Map<String, Set<String>> catMap = new HashMap<String, Set<String>>();
	protected Map<String, Set<String>> catSuperMap = new HashMap<String, Set<String>>();
	protected Map<String, Set<String>> navMap = new HashMap<String, Set<String>>();
	protected Map<String, Set<String>> portalMap = new HashMap<String, Set<String>>();

	protected ExtendedProperties catProperties = new ExtendedProperties();
	protected ExtendedProperties navProperties = new ExtendedProperties();
	protected ExtendedProperties portalProperties = new ExtendedProperties();
	protected ExtendedProperties suffixProperties = new ExtendedProperties();

	protected String catFile;
	protected String catSuperFile;
	protected String navFile;
	protected String portalFile;

	protected String catMapFile;
	protected String navMapFile;
	protected String portalMapFile;
	protected String suffixMapFile;


	public LangModel() {
		this.maxDepth = DEFAULT_DEPTH;
	}

	public int getMaxDepth() {
		return maxDepth;
	}

	public void setMaxDepth(int maxDepth) {
		this.maxDepth = maxDepth;
	}

	public boolean isUseCategories() {
		return useCategories;
	}

	public boolean isUseNavs() {
		return useNavs;
	}

	public boolean isUsePortals() {
		return usePortals;
	}

	public boolean isUseSuffixes() {
		return useSuffixes;
	}

	public Map<String, Set<String>> getCatMap() {
		return catMap;
	}

	public Map<String, Set<String>> getCatSuperMap() {
		return catSuperMap;
	}

	public Map<String, Set<String>> getNavMap() {
		return navMap;
	}

	public Map<String, Set<String>> getPortalMap() {
		return portalMap;
	}

	public Properties getCatProperties() {
		return catProperties;
	}

	public Properties getNavProperties() {
		return navProperties;
	}

	public Properties getPortalProperties() {
		return portalProperties;
	}

	public Properties getSuffixProperties() {
		return suffixProperties;
	}

	public String getSuffixMapFile() {
		return suffixMapFile;
	}

	public void setSuffixMapFile(String suffixMapFile) {
		this.suffixMapFile = suffixMapFile;
	}

	public String getPortalMapFile() {
		return portalMapFile;
	}

	public void setPortalMapFile(String portalMapFile) {
		this.portalMapFile = portalMapFile;
	}

	public String getNavMapFile() {
		return navMapFile;
	}

	public void setNavMapFile(String navMapFile) {
		this.navMapFile = navMapFile;
	}

	public String getCatMapFile() {
		return catMapFile;
	}

	public void setCatMapFile(String catMapFile) {
		this.catMapFile = catMapFile;
	}

	public String getPortalFile() {
		return portalFile;
	}

	public void setPortalFile(String portalFile) {
		this.portalFile = portalFile;
	}

	public String getNavFile() {
		return navFile;
	}

	public void setNavFile(String navFile) {
		this.navFile = navFile;
	}

	public String getCatSuperFile() {
		return catSuperFile;
	}

	public void setCatSuperFile(String catSuperFile) {
		this.catSuperFile = catSuperFile;
	}

	public String getCatFile() {
		return catFile;
	}

	public void setCatFile(String catFile) {
		this.catFile = catFile;
	}

	public static boolean checkExistance(String... files) {
		for (String s : files) {
			if (s == null) {
				logger.debug("File null");
				return false;
			}
			File tmp = new File(s);
			if (!tmp.exists()) {
				logger.debug("File " + s + " does not exist");
				return false;
			}
		}

		return true;
	}

	protected Map<String, Set<String>> readMap(String name) throws IOException {
		logger.info("Reading " + name + "...");
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));

		String line = null;
		int count = 0, key = 0, value = 1, part = 0, tot = 0;
		String oldKey = "";
		Set<String> set = new HashSet<String>();
		String[] t;
		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				if (!t[key].equals(oldKey)) {
					result.put(oldKey, set);
					count++;
					set = new HashSet<String>();
					part = 0;
				}
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		end = System.currentTimeMillis();
		logger.debug(df.format(tot) + "\t" + df.format(count) + "\t" + tf.format(end - begin) + " ms " + new Date());

		// and the last line
		result.put(oldKey, set);
		lnr.close();
		return result;
	}

	public static String normalizePageName(String s) {
		if (s.length() == 0) {
			return s;
		}

		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		}

		return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
	}

	protected String tabulator(int l) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < l; i++) {
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
		}
		return sb.toString();
	}

	public void load() {
		useCategories = checkExistance(catFile, catSuperFile, catMapFile);
		logger.debug("Use categories: " + useCategories);
		usePortals = checkExistance(portalFile, portalMapFile);
		logger.debug("Use portals: " + usePortals);
		useNavs = checkExistance(navFile, navMapFile);
		logger.debug("Use navigation templates: " + useNavs);
		useSuffixes = checkExistance(suffixMapFile);
		logger.debug("Use suffixes: " + useSuffixes);

		try {
			if (useCategories) {
				catMap = readMap(catFile);
				catSuperMap = readMap(catSuperFile);
				catProperties = new ExtendedProperties(catMapFile);
			}
			if (usePortals) {
				portalMap = readMap(portalFile);
				portalProperties = new ExtendedProperties(portalMapFile);
			}
			if (useNavs) {
				navMap = readMap(navFile);
				navProperties = new ExtendedProperties(navMapFile);
			}
			if (useSuffixes) {
				suffixProperties = new ExtendedProperties(suffixMapFile);
			}
		} catch (Exception e) {
			logger.error(e.getMessage() + " - Categories disabled");
			useCategories = false;
		}
	}


}
