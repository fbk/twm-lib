package eu.fbk.twm.utils;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.*;

/**
 * A set of strings in which each string is associated with
 * the frequency weightPageWeight
 *
 * @author Claudio Giuliano
 * @version %I%, %G%
 * @since 1.0
 */
public class WeightedSet implements Iterable<String> {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WeightedSet</code>.
	 */
	static Logger logger = Logger.getLogger(WeightedSet.class.getName());

	protected Map<String, Weight> map;

	protected double total;

	public WeightedSet(boolean preserveOrder) {
		this();
		if (preserveOrder) {
			map = new LinkedHashMap<>();
		}
	}

	public WeightedSet(byte[] byteArray) throws IOException {
		this(byteArray, false);
	}

	public WeightedSet(byte[] byteArray, boolean preserveOrder) throws IOException {
		this(preserveOrder);
		ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);
		DataInputStream dataStream = new DataInputStream(byteStream);

		// Number of elements
		int num = dataStream.readInt();
		for (int i = 0; i < num; i++) {
			String key = dataStream.readUTF();
			Double value = dataStream.readDouble();
			this.add(key, value);
		}
	}

	public WeightedSet(final DBpediaOntology ontology) {
		map = new TreeMap<>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Integer.signum(ontology.getDepth(o2) - ontology.getDepth(o1));
			}
		});
	}

	public WeightedSet() {
		map = new TreeMap<String, Weight>();
	}

	public void clear() {
		map.clear();
	}

	/*public WeightedSet(boolean threadSafe) {
		if (threadSafe) {
			map = Collections.synchronizedMap(new TreeMap<String, Weight>());
		}
		else {
			map = new TreeMap<String, Weight>();
		}
	}*/

	public double total() {
		return total;
	}

	public byte[] toByte() throws IOException {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(1024);
		DataOutputStream dataStream = new DataOutputStream(byteStream);

		// Number of elements
		dataStream.writeInt(map.size());
		for (String s : map.keySet()) {
			dataStream.writeUTF(s);
			dataStream.writeDouble(map.get(s).get());
		}

		return byteStream.toByteArray();
	}

	public static WeightedSet fromByte(byte[] byteArray) throws IOException {
		return fromByte(byteArray, false);
	}

	public static WeightedSet fromByte(byte[] byteArray, boolean preserveOrder) throws IOException {
		return new WeightedSet(byteArray, preserveOrder);
	}

	public void normalize() {
		Iterator<String> it = map.keySet().iterator();
		for (int i = 0; it.hasNext(); i++) {
			String ngram = it.next();
			Weight weight = map.get(ngram);
			weight.set(weight.get() / total);
		}
		total = 1;
	}

	public void addAll(Set<String> set) {
		Iterator<String> it = set.iterator();
		while (it.hasNext()) {
			add(it.next());
		}
	}

	public void add(WeightedSet weightedSet) {
		Iterator<String> it = weightedSet.iterator();
		for (int i = 0; it.hasNext(); i++) {
			String ngram = it.next();
			double weight = weightedSet.get(ngram);
			//logger.debug("A\t" + i + "\t" + ngram + "\t" + weight);
			add(ngram, weight);
			//logger.debug("C\t" + i + "\t" + ngram + "\t" + get(ngram));
		}
	}

	public double add(String ngram, double weight) {
		//todo: this is not thread safe
		total += weight;
		Weight c = (Weight) map.get(ngram);
		if (c == null) {
			map.put(ngram, new Weight(weight));
			return weight;
		}
		c.inc(weight);
		return c.weight;
	}

	public double add(String ngram) {
		//todo: this is not thread safe
		total++;
		Weight c = (Weight) map.get(ngram);
		if (c == null) {
			map.put(ngram, new Weight(1));
			return 1;
		}
		c.inc();
		return c.weight;
	}

	public boolean contains(String ngram) {
		Weight c = (Weight) map.get(ngram);
		if (c == null) {
			return false;
		}

		return true;
	} // end contains

	//
	public Collection values() {
		return map.values();
	} // end values

	public Set<String> keySet() {
		return map.keySet();
	}

	public Object[] toArray() {
		return map.keySet().toArray();
	} // end toArray

	//
	public Iterator<String> iterator() {
		return map.keySet().iterator();
	} // end iterator

	//
	public double get(String ngram) {
		//logger.debug("get: " + ngram + ", " + toChar(ngram));
		Weight c = (Weight) map.get(ngram);
		if (c == null) {
			return 0;
		}

		return c.get();

	} // end get

	//
	public static String toChar(String w) {
		StringBuilder sb = new StringBuilder();
		int ch = 0;
		for (int i = 0; i < w.length(); i++) {
			ch = w.charAt(i);
			if (i > 0) {
				sb.append(" ");
			}
			sb.append(ch);
		}

		sb.append("\n");

		for (int i = 0; i < w.length(); i++) {
			ch = w.charAt(i);
			if (i > 0) {
				sb.append(" ");
			}
			sb.append((char) ch);
		}
		return sb.toString();
	} // end toChar


	//
	public int size() {
		return map.size();
	} // end size

	public void write(Writer out) throws IOException {

		Iterator it = map.entrySet().iterator();
		//logger.info("writing freq set " + map.entrySet().size());
		//logger.info("writing freq set " + map.size());

		int i = 0;
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();

			// freq key
			out.write(entry.getValue().toString());
			out.write("\t");
			out.write(entry.getKey().toString());
			out.write("\n");

			if ((i % 100000) == 0) {
				out.flush();
			}
		} // end while
	} // end write

	public void write(Writer writer, boolean sort) throws IOException {
		SortedMap<Double, List<String>> sortedMap = toSortedMap();
		Iterator<Double> it = sortedMap.keySet().iterator();
		for (int i = 0; it.hasNext(); i++) {
			Double freq = it.next();
			List<String> list = sortedMap.get(freq);
			for (int j = 0; j < list.size(); j++) {
				writer.write(freq.toString());
				writer.write(StringTable.HORIZONTAL_TABULATION);
				writer.write(list.get(j));
				writer.write(StringTable.LINE_FEED);
			}
		}
		writer.flush();
	}

	/**
	 * Reads the frequency set from the specified input stream.
	 * <p/>
	 * This method processes input in terms of lines. A natural
	 * line of input is terminated either by a set of line
	 * terminator  characters (\n or \r or  \r\n) or by the end
	 * of the filePageWeight. A natural line  may be either a blank line,
	 * a comment line, or hold some part  of a id-feature pair.
	 * Lines are read from the input stream until  end of filePageWeight
	 * is reached.
	 * <p/>
	 * A natural line that contains only white space characters
	 * is  considered blank and is ignored. A comment line has
	 * an ASCII  '#' as its first non-white  space character;
	 * comment lines are also ignored and do not encode id-feature
	 * information.
	 * <p/>
	 * The id contains all of the characters in the line starting
	 * with the first non-white space character and up to, but
	 * not  including, the first '\t'. All remaining characters
	 * on the line become part of  the associated feature string;
	 * if there are no remaining  characters, the feature is the
	 * empty string "".
	 *
	 * @param in a <code>Reader</code> object to
	 *           provide the underlying stream.
	 * @throws IOException if reading this feature termIndex
	 *                     from the specified  input stream
	 *                     throws an <code>IOException</code>.
	 */
	public void read(Reader in) throws IOException {
		logger.info("reading vocabulary...");

		LineNumberReader lnr = new LineNumberReader(in);

		String line;
		String[] s;
		Integer id;
		int weight = 0;
		while ((line = lnr.readLine()) != null) {
			line = line.trim();
			//logger.debug(line);
			if (!line.startsWith("#")) {
				s = line.split("\t");
				// token index
				//logger.debug(line);
				if (s.length == 2) {
					/*
					SynchronizedWeight c = map.get(s[0]);
					if (c == null)
					{
						map.put(s[0], new SynchronizedWeight(Integer.parseLong(s[1])));
					}
					else
					{
						c.inc(Integer.parseLong(s[1]));
					}
					*/
					int freq = Integer.parseInt(s[0]);
					total += freq;
					//if (freq > 5)
					{
						//logger.debug("added: " + toChar(s[0]));
						map.put(s[1], new Weight(freq));
					}

				} // end if

			} // end if
		}
		lnr.close();

		logger.info(map.size() + " n-grams read");
	}

	public String getMaxValue() {
		double max = 0;
		String maxs = null;
		double f = 0;
		String s = null;
		Weight c = null;
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			s = it.next();
			c = map.get(s);
			f = c.get();
			if (f > max) {
				max = f;
				maxs = s;
			} // end if
		} // end
		return maxs;
	}

	public SortedMap<Double, List<String>> toSortedMap() {
		SortedMap<Double, List<String>> smap = new TreeMap<Double, List<String>>(new Comparator<Double>() {
			public int compare(Double e1, Double e2) {
				return e2.compareTo(e1);
			}
		});

		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String s = it.next();
			Weight c = map.get(s);
			List<String> list = smap.get(c.get());
			if (list == null) {
				list = new ArrayList<String>();
				list.add(s);
				smap.put(c.get(), list);
			}
			else {
				list.add(s);
			}
		}

		return smap;
	}

	public String toString(boolean b) {
		StringBuilder sb = new StringBuilder();
		String s = null;
		Weight c = null;
		Iterator<String> it = map.keySet().iterator();
		for (int i = 0; it.hasNext(); i++) {
			if (i > 0) {
				sb.append("\t");
			}

			s = it.next();
			c = map.get(s);

			sb.append(s);
			sb.append("\t");
			sb.append(c);
		}

		return sb.toString();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String s = it.next();
			Weight c = map.get(s);
			sb.append(s);
			sb.append("\t");
			sb.append(c);
			sb.append(", ");
		}
		sb.append("...)");
		return sb.toString();
	}

	static class Weight {
		double weight;

		public Weight(double weight) {
			this.weight = weight;
		}

		public void inc() {
			weight++;
		}

		public void inc(double l) {
			weight += l;
		}

		public double get() {
			return weight;
		}

		void set(double weight) {
			this.weight = weight;
		}

		public String toString() {
			return Double.toString(weight);
		}

	}

	public static void main(String args[]) throws Exception {
		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		commandLineWithLogger.addOption(OptionBuilder.withDescription("Ontology file").isRequired().hasArg().withArgName("file").withLongOpt("ontology").create("o"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}

		String ontologyFile = commandLine.getOptionValue("o");
		DBpediaOntology ontology = new DBpediaOntology(ontologyFile);

		WeightedSet ws = new WeightedSet(ontology);
		ws.add("Band");
		ws.add("Place");
		ws.add("BeachVolleyballPlayer");
		System.out.println(ws);

		System.exit(0);

		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 2) {
			//logger.info("java -mx1024M org.fbk.irst.tcc.web1t.WeightedSet in-ngram-filePageWeight out-ngram-filePageWeight");
			logger.info("java -mx1024M org.fbk.irst.tcc.web1t.WeightedSet in-ngram-filePageWeight term");
			System.exit(-1);
		}

		WeightedSet set = new WeightedSet();
		//set.read(new FileReader(new File(args[0])));
		InputStreamReader reader = new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8");
		set.read(reader);
		//logger.info(args[1] + ": " + set.get(args[1]));
		//logger.info(args[1] + ": " + set.get("pluralita'"));
		//set.write(new FileWriter(new File(args[1])));
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(new File(args[1])), "ISO-8859-1");
		//set.write(new FileWriter(new File(args[1])));
		set.write(writer);
	}
} // end class WeightedSet