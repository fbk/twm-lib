package eu.fbk.twm.utils;

import org.apache.log4j.Logger;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 8/27/13
 * Time: 5:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class UnixBunzip2Wrapper {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>UnixSortWrapper</code>.
	 */
	static Logger logger = Logger.getLogger(UnixBunzip2Wrapper.class.getName());
	static final String bunzip2 = "/bin/bunzip2";

	public static int bunzip2(String input) throws Exception {
		return bunzip2(input, true);
	}

	public static int bunzip2(String input, boolean deleteAfterExtraction) throws Exception {
		long begin = System.currentTimeMillis();

		String[] cmd;
		if (deleteAfterExtraction) {
			cmd = new String[]{bunzip2, input};
		}
		else {
			cmd = new String[]{bunzip2, "-k", input};
		}

		logger.debug(Arrays.toString(cmd));
		Process p = Runtime.getRuntime().exec(cmd);

		logger.info("Decompressing file: " + input);
		p.waitFor();

		int exitValue = p.exitValue();
		if (exitValue != 0) {
			InputStream is = p.getErrorStream();
			LineNumberReader lnr = new LineNumberReader(new InputStreamReader(is));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = lnr.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
			logger.error(sb.toString());
		}

		long end = System.currentTimeMillis();
		logger.info("Decompression finished in " + (end - begin) + " ms");

		return exitValue;
	}

}
