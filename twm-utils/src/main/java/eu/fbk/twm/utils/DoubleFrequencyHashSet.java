package eu.fbk.twm.utils;

import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 1/15/13
 * Time: 5:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class DoubleFrequencyHashSet<K> implements Serializable {

	private HashMap<K, Double> support = new HashMap<K, Double>();

	static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map) {
		SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<Map.Entry<K, V>>(
				new Comparator<Map.Entry<K, V>>() {
					@Override
					public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
						return e2.getValue().compareTo(e1.getValue());
					}
				}
		);
		sortedEntries.addAll(map.entrySet());
		return sortedEntries;
	}

	public SortedSet getSorted() {
		return entriesSortedByValues(support);
	}

	public LinkedHashMap<K, Double> getLinked() {
		SortedSet<Map.Entry<K, Double>> sorted = entriesSortedByValues(support);
		LinkedHashMap<K, Double> temp = new LinkedHashMap<K, Double>();
		for (Object o : sorted) {
			Map.Entry entry = (Map.Entry) o;
			temp.put((K) entry.getKey(), (Double) entry.getValue());
		}
		return temp;
	}

//	<K, Double extends Comparable<? super Double>> SortedSet<Map.Entry<K, Double>> entriesSortedByValues() {
//		SortedSet<Map.Entry<K, Double>> sortedEntries = new TreeSet<Map.Entry<K, Double>>(
//				new Comparator<Map.Entry<K, Double>>() {
//					@Override
//					public int compare(Map.Entry<K, Double> e1, Map.Entry<K, Double> e2) {
//						return e1.getValue().compareTo(e2.getValue());
//					}
//				}
//		);
//		sortedEntries.addAll(support.entrySet());
//		return sortedEntries;
//	}

	public void add(K o, double quantity) {
		double count = support.containsKey(o) ? (Double) support.get(o) + quantity : quantity;
		support.put(o, count);
	}

	public DoubleFrequencyHashSet<K> clone() {
		DoubleFrequencyHashSet<K> ret = new DoubleFrequencyHashSet<K>();
		for (K k : support.keySet()) {
			ret.add(k, support.get(k));
		}
		return ret;
	}

	public void remove(K o) {
		support.remove(o);
	}

	public void add(K o) {
		add(o, 1);
	}

	public int size() {
		return support.size();
	}

	public K mostFrequent() {
		Iterator it = support.keySet().iterator();
		Double max = null;
		K o = null;
		while (it.hasNext()) {
			K index = (K) it.next();
			if (max == null || support.get(index) > max) {
				o = index;
				max = support.get(index);
			}
		}
		return o;
	}

	public Set<K> keySet() {
		return support.keySet();
	}

	public Double get(K o) {
		return support.get(o);
	}

	public Double getZero(K o) {
		return support.get(o) != null ? support.get(o) : 0;
	}

	public Set<K> keySetWithLimit(double limit) {
		HashSet<K> ret = new HashSet<K>();

		for (K key : support.keySet()) {
			double value = support.get(key);
			if (value >= limit) {
				ret.add(key);
			}
		}

		return ret;
	}

	public String toString() {
		return support.toString();
	}
}
