package eu.fbk.twm.utils;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 06/02/14
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */

public abstract class BaseFolder {

	static Logger logger = Logger.getLogger(BaseFolder.class.getName());
	protected HashMap<String, String> folders = new HashMap<String, String>();
	protected HashMap<String, String> files = new HashMap<String, String>();

	protected static final Pattern langPattern = Pattern.compile("^\\w{2}$");
	protected static final Pattern wikiVersionPattern = Pattern.compile("^([0-9]{8})/?$");

	private HashSet<String> filesToCheck = new HashSet<>();
	private HashSet<String> foldersToCheck = new HashSet<>();


	protected void addMandatoryFile(String fileName) {
		filesToCheck.add(fileName);
	}

	protected void addMandatoryFolder(String folderName) {
		foldersToCheck.add(folderName);
	}

	protected static void exitOnMissingFile(String fileName) {
		File file = new File(fileName);
		if (!file.exists()) {
			logger.error("File " + fileName + " does not exist");
			System.exit(1);
		}
	}

	protected static void exitOnMissingFolder(String folder) {
		exitOnMissingFolder(folder, false);
	}
	
	protected static void exitOnMissingFolder(String folder, boolean create) {
//		exitOnMissingFile(folder);
		File folderFile = new File(folder);
		if (!folderFile.exists()) {
			if (create) {
				logger.info("File " + folder + " does not exist, creating it");
				if (!folderFile.mkdirs()) {
					logger.error("Unable to create folder " + folder);
					System.exit(1);
				}
			}
			else {
				logger.error("File " + folder + " does not exist");
				System.exit(1);
			}
		}
		if (!folderFile.isDirectory()) {
			logger.error(folder + " is not a directory");
			System.exit(1);
		}
	}

	protected void exitOnMissingStuff(boolean createFolder) {
		boolean checkAllFolders = foldersToCheck.size() == 0;
		boolean checkAllFiles = filesToCheck.size() == 0;

		for (String key : folders.keySet()) {
			String folder = folders.get(key);
			if (checkAllFolders || foldersToCheck.contains(key)) {
				exitOnMissingFolder(folder, createFolder);
			}
		}
		for (String key : files.keySet()) {
			String file = files.get(key);
			if (checkAllFiles || filesToCheck.contains(key)) {
				exitOnMissingFile(file);
			}
		}
	}

	protected void extendCommandLine(CommandLineWithLogger commandLineWithLogger) {
		for (String folderIndex : folders.keySet()) {
			
			// Data folder -- mandatory!
			if (folderIndex.equals("data")) {
				commandLineWithLogger.addOption(OptionBuilder.isRequired().withDescription(String.format("%s folder", folderIndex)).hasArg().withArgName("folder").withLongOpt(String.format("%s-dir", folderIndex)).create());
			}
			else {
				commandLineWithLogger.addOption(OptionBuilder.withDescription(String.format("%s folder", folderIndex)).hasArg().withArgName("folder").withLongOpt(String.format("%s-dir", folderIndex)).create());
			}
		}
		for (String fileIndex : files.keySet()) {
			commandLineWithLogger.addOption(OptionBuilder.withDescription(String.format("%s file", fileIndex)).hasArg().withArgName("file").withLongOpt(String.format("%s-file", fileIndex)).create());
		}
	}

	protected Map<String, TreeSet<Integer>> getPresentVersions() {
		return getPresentVersions(null);
	}

	protected Map<String, TreeSet<Integer>> getPresentVersions(String[] givenLanguages) {
		Map<String, TreeSet<Integer>> alreadyPresentVersions = new HashMap<String, TreeSet<Integer>>();

		File[] listOfVersions;
		File[] listOfLangs;

		for (String langName : givenLanguages) {
			alreadyPresentVersions.put(langName, new TreeSet<Integer>(Collections.reverseOrder()));
		}

		File outputFolderFile = new File(folders.get("base"));
		listOfLangs = outputFolderFile.listFiles();
		if (listOfLangs != null) {
			for (File f : listOfLangs) {
				if (!f.isDirectory()) {
					continue;
				}
				Matcher langMatcher = langPattern.matcher(f.getName());
				if (langMatcher.matches()) {
					String langName = f.getName();
					if (givenLanguages != null &&
							!Arrays.asList(givenLanguages).contains(langName)) {
						continue;
					}
					alreadyPresentVersions.put(langName, new TreeSet<Integer>(Collections.reverseOrder()));
					listOfVersions = f.listFiles();
					if (listOfVersions != null) {
						for (File version : listOfVersions) {
							if (!version.isDirectory()) {
								continue;
							}
							String versionName = version.getName();
							Matcher versMatcher = wikiVersionPattern.matcher(versionName);
							if (versMatcher.matches()) {
								alreadyPresentVersions.get(langName).add(Integer.parseInt(versionName));
							}
						}
					}
				}
			}
		}

		return alreadyPresentVersions;
	}

	protected void init() {
		init(null, null);
	}

	protected void init(String dataFolder) {
		init(null, dataFolder);
	}

	protected void init(CommandLine commandLine) {
		init(commandLine, null);
	}

	protected void init(CommandLine commandLine, String dataFolder) {

		String option = "data-folder";
		String folderIndex = "data";
		if (commandLine != null && commandLine.hasOption(option)) {
			folders.put(folderIndex, commandLine.getOptionValue(option));
			if (!folders.get(folderIndex).endsWith(File.separator)) {
				folders.put(folderIndex, folders.get(folderIndex) + File.separator);
			}
		}

		// Set default
		if (folders.get(folderIndex) == null) {
			folders.put(folderIndex, dataFolder);
			if (!folders.get(folderIndex).endsWith(File.separator)) {
				folders.put(folderIndex, folders.get(folderIndex) + File.separator);
			}
		}
		else {
			dataFolder = folders.get(folderIndex);
		}

		folders.put("lsa", dataFolder + "/models/lsa/");
		folders.put("base", dataFolder + "/models/wikipedia/");
		folders.put("dump", dataFolder + "/corpora/wikipedia/");
		folders.put("bncf", dataFolder + "/corpora/bncf/");
		folders.put("cl", dataFolder + "/models/wikidata/");
		folders.put("res", dataFolder + "/resources/");
		folders.put("topic", dataFolder + "/models/topics/");
		folders.put("namnom", dataFolder + "/models/namnom/");
		folders.put("airpedia", dataFolder + "/models/airpedia/");

		files.put("ontology", dataFolder + "/corpora/dbpedia/dbpedia_3.9.owl");

		for (String thisIndex : folders.keySet()) {
			option = String.format("%s-dir", thisIndex);
			if (commandLine != null && commandLine.hasOption(option)) {
				folders.put(thisIndex, commandLine.getOptionValue(option));
			}

			if (!folders.get(thisIndex).endsWith(File.separator)) {
				folders.put(thisIndex, folders.get(thisIndex) + File.separator);
			}
		}
		for (String fileIndex : files.keySet()) {
			option = String.format("%s-file", fileIndex);
			if (commandLine != null && commandLine.hasOption(option)) {
				folders.put(fileIndex, commandLine.getOptionValue(option));
			}
		}
	}
}
