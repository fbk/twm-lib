/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/20/13
 * Time: 4:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class FreqExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>FreqExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(FreqExtractor.class.getName());

	private static Pattern tabPattern = Pattern.compile("\t");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public FreqExtractor(String in, String out, int col) throws IOException {
		this(new File(in), new File(out), col);
	}

	public FreqExtractor(File in, File out, int col) throws IOException {
		logger.info("reading " + in + "...");

		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new FileReader(in));
		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
		String line;
		int count = 0;
		int part = 0;
		int tot = 0;
		String oldKey = "";
		logger.info("lines\titems\ttime (ms)\tdate");
		// first line
		if ((line = lnr.readLine()) != null) {
			try {
				String[] t = tabPattern.split(line);
				if (t.length > col) {
					oldKey = t[col];
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + count);
				logger.error(e);
			} finally {
				tot++;
			}
		} // end if

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			try {
				String[] t = tabPattern.split(line);
				if (t.length > col) {
					if (!t[col].equals(oldKey)) {
						pw.print(part);
						pw.print(CharacterTable.HORIZONTAL_TABULATION);
						pw.println(oldKey);
						count++;
						part = 0;
					}
					oldKey = t[col];
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + tot);
				logger.error(e);
			} finally {
				tot++;
			}

			if ((tot % 100000) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
				pw.flush();
			}
		} // end while

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());

		// and the last document
		pw.print(part);
		pw.print(CharacterTable.HORIZONTAL_TABULATION);
		pw.println(oldKey);
		pw.close();
		lnr.close();
	}

	public static void main(String argv[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (argv.length < 3) {
			System.out.println("");
			System.out.println("USAGE:");
			System.out.println("");
			System.out.println("java eu.fbk.twm.utils.FreqExtractor\n" +
					" in-file-csv -- Input file\n" +
					" out-file-csv -- Output template file\n" +
					" col -- Lines to stop, 0 means never stop");
			System.out.println("");
			System.exit(1);
		}

		int parID = 0;

		String in = argv[parID++];
		String out = argv[parID++];
		int col = Integer.parseInt(argv[parID++]);


		new FreqExtractor(in, out, col);
	}
}
