/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 6/26/13
 * Time: 7:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClassResource implements Comparable<ClassResource> {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>ClassResource</code>.
	 */
	static Logger logger = Logger.getLogger(ClassResource.class.getName());

	private String label;

	private int namespace;

	private double confidence;

	private int resource;

	public ClassResource(String label, int namespace, double confidence, int resource) {
		this.label = label;
		this.namespace = namespace;
		this.confidence = confidence;
		this.resource = resource;

	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getNamespace() {
		return namespace;
	}

	public void setNamespace(char namespace) {
		this.namespace = namespace;
	}

	public double getConfidence() {
		return confidence;
	}

	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}

	public int getResource() {
		return resource;
	}

	public void setResource(char resource) {
		this.resource = resource;
	}

	@Override
	public int compareTo(ClassResource o) {
		return label.compareTo(o.getLabel());
	}

	@Override
	public String toString() {
		return label + "\t" +
				namespace + "\t" +
				confidence + "\t" +
				resource;
	}
}
