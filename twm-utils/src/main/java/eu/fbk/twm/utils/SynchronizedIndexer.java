/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.Writer;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/23/13
 * Time: 11:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class SynchronizedIndexer<T> {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>SynchronizedIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(SynchronizedIndexer.class.getName());

	protected Map<T, Integer> map;

	protected int total;

	public SynchronizedIndexer() {
		map = new HashMap<T, Integer>();
		total = -1;
	}

	public synchronized int get(T o) {
		Integer i = map.get(o);
		if (i == null) {
			map.put(o, ++total);
			return total;
		}
		return i;
	}

	public int total() {
		return total;
	}

	public int size() {
		return map.size();
	}

	/*public synchronized SortedMap<Integer, List<T>> getSortedMap() {
		SortedMap<Integer, List<T>> sortedMap = new TreeMap<Integer, List<T>>(new Comparator<Integer>() {
			public int compare(Integer e1, Integer e2) {
				if (e1.intValue() > e2.intValue()) {
					return -1;
				}
				else if (e1.intValue() < e2.intValue()) {
					return 1;
				}
				return 0;
			}
		});

		Iterator<T> it = map.keySet().iterator();
		while (it.hasNext()) {
			T o = it.next();
			Integer i = map.get(o);
			List<T> list = sortedMap.get(i);
			if (list == null) {
				list = new ArrayList<T>();
				list.add(o);
				sortedMap.put(i, list);
			}
			else {
				list.add(o);
			}
		}

		return sortedMap;
	}*/

	public synchronized SortedMap<Integer, T> reverseIndex() {
		SortedMap<Integer, T> sortedMap = new TreeMap<Integer, T>(new Comparator<Integer>() {
			public int compare(Integer e1, Integer e2) {
				if (e1.intValue() > e2.intValue()) {
					return -1;
				}
				else if (e1.intValue() < e2.intValue()) {
					return 1;
				}
				return 0;
			}
		});

		Iterator<T> it = map.keySet().iterator();
		while (it.hasNext()) {
			T o = it.next();
			Integer i = map.get(o);

			sortedMap.put(i, o);

		}

		return sortedMap;
	}

	public synchronized void write(Writer w) throws IOException {
		SortedMap<Integer, T> sortedMap = reverseIndex();
		Iterator<Integer> it = sortedMap.keySet().iterator();
		Integer i;
		for (; it.hasNext(); ) {
			i = it.next();
			T o = sortedMap.get(i);
			w.write(i.toString());
			w.write(CharacterTable.HORIZONTAL_TABULATION);
			w.write(o.toString());
			w.write(CharacterTable.LINE_FEED);

		}
	}
}
