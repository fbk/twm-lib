package eu.fbk.twm.utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 23/01/14
 * Time: 17:51
 * To change this template use File | Settings | File Templates.
 */
public class LangTypeModel extends LangModel {
//	private String lang;

    //	private HashMap<Integer, HashSet<String>> dbpediaClasses;
    private AirpediaOntology ontology;
//	private HashSet<String> stopClasses;
//	private HashSet<Integer> nomIDs;

//	public HashSet<Integer> getNomIDs() {
//		return nomIDs;
//	}
//
//	public void setNomIDs(HashSet<Integer> nomIDs) {
//		this.nomIDs = nomIDs;
//	}

    public AirpediaOntology getOntology() {
        return ontology;
    }

    public void setOntology(AirpediaOntology ontology) {
        this.ontology = ontology;
    }

//	public HashMap<Integer, HashSet<String>> getDbpediaClasses() {
//		return dbpediaClasses;
//	}
//
//	public void setDbpediaClasses(HashMap<Integer, HashSet<String>> dbpediaClasses) {
//		this.dbpediaClasses = dbpediaClasses;
//	}

    public LangTypeModel() {
        super();
//		resetStopClasses();
    }

//	public void addStopClass(String className) {
//		stopClasses.add(className);
//	}
//
//	public void removeStopClass(String className) {
//		stopClasses.remove(className);
//	}
//
//	public void resetStopClasses() {
//		stopClasses = new HashSet<>();
//	}

    private void searchCat(Set<String> categories, WeightedSet weightedSet, int depth, Set<String> visitedSet) {

        if (categories == null || depth > maxDepth) {
            return;
        }

//		int i = 0;
        for (String category : categories) {
            String normalizedCategory = normalizePageName(category);
            String label = catProperties.getProperty(normalizedCategory);
            //logger.debug(i + "\t" + normalizedCategory + "\t" + label);
            //String label = categories[i];

            if (label != null) {
                //logger.debug("<<<" + normalizedCategory + "\t" + label + "\t" + depth + ">>>");
                //WeightedSet.add(categories[i]);

                if (label.length() == 0) {
                    //logger.warn("stop category " + normalizedCategory);
                    logger.trace(tabulator(depth) + "<" + normalizedCategory + ", STOP, " + depth + ">");
                } else {
                    HashSet<String> classes = new HashSet<>();
                    classes.add(label);
                    classes = ontology.completeClasses(classes);
                    for (String c : classes) {
                        weightedSet.add(c, (double) 1 / depth);
                    }
                    logger.trace(tabulator(depth) + "<" + normalizedCategory + ", '" + label + "', " + depth + ">");
                }

            } else {
                if (!visitedSet.contains(normalizedCategory)) {
                    visitedSet.add(normalizedCategory);
                    try {
                        Set<String> superCategories = catSuperMap.get(normalizedCategory);
                        if (superCategories != null) {
                            //logger.debug(i + "\t" + depth + "\t" + normalizedCategory + ": " + superCategories);
                            logger.trace(
                                    tabulator(depth) + "{" + normalizedCategory + ", " + depth + ", " + superCategories
                                            .size() + ", " + superCategories + "}");
                            searchCat(superCategories, weightedSet, depth + 1, visitedSet);
                        }
                    } catch (Exception e) {
                        logger.error(e);
                    }
                }
            }

//			i++;
        }
    }

//	private void searchSimple(Map<String, Set<String>> s, Properties p, String page, WeightedSet weightedSet) {
//		searchSimple(s, p, page, weightedSet, 1);
//	}

//	private void searchSimple(Map<String, Set<String>> s, Properties p, String page, WeightedSet weightedSet, double weight) {
//		Set<String> result = s.get(page);
//		HashSet<String> okResults = new HashSet<String>();
//
//		if (result != null) {
//			for (String value : result) {
//				if (p != null) {
//					if (p.getProperty(value) != null && !p.getProperty(value).equals("")) {
//						String topic = p.getProperty(value);
//						okResults.add(topic);
//						logger.debug("Key: " + value + " - Topic: " + topic);
//					}
//				}
//			}
//		}
//
//		for (String topic : okResults) {
//			weightedSet.add(topic, weight / okResults.size());
//		}
//
//	}

//	public void search(String page, WeightedSet weightedSet) {
//		search(page, weightedSet, 1);
//	}

//	private HashSet<String> completeClasses(HashSet<String> tmpClasses) {
//		HashSet<String> classes = new HashSet<>();
//		if (tmpClasses != null) {
//			for (String c : tmpClasses) {
//				String[] parts = c.split("/");
//				for (String s : parts) {
//					List<DBpediaOntologyNode> nodes = ontology.getHistoryFromName(s);
//					if (nodes == null) {
//						logger.trace(String.format("Error in class %s", s));
//						continue;
//					}
//					for (DBpediaOntologyNode n : nodes) {
//						classes.add(n.className);
//					}
//				}
//			}
//		}
//
//		return classes;
//	}

    public void search(String page, Integer wikiID, WeightedSet weightedSet) {

        logger.debug("PAGE: " + page);

//		HashSet<String> classes = dbpediaClasses.get(wikiID);
//		classes = completeClasses(classes);
//
//		for (String c : classes) {
//			weightedSet.add(c);
//		}
//
//		HashSet<String> intersection = new HashSet<>(classes);
//		intersection.retainAll(stopClasses);
//
//		if (intersection.size() > 0) {
//			logger.debug(String.format("%s is %s, stopping", page, intersection));
//			return;
//		}
//
//		if (nomIDs != null && nomIDs.size() > 0 && !nomIDs.contains(wikiID)) {
//			logger.debug(String.format("%s is a NAM, stopping", page));
//			return;
//		}

        // Categories
        if (useCategories) {
            logger.trace("Searching categories");
            Set<String> visitedSet = new HashSet<String>();
            Set<String> categories = catMap.get(page);
            logger.trace(String.format("Searching page %s [ID: %d]", page, wikiID));
            if (categories == null) {
                logger.trace("This page has no categories");
            } else {
                logger.trace("categories\t" + categories.size() + "\t" + categories);
                searchCat(categories, weightedSet, 1, visitedSet);
                logger.debug(page + "\t" + weightedSet.size() + "\t" + weightedSet.toSortedMap());
            }
        }

//		// Portals
//		if (usePortals) {
//			logger.debug("Searching portals");
//			searchSimple(portalMap, portalProperties, page, weightedSet);
//		}

//		// Navigation templates
//		if (useNavs) {
//			logger.debug("Searching navigation templates");
//			searchSimple(navMap, navProperties, page, weightedSet);
//		}

//		// Suffix
//		if (useSuffixes) {
//			logger.debug("Searching suffix");
//			ParsedPageTitle title = new ParsedPageTitle(page);
//			if (title.hasSuffix()) {
//				String suffix = title.getSuffix();
//				if (suffixProperties != null) {
//					if (suffixProperties.getProperty(suffix) != null && !suffixProperties.getProperty(suffix).equals("")) {
//						String topic = suffixProperties.getProperty(suffix);
//						weightedSet.add(topic);
//						logger.debug("Key: " + suffix + " - Topic: " + topic);
//					}
//				}
//			}
//		}

    }

}
