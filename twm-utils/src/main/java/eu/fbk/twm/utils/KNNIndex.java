package eu.fbk.twm.utils;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 11/30/12
 * Time: 2:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class KNNIndex {

	private ArrayList<Integer> classIDs;
	private ArrayList<Double> values;
	private int k = 10;
	private int abstain = 10;

	private Map<Integer, Double> sortedMap = null;
	private FrequencyHashSet<Integer> frequencies = null;
	private DBpediaOntology o = null;
	// private HashMap<Integer, String> translation

	public KNNIndex(int abstain, int k) {
		this();
		this.abstain = abstain;
		this.k = k;
	}

	public KNNIndex() {
		classIDs = new ArrayList<Integer>();
		values = new ArrayList<Double>();
	}

	public void add(int classID, double value) {
		classIDs.add(classID);
		values.add(value);
	}

	public FrequencyHashSet<Integer> getFrequencyHashSet() {
		if (frequencies == null) {
			frequencies = new FrequencyHashSet<Integer>();
			getSortedMap();
			int i = 0;
			for (Integer integer : sortedMap.keySet()) {
				i++;
				if (i > k) {
					break;
				}
				frequencies.add(classIDs.get(integer));
			}
		}
		return frequencies;
	}

	public Map<Integer, Double> getSortedMap() {
		if (sortedMap == null) {
			Map<Integer, Double> unsortedMap = new HashMap<Integer, Double>();

			for (int i = 0; i < values.size(); i++) {
				unsortedMap.put(i, values.get(i));
			}

			List list = new LinkedList(unsortedMap.entrySet());

			//sort list based on comparator
			Collections.sort(list, new Comparator() {
				public int compare(Object o1, Object o2) {
					return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
				}
			});

			//put sorted list into map again
			Map sortedMap = new LinkedHashMap();
			for (Iterator it = list.iterator(); it.hasNext(); ) {
				Map.Entry entry = (Map.Entry) it.next();
				sortedMap.put(entry.getKey(), entry.getValue());
			}
			this.sortedMap = sortedMap;
		}
		return sortedMap;
	}

	public int size() {
		return values.size();
	}

	public String toString() {
		return getSortedMap().toString();
	}

}
