/*
 * Copyright (2010) Fondazione Bruno Kessler (FBK)
 * 
 * FBK reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced
 * in any title whatsoever except as provided by license
 * without the written consent of FBK.  A license under FBK's
 * rights in the Program may be available directly from FBK.
 */

package eu.fbk.twm.utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.CharacterTable;

/**
 * This class ...
 *
 * @author Claudio Giuliano
 * @version 1.0
 * @since 1.0
 */
public class ParsedPageTitle extends AbstractParsedPage {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>ParsedPageTitle</code>.
	 */
	static Logger logger = Logger.getLogger(ParsedPageTitle.class.getName());

	//private String form;

	//private String page;

	private String suffix;

	public final static String START_SUFFIX_PATTERN = "_(";

	public final static char END_SUFFIX_PATTERN = ')';


	public ParsedPageTitle(String s) {
		//s = removeQuotes(s);
		int b = s.lastIndexOf(START_SUFFIX_PATTERN);
		if (b == -1) {
			// no suffix
			form = s.replace(CharacterTable.LOW_LINE, CharacterTable.SPACE);
			page = s;
			return;
		}
		else {
			int e = s.length() - 1;
			if (s.charAt(e) == END_SUFFIX_PATTERN) {
				page = s;
				suffix = s.substring(b + 2, e);
				form = s.substring(0, b).replace(CharacterTable.LOW_LINE, CharacterTable.SPACE);
			}
			else {
				form = s.replace(CharacterTable.LOW_LINE, CharacterTable.SPACE);
				page = s;
			}
		}
	}

	public boolean hasSuffix() {
		return (suffix != null);
	}

	/*public String getPage() {
		return page;
	}  */

	public String getSuffix() {
		return suffix;
	}

	@Override
	public String toString() {
		return "ParsedPageTitle{" +
						"form='" + form + '\'' +
						", page='" + page + '\'' +
						", suffix='" + suffix + '\'' +
						'}';
	}

	/*public String getForm() {
		return form;
	} */

	public static void main(String[] args) throws Exception {

		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}
		// java eu.fbk.twm.utils.ParsedPageTitle
		PropertyConfigurator.configure(logConfig);

		ParsedPageTitle t = new ParsedPageTitle(args[0]);
		logger.info(t);
		//logger.info(t.containsLetter() ? "yes" : "no");
	}
}
