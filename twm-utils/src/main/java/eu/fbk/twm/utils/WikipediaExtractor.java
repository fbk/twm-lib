/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/21/13
 * Time: 1:32 PM
 * To change this template use File | Settings | File Templates.
 */
public interface WikipediaExtractor {

    public final static int DEFAULT_MAXIMUM_FORM_FREQ = 1000;

    public static final int DEFAULT_MINIMUM_FORM_FREQ = 0;

    public static final boolean DEFAULT_COMPRESS_OUTPUT = false;

    public static final boolean DEFAULT_NORMALIZE = false;

    public static final int DEFAULT_LSA_DIM = 100;

    public abstract void start(ExtractorParameters extractorParameters);

    public abstract void disambiguationPage(String text, String title, int wikiID);

    public abstract void categoryPage(String text, String title, int wikiID);

    public abstract void templatePage(String text, String title, int wikiID);

    public abstract void redirectPage(String text, String title, int wikiID);

    public abstract void contentPage(String text, String title, int wikiID);

    public abstract void portalPage(String text, String title, int wikiID);

    public abstract void projectPage(String text, String title, int wikiID);

    public abstract void filePage(String text, String title, int wikiID);

    public abstract void setNotificationPoint(int notificationPoint);

    public abstract int getNotificationPoint();

    public static final int DEFAULT_SORT_SIZE = 25000000;

}
