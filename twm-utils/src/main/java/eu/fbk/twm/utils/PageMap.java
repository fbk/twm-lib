/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.utils;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * This class represents a map between redirectPageCounter pages and pages.
 * The redirectPageCounter map is read from a filePageCounter with format:
 * <p/>
 * <code>(redirect_page \t page \n)+</code>
 *
 * @author Claudio Giuliano
 * @version 1.0
 * @see ReversePageMap, WikipediaRedirectExtractor
 * @since 1.0
 */
public class PageMap {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageMap</code>.
	 */
	static Logger logger = Logger.getLogger(PageMap.class.getName());

	//
	protected Map<String, String> map;

	//
	protected static Pattern tabPattern = Pattern.compile("\t");

	//
	public PageMap() throws IOException {
		map = new HashMap<String, String>();
	} // end

	//
	public PageMap(File redir) throws IOException {
		long begin = System.currentTimeMillis(), end = 0;
		logger.info("reading " + redir + "...");
		map = read(redir);
		end = System.currentTimeMillis();
		logger.info(map.size() + " pages read in " + (end - begin) + " ms");
	} // end

	//
	public Set<String> keySet() {
		return map.keySet();
	} // end keySet

	//
	private Map<String, String> read(File redir) throws IOException {
		Map<String, String> map = new HashMap<String, String>();
		if (!redir.exists()) {
			return map;
		}

		LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(redir), "UTF-8"));

		String line = null;
		int j = 1;
		String old = null;
		String[] s = null;
		// read
		while ((line = reader.readLine()) != null) {
			s = tabPattern.split(line);

			if (s.length == 2) {
				old = map.put(s[0], s[1]);
				if (old != null) {
					logger.warn(s[0] + " also redirects to " + old + ", but only " + s[1] + " is used");
				}

			}
			else {
				logger.warn(j + "\t" + line + " has no target page");
			}

			if ((j % 100000) == 0) {
				System.out.print(".");
			}
			j++;
		} // end while
		reader.close();

		if (j > 100000) {
			System.out.print("\n");
		}

		return map;
	}

	public String get(String page) {
		return map.get(page);
	}

	public int size() {
		return map.size();
	}

	public static void main(String[] args) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 1) {
			logger.info("java -mx1024M org.fbk.cit.hlt.wm.wiki.xmldump.PageMap redirect-file");
			System.exit(1);
		}

		File redir = new File(args[0]);
		PageMap map = new PageMap(redir);
		logger.info(map.size());
	}

}
