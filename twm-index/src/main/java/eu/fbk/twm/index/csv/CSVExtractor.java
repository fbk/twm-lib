/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import org.apache.log4j.Logger;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/20/13
 * Time: 9:01 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class CSVExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CSVExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(CSVExtractor.class.getName());

	private int numThreads;

	private ExecutorService myExecutor;

	public final static int DEFAULT_THREADS_NUMBER = 1;

	public final static int DEFAULT_QUEUE_SIZE = 10000;

	public final static int DEFAULT_NOTIFICATION_POINT = 10000;

	private int notificationPoint;

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private int numPages;

	public static final int DEFAULT_NUM_PAGES = Integer.MAX_VALUE;

	public CSVExtractor() {
		this(DEFAULT_THREADS_NUMBER, DEFAULT_NUM_PAGES);
	}

	protected CSVExtractor(int numThreads) {
		this(numThreads, DEFAULT_NUM_PAGES);
	}

	protected CSVExtractor(int numThreads, int numPages) {
		this.numThreads = numThreads;
		this.numPages = numPages;
		notificationPoint = DEFAULT_NOTIFICATION_POINT;

		logger.info("creating the thread executor (" + numThreads + ")");
		//myExecutor = Executors.newFixedThreadPool(numThreads);
		int blockQueueSize = DEFAULT_QUEUE_SIZE;
		BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(blockQueueSize);
		RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();
		myExecutor = new ThreadPoolExecutor(numThreads, numThreads, 1, TimeUnit.MINUTES, blockingQueue, rejectedExecutionHandler);
	}

	public int getNumPages() {
		return numPages;
	}

	public abstract void processLine(String line);

	public abstract void start(ExtractorParameters extractorParameters);

	public abstract void end();

	public int getNumThreads() {
		return numThreads;
	}

	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
	}

	public int getNotificationPoint() {
		return notificationPoint;
	}

	public void setNotificationPoint(int notificationPoint) {
		this.notificationPoint = notificationPoint;
	}

	public class LineProcessor implements Runnable {
		private String line;

		public LineProcessor(String line) {
			this.line = line;
		}

		public void run() {
			processLine(line);
		}
	}

	public void read(String name) throws IOException {
		read(new File(name));
	}

	public void read(String name, boolean compress) throws IOException {
		read(new File(name), compress);
	}

	public void read(File in) throws IOException {
		read(in, false);
	}

	public void read(File in, boolean compress) throws IOException {
		logger.info("reading a " + (compress ? "compressed" : "uncompressed") + " csv from " + in + " (" + new Date() + ")...");

		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		if (compress) {
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(in)), "UTF-8"));
		}
		else {
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		}

		Tokenizer tokenizer = HardTokenizer.getInstance();
		String line;
		int tot = 0;
		logger.info("totalFreq\tsize\ttime (ms)\tdate");
		while ((line = lnr.readLine()) != null) {

			if (tot > numPages) {
				logger.info("Exit after " + tot + " lines (" + numPages + ")");
				end();
				System.exit(0);
			}

			if (line.length() > 0) {
				myExecutor.execute(new LineProcessor(line));
			}

			if ((tot % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				//logger.info(df.format(tot) + "\t" + df.format(end - begin) + "\t" + new Date());
				notification(tot, begin, end);
				begin = System.currentTimeMillis();
			}
			tot++;
		}
		lnr.close();
		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(end - begin) + "\t" + new Date());

		try {
			myExecutor.shutdown();
			logger.debug("waiting to end " + new Date() + "...");
			myExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			logger.error(e);
		}


		logger.info("ending the process " + new Date() + "...");
		end();

	}

	public void notification(int tot, long begin, long end) {
		logger.info(df.format(tot) + "\t" + df.format(end - begin) + "\t" + new Date());
	}
}
