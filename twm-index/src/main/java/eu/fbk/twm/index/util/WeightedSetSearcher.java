/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.twm.utils.WeightedSet;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:11 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class WeightedSetSearcher extends AbstractSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>FreqSetSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(WeightedSetSearcher.class.getName());

	public static final int DEFAULT_MIN_FREQ = 1000;

	public static final boolean DEFAULT_THREAD_SAFE = false;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected boolean threadSafe;

	private Map<String, WeightedSet> cache;

	private Term keyTerm;

	protected String keyFieldName;

	protected String valueFieldName;

	private int cutoff;

	public WeightedSetSearcher(String indexName) throws IOException {
		this(indexName, FreqSetIndexer.DEFAULT_KEY_FIELD_NAME, FreqSetIndexer.DEFAULT_VALUE_FIELD_NAME, DEFAULT_THREAD_SAFE);
	}

	public WeightedSetSearcher(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		this(indexName, keyFieldName, valueFieldName, false);
	}

	public WeightedSetSearcher(String indexName, String keyFieldName, String valueFieldName, boolean threadSafe) throws IOException {
		super(indexName);
		this.threadSafe = threadSafe;
		this.keyFieldName = keyFieldName;
		this.valueFieldName = valueFieldName;
		logger.debug(keyFieldName + "\t" + valueFieldName);

		keyTerm = new Term(keyFieldName, "");
		logger.debug(keyTerm);
		logger.trace(toString(10));
	}

	public int getCutoff() {
		return cutoff;
	}

	public void setCutoff(int cutoff) {
		this.cutoff = cutoff;
	}

	public void setKeyFieldName(String keyFieldName) {
		keyTerm = new Term(keyFieldName, "");
		this.keyFieldName = keyFieldName;
	}

	public String getKeyFieldName() {
		return keyFieldName;
	}

	public String getValueFieldName() {
		return valueFieldName;
	}

	public void setValueFieldName(String valueFieldName) {
		this.valueFieldName = valueFieldName;
	}


	public void loadCache(String name) throws IOException {
		loadCache(new File(name));
	}

	public void loadCache(String name, int minFreq) throws IOException {
		loadCache(new File(name), minFreq);
	}

	public void loadCache(File f) throws IOException {
		loadCache(f, DEFAULT_MIN_FREQ);
	}

	public void loadCache(File f, int minFreq) throws IOException {
		logger.info("loading cache from " + f + " (freq>" + minFreq + ")...");
		long begin = System.nanoTime();

		if (threadSafe) {
			logger.info(this.getClass().getName() + "'s cache is thread safe");
			cache = Collections.synchronizedMap(new HashMap<String, WeightedSet>());
		}
		else {
			logger.warn(this.getClass().getName() + "'s cache isn't thread safe");
			cache = new HashMap<String, WeightedSet>();
		}

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;
		int i = 1;
		String[] t;
		int freq = 0;
		WeightedSet result;
		Document doc;
		TermDocs termDocs;
		//logger.debug("notificatin point " + getNotificationPoint());
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				freq = Integer.parseInt(t[0]);
				if (freq < minFreq) {
					break;
				}
				termDocs = indexReader.termDocs(keyTerm.createTerm(t[1]));
				if (termDocs.next()) {
					doc = indexReader.document(termDocs.doc());
					result = WeightedSet.fromByte(doc.getBinaryValue(valueFieldName));
					cache.put(t[1], result);
				}
			}
			if ((i % notificationPoint) == 0) {
				System.out.print(CharacterTable.FULL_STOP);
			}
			i++;
		}
		System.out.print(CharacterTable.LINE_FEED);
		lnr.close();
		long end = System.nanoTime();
		logger.info(df.format(cache.size()) + " (" + df.format(indexReader.numDocs()) + ") keys cached in " + tf.format(end - begin) + " ns");
	}

	public WeightedSet search(String key) {
		return search(key, false);
	}

	public WeightedSet search(String key, boolean preserveOrder) {
		WeightedSet result = null;
		if (cache != null) {
			result = cache.get(key);
		}

		if (result != null) {
			return result;
		}

		try {
			TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(key));

			if (termDocs.next()) {
				Document doc = indexReader.document(termDocs.doc());
				result = WeightedSet.fromByte(doc.getBinaryValue(valueFieldName), preserveOrder);

				return result;
			}
		} catch (IOException e) {
			logger.error(e);
		}
		return new WeightedSet();
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				WeightedSet result = search(s[0]);
				long end = System.nanoTime();

				if (result != null) {
					logger.info(result.toString(true));
				}
				else {
					logger.info(s[0] + " not found in " + tf.format(end - begin) + " ns");
				}
			}
		}
	}
}
