package eu.fbk.twm.index.atomicindexers;

import eu.fbk.twm.index.util.AbstractIndexer;
import eu.fbk.twm.index.util.SerialUtils;
import eu.fbk.utils.math.Node;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 2/28/13
 * Time: 9:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class SingleLanguageFeatureSchemaIndexer extends AbstractIndexer {

	public static String FIELD_ID_NAME = "id";
	public static String FIELD_VALUE_NAME = "value";

	public SingleLanguageFeatureSchemaIndexer(String indexName) throws IOException {
		super(indexName);
	}

	public void add(Integer index, Node[] value) {

		Document d = new Document();

//		NumericField field = new NumericField(FIELD_ID_NAME);
//		field.setIntValue(index);
//		d.add(field);
		d.add(new Field(FIELD_ID_NAME, SerialUtils.toByteArray(index), Field.Store.YES));
		d.add(new Field(FIELD_VALUE_NAME, SerialUtils.object2ByteArray(value), Field.Store.YES));
		try {
			indexWriter.addDocument(d);
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}
}
