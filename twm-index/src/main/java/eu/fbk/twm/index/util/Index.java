/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import eu.fbk.twm.utils.OptionBuilder;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:23 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Index {
    
	public final static int DEFAULT_NOTIFICATION_POINT = 100000;

	protected int notificationPoint;

	protected String indexName;

	protected Index(String indexName) {
		this(indexName, DEFAULT_NOTIFICATION_POINT);
	}

	protected Index(String indexName, int notificationPoint) {
		this.indexName = indexName;
		this.notificationPoint = notificationPoint;
	}

	public String getIndexName() {
		return indexName;
	}

	public int getNotificationPoint() {
		return notificationPoint;
	}

	public void setNotificationPoint(int notificationPoint) {
		this.notificationPoint = notificationPoint;
	}
    
    protected static Option getOptionForNotificationPoint()
    {
        return new OptionBuilder()
                .withArgName("int")
                .hasArg()
                .withDescription("Receive notification every n pages (default is " + Index.DEFAULT_NOTIFICATION_POINT + ")")
                .withLongOpt("notification-point")
                .toOption("b");
    }
    
    protected static int resolveNotificationPoint(CommandLine line) throws NumberFormatException
    {
        int notificationPoint = Index.DEFAULT_NOTIFICATION_POINT;
        if (line.hasOption("notification-point")) {
            notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
        }
        return notificationPoint;
    }
    
    protected static Option getOptionForInputIndex()
    {
        return new OptionBuilder()
                .withArgName("dir")
                .hasArg()
                .withDescription("Open an index with the specified name")
                .isRequired()
                .withLongOpt("index")
                .toOption("i");
    }
}
