/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractIndexer;
import eu.fbk.twm.utils.ClassResource;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 4:01 PM
 * To change this template use File | Settings | File Templates.
 *
 * old: /data/models/dbpedia/current/dbpedia-classes/
 * new: /data/models/dbpedia/ml-classes/
 *
 * @see org.fbk.cit.hlt.thewikimachine.airpedia.PageAirpediaClassExtractor
 *
 * @deprecated
 * @see PageAirpediaTypeIndexer
 *
 */
@Deprecated public class PageAirpediaClassIndexer extends AbstractIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageAirpediaClassIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(PageAirpediaClassIndexer.class.getName());

	public static final int CLASS_COLUMN_INDEX = 0;

	public static final int PAGE_COLUMN_INDEX = 1;

	public static final int CONFIDENCE_COLUMN_INDEX = 2;

	public static final int NAMESPACE_COLUMN_INDEX =3;

	public static final int RESOURCE_COLUMN_INDEX = 4;

	public static final int PAGE_FREQ_COLUMN_INDEX = 5;

	public static final String CLASS_FIELD_NAME = "class";

	public static final String PAGE_FIELD_NAME = "page";

	//public static final String CONFIDENCE_FIELD_NAME = "conf";

	protected static Pattern tabPattern = Pattern.compile("\t");

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public PageAirpediaClassIndexer(String indexName) throws IOException {
		super(indexName);
	}

	public void index(String fileName, boolean compress) throws IOException {
		index(new File(fileName), compress);
	}

	public void index(File file, boolean compress) throws IOException {
		logger.info("indexing " + file + "...");
		//int max = Math.max(key, value);
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		if (compress) {
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file)), "UTF-8"));
		}
		else {
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		}

		String line = null;
		int count = 0, part = 0, tot = 0;
		String oldKey = "";
		Set<ClassResource> set = new TreeSet<ClassResource>();
		String[] t;
		logger.info("tot\tcount\ttime\tdate");
		// read the first line
		if ((line = lnr.readLine()) != null) {
			//logger.debug(line + "\t(" + oldKey + ")");
			t = tabPattern.split(line);
			if (t.length == 6) {
				//logger.debug(t[CLASS_COLUMN_INDEX] + "\t" + Double.parseDouble(t[CONFIDENCE_COLUMN_INDEX]));
				//String label, String url, double confidence, String resource
				set.add(new ClassResource(t[CLASS_COLUMN_INDEX], Integer.parseInt(t[NAMESPACE_COLUMN_INDEX]), Double.parseDouble(t[CONFIDENCE_COLUMN_INDEX]), Integer.parseInt(t[RESOURCE_COLUMN_INDEX])));
				oldKey = t[PAGE_COLUMN_INDEX];
				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			//logger.debug(line + "\t(" + oldKey + ")");
			t = tabPattern.split(line);
			if (t.length == 6) {
				if (!t[PAGE_COLUMN_INDEX].equals(oldKey)) {
					add(oldKey, set);
					count++;
					set = new TreeSet<ClassResource>();
					part = 0;
				}
				//logger.debug(t[CLASS_COLUMN_INDEX] + "\t" + Double.parseDouble(t[CONFIDENCE_COLUMN_INDEX]));
				//set.add(new ClassResource(t[CLASS_COLUMN_INDEX], Double.parseDouble(t[CONFIDENCE_COLUMN_INDEX])));
				set.add(new ClassResource(t[CLASS_COLUMN_INDEX], Integer.parseInt(t[NAMESPACE_COLUMN_INDEX]), Double.parseDouble(t[CONFIDENCE_COLUMN_INDEX]), Integer.parseInt(t[RESOURCE_COLUMN_INDEX])));
				oldKey = t[PAGE_COLUMN_INDEX];
				part++;
			}
			tot++;

			//if (tot > 1000000)
			//	break;

			if ((tot % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + " lines read, key indexed: " + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last document
		add(oldKey, set);
		lnr.close();
	} // end read


	public void add(String key, Set<ClassResource> value) {
		Document doc = new Document();
		try {
			//if (value.size() > 1)
			//	logger.debug(value.size() + "\t" + key + "\t" + value);
			doc.add(new Field(PAGE_FIELD_NAME, key, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field(CLASS_FIELD_NAME, toByte(value, key), Field.Store.YES));
			indexWriter.addDocument(doc);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	protected byte[] toByte(Set<ClassResource> set, String key) throws IOException {
		int freq = 0;
		String form = null;
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(1024);
		DataOutputStream dataStream = new DataOutputStream(byteStream);
		// number of distinct classes
		dataStream.writeInt(set.size());
		Iterator<ClassResource> it = set.iterator();
		for (int i = 0; it.hasNext(); i++) {
			ClassResource entry = it.next();
			//logger.warn(key + "\t" + i + "\t" + entry);
			dataStream.writeUTF(entry.getLabel());
			dataStream.writeChar(entry.getNamespace());

			/*if (entry.getConfidence() == 100) {
				dataStream.writeDouble(1.0);
			} else {
				dataStream.writeDouble(entry.getConfidence()  / 10.0);
			}*/
			//todo: check if the confidence is between 0 and 1
			dataStream.writeDouble(entry.getConfidence());
			dataStream.writeChar(entry.getResource());
		}
		return byteStream.toByteArray();
	}


	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}


		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("create an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option inputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the key/value pairs to index from the specified file").withLongOpt("file").create("f");
			Option compressOpt = OptionBuilder.withArgName("compress").withDescription("the input file is compressed (default is " + WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT + ")").withLongOpt("compress").create("c");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			//Option freqFileOpt = OptionBuilder.withArgName("key-freq").withDescription("key frequency file").withLongOpt("key-freq").create("f");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(inputFileOpt);
			options.addOption(compressOpt);
			//options.addOption(valueFieldNameOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			boolean compress = WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT;
			if (line.hasOption("compress")) {
				compress = true;
			}

			PageAirpediaClassIndexer formPageIndexer = new PageAirpediaClassIndexer(line.getOptionValue("index"));
			/*if (line.hasOption("key-field-name")) {
				pageFormIndexer.setKeyFieldName(line.getOptionValue("key-field-name"));
			}
			if (line.hasOption("value-field-name")) {
				pageFormIndexer.setKeyFieldName(line.getOptionValue("value-field-name"));
			}*/
			formPageIndexer.index(line.getOptionValue("file"), compress);
			formPageIndexer.close();
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PageAirpediaClassIndexer", "\n", options, "\n", true);
		}
	}
}
