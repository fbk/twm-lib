/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractSearcher;
import eu.fbk.twm.index.util.SerialUtils;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import eu.fbk.twm.utils.Defaults;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/24/13
 * Time: 11:37 PM
 * To change this template use File | Settings | File Templates.
 * @deprecated
 * @see PageTypeSearcher
 */
@Deprecated public class TypeSearcher extends AbstractSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TypeSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(TypeSearcher.class.getName());

	public static final int DEFAULT_MIN_FREQ = 1000;

	public static final boolean DEFAULT_THREAD_SAFE = false;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	private static DecimalFormat ff = new DecimalFormat("###,###,##0.000");

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	public static final String NOM_LABEL = "TopicalConcept";

	public static final String NAM_LABEL = "NameEntity";

	protected boolean threadSafe;

	private Map<String, Entry> cache;

	private Term keyTerm;

	public TypeSearcher(String indexName) throws IOException {
		this(indexName, false);
	}

	public TypeSearcher(String indexName, boolean threadSafe) throws IOException {
		super(indexName);
		this.threadSafe = threadSafe;
		keyTerm = new Term(TypeIndexer.PAGE_FIELD_NAME, "");
		logger.debug(keyTerm);
		//logger.debug(indexReader.numDocs());
		logger.trace(toString(10));
	}

	public void loadCache() throws IOException {
		logger.info("loading cache...");
		long begin = System.nanoTime();
		if (threadSafe) {
			logger.info(this.getClass().getName() + "'s cache is thread safe");
			cache = Collections.synchronizedMap(new HashMap<String, Entry>());
		}
		else {
			logger.warn(this.getClass().getName() + "'s cache isn't thread safe");
			cache = new HashMap<String, Entry>();
		}

		StringBuilder sb = new StringBuilder();
		Document doc;
		String page;
		Entry result;
		for (int i = 1; i < indexReader.numDocs(); i++) {

			doc = indexReader.document(i);
			page = doc.get(TypeIndexer.PAGE_FIELD_NAME);
			result = new Entry(SerialUtils.toDouble(doc.getBinaryValue(TypeIndexer.FREQ_FIELD_NAME)));

			cache.put(page, result);
			if ((i % notificationPoint) == 0) {
				//System.out.print(CharacterTable.FULL_STOP);
				logger.debug(i + " keys read (" + cache.size() + ") " + new Date());
			}
		}
		long end = System.nanoTime();
		logger.info(df.format(cache.size()) + " (" + df.format(indexReader.numDocs()) + ") keys cached in " + tf.format(end - begin) + " ns");

	}

	public void dump(File f) throws IOException {
		logger.info("dumping " + f + "...");
		long begin = System.nanoTime();
		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8")));

		Document doc;
		String page;
		Entry result;
		for (int i = 0; i < indexReader.numDocs(); i++) {

			doc = indexReader.document(i);
			page = doc.get(TypeIndexer.PAGE_FIELD_NAME);
			result = new Entry(SerialUtils.toDouble(doc.getBinaryValue(TypeIndexer.FREQ_FIELD_NAME)));
			pw.print(page);
			pw.print(CharacterTable.HORIZONTAL_TABULATION);
			pw.println(result.getFreq());

		}
		long end = System.nanoTime();
		logger.info(df.format(indexReader.numDocs()) + " nom dumped in " + tf.format(end - begin) + " ns");
		pw.close();
	}

	public void loadCache(String name) throws IOException {
		loadCache(new File(name));
	}

	public void loadCache(String name, int minFreq) throws IOException {
		loadCache(new File(name), minFreq);
	}

	public void loadCache(File f) throws IOException {
		loadCache(f, DEFAULT_MIN_FREQ);
	}

	public void loadCache(File f, int minFreq) throws IOException {
		logger.info("loading cache from " + f + " (freq>" + minFreq + ")...");
		long begin = System.nanoTime();

		if (threadSafe) {
			logger.info(this.getClass().getName() + "'s cache is thread safe");
			cache = Collections.synchronizedMap(new HashMap<String, Entry>());
		}
		else {
			logger.warn(this.getClass().getName() + "'s cache isn't thread safe");
			cache = new HashMap<String, Entry>();
		}

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;
		int i = 0;
		String[] t;
		int freq = 0;
		Entry result;
		Document doc;
		TermDocs termDocs;
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				freq = Integer.parseInt(t[0]);
				if (freq < minFreq) {
					break;
				}
				termDocs = indexReader.termDocs(keyTerm.createTerm(t[1]));
				if (termDocs.next()) {
					doc = indexReader.document(termDocs.doc());
					result = new Entry(SerialUtils.toDouble(doc.getBinaryValue(TypeIndexer.FREQ_FIELD_NAME)));
					cache.put(t[1], result);
				}
			}
			if ((i % notificationPoint) == 0) {
				//System.out.print(CharacterTable.FULL_STOP);
				logger.debug(i + " keys read (" + cache.size() + ") " + new Date());
			}
			i++;
		}
		System.out.print(CharacterTable.LINE_FEED);
		lnr.close();
		long end = System.nanoTime();
		logger.info(df.format(cache.size()) + " (" + df.format(indexReader.numDocs()) + ") keys cached in " + tf.format(end - begin) + " ns");
	}

	public Entry search(String page) {
		//logger.debug("searching " + q + "...");
		//long begin = 0, end = 0;
		//begin = System.nanoTime();
		Entry result = null;
		if (cache != null) {
			result = cache.get(page);
		}

		if (result != null) {
			//end = System.nanoTime();
			//logger.debug("found in cache in " + tf.format(end - begin) + " ns");
			return result;
		}

		//end = System.nanoTime();
		//logger.debug("not found in cache in " + tf.format(end - begin) + " ns");

		Entry entry;
		try {
			//begin = System.nanoTime();
			TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(page));
			//end = System.nanoTime();
			//begin = System.nanoTime();
			if (termDocs.next()) {
				Document doc = indexReader.document(termDocs.doc());
				entry = new Entry(SerialUtils.toDouble(doc.getBinaryValue(TypeIndexer.FREQ_FIELD_NAME)));
				return entry;
				//end = System.nanoTime();
				//logger.debug("retrieved in " + tf.format(end - begin) + " ns");
			}
		} catch (IOException e) {
			logger.error(e);
		}

		return new Entry(0);
	}

	public class Entry {
		private String type;

		private double freq;

		public Entry(double freq) {
			if (freq > 0) {
				this.freq = freq;
				type = NOM_LABEL;
			}
			else {
				//this.freq = 1 - freq;
				this.freq = 1;
				type = NAM_LABEL;
			}
		}

		public String getType() {
			return type;
		}

		public double getFreq() {
			return freq;
		}

		@Override
		public String toString() {
			return type + '\t' + freq;
		}
	}

	public void interactive() throws Exception {
		InputStreamReader indexReader = null;
		BufferedReader myInput = null;
		long begin = 0, end = 0;
		while (true) {
			System.out.println("\nPlease write a query and type <return> to continue (CTRL C to exit):");

			indexReader = new InputStreamReader(System.in);
			myInput = new BufferedReader(indexReader);
			//String query = myInput.readLine().toString().replace(' ', '_');
			String query = myInput.readLine().toString();

			begin = System.nanoTime();
			Entry entry = search(query);
			end = System.nanoTime();
			logger.info(query + "\t" + entry.getType() + "\t" + ff.format(entry.getFreq()) + "\t" + tf.format(end - begin) + " ns");
			/*begin = System.nanoTime();
			Object o = cache.get(query);
			end = System.nanoTime();
			logger.info(query + "\t" + o  + "\t" + tf.format(end - begin) + " ns");*/

		} // end while
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option dumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("dump the index").withLongOpt("dump").create("d");
			Option searchOpt = OptionBuilder.withArgName("string").hasArg().withDescription("search for the specified key").withLongOpt("search").create("s");
			Option freqFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			Option minimumKeyFreqOpt = OptionBuilder.withArgName("int").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			options.addOption(freqFileOpt);
			options.addOption(dumpOpt);
			//options.addOption(valueFieldNameOpt);
			options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			TypeSearcher typeSearcher = new TypeSearcher(line.getOptionValue("index"));
			typeSearcher.setNotificationPoint(notificationPoint);
			if (line.hasOption("key-freq")) {
				typeSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
			}
			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				TypeSearcher.Entry result = typeSearcher.search(line.getOptionValue("search"));
				logger.info(result);
			}
			if (line.hasOption("interactive-mode")) {
				typeSearcher.interactive();
			}

			if (line.hasOption("dump")) {
				typeSearcher.dump(new File(line.getOptionValue("dump")));
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.TypeSearcher", "\n", options, "\n", true);
		}
	}
}
