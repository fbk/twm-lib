package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractIndexer;
import eu.fbk.twm.utils.OptionBuilder;
import eu.fbk.twm.utils.Stopwatch;
import eu.fbk.twm.utils.StringTable;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * Class that created an index for PageSectionTextSearcher
 * Index is in the following format: Page Title (indexed) - Section Title — Section Text
 *
 * @see PageSectionTextSearcher
 */
public class PageSectionTextIndexer extends AbstractIndexer {
	private static Logger logger = Logger.getLogger(PageSectionTextIndexer.class.getName());

	//Field related constants
	public static final String PAGE_TITLE_FIELD_NAME = "page";
	public static final String SECTION_TITLE_FIELD_NAME = "section";
	public static final String SECTION_TEXT_FIELD_NAME = "text";

	public static final int PAGE_TITLE_COL_IDX = 0;
	public static final int SECTION_TITLE_COL_IDX = 1;
	public static final int SECTION_TEXT_COL_IDX = 2;

	//Index handling setting
	public static final boolean DEFAULT_FILE_COMPRESS = false;

	protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);
	protected static DecimalFormat decFormat = new DecimalFormat("###,###,###,###");

	public PageSectionTextIndexer(String indexName) throws IOException {
		super(indexName);
	}

	public PageSectionTextIndexer(String indexName, boolean clean) throws IOException {
		super(indexName, clean);
	}

	public void index(String fileName) throws IOException {
		index(new File(fileName), DEFAULT_FILE_COMPRESS);
	}

	public void index(String fileName, boolean compress) throws IOException {
		index(new File(fileName), compress);
	}

	public void index(File file) throws IOException {
		index(file, DEFAULT_FILE_COMPRESS);
	}

	public void index(File file, boolean compress) throws IOException {
		logger.info("indexing " + file + "...");
		Stopwatch stopwatch = Stopwatch.start();
		LineNumberReader lnr;
		if (compress) {
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file)), "UTF-8"));
		}
		else {
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		}

		String line;
		int tot = 0, count = 0;
		String[] t;
		logger.info("Overall\tSaved\tChunk time\tOverall time");

		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length > SECTION_TEXT_COL_IDX) {
				add(t);
				count++;
			}
			tot++;

			if ((tot % notificationPoint) == 0) {
				logInd(tot, count, stopwatch);
			}
		}

		logInd(tot, count, stopwatch);
		lnr.close();
	}

	private void logInd(int tot, int count, Stopwatch stopwatch) {
		logger.info(decFormat.format(tot) + "\t" + decFormat.format(count) + "\t" + decFormat.format(stopwatch.click()) + " ms \t" + decFormat.format(stopwatch.getTimeSinceStart()) + " ms");
	}

	/**
	 * Adding tuple to the index
	 *
	 * @param t array representing a line from section-text.csv
	 */
	public void add(String[] t) {
		if (t.length > 1) {
			Document doc = new Document();
			try {
				doc.add(new Field(PAGE_TITLE_FIELD_NAME, t[PAGE_TITLE_COL_IDX], Field.Store.YES, Field.Index.NOT_ANALYZED));
				doc.add(new Field(SECTION_TITLE_FIELD_NAME, t[SECTION_TITLE_COL_IDX].getBytes(), Field.Store.YES));
				doc.add(new Field(SECTION_TEXT_FIELD_NAME, t[SECTION_TEXT_COL_IDX].getBytes(), Field.Store.YES));

				indexWriter.addDocument(doc);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = new OptionBuilder().withArgName("index").hasArg().withDescription("create an index with the specified name").isRequired().withLongOpt("index").toOption("i");
			Option inputFileOpt = new OptionBuilder().withArgName("file").hasArg().withDescription("read the PAGE_COLUMN_INDEX/value pairs to index from the specified file").isRequired().withLongOpt("file").toOption("f");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(inputFileOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("Switching to help mode");
			}

			PageSectionTextIndexer indexer = new PageSectionTextIndexer(line.getOptionValue("index"));
			indexer.index(line.getOptionValue("file"));
			indexer.close();
		} catch (Exception e) {
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			//new HelpFormatter().printHelp(400, "java -cp dist/thewikimachine.jar "+PageSectionTextIndexer.class.getName(), "\n", options, "\n", true);
			new HelpFormatter().printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PageSectionTextIndexer", "\n", options, "\n", true);
		}
	}
}
