package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.ParsedPageTitle;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.index.PageFormSearcher;
import eu.fbk.twm.index.TypeSearcher;
import eu.fbk.twm.index.util.FreqSetSearcher;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 8/26/13
 * Time: 10:28 PM
 * To change this template use File | Settings | File Templates.
 *
 * @deprecated
 */
@Deprecated public class RedirectFormExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>RedirectFormExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(RedirectFormExtractor.class.getName());

	private static final int DEFAULT_MIN_FREQ = 100000;

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	TypeSearcher typeSearcher;

	PageFormSearcher pageFormSearcher;

	private Map<String, Integer> frePageMap;

	private Tokenizer tokenizer;

	public RedirectFormExtractor(TypeSearcher typeSearcher, PageFormSearcher pageFormSearcher, String freqPageFileName) throws IOException {
		this.typeSearcher = typeSearcher;
		this.pageFormSearcher = pageFormSearcher;
		tokenizer = new HardTokenizer();
		frePageMap = createFreqPageMap(freqPageFileName);
	}

	private Map<String, Integer> createFreqPageMap(String freqPageFileName) throws IOException {
		logger.info("reading page/freq pairs from " + freqPageFileName + "...");

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(freqPageFileName), "UTF-8"));
		Map<String, Integer> map = new HashMap<String, Integer>();
		String line = null;
		int count = 0, tot = 0;
		String[] t = null;
		// read the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				map.put(t[1], new Integer(t[0]));
			}
		}
		logger.info(map.size() + " forms read");
		return map;
	}

	private String find(TypeSearcher.Entry type, FreqSetSearcher.Entry[] forms, String redirect, String page) {

		for (int i = 0; i < forms.length; i++) {
			if (type.getType().equals(TypeSearcher.NOM_LABEL)) {
				if (redirect.equalsIgnoreCase(forms[i].getValue())) {
					return forms[i].getValue();
				}
			}
			else {
				if (redirect.equals(forms[i].getValue())) {
					//return true;
					return forms[i].getValue();
				}
			}
		}
		//return false;
		return null;
	}

	public void start(String name) throws IOException {
		logger.info("reading redirect pairs " + name + "...");

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));
		Map<String, String> map = new HashMap<String, String>();
		String line = null;
		int count = 0, tot = 0;
		String[] t = null;
		// read the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				//logger.debug(line);
				Integer freq = frePageMap.get(t[1]);
				if (freq != null) {
					TypeSearcher.Entry type = typeSearcher.search(t[1]);
					FreqSetSearcher.Entry[] forms = pageFormSearcher.search(t[1]);
					ParsedPageTitle redirect = new ParsedPageTitle(t[0]);
					String tokenizedRedirect = tokenizer.tokenizedString(redirect.getForm());
					String form = find(type, forms, tokenizedRedirect, t[1]);
					if (form != null) {
						System.out.println("SI\t" + type.getType() + "\t" + line + "\t" + tokenizedRedirect + "\t" + form + "\t" + freq);
					}
					else {
						System.out.println("NO\t" + type.getType() + "\t" + line + "\t" + tokenizedRedirect+ "\t[" + forms[0] + "]\t" + freq);
					}
					tot++;
				}
			}
		}
		logger.info(count + "/" + tot);

	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			Option ngramSizeOpt = OptionBuilder.withArgName("n-gram").hasArg().withDescription("n-grams size (default is " + PageNGramExtractor.DEFAULT_N_GRAM + ")").withLongOpt("n-gram").create("n");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");


			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(ngramSizeOpt);
			options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}
			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);

			TypeSearcher typeSearcher = new TypeSearcher(extractorParameters.getWikipediaTypeIndexName());
			typeSearcher.loadCache();
			PageFormSearcher pageFormSearcher = new PageFormSearcher(extractorParameters.getWikipediaPageFormIndexName());

			if (line.hasOption("key-freq")) {
				pageFormSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
			}
			RedirectFormExtractor redirectFormExtractor = new RedirectFormExtractor(typeSearcher, pageFormSearcher, extractorParameters.getWikipediaPageFreqFileName());

			redirectFormExtractor.start(extractorParameters.getWikipediaRedirFileName());


		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.RedirectFormExtractor", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
