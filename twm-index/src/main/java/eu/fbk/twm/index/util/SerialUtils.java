/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import eu.fbk.utils.math.Node;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/24/13
 * Time: 7:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class SerialUtils {

	public static Node[] valuesByteArrayToVector(byte[] byteArray) throws IOException {
		ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);
		DataInputStream dataStream = new DataInputStream(byteStream);
		int m = dataStream.readInt();

		TreeMap<Integer, Double> feats = new TreeMap<Integer, Double>();

		for (int j = 0; j < m; j++) {
			int id = dataStream.readInt();
			feats.put(id, dataStream.readDouble());
		}

		Node[] ret = new Node[m];
		int j = 0;
		for (Integer featID : feats.keySet()) {
			ret[j++] = new Node(featID, feats.get(featID));
		}
		return ret;
	}

	public static String valuesByteArrayToString(byte[] byteArray, int offset) throws IOException {
		ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);
		DataInputStream dataStream = new DataInputStream(byteStream);
		int m = dataStream.readInt();
		String ret = "";
		// Node[] node = new Node[m];
		for (int j = 0; j < m; j++) {
			int id = dataStream.readInt();
			id += offset;
			ret += id + ":" + dataStream.readDouble() + " ";
		}

		return ret.trim();
	}

	public static byte[] featureStringToByteArray(String str) throws IOException {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(1024);
		DataOutputStream dataStream = new DataOutputStream(byteStream);
		StringTokenizer st = new StringTokenizer(str, " \t\n\r\f:");
		int m = st.countTokens() / 2;
		dataStream.writeInt(m);
		for (int j = 0; j < m; j++) {
			dataStream.writeInt(Integer.parseInt(st.nextToken()));
			dataStream.writeDouble(Double.valueOf(st.nextToken()));
		}

		return byteStream.toByteArray();
	}

	public static final byte[] toByteArray(int value) {
		return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
	}

	public static final int toInt(byte[] b) {
		return (b[0] << 24) + ((b[1] & 0xFF) << 16) + ((b[2] & 0xFF) << 8) + (b[3] & 0xFF);
	}

	public static byte[] toByteArray(double value) {
		byte[] bytes = new byte[8];
		ByteBuffer.wrap(bytes).putDouble(value);
		return bytes;
	}

	public static double toDouble(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getDouble();
	}

	public static byte[] object2ByteArray(Object o) {
		ByteArrayOutputStream baos = null;
		try {
			baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(o);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (baos == null) {
			return null;
		}
		return baos.toByteArray();
	}

	public static Object loadObjectFromByteArray(byte[] input) {
		Object ret = null;

		try {
			ObjectInputStream oisFr = new ObjectInputStream(new ByteArrayInputStream(input));
			ret = oisFr.readObject();
			oisFr.close();
		} catch (Exception e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}

		return ret;
	}

}
