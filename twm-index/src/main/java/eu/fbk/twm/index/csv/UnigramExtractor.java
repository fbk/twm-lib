/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.*;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/20/13
 * Time: 9:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class UnigramExtractor extends CSVExtractor {

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	private static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	private SynchronizedCounter synchronizedCounter;

	private PrintWriter unigramWriter;

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");


	public UnigramExtractor(int numThreads, int numPages) {
		super(numThreads, numPages);
		synchronizedCounter = new SynchronizedCounter();
	}

	public UnigramExtractor(int numThreads) {
		super(numThreads);
		synchronizedCounter = new SynchronizedCounter();
	}

	@Override
	public void processLine(String line) {
		//todo: investigate why this is so slow (multi-threading is working)
		String[] tokens = spacePattern.split(line.toLowerCase());
		for (int i = 1; i < tokens.length; i++) {
			if (isWord(tokens[i])) {
				synchronizedCounter.add(tokens[i]);
			}
		}

		/*if ((synchronizedCounter.size() % 10000) == 0)
		{
			logger.debug(synchronizedCounter.size());
		}*/
	}

	private boolean isWord(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (!Character.isLetter(s.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			logger.info("writing unigrams in " + extractorParameters.getWikipediaUnigramFileName() + "...");
			unigramWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaUnigramFileName()), "UTF-8")));
			read(extractorParameters.getWikipediaTextFileName());
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public void end() {
		logger.info("sorting...");
		long begin = System.currentTimeMillis();
		SortedMap<AtomicInteger, List<String>> sortedMap = synchronizedCounter.getSortedMap();
		try {
			writeSortedMap(sortedMap);
			unigramWriter.close();
		} catch (IOException e) {
			logger.error(e);
		}
		long end = System.currentTimeMillis();
		logger.info(sortedMap.size() + " lines sorted in " + df.format(end - begin) + " ms " + new Date());
	}

	private void writeSortedMap(SortedMap<AtomicInteger, List<String>> sortedMap) throws IOException {
		logger.info("writing...");
		long begin = System.currentTimeMillis();
		int lf;
		String form;
		Iterator<AtomicInteger> it = sortedMap.keySet().iterator();
		StringBuilder sb;
		AtomicInteger freq;
		for (int i = 0; it.hasNext(); i++) {
			freq = it.next();
			List<String> list = sortedMap.get(freq);
			sb = new StringBuilder();
			for (int j = 0; j < list.size(); j++) {
				form = list.get(j);

				sb.append(freq);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(form);
				sb.append(CharacterTable.LINE_FEED);
			}
			unigramWriter.print(sb.toString());
		}
		long end = System.currentTimeMillis();
		logger.info(df.format(sortedMap.size()) + " lines wrote in " + df.format(end - begin) + "\t" + new Date());
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");


			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);
			CSVExtractor unigramExtractor = new UnigramExtractor(numThreads);
			unigramExtractor.setNotificationPoint(notificationPoint);
			unigramExtractor.start(extractorParameters);

		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.UnigramExtractor", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
