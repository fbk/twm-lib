package eu.fbk.twm.index;

import eu.fbk.twm.index.util.WeightedSetIndexer;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 27/01/14
 * Time: 09:39
 * To change this template use File | Settings | File Templates.
 */
public class PageAirpediaTypeIndexer extends WeightedSetIndexer {

	static Logger logger = Logger.getLogger(PageAirpediaTypeIndexer.class.getName());

	public PageAirpediaTypeIndexer(String indexName) throws IOException {
		this(indexName, false);
	}

	public PageAirpediaTypeIndexer(String indexName, boolean clean) throws IOException {
		super(indexName, clean);
	}
}
