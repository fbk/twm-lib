/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.lsa.LSM;
import eu.fbk.utils.math.Vector;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/30/13
 * Time: 7:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class VectorExtractor extends CSVExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>VectorExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(VectorExtractor.class.getName());

	private PrintWriter vectorWriter;

	private LSM lsm;

	private static Pattern spacePattern = Pattern.compile(" ");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public static final int DEFAULT_LSA_DIM = 100;

	public static final boolean DEFAULT_NORMALIZE = false;

	public VectorExtractor(int numThreads, int numPages, String lsaRoot) throws IOException {
		this(numThreads, numPages, lsaRoot, DEFAULT_LSA_DIM, DEFAULT_NORMALIZE);
	}

	public VectorExtractor(int numThreads, int numPages, String lsaRoot, int dim, boolean normalized) throws IOException {
		super(numThreads, numPages);
		if (!lsaRoot.endsWith(File.separator)) {
			lsaRoot += File.separator;
		}
		logger.info("reading lsm model from " + lsaRoot + " (" + dim + ")...");
		File Ut = new File(lsaRoot + "X-Ut");
		File Sk = new File(lsaRoot + "X-S");
		File r = new File(lsaRoot + "X-row");
		File c = new File(lsaRoot + "X-col");
		File df = new File(lsaRoot + "X-df");
		boolean rescaleIdf = true;
		lsm = new LSM(Ut, Sk, r, c, df, dim, rescaleIdf, normalized);
	}

	@Override
	public void processLine(String line) {
		//To change body of implemented methods use File | Settings | File Templates.
		String[] tokens = spacePattern.split(line);
		if (tokens.length < 2) {
			return;
		}
		try {

			BOW bow = new BOW();
			for (int i = 1; i < tokens.length; i++) {
				//logger.debug(i + "\t'" + tokens[i] + "'\t" + tokens[0]);
				bow.add(tokens[i].toLowerCase());
			}

			Vector d = lsm.mapDocument(bow);
			Vector pd = lsm.mapPseudoDocument(d);
			d.normalize();
			pd.normalize();

			synchronized (this) {
				vectorWriter.print(tokens[0]);
				//vectorWriter.print(CharacterTable.HORIZONTAL_TABULATION);
				//vectorWriter.print(bow.toSingleLine());
				vectorWriter.print(CharacterTable.HORIZONTAL_TABULATION);
				vectorWriter.print(pd.toString());
				vectorWriter.print(CharacterTable.HORIZONTAL_TABULATION);
				vectorWriter.println(d.toString());
			}
		} catch (Exception e) {
			logger.error("Error processing page " + tokens[0]);
		}
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			vectorWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaVectorFileName()), "UTF-8")));
			read(extractorParameters.getWikipediaTextFileName());
		} catch (IOException e) {
			logger.error(e);
		}
	}

	@Override
	public void end() {
		vectorWriter.close();
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create();
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			Option lsaDimOpt = OptionBuilder.withArgName("int").hasArg().withDescription("lsa dimension (default is " + VectorExtractor.DEFAULT_LSA_DIM + ")").withLongOpt("lsa-dim").create();
			Option lsaDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("lsa dir").isRequired().withLongOpt("lsa-dir").create("l");
			Option normalizedOpt = OptionBuilder.withDescription("normalize vectors (default is " + DEFAULT_NORMALIZE + ")").withLongOpt("normalized").create();

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");
			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(notificationPointOpt);
			options.addOption(lsaDirOpt);
			options.addOption(lsaDimOpt);
			options.addOption(lsaDimOpt);
			options.addOption(normalizedOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			int lsaDim = VectorExtractor.DEFAULT_LSA_DIM;
			if (line.hasOption("lsa-dim")) {
				lsaDim = Integer.parseInt(line.getOptionValue("lsa-dim"));
			}

			boolean normalized = DEFAULT_NORMALIZE;
			if (line.hasOption("normalized")) {
				normalized = true;
			}
			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);
			CSVExtractor ngramExtractor = new VectorExtractor(numThreads, numPages, line.getOptionValue("lsa-dir"), lsaDim, normalized);
			ngramExtractor.setNotificationPoint(notificationPoint);
			ngramExtractor.start(extractorParameters);
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}

			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.VectorExtractor", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
