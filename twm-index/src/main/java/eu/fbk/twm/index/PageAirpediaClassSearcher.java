/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractSearcher;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.ClassResource;
import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.StringTable;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:05 PM
 * To change this template use File | Settings | File Templates.
 * <p>
 * /data/models/dbpedia/current/dbpedia-classes/
 *
 * @see PageAirpediaTypeSearcher
 * @deprecated
 */
@Deprecated public class PageAirpediaClassSearcher extends AbstractSearcher {

    /**
     * Define a static logger variable so that it references the
     * Logger instance named <code>PageAirpediaClassSearcher</code>.
     */
    static Logger logger = Logger.getLogger(PageAirpediaClassSearcher.class.getName());

    public final static String[] NAMESPACES = { "http://en.wikipedia.org/wiki/", "http://dbpedia.org/ontology/",
            "http://www.wikidata.org/wiki/", "http://airpedia.org/ontology/" };

    public final static String[] RESOURCES = { "wikipedia", "dbpedia", "wikidata", "airpedia" };

    public static final int DEFAULT_MIN_FREQ = 1000;

    public static final boolean DEFAULT_THREAD_SAFE = false;

    protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

    protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

    protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

    protected boolean threadSafe;

    protected Map<String, ClassResource[]> cache;

    protected Term keyTerm;

    public PageAirpediaClassSearcher(String indexName) throws IOException {
        this(indexName, false);
    }

    public PageAirpediaClassSearcher(String indexName, boolean threadSafe) throws IOException {
        super(indexName);
        this.threadSafe = threadSafe;

        keyTerm = new Term(PageAirpediaClassIndexer.PAGE_FIELD_NAME, "");
        logger.debug(keyTerm);
        logger.trace(toString(10));
    }

    public Map<String, ClassResource[]> getCache() {
        return cache;
    }

    public void loadCache(String name) throws IOException {
        loadCache(new File(name));
    }

    public void loadCache(String name, int minFreq) throws IOException {
        loadCache(new File(name), minFreq);
    }

    public void loadCache(File f) throws IOException {
        loadCache(f, DEFAULT_MIN_FREQ);
    }

    public void loadCache(File f, int minFreq) throws IOException {
        logger.info("loading cache from " + f + " (freq>" + minFreq + ")...");
        long begin = System.nanoTime();

        if (threadSafe) {
            logger.info(this.getClass().getName() + "'s cache is thread safe");
            cache = Collections.synchronizedMap(new HashMap<String, ClassResource[]>());
        } else {
            logger.warn(this.getClass().getName() + "'s cache isn't thread safe");
            cache = new HashMap<String, ClassResource[]>();
        }

        LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
        String line;
        int i = 0;
        String[] t;
        int freq = 0;
        ClassResource[] result;
        Document doc;
        TermDocs termDocs;
        while ((line = lnr.readLine()) != null) {
            t = tabPattern.split(line);
            if (t.length == 2) {
                freq = Integer.parseInt(t[0]);
                if (freq < minFreq) {
                    break;
                }
                termDocs = indexReader.termDocs(keyTerm.createTerm(t[1]));
                if (termDocs.next()) {
                    doc = indexReader.document(termDocs.doc());
                    result = fromByte(doc.getBinaryValue(PageAirpediaClassIndexer.CLASS_FIELD_NAME));
                    cache.put(t[1], result);
                }
            }
            if ((i % notificationPoint) == 0) {
                System.out.print(CharacterTable.FULL_STOP);
            }
            i++;
        }
        System.out.print(CharacterTable.LINE_FEED);
        lnr.close();
        long end = System.nanoTime();
        logger.info(df.format(cache.size()) + " (" + df.format(indexReader.numDocs()) + ") keys cached in " + tf
                .format(end - begin) + " ns");
    }

    public ClassResource[] search(String key) {
        //logger.debug("searching " + key + "...");
        //long begin = 0, end = 0;

        //begin = System.nanoTime();
        ClassResource[] result = null;
        if (cache != null) {
            result = cache.get(key);
        }
        //end = System.nanoTime();

        if (result != null) {
            //logger.debug("found in cache in " + tf.format(end - begin) + " ns");
            return result;
        }

        try {
            //begin = System.nanoTime();
            TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(key));
            //end = System.nanoTime();
            //logger.debug("found in index in " + tf.format(end - begin) + " ns");

            if (termDocs.next()) {
                //begin = System.nanoTime();
                Document doc = indexReader.document(termDocs.doc());
                result = fromByte(doc.getBinaryValue(PageAirpediaClassIndexer.CLASS_FIELD_NAME));
                //end = System.nanoTime();
                //logger.debug(termDocs.freq() + " deserialized in " + tf.format(end - begin) + " ns");

                return result;
            }
        } catch (IOException e) {
            logger.error(e);
        }
        return new ClassResource[0];
    }

    public void interactive() throws Exception {
        InputStreamReader reader = null;
        BufferedReader myInput = null;
        while (true) {
            System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

            reader = new InputStreamReader(System.in);
            myInput = new BufferedReader(reader);
            String query = myInput.readLine().toString();
            String[] s = tabPattern.split(query);

            if (s.length == 1) {
                long begin = System.nanoTime();
                ClassResource[] result = search(s[0]);
                long end = System.nanoTime();

                if (result != null && result.length > 0) {
                    for (int i = 0; i < result.length; i++) {
                        //logger.info(i + "\t" + result[i].getNamespace() + "\t" + result[i].getLabel() + "\t" + result[i].getConfidence() + "\t" + result[i].getResource());
                        logger.info(i + "\t" + NAMESPACES[result[i].getNamespace()] + result[i].getLabel() + "\t"
                                + result[i].getConfidence() + "\t" + RESOURCES[result[i].getResource()]);
                    }
                    logger.info(s[0] + " found in " + tf.format(end - begin) + " ns");

                } else {
                    logger.info(s[0] + " not found in " + tf.format(end - begin) + " ns");
                }
            }
        }
    }

    private ClassResource[] fromByte(byte[] byteArray) throws IOException {
        ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);
        DataInputStream dataStream = new DataInputStream(byteStream);

        int size = dataStream.readInt();
        //logger.debug(size);
        ClassResource[] entryArray = new ClassResource[size];
        for (int j = 0; j < size; j++) {

            entryArray[j] = new ClassResource(dataStream.readUTF(), dataStream.readChar(), dataStream.readDouble(),
                    dataStream.readChar());
            //logger.debug(j + "\t" + entryArray[j]);
        }

        return entryArray;
    }

	/*public class ClassResource {

		private String label;

		private double probability;


		public ClassResource(String label, double probability) {
			this.label = label;
			this.probability = probability;
		}

		private String getLabel() {
			return label;
		}

		private double getProbability() {
			return probability;
		}

		@Override
		public String toString() {
			return label + '\t' + probability;
		}
	}   */

    public static void main(String args[]) throws Exception {
        String logConfig = System.getProperty("log-config");
        if (logConfig == null) {
            logConfig = "configuration/log-config.txt";
        }

        PropertyConfigurator.configure(logConfig);
        Options options = new Options();
        try {
            Option indexNameOpt = OptionBuilder.withArgName("index").hasArg()
                    .withDescription("open an index with the specified name").isRequired().withLongOpt("index")
                    .create("i");
            Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode")
                    .withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
            Option searchOpt = OptionBuilder.withArgName("search").hasArg()
                    .withDescription("search for the specified key").withLongOpt("search").create("s");
            Option freqFileOpt = OptionBuilder.withArgName("key-freq").hasArg()
                    .withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq")
                    .create("f");
            Option minimumKeyFreqOpt = OptionBuilder.withArgName("minimum-freq").hasArg()
                    .withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")")
                    .withLongOpt("minimum-freq").create("m");
            Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription(
                    "receive notification every n pages (default is "
                            + Defaults.DEFAULT_NOTIFICATION_POINT + ")")
                    .withLongOpt("notification-point").create("b");
            options.addOption("h", "help", false, "print this message");
            options.addOption("v", "version", false, "output version information and exit");

            options.addOption(indexNameOpt);
            options.addOption(interactiveModeOpt);
            options.addOption(searchOpt);
            options.addOption(freqFileOpt);
            //options.addOption(keyFieldNameOpt);
            //options.addOption(valueFieldNameOpt);
            options.addOption(minimumKeyFreqOpt);
            options.addOption(notificationPointOpt);

            CommandLineParser parser = new PosixParser();
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("help") || line.hasOption("version")) {
                throw new ParseException("");
            }

            int minFreq = DEFAULT_MIN_FREQ;
            if (line.hasOption("minimum-freq")) {
                minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
            }

            int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
            if (line.hasOption("notification-point")) {
                notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
            }

            PageAirpediaClassSearcher pageFormSearcher = new PageAirpediaClassSearcher(line.getOptionValue("index"));
            pageFormSearcher.setNotificationPoint(notificationPoint);
            /*logger.debug(line.getOptionValue("key-field-name") + "\t" + line.getOptionValue("value-field-name"));
			if (line.hasOption("key-field-name"))
			{
				pageFormSearcher.setKeyFieldName(line.getOptionValue("key-field-name"));
			}
			if (line.hasOption("value-field-name"))
			{
				pageFormSearcher.setValueFieldName(line.getOptionValue("value-field-name"));
			} */
            if (line.hasOption("key-freq")) {
                pageFormSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
            }
            if (line.hasOption("search")) {
                logger.debug("searching " + line.getOptionValue("search") + "...");
                ClassResource[] result = pageFormSearcher.search(line.getOptionValue("search"));
                logger.debug(result.length);
                for (int i = 0; i < result.length; i++) {
                    logger.debug(i + "\t" + result[i]);
                }
                //logger.info(Arrays.toString(result));
            }
            if (line.hasOption("interactive-mode")) {
                pageFormSearcher.interactive();
            }
        } catch (ParseException e) {
            // oops, something went wrong
            if (e.getMessage().length() > 0) {
                System.out.println("Parsing failed: " + e.getMessage() + "\n");
            }
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(400,
                    "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PageAirpediaClassSearcher",
                    "\n", options, "\n", true);
        }
    }

}
