/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import eu.fbk.twm.utils.StringTable;
import org.apache.commons.lang.SerializationUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import java.io.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:11 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class MapSearcher extends AbstractSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>SetSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(MapSearcher.class.getName());

	private IndexReader clSchema = null;
	private String clID = "wikiID";

	public void setClSchema(IndexReader clSchema) {
		this.clSchema = clSchema;
	}

	public static final int DEFAULT_MIN_FREQ = 1000;

	public static final boolean DEFAULT_THREAD_SAFE = false;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected boolean threadSafe;

	protected Map<String, HashMap> cache;

	protected Term keyTerm;

	protected String keyFieldName;

	protected String valueFieldName;

	protected MapSearcher(String indexName) throws IOException {
		this(indexName, FreqSetIndexer.DEFAULT_KEY_FIELD_NAME, FreqSetIndexer.DEFAULT_VALUE_FIELD_NAME, DEFAULT_THREAD_SAFE);
	}

	protected MapSearcher(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		this(indexName, keyFieldName, valueFieldName, false);
	}

	protected MapSearcher(String indexName, String keyFieldName, String valueFieldName, boolean threadSafe) throws IOException {
		super(indexName);
		this.threadSafe = threadSafe;
		this.keyFieldName = keyFieldName;
		this.valueFieldName = valueFieldName;
		logger.debug(keyFieldName + "\t" + valueFieldName);

		keyTerm = new Term(keyFieldName, "");
	}

	public void setKeyFieldName(String keyFieldName) {
		keyTerm = new Term(keyFieldName, "");
		this.keyFieldName = keyFieldName;
	}

	public String getKeyFieldName() {
		return keyFieldName;
	}

	public String getValueFieldName() {
		return valueFieldName;
	}

	public void setValueFieldName(String valueFieldName) {
		this.valueFieldName = valueFieldName;
	}

	public HashMap<String, Integer> search(String key) {

		logger.info("Searching for: " + key);
		HashMap<String, Integer> result = new HashMap<String, Integer>();

		try {
			Term t = new Term(keyFieldName, key);
			logger.debug(t);
			TermDocs termDocs = indexReader.termDocs(t);

			if (termDocs.next()) {

				Document doc = indexReader.document(termDocs.doc());
				result = (HashMap<String, Integer>) SerializationUtils.deserialize(doc.getBinaryValue(valueFieldName));
				logger.debug(result);
//				result = fromByte(doc.getBinaryValue(valueFieldName));

				return result;
			}
		} catch (IOException e) {
			logger.error(e);
		}
		return null;
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				HashMap result = search(s[0]);
				long end = System.nanoTime();

				if (result != null && result.size() > 0) {
					logger.info(s[0] + " found: " + result);

					if (clSchema != null) {
						Term term = new Term(clID, s[0]);
						TermDocs td = clSchema.termDocs(term);
						if (td.next()) {
							logger.info(clSchema.document(td.doc()));
						}
					}
				}
				else {
					logger.info(s[0] + " not found in " + tf.format(end - begin) + " ns");
				}
			}
		}
	}

	public static String[] fromByte(byte[] byteArray) throws IOException {
		ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);
		DataInputStream dataStream = new DataInputStream(byteStream);

		int size = dataStream.readInt();
		String[] entryArray = new String[size];
		for (int j = 0; j < size; j++) {
			entryArray[j] = dataStream.readUTF();
		}

		return entryArray;
	}

}
