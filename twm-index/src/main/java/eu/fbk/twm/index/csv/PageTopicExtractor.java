package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.*;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/2/13
 * Time: 6:53 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * This class extracts for each page the top categories
 * It uses the in memory mapping between pages and categories and categories and super categories (read from file)
 * The depth threshold must be fixed in advance (7?)
 * The mapping category - top-category is loaded from a file in  /data/resources/category-mapping/[lang].properties
 * To verify if the top categories must be mapped to an integer
 */
public class PageTopicExtractor extends CSVExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageTopicExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(PageTopicExtractor.class.getName());

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	public static final int DEFAULT_DEPTH = 10;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	Map<String, Set<String>> pageCategoryMap;

	Map<String, Set<String>> categorySuperCategoryMap;

	ExtendedProperties properties;

	PrintWriter pageTopCategoryWriter;

	private String categoryMappingDir;

	int maxDepth;

	public PageTopicExtractor(String categoryMappingDir) {
		this.categoryMappingDir = categoryMappingDir;

	}

	public PageTopicExtractor(int numThreads, String categoryMappingDir) {
		super(numThreads);
		this.categoryMappingDir = categoryMappingDir;
	}

	public PageTopicExtractor(int numThreads, int maxDepth, String categoryMappingDir) {
		super(numThreads);
		this.maxDepth = maxDepth;
		this.categoryMappingDir = categoryMappingDir;
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				WeightedSet weightedSet = search(s[0].replace(' ', CharacterTable.LOW_LINE));
				long end = System.nanoTime();
				Map<Double, List<String>> sortedMap = weightedSet.toSortedMap();
				logger.debug(sortedMap + "\t" + tf.format(end - begin));

			}
		}
	}

	//todo: this must be done when the mapping is created
	public static String normalizePageName(String s) {
		if (s.length() == 0) {
			return s;
		}

		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		}

		return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
	}

	private String tabulator(int l) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < l; i++) {
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
		}
		return sb.toString();
	}

	void search(Set<String> categories, WeightedSet weightedSet, int depth, Set<String> visitedSet) {
		//logger.debug("searching: " + categories + "\t" + depth + "...");

		//if (depth > 5 && WeightedSet.size() > 0) {
		if (categories == null || depth > maxDepth) {
			//logger.debug("{{{" + depth + "}}}");
			//logger.debug("stop1 " + depth);
			return;
		}

		Iterator<String> it = categories.iterator();
		for (int i = 0; it.hasNext(); i++) {
			//String label = topMap.get(categories[i]);
			String normalizedCategory = normalizePageName(it.next());
			String label = properties.getProperty(normalizedCategory);
			//logger.debug(i + "\t" + normalizedCategory + "\t" + label);
			//String label = categories[i];

			if (label != null) {
				//logger.debug("<<<" + normalizedCategory + "\t" + label + "\t" + depth + ">>>");
				//WeightedSet.add(categories[i]);

				if (label.length() == 0) {
					//logger.warn("stop category " + normalizedCategory);
					System.out.println(tabulator(depth) + "<" + normalizedCategory + ", STOP, " + depth + ">");
				}
				else {
					weightedSet.add(label, (double) 1 / depth);
					System.out.println(tabulator(depth) + "<" + normalizedCategory + ", '" + label + "', " + depth + ">");
					//weightedSet.add(label, 1.0);
				}

			}
			else {
				if (!visitedSet.contains(normalizedCategory)) {
					visitedSet.add(normalizedCategory);
					try {
						Set<String> superCategories = categorySuperCategoryMap.get(normalizedCategory);
						if (superCategories != null) {
							//logger.debug(i + "\t" + depth + "\t" + normalizedCategory + ": " + superCategories);
							System.out.println(tabulator(depth) + "{" + normalizedCategory + ", " + depth + ", " + superCategories.size() + ", " + superCategories + "}");
							search(superCategories, weightedSet, depth + 1, visitedSet);
						}
					} catch (Exception e) {
						logger.error(e);
					}
				}
			}

		}
		//logger.debug("stop2 " + depth);
		return;
	}

	WeightedSet search(String page) {
		WeightedSet weightedSet = new WeightedSet();
		Set<String> categories = pageCategoryMap.get(page);
		logger.debug(page + ": " + categories);
		//long begin = System.nanoTime();
		//logger.debug(page);
		Set<String> visitedSet = new HashSet<String>();
		search(categories, weightedSet, 1, visitedSet);
		//long end = System.nanoTime();
		//long time = end - begin;
		//logger.info(WeightedSet.size() + " topics found in " + nf.format(time) + " ns");
		return weightedSet;
	}

	Map<String, Set<String>> readMap(String name) throws IOException {
		logger.info("reading " + name + "...");
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));

		String line = null;
		int count = 0, key = 0, value = 1, part = 0, tot = 0;
		String oldKey = "";
		Set<String> set = new HashSet<String>();
		String[] t;
		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				if (!t[key].equals(oldKey)) {
					result.put(oldKey, set);
					count++;
					set = new HashSet<String>();
					part = 0;
				}
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last line
		result.put(oldKey, set);
		lnr.close();
		return result;
	}

	@Override
	public void processLine(String line) {
		//logger.debug("begin " +line);
		String[] t = tabPattern.split(line);
		WeightedSet weightedSet = search(t[0]);
		//Map<Integer, List<String>> sortedMap = WeightedSet.toSortedMap();
		//logger.debug(t[0] + "\t" + sortedMap);
		StringBuilder sb = new StringBuilder();
		//sb.append(t[0]);
		//sb.append(CharacterTable.HORIZONTAL_TABULATION);
		//sb.append(sortedMap.toString());
		/*Iterator<Integer> it = sortedMap.keySet().iterator();
		for (int i = 0; it.hasNext() ; i++) {
			Integer freq = it.next();
			List<String> senseList = sortedMap.get(freq);
			for (int k = 0; k < freq; k++) {
				for (int j = 0; j <senseList.size() ; j++) {
					sb.append(t[0]);
					sb.append(CharacterTable.HORIZONTAL_TABULATION);
					sb.append(senseList.get(j));
					sb.append(CharacterTable.CARRIADGE_RETURN);
				}
			}

		} */
		Iterator<String> it = weightedSet.iterator();
		for (int i = 0; it.hasNext(); i++) {
			String key = it.next();
			double value = weightedSet.get(key);
			//logger.debug(t[0] + "\t" + key + "\t" + value);
			for (int j = 0; j < value; j++) {
				sb.append(t[0]);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(key);
				sb.append("\n");

			}
			//sb.append(t[0]);
			//sb.append(CharacterTable.HORIZONTAL_TABULATION);
			//sb.append(key);
			//sb.append(CharacterTable.HORIZONTAL_TABULATION);
			//sb.append(value);
			//sb.append("\n");
		}
		//logger.debug(t[0] + ">>\n" + sb.toString());

		synchronized (this) {
			pageTopCategoryWriter.print(sb.toString());
			//pageTopCategoryWriter.println(t[0] + "\t" + WeightedSet);
		}
		//logger.debug("end " +line);
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			if (!categoryMappingDir.endsWith(File.separator)) {
				categoryMappingDir += File.separator;
			}
			File file = new File(categoryMappingDir + extractorParameters.getLang() + ".properties");
			logger.info("reading category mappings from " + file + "...");

//			properties.load(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
			properties = new ExtendedProperties(file.getAbsolutePath());

			logger.debug(properties.size() + " mappings read");

			pageCategoryMap = readMap(extractorParameters.getWikipediaPageCategoryFileName());
			categorySuperCategoryMap = readMap(extractorParameters.getWikipediaCategorySuperCategoryFileName());

			//todo: uncomment here to extract
			//pageTopCategoryWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaPageTopCategoryFileName()), "UTF-8")));
			//read(extractorParameters.getWikipediaTitleIdFileName());
		} catch (IOException e) {
			logger.error(e);
		}

	}

	@Override
	public void end() {
		pageTopCategoryWriter.close();
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option categoryMappingDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("directory from which to read the category mapping files").isRequired().withLongOpt("category-mapping-dir").create("c");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			Option depthOpt = OptionBuilder.withArgName("int").hasArg().withDescription("recursion depth (default is " + PageTopicExtractor.DEFAULT_DEPTH + ")").withLongOpt("depth").create();
			Option interactiveModeOpt = OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create();

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(interactiveModeOpt);
			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(categoryMappingDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(depthOpt);
			options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			int depth = DEFAULT_DEPTH;
			if (line.hasOption("depth")) {
				depth = Integer.parseInt(line.getOptionValue("depth"));
			}

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);
			PageTopicExtractor pageTopicExtractor = new PageTopicExtractor(numThreads, depth, line.getOptionValue("category-mapping-dir"));
			pageTopicExtractor.setNotificationPoint(notificationPoint);
			pageTopicExtractor.start(extractorParameters);
			if (line.hasOption("interactive-mode")) {

				try {
					pageTopicExtractor.interactive();
				} catch (Exception e) {
					logger.error(e);
				}
			}

		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.PageTopicExtractor", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
