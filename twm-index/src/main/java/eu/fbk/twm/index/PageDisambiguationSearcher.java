/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractSearcher;
import eu.fbk.twm.utils.StringTable;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/24/13
 * Time: 11:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageDisambiguationSearcher extends AbstractSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageAbstractSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(PageDisambiguationSearcher.class.getName());

	public static final int DEFAULT_MIN_FREQ = 1000;

	public static final boolean DEFAULT_THREAD_SAFE = false;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected static DecimalFormat vf = new DecimalFormat("###,###,###,##0.000");

	private static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected boolean threadSafe;

	private Map<String, String> cache;

	private Term keyTerm;

	public PageDisambiguationSearcher(String indexName) throws IOException {
		this(indexName, false);
	}

	public PageDisambiguationSearcher(String indexName, boolean threadSafe) throws IOException {
		super(indexName);
		this.threadSafe = threadSafe;
		keyTerm = new Term("page", "");
		logger.debug(keyTerm);
		logger.trace(toString(10));
	}

	public String search(String page) {
		String pageAbstract = null;

		try {
			TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(page));

			if (termDocs.next()) {
				Document doc = indexReader.document(termDocs.doc());
				pageAbstract = new String(doc.getBinaryValue("xml"));
			}

		} catch (Exception e) {
			return new String();
		}
		return pageAbstract;
	}

	public void interactive() throws Exception {
		InputStreamReader indexReader = null;
		BufferedReader myInput = null;
		long begin = 0, end = 0;
		while (true) {
			System.out.println("\nPlease write a query and type <return> to continue (CTRL C to exit):");

			indexReader = new InputStreamReader(System.in);
			myInput = new BufferedReader(indexReader);

			String query = myInput.readLine().toString();
			String[] s = query.split("\t");
			if (s.length == 1) {
				begin = System.nanoTime();
				String pageAbstract = search(query);
				end = System.nanoTime();
				logger.info(query + "\n" + pageAbstract + "\n" + tf.format(end - begin) + " ns");

			}

		}
	}

	public static void main(String args[]) throws Exception {

	}
}
