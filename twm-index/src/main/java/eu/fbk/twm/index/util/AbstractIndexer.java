/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import eu.fbk.twm.utils.GenericFileUtils;
import eu.fbk.twm.utils.Stopwatch;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 4:32 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractIndexer extends Index {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AbstractIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(AbstractIndexer.class.getName());

	protected IndexWriter indexWriter;

	public AbstractIndexer(String indexName) throws IOException {
		this(indexName, false);
	}

	public AbstractIndexer(String indexName, boolean clean) throws IOException {
		super(indexName);
		if (clean) {
			GenericFileUtils.checkWriteableFolder(indexName, true);
		}
		indexWriter = new IndexWriter(FSDirectory.open(new File(indexName)), new WhitespaceAnalyzer(), IndexWriter.MaxFieldLength.LIMITED);
	}

	public void close() throws IOException {
		logger.info("Optimizing index and closing...");
        Stopwatch stopwatch = Stopwatch.start();
        indexWriter.optimize();
		indexWriter.close();
        logger.info("Optimized in "+ stopwatch.click()+" ms");
	}


}
