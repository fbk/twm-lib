/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.SingleValueIndexer;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/24/13
 * Time: 11:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class CrossLanguageIndexer extends SingleValueIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CrossLanguageIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(CrossLanguageIndexer.class.getName());

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	private static Pattern semicolonPattern = Pattern.compile(":");

	public static final int PAGE_COLUMN_INDEX = 0;

	public static final String PAGE_FIELD_NAME = "page";

	public static final String ENTRY_FIELD_NAME = "entry";

	public CrossLanguageIndexer(String indexName) throws IOException {
		super(indexName);
	}

	@Override
	public void index(String fileName) throws IOException {
		index(new File(fileName));
	}

	@Override
	public void index(File file) throws IOException {
		index(file, PAGE_COLUMN_INDEX);
	}

	@Override
	protected void add(String[] t) throws IOException {
		Document doc = new Document();
		doc.add(new Field(PAGE_FIELD_NAME, t[PAGE_COLUMN_INDEX], Field.Store.YES, Field.Index.NOT_ANALYZED));
		doc.add(new Field(ENTRY_FIELD_NAME, toByte(t), Field.Store.YES));
		indexWriter.addDocument(doc);
	}

	private byte[] toByte(String[] value) throws IOException {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(1024);
		DataOutputStream dataStream = new DataOutputStream(byteStream);
		dataStream.writeInt(value.length - 1);
		//List<String> list = null;
		String[] s;
		int j=0;
		for (int i = 1; i < value.length; i++) {
			j = value[i].indexOf(CharacterTable.COLON);
			if (j != -1) {
				dataStream.writeUTF(value[i].substring(0, j));
				dataStream.writeUTF(value[i].substring(j+1, value[i].length()));
				//logger.trace(j + "\t" +value[i].substring(0, j)+ "\t"+ value[i].substring(j+1, value[i].length()));
			}
			else {
				dataStream.writeUTF(StringTable.EMPTY_STRING);
				dataStream.writeUTF(StringTable.EMPTY_STRING);
			}

		}
		return byteStream.toByteArray();
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}


		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("create an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option inputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the key/value pairs to index from the specified file").withLongOpt("file").create("f");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(inputFileOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			CrossLanguageIndexer crossLanguageIndexer = new CrossLanguageIndexer(line.getOptionValue("index"));
			crossLanguageIndexer.index(line.getOptionValue("file"));
			crossLanguageIndexer.close();
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.CrossLanguageIndexer", "\n", options, "\n", true);
		}
	}
}
