/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractIndexer;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 4:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageIncomingOutgoingWeightedIndexer extends AbstractIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageIncomingOutgoingWeightedIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(PageIncomingOutgoingWeightedIndexer.class.getName());

	public static final int LINK_ID_COLUMN_INDEX = 3;

	public static final int LINK_COLUMN_INDEX = 2;

	public static final int PAGE_COLUMN_INDEX = 1;

	public static final int PAGE_ID_COLUMN_INDEX = 0;

	public static final int COLUMN_NUMBER = 4;

	public static final String PAGE_FIELD_NAME = "page";

	private static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	public static final String VECTOR_FIELD_NAME = "vec";

	protected static Pattern tabPattern = Pattern.compile("\t");

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	Map<String, Double> pageFreqMap;

	public PageIncomingOutgoingWeightedIndexer(String indexName, String pageFreqIndexName) throws IOException {
		super(indexName);
		pageFreqMap = loadPageFreq(new File(pageFreqIndexName));
	}

	public Map<String, Double> loadPageFreq(File f) throws IOException {
		logger.info("loading cache from " + f + "...");
		long begin = System.nanoTime();

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;
		int i = 0;
		String[] t;
		Map<String, Double> map = new HashMap<String, Double>();
		int freq;
		int maxFreq = Integer.MAX_VALUE;
		double idf = 0;
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				maxFreq = Integer.parseInt(t[0]);
				// the first key is not saved as it's zero
				i++;
			}
		}
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				freq = Integer.parseInt(t[0]);
				map.put(t[1], Math.log10((double) maxFreq / freq));
				i++;
			}
			if ((i % notificationPoint) == 0) {
				System.out.print(CharacterTable.FULL_STOP);
			}

		}
		System.out.print(CharacterTable.LINE_FEED);
		lnr.close();
		long end = System.nanoTime();
		logger.info(df.format(map.size()) + " (" + df.format(i) + ") pages cached in " + tf.format(end - begin) + " ns");
		return map;
	}

	public void index(String fileName, boolean compress) throws IOException {
		index(new File(fileName), compress);
	}

	public void index(File file, boolean compress) throws IOException {
		logger.info("indexing " + file + "...");

		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		if (compress) {
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file)), "UTF-8"));
		}
		else {
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		}
		Double value;
		String line = null;
		int count = 0, part = 0, tot = 0;
		String oldKey = "";
		Map<Integer, Double> map = new TreeMap<Integer, Double>();
		String[] t;
		logger.info("tot\tcount\ttime\tdate");
		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == COLUMN_NUMBER) {
				value = pageFreqMap.get(t[LINK_COLUMN_INDEX]);
				if (value != null) {
					map.put(new Integer(t[LINK_ID_COLUMN_INDEX]), value);
				}
				oldKey = t[PAGE_COLUMN_INDEX];
				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == COLUMN_NUMBER) {
				if (!t[PAGE_COLUMN_INDEX].equals(oldKey)) {
					add(oldKey, map);
					count++;
					map = new TreeMap<Integer, Double>();
					part = 0;
				}
				value = pageFreqMap.get(t[LINK_COLUMN_INDEX]);
				if (value != null) {
					map.put(new Integer(t[LINK_ID_COLUMN_INDEX]), value);
				}

				oldKey = t[PAGE_COLUMN_INDEX];
				part++;
			}
			tot++;

			//if (tot > 1000000) {
			//	break;
			//}

			if ((tot % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + " lines read, key indexed: " + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last document
		add(oldKey, map);
		lnr.close();
	} // end read

	public void add(String key, Map<Integer, Double> map) {
		//logger.debug(key + "\t" + map);
		Document doc = new Document();
		try {
			doc.add(new Field(PAGE_FIELD_NAME, key, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field(VECTOR_FIELD_NAME, toByte(map), Field.Store.YES));
			indexWriter.addDocument(doc);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	protected byte[] toByte(Map<Integer, Double> map) throws IOException {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(1024);
		DataOutputStream dataStream = new DataOutputStream(byteStream);
		// number of distinct items
		dataStream.writeInt(map.size());
		Iterator<Integer> it = map.keySet().iterator();
		Integer index;
		Double value;
		for (int i = 0; it.hasNext(); i++) {
			index = it.next();
			value = map.get(index);
			//logger.debug(i + "\t" + index + "\t" + value);
			dataStream.writeInt(index);
			dataStream.writeDouble(value);

		}
		return byteStream.toByteArray();
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("create an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option inputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the key/value pairs to index from the specified file").withLongOpt("file").create("f");
			Option compressOpt = OptionBuilder.withDescription("the input file is compressed (default is " + WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT + ")").withLongOpt("compress").create("c");
			Option pageFreqFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("page frequency file").withLongOpt("page-freq").create("p");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(inputFileOpt);
			options.addOption(compressOpt);
			options.addOption(pageFreqFileOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			boolean compress = WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT;
			if (line.hasOption("compress")) {
				compress = true;
			}

			PageIncomingOutgoingWeightedIndexer pageIncomingOutgoingIndexer = new PageIncomingOutgoingWeightedIndexer(line.getOptionValue("index"), line.getOptionValue("page-freq"));
			pageIncomingOutgoingIndexer.index(line.getOptionValue("file"), compress);
			pageIncomingOutgoingIndexer.close();
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PageIncomingOutgoingWeightedIndexer", "\n", options, "\n", true);
		}
	}
}
