package eu.fbk.twm.index.atomicindexers;

import eu.fbk.twm.index.util.UniqueIDValueIndexer;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 2/7/13
 * Time: 8:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class IDValueIndexer extends UniqueIDValueIndexer {

	public IDValueIndexer(String indexName) throws IOException {
		this(indexName, "id", "value");
	}

	protected IDValueIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		super(indexName, keyFieldName, valueFieldName);
	}

	@Override
	public void index(File file) throws IOException {
		index(file, 1);
	}
}
