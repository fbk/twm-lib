/*
 * Copyright (2014) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.mt;

import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.StringTable;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/3/14
 * Time: 9:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class Cleaner {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Cleaner</code>.
	 */
	static Logger logger = Logger.getLogger(Cleaner.class.getName());

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	private static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public Cleaner(String name) throws IOException {
		String text = read(name);
		String[] regex = {"&([^;]+);"};
		String[] replacement = {"\1"};
		for (int i = 0; i < regex.length; i++) {
			text = text.replaceAll(regex[i],replacement[i]);
		}
		String cleanName = name.substring(0, name.lastIndexOf('.')) + ".clean.txt";
		logger.debug(text);
		//write(cleanName, text);
	}

	public String read(String name) throws IOException {
		logger.info("reading " + name + "...");
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = lnr.readLine()) != null) {
			sb.append(line);
			sb.append('\n');
		}
		return sb.toString();
	}

	public void write(String name, String text) throws IOException {
		logger.info("writing " + name + "...");

		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(name), "UTF-8")));
		pw.print(text);
		pw.close();
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			
			Option modelDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("directory from which to read the text files").isRequired().withLongOpt("dir").create("d");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(modelDirOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			File f = new File(line.getOptionValue("dir"));
			if (f.isFile()) {
				Cleaner cleaner = new Cleaner(line.getOptionValue("dir"));

			}
			else {
				File[] files = f.listFiles(new FilenameFilter() {
					public boolean accept(File directory, String fileName) {
						return fileName.endsWith(".txt");
					}
				});
				for (int i = 0; i < files.length; i++) {
					try {
						//File outputFile = new File(f.getAbsolutePath() + File.separator + files[i].getName() + ".terms");
						Cleaner cleaner = new Cleaner(files[i].getAbsolutePath());
					} catch (Exception ex) {
						logger.error("Error at " + files[i]);
						logger.error(ex);
					}
				}
			}
		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.experiments.mt.Cleaner", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
