package eu.fbk.twm.index.csv;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 8/27/13
 * Time: 5:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class UnixSortWrapper {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>UnixSortWrapper</code>.
	 */
	static Logger logger = Logger.getLogger(UnixSortWrapper.class.getName());

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public static int sort(String in, String out, int key, int numThreads) throws Exception {
		return sort(in, out, key, "-n", numThreads);
	}

	public static int sort(String in, String out, int key, String param, int numThreads) throws Exception {
		logger.debug("sorting " + in + " in " + out);
		long begin = System.currentTimeMillis();
		String dir = new File(out).getParent();
		logger.debug("new tmo dir" + dir);

		if (!param.startsWith("-")) {
			param = "-" + param;
		}
		String[] cmd = null;
		//String[] cmd = new String[]{"/bin/sh", "-c", "/usr/bin/sort --parallel=12 " + param + " " + in + " -k " + key + " -o " + out};

		//todo: this has been introduce because some computers do not have parallel sort installed: find a best way to distinguish
		if (numThreads > 4) {
			// it should be numThreads > 1
			cmd = new String[]{"/bin/sh", "-c", "`which sort` --temporary-directory=" + dir + " --parallel=" + numThreads + " " + param + " " + in + " -k" + key + " -o " + out + " -S 1024M --batch-size=128"};
		}
		else {
			cmd = new String[]{"/bin/sh", "-c", "`which sort` --temporary-directory=" + dir + " " + param + " " + in + " -k " + key + " -o " + out + " -S 1024M --batch-size=128"};
		}

		logger.debug(Arrays.toString(cmd));
		Process p = Runtime.getRuntime().exec(cmd);
		logger.info("sorting...");
		p.waitFor();
		int exitValue = p.exitValue();
		logger.info("done " + exitValue);
		if (exitValue != 0) {
			InputStream is = p.getErrorStream();
			LineNumberReader lnr = new LineNumberReader(new InputStreamReader(is));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = lnr.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
			logger.error(sb.toString());
		}

		long end = System.currentTimeMillis();
		logger.info(in + " sorted in " + (end - begin) + " ms");

		return exitValue;
	}
	// k origin 1
	public static int sort(String in, String out, String columns, int key, int numThreads) throws Exception {
		return sort(in, out, columns, key, "-n", numThreads);
	}

	public static int sort(String in, String out, String columns, int key, String param, int numThreads) throws Exception {
		logger.debug("sorting " + in + " in " + out);
		long begin = System.currentTimeMillis();
		String dir = new File(out).getParent();
		logger.debug("new tmo dir" + dir);

		if (!param.startsWith("-")) {
			param = "-" + param;
		}
		//String[] cmd = new String[]{"/bin/sh", "-c", "/usr/bin/cut -f" + columns + " " + in + "| /usr/bin/sort --parallel=12 " + param + " -k " + key + " -o" + out};
		String[] cmd=null;
		//todo: this has been introduce because some computers do not have parallel sort installed: find a best way to distinguish
		if (numThreads>10){
			// it should be numThreads > 1
			cmd = new String[]{"/bin/sh", "-c", "`which cut` -f" + columns + " " + in + "| `which sort` --temporary-directory=" + dir + " --parallel=" + numThreads + " " + param + " -k " + key + " -o " + out};
		} else {
			cmd = new String[]{"/bin/sh", "-c", "`which cut` -f" + columns + " " + in + "| `which sort` --temporary-directory=" + dir + " " + param + " -k " + key + " -o " + out};
		}

		logger.debug(Arrays.toString(cmd));
		Process p = Runtime.getRuntime().exec(cmd);
		logger.info("sorting...");
		p.waitFor();
		int exitValue = p.exitValue();
		logger.info("done " + exitValue);
		if (exitValue != 0) {
			InputStream is = p.getErrorStream();
			LineNumberReader lnr = new LineNumberReader(new InputStreamReader(is));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = lnr.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
			logger.error(sb.toString());
		}

		long end = System.currentTimeMillis();
		logger.info(in + " sorted in " + (end - begin) + " ms");

		return exitValue;
	}

}
