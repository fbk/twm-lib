/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.SetSearcher;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.Defaults;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/24/13
 * Time: 11:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageFileSearcher extends SetSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageFileSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(PageFileSearcher.class.getName());

	public static final String WIKIPEDIA_URL_PREFIX = "http://upload.wikimedia.org/wikipedia/";

	public static final String THUMB_LABEL = "thumb";

	public static final String PX_LABEL = "px-";

	public static final String LOWERCASE_SVG_EXTENSION = ".svg";

	public static final String UPPERCASE_SVG_EXTENSION = ".SVG";

	public static final String PNG_EXTENSION = ".png";

	public static final int DEFAULT_IMAGE_SIZE = 100;

	public PageFileSearcher(String indexName) throws IOException {
		super(indexName, PageFileIndexer.PAGE_FIELD_NAME, PageFileIndexer.SOURCE_FIELD_NAME);
		logger.trace(toString(10));
	}

	public Entry[] images(String key) {
		return images(key, DEFAULT_IMAGE_SIZE);
	}

	public Entry[] images(String key, int size) {
		String[] s = search(key);
		Entry[] e = new Entry[s.length];
		int first, last;

		for (int i = 0; i < s.length; i++) {
			e[i] = new Entry(s[i], size);
		}
		return e;
	}

	@Deprecated
	public String[] files(String key) {
		String[] s = search(key);
		String[] t = new String[s.length];
		for (int i = 0; i < s.length; i++) {
			t[i] = WIKIPEDIA_URL_PREFIX + s[i];
		}
		return t;
	}

	@Deprecated
	public String[] thumbs(String key, int dim) {
		String[] s = search(key);
		String[] t = new String[s.length];
		int first, last;
		for (int i = 0; i < s.length; i++) {
			first = s[i].indexOf(File.separator);
			last = s[i].lastIndexOf(File.separator);
			t[i] = WIKIPEDIA_URL_PREFIX + s[i].substring(0, first) + File.separator + THUMB_LABEL + s[i].substring(first, s[i].length()) + File.separator + dim + PX_LABEL + s[i].substring(last + 1, s[i].length());
		}
		return t;
	}


	public class Entry {
		String image;

		String thumb;

		public Entry(String base, int size) {
			int first = base.indexOf(File.separator);
			int last = base.lastIndexOf(File.separator);
			image = WIKIPEDIA_URL_PREFIX + base;
			thumb = WIKIPEDIA_URL_PREFIX + base.substring(0, first) + File.separator + THUMB_LABEL + base.substring(first, base.length()) + File.separator + size + PX_LABEL + base.substring(last + 1, base.length());
			if (image.endsWith(LOWERCASE_SVG_EXTENSION) || image.endsWith(UPPERCASE_SVG_EXTENSION)) {
				thumb += PNG_EXTENSION;
			}
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public String getThumb() {
			return thumb;
		}

		public void setThumb(String thumb) {
			this.thumb = thumb;
		}

		@Override
		public String toString() {
			return "Entry{" +
					"image='" + image + '\'' +
					", thumb='" + thumb + '\'' +
					'}';
		}
	}

	@Override
	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				Entry[] entry = images(s[0]);
				long end = System.nanoTime();

				if (entry != null) {
					for (int i = 0; i < entry.length; i++) {
						logger.info(i + "\t" + entry[i]);
					}
					logger.info(s[0] + " found in " + tf.format(end - begin) + " ns");

				}
				else {
					logger.info(s[0] + " not found in " + tf.format(end - begin) + " ns");
				}
			}
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode").withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option searchOpt = OptionBuilder.withArgName("search").hasArg().withDescription("search for the specified key").withLongOpt("search").create("s");
			//Option freqFileOpt = OptionBuilder.withArgName("key-freq").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			//Option minimumKeyFreqOpt = OptionBuilder.withArgName("minimum-freq").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			//options.addOption(freqFileOpt);
			//options.addOption(keyFieldNameOpt);
			//options.addOption(valueFieldNameOpt);
			//options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			PageFileSearcher pageFileSearcher = new PageFileSearcher(line.getOptionValue("index"));
			pageFileSearcher.setNotificationPoint(notificationPoint);
			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				String[] result = pageFileSearcher.search(line.getOptionValue("search"));
				logger.info(result);
			}
			if (line.hasOption("interactive-mode")) {
				pageFileSearcher.interactive();
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PageFileSearcher", "\n", options, "\n", true);
		}
	}
}
