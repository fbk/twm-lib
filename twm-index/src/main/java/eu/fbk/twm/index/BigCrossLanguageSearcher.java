/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractSearcher;
import eu.fbk.twm.utils.StringTable;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class BigCrossLanguageSearcher extends AbstractSearcher {
	static Logger logger = Logger.getLogger(BigCrossLanguageSearcher.class.getName());

	public static final boolean DEFAULT_THREAD_SAFE = false;
	protected boolean threadSafe;
	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	public BigCrossLanguageSearcher(String indexName) throws IOException {
		this(indexName, DEFAULT_THREAD_SAFE);
	}

	public BigCrossLanguageSearcher(String indexName, boolean threadSafe) throws IOException {
		super(indexName);
		this.threadSafe = threadSafe;
	}

	public static Map<String, String> documentToMap(Document doc) {
		HashMap<String, String> result = new HashMap<String, String>();
		for (Object f : doc.getFields()) {
			Field field = (Field) f;
			result.put(field.name(), field.stringValue());
		}
		return result;
	}

	public Map<String, String> search(String lang, String key) {
		Map<String, String> result = null;

		try {
			TermDocs termDocs = indexReader.termDocs(new Term(lang, key));

			if (termDocs.next()) {
				Document doc = indexReader.document(termDocs.doc());
				result = documentToMap(doc);

				return result;
			}
		} catch (IOException e) {
			logger.error(e);
		}
		return new HashMap<String, String>();
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key/value and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 2) {
				System.out.println(search(s[0], s[1]));
			}
		}
	}


	public static void main(String[] args) {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			BigCrossLanguageSearcher crossLanguageSearcher = new BigCrossLanguageSearcher(line.getOptionValue("index"));
			crossLanguageSearcher.interactive();
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.CrossLanguageSearcher", "\n", options, "\n", true);
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
