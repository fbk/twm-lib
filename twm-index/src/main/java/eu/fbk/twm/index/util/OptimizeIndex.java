package eu.fbk.twm.index.util;

import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.FSDirectory;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 2/19/13
 * Time: 9:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class OptimizeIndex {
	public static void main(String[] args) {
		String index = args[0];
		try {
			IndexWriter i = new IndexWriter(FSDirectory.open(new File(index)), new WhitespaceAnalyzer(), IndexWriter.MaxFieldLength.LIMITED);
			i.optimize();
			i.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
