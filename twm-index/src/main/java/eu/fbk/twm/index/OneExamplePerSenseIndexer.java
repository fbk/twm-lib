/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractIndexer;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/5/13
 * Time: 4:03 PM
 * To change this template use File | Settings | File Templates.
 */
//todo: see if it can be saved sorted
public class OneExamplePerSenseIndexer extends AbstractIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OneExamplePerSenseIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(OneExamplePerSenseIndexer.class.getName());
	public static final int PAGE_COLUMN_INDEX = 1;

	public static final int FREQ_COLUMN_INDEX = 2;

	public static final int LS_COLUMN_INDEX = 3;

	public static final int BOW_COLUMN_INDEX = 4;

	protected static Pattern tabPattern = Pattern.compile("\t");

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public static final String PAGE_FIELD_NAME = "page";

	public static final String ENTRY_FIELD_NAME = "entry";

	public OneExamplePerSenseIndexer(String indexName) throws IOException {
		super(indexName);
	}

	public void index(String fileName, boolean compress) throws IOException {
		index(new File(fileName), 0, compress);
	}

	protected void index(String fileName, int key, boolean compress) throws IOException {
		index(new File(fileName), key, compress);
	}

	protected void index(File file, int key, boolean compress) throws IOException {
		logger.info("indexing " + file + "...");
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		if (compress) {
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file)), "UTF-8"));
		}
		else {
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		}

		String line = null;
		int count = 0, part = 0, tot = 0;
		String oldKey = "";
		List<String[]> list = new ArrayList<String[]>();
		String[] t;
		logger.info("tot\tcount\ttime\tdate");
		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length > key && t[key] != null) {
				list.add(t);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length > key && t[key] != null) {
				if (!t[key].equals(oldKey)) {
					add(oldKey, list);
					count++;
					list = new ArrayList<String[]>();
					part = 0;
				}
				list.add(t);
				oldKey = t[key];
				part++;
			}
			tot++;

			//if (tot > 1000000)
			//	break;

			if ((tot % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + " lines read, key indexed: " + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last document
		add(oldKey, list);
		lnr.close();
	} // end read

	public void add(String key, List<String[]> list) {
		Document doc = new Document();
		try {
			doc.add(new Field(PAGE_FIELD_NAME, key, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field(ENTRY_FIELD_NAME, toByte(list), Field.Store.YES));
			indexWriter.addDocument(doc);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	protected byte[] toByte(List<String[]> list) throws IOException {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(1024);
		DataOutputStream dataStream = new DataOutputStream(byteStream);
		// number of distinct example
		dataStream.writeInt(list.size());
		String[] t;
		for (int i = 0; i < list.size(); i++) {
			t = list.get(i);
			dataStream.writeUTF(t[PAGE_COLUMN_INDEX]);
			dataStream.writeDouble(Double.parseDouble(t[FREQ_COLUMN_INDEX]));
			if (t.length > LS_COLUMN_INDEX)
			{
				writeVector(dataStream, t[LS_COLUMN_INDEX]);
			}
			else
			{
				writeVector(dataStream, "");
			}
			if (t.length > BOW_COLUMN_INDEX)
			{
				writeVector(dataStream, t[BOW_COLUMN_INDEX]);
			}
			else
			{
				writeVector(dataStream, "");
			}

		}
		return byteStream.toByteArray();
	}

	private void writeVector(DataOutputStream dataStream, String v) throws IOException {
		StringTokenizer st = new StringTokenizer(v, " :");
		int m = st.countTokens() / 2;
		dataStream.writeInt(m);
		for (int j = 0; j < m; j++) {
			dataStream.writeInt(Integer.parseInt(st.nextToken()));
			dataStream.writeDouble(Double.valueOf(st.nextToken()));
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}


		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("create an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option inputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the key/value pairs to index from the specified file").withLongOpt("file").create("f");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			Option compressOpt = OptionBuilder.withArgName("compress").withDescription("the input file is compressed (default is " + WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT + ")").withLongOpt("compress").create("c");
			//Option freqFileOpt = OptionBuilder.withArgName("key-freq").withDescription("key frequency file").withLongOpt("key-freq").create("f");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(inputFileOpt);
			options.addOption(compressOpt);
			//options.addOption(valueFieldNameOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			boolean compress = WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT;
			if (line.hasOption("compress")) {
				compress = true;
			}

			OneExamplePerSenseIndexer oneExamplePerSenseIndexer = new OneExamplePerSenseIndexer(line.getOptionValue("index"));
			oneExamplePerSenseIndexer.index(line.getOptionValue("file"), compress);
			oneExamplePerSenseIndexer.close();
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.OneExamplePerSenseIndexer", "\n", options, "\n", true);
		}
	}
}
