/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.SetSearcher;
import eu.fbk.twm.utils.CommandLineWithLogger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.Defaults;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 1/24/13
 * Time: 11:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageClassSearcher extends SetSearcher {
	static Logger logger = Logger.getLogger(PageClassSearcher.class.getName());

	public PageClassSearcher(String indexName) throws IOException {
		super(indexName, PageClassIndexer.KEY_FIELD_NAME, PageClassIndexer.VALUE_FIELD_NAME);
		logger.trace(toString(10));
	}

	public static void main(String args[]) throws Exception {
		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		commandLineWithLogger.addOption(OptionBuilder.withDescription("Index folder").isRequired().hasArg().withArgName("folder").withLongOpt("index-folder").create("f"));

		commandLineWithLogger.addOption(OptionBuilder.withDescription("Notification point").hasArg().withArgName("num").withLongOpt("notification-point").create("n"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("Search for specified page").hasArg().withArgName("pagename").withLongOpt("search").create("s"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("Interactive mode").withLongOpt("interactive").create("i"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}

		String folder = commandLine.getOptionValue("index-folder");
		if (!folder.endsWith(File.separator)) {
			folder += File.separator;
		}

		int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
		if (commandLine.hasOption("notification-point")) {
			notificationPoint = Integer.parseInt(commandLine.getOptionValue("notification-point"));
		}

		PageClassSearcher pageClassSearcher = new PageClassSearcher(folder);
		pageClassSearcher.setNotificationPoint(notificationPoint);
		if (commandLine.hasOption("search")) {
			logger.debug("searching " + commandLine.getOptionValue("search") + "...");
			String[] result = pageClassSearcher.search(commandLine.getOptionValue("search"));
			logger.info(result);
		}
		if (commandLine.hasOption("interactive")) {
			pageClassSearcher.interactive();
		}
	}
}
