package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractSearcher;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.Stopwatch;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import java.io.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * A searcher for index created by SectionTextIndexer
 * Outputs all Sections in the given Page
 *
 * @see PageSectionTextIndexer
 */
public class PageSectionTextSearcher extends AbstractSearcher {
    private static Logger logger = Logger.getLogger(PageSectionTextSearcher.class.getName());

    public static final int DEFAULT_MAXIMUM_TEXT_LENGTH = Integer.MAX_VALUE;
    
    protected static final DecimalFormat decFormat = new DecimalFormat("###,###,###,###");
    private final Term keyTerm;
    
    private int maxTextLength;

    public PageSectionTextSearcher(String indexName, int maxTextLength) throws IOException {
        super(indexName);
        this.maxTextLength = maxTextLength;
        keyTerm = new Term(PageSectionTextIndexer.PAGE_TITLE_FIELD_NAME, "");
        logger.debug(keyTerm);
        if (logger.isTraceEnabled()) {
            logger.trace(toString(10));
        }
    }
    
    public PageSectionTextSearcher(String indexName) throws IOException {
        this(indexName, DEFAULT_MAXIMUM_TEXT_LENGTH);
    }

    /**
     * Take a Page Title as an input and find all section titles
     * 
     * @param page Page Title
     * @return something at least
     */
    public HashMap<String, String> search(String page) {
        HashMap<String, String> result = new HashMap<>();
        try {
            //Trying to find selected page in our pre-built index
            TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(page));
            while (termDocs.next()) {
                Document doc = indexReader.document(termDocs.doc());
                String sectionTitle = new String(doc.getBinaryValue(PageSectionTextIndexer.SECTION_TITLE_FIELD_NAME));
                String sectionText = new String(doc.getBinaryValue(PageSectionTextIndexer.SECTION_TEXT_FIELD_NAME));
                result.put(sectionTitle, sectionText);
            }
        } catch (IOException e) {
            logger.error(e);
        }

        return result;
    }

    public String fromByte(byte[] byteArray) throws IOException {
        ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);
        DataInputStream dataStream = new DataInputStream(byteStream);

        int length = dataStream.readInt();
        StringBuilder sb = new StringBuilder();
        sb.append(dataStream.readUTF());
        for (int j = 1; j < length && j < maxTextLength; j++) {
            sb.append(CharacterTable.SPACE);
            sb.append(dataStream.readUTF());
        }

        return sb.toString();
    }

    public void interactive() throws Exception {
        interactive(DEFAULT_MAXIMUM_TEXT_LENGTH);
    }
    
    public void interactive(int maxTextLength) throws Exception {
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
        Stopwatch stopwatch = Stopwatch.start();
        
        while (true) {
            System.out.println("\nPlease write a query and type <return> to continue (CTRL C to exit):");

            String query = inputReader.readLine();

            stopwatch.click();
            HashMap<String,String> result = search(query);
            stopwatch.click();

            printResult(result, maxTextLength);
            logger.info(query + "\t" + decFormat.format(stopwatch.getLastLapTime()) + " ms");
        }
    }
    
    private static void printResult(HashMap<String, String> result, int maxTextLength) {
        if (result.size() == 0) {
            logger.info("Elements not found");
            return;
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append("Results found: ");
        sb.append(result.size());
        sb.append(CharacterTable.LINE_FEED);
        
        SortedSet<String> keys = new TreeSet<>(result.keySet());
        for (String key : keys) {
            String value = result.get(key);
            sb.append(key);
            sb.append("\t");
            sb.append(decFormat.format(value.length()));
            sb.append(" bytes \t");
            sb.append(value.substring(0, maxTextLength > value.length() ? value.length() : maxTextLength));
            sb.append(CharacterTable.LINE_FEED);
        }
        logger.info(sb.toString());
    }

    public static void main(String args[]) throws Exception {
        String logConfig = System.getProperty("log-config");
        if (logConfig == null) {
            logConfig = "configuration/log-config.txt";
        }

        PropertyConfigurator.configure(logConfig);
        
        Options options = new Options();
        options.addOption("l", "maximum-length", true, "Maximum length of the text (default is all)");
        options.addOption("s", "search", true, "Search for the specified key");
        options.addOption("t", "interactive-mode", false, "Enter in the interactive mode");

        options.addOption("h", "help", false, "Print this message");
        options.addOption("v", "version", false, "Output version information and exit");

        options.addOption(getOptionForInputIndex());
        options.addOption(getOptionForNotificationPoint());

        CommandLineParser parser = new PosixParser();
        try {
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("help") || line.hasOption("version")) {
                throw new ParseException("");
            }

            int maximumTextLength = DEFAULT_MAXIMUM_TEXT_LENGTH;
            if (line.hasOption("maximum-length")) {
                maximumTextLength = Integer.parseInt(line.getOptionValue("maximum-length"));
            }
            int notificationPoint = resolveNotificationPoint(line);

            PageSectionTextSearcher searcher = new PageSectionTextSearcher(line.getOptionValue("index"), maximumTextLength);
            searcher.setNotificationPoint(notificationPoint);
            if (line.hasOption("search")) {
                String query = line.getOptionValue("search");
                logger.debug("Searching " + query + "...");
                printResult(searcher.search(query), maximumTextLength);
                return;
            }
            if (line.hasOption("interactive-mode")) {
                searcher.interactive(maximumTextLength);
            }
        } catch (Exception e) {
            if (e.getMessage().length() > 0) {
                System.out.println("Parsing failed: " + e.getMessage() + "\n");
            }
            //new HelpFormatter().printHelp(400, "java -cp dist/thewikimachine.jar "+PageSectionTextSearcher.class.getName(), "\n", options, "\n", true);
					new HelpFormatter().printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PageSectionTextSearcher", "\n", options, "\n", true);
        }
    }
}
