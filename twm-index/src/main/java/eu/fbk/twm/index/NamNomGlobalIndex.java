/*
 * Copyright 2012 FBK (http://www.fbk.eu)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.CommandLineWithLogger;
import eu.fbk.twm.utils.FakeExtractorParameters;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.store.FSDirectory;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class produces the cross-language
 * schema where to add features.
 *
 * @author Alessio Palmero Aprosio
 * @version 1.0
 * @since 1.0
 */
public class NamNomGlobalIndex {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageCategoryIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(NamNomGlobalIndex.class.getName());
	static final Pattern langPattern = Pattern.compile("^\\w{2}$");

	public static void main(String args[]) throws Exception {

		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		commandLineWithLogger.addOption(OptionBuilder.withDescription("Big index").isRequired().hasArg().withArgName("folder").withLongOpt("big-index").create("i"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("Base folder").isRequired().hasArg().withArgName("folder").withLongOpt("base-folder").create("b"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("Output folder").hasArg().withArgName("file").withLongOpt("output").create("o"));

		commandLineWithLogger.addOption(OptionBuilder.withDescription("Interactive mode").withLongOpt("interactive").create("t"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("NAM-NOM global folder").hasArg().withArgName("folder").withLongOpt("namnom-folder").create("n"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());

			if (!commandLine.hasOption("interactive") && !commandLine.hasOption("output")) {
				System.err.println("Parsing failed: Output folder and interactive cannot be both missing\n");
				commandLineWithLogger.printHelp();
				throw new ParseException("");
			}
		} catch (Exception e) {
			System.exit(1);
		}

		String bigIndex = commandLine.getOptionValue("big-index");
		if (!bigIndex.endsWith(File.separator)) {
			bigIndex += File.separator;
		}
		String baseFolder = commandLine.getOptionValue("base-folder");
		if (!baseFolder.endsWith(File.separator)) {
			baseFolder += File.separator;
		}

		String outputFolder = null;
		if (commandLine.hasOption("output")) {
			outputFolder = commandLine.getOptionValue("output");
			if (!outputFolder.endsWith(File.separator)) {
				outputFolder += File.separator;
			}
		}

		String namnomFolder = null;
		if (commandLine.hasOption("namnom-folder")) {
			namnomFolder = commandLine.getOptionValue("namnom-folder");
			if (!namnomFolder.endsWith(File.separator)) {
				namnomFolder += File.separator;
			}
		}

		boolean interactive = commandLine.hasOption("interactive");

		File baseFolderFile = new File(baseFolder);
		ArrayList<String> langs = new ArrayList<>();

		File[] listOfLangs = baseFolderFile.listFiles();
		if (listOfLangs != null) {
			for (File f : listOfLangs) {
				if (!f.isDirectory()) {
					continue;
				}
				Matcher langMatcher = langPattern.matcher(f.getName());
				if (langMatcher.matches()) {
					langs.add(f.getName());
				}
			}
		}

		IndexReader clIndexReader = IndexReader.open(FSDirectory.open(new File(bigIndex)), true);

		// Interactive part
		if (interactive) {

			PageTypeSearcher searcherWikiID = null;
			if (namnomFolder != null) {
				searcherWikiID = new PageTypeSearcher(namnomFolder + "wikiID" + File.separator);
			}
//			HashMap<Integer, HashSet<String>> reverse = null;
//			if (binFile != null) {
//				logger.info("Loading file");
//				HashMap<String, HashSet<Integer>> data = (HashMap<String, HashSet<Integer>>) GenericFileUtils.loadObjectFromDisk(binFile);
//				reverse = new HashMap<Integer, HashSet<String>>();
//				for (String s : data.keySet()) {
//					for (Integer id : data.get(s)) {
//						if (reverse.get(id) == null) {
//							reverse.put(id, new HashSet<String>());
//						}
//						reverse.get(id).add(s);
//					}
//				}
//			}

			HashMap<String, PageTypeSearcher> searchers = new HashMap<>();

			for (String l : langs) {
				try {
					FakeExtractorParameters extractorParameters = new FakeExtractorParameters(l, baseFolder);

					PageTypeSearcher searcher = new PageTypeSearcher(extractorParameters.getWikipediaTypeIndexName());
					searchers.put(l, searcher);
				} catch (Exception e) {
					logger.error(e.getMessage());
//					langs.remove(l);
					searchers.remove(l);
				}
			}

			for (String l : searchers.keySet()) {
				if (searchers.get(l) == null) {
					langs.remove(l);
				}
			}

			IndexReader indexReader = IndexReader.open(FSDirectory.open(new File(bigIndex)), true);

			InputStreamReader reader = null;
			BufferedReader myInput = null;
			while (true) {
				System.out.println("\nPlease write a key and type <return> to continue (<enter> without keys to exit):");

				reader = new InputStreamReader(System.in);
				myInput = new BufferedReader(reader);
				String query = myInput.readLine().toString();
				query = query.trim();

				if (query.length() == 0) {
					indexReader.close();
					System.exit(0);
				}

				// Write here the code
				Document d = null;

				if (query.startsWith("?")) {
					query = query.substring(1).trim();
				}
				else {
					query = query.replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);

					TermDocs termDocs;

					// English
					termDocs = clIndexReader.termDocs(new Term("en", query));
					if (termDocs.next()) {
						query = indexReader.document(termDocs.doc()).get("wikiID");
					}
					else {
						// Italian
						termDocs = clIndexReader.termDocs(new Term("it", query));
						if (termDocs.next()) {
							query = indexReader.document(termDocs.doc()).get("wikiID");
						}
					}
				}

				TermDocs termDocs = indexReader.termDocs(new Term("wikiID", query));
				if (termDocs.next()) {
					d = indexReader.document(termDocs.doc());
				}

				for (String l : langs) {
					String page = d.get(l);
					if (page == null) {
						continue;
					}
					PageTypeSearcher.Entry pageType = searchers.get(l).search(page);
					if (pageType == null) {
						continue;
					}
					logger.info(String.format("Result: [%s] %s --> %s", l, page, pageType.getType()));
				}

				if (searcherWikiID != null) {
					logger.info(String.format("From index: %s", searcherWikiID.search(query).getType()));
				}

			}

		}

		// Saving part

		HashMap<String, HashSet<String>> cache = new HashMap<>();
		for (String l : langs) {
			logger.info(String.format("Loading %s", l));
			try {
				cache.put(l, new HashSet<String>());
				FakeExtractorParameters extractorParameters = new FakeExtractorParameters(l, baseFolder);

				PageTypeSearcher searcher = new PageTypeSearcher(extractorParameters.getWikipediaTypeIndexName());

				IndexReader reader = searcher.getIndexReader();

				for (int i = 0; i < reader.maxDoc(); i++) {
					Document d = reader.document(i);
					String page = d.get(PageTypeIndexer.PAGE_FIELD_NAME);
					cache.get(l).add(page);
				}

				logger.debug(String.format("Added %d pages", cache.get(l).size()));
				searcher.close();
			} catch (Exception e) {
				logger.error(e.getMessage());
//				langs.remove(l);
				cache.remove(l);
			}
		}

		for (String l : cache.keySet()) {
			if (cache.get(l) == null) {
				langs.remove(l);
			}
		}

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		String today = dateFormat.format(cal.getTime());
		outputFolder += today + File.separator;

		HashMap<String, PageTypeIndexer> indexers = new HashMap<>();
		indexers.put("wikiID", new PageTypeIndexer(outputFolder + "wikiID" + File.separator, true));
		for (String l : langs) {
			String folder = outputFolder + l + File.separator;
			indexers.put(l, new PageTypeIndexer(folder, true));
		}

		logger.info("Starting reading");
		for (int i = 0; i < clIndexReader.numDocs(); i++) {
			Document d = clIndexReader.document(i);
			String wikiID = d.get("wikiID");
			int nom = 0;
			int total = 0;

			for (String l : langs) {
				String page = d.get(l);
				if (page == null) {
					continue;
				}

				total++;
				if (cache.get(l).contains(page)) {
					nom++;
				}
			}

			double ratio = (double) nom / total;

			logger.trace(wikiID);
			logger.trace(String.format("%d / %d = %f", nom, total, ratio));

			if (ratio > .5) {
				indexers.get("wikiID").add(wikiID);
			}
			for (String l : langs) {
				String page = d.get(l);
				if (page == null) {
					continue;
				}
				if (ratio > .5) {
					indexers.get(l).add(page);
				}
			}

			if ((i + 1) % 10000 == 0) {
				System.out.print(".");
			}
			if ((i + 1) % 1000000 == 0) {
				System.out.format(" %d/%d\n", i + 1, clIndexReader.numDocs());
			}
		}

		System.out.println();

		logger.info("Closing indexes");
		indexers.get("wikiID").close();
		for (String l : langs) {
			indexers.get(l).close();
		}

	}

} // end PageCategoryIndexer
