package eu.fbk.twm.index;

import eu.fbk.twm.index.util.WeightedSetSearcher;
import eu.fbk.twm.utils.CommandLineWithLogger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 27/01/14
 * Time: 09:42
 * To change this template use File | Settings | File Templates.
 */
public class PageTopicSearcher extends WeightedSetSearcher {

	static Logger logger = Logger.getLogger(PageTopicSearcher.class.getName());

	public PageTopicSearcher(String indexName) throws IOException {
		super(indexName);
	}

	public static void main(String[] args) {
		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		commandLineWithLogger.addOption(OptionBuilder.withDescription("Index folder").isRequired().hasArgs().withArgName("folder").withLongOpt("index").create("i"));
		commandLineWithLogger.addOption(OptionBuilder.withArgName("key-freq").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f"));
		commandLineWithLogger.addOption(OptionBuilder.withArgName("minimum-freq").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}

		String indexDir = commandLine.getOptionValue("index");
		if (!indexDir.endsWith(File.separator)) {
			indexDir += File.separator;
		}

		try {
			PageTopicSearcher searcher = new PageTopicSearcher(indexDir);

			int minFreq = DEFAULT_MIN_FREQ;
			if (commandLine.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(commandLine.getOptionValue("minimum-freq"));
			}

			if (commandLine.hasOption("key-freq")) {
				searcher.loadCache(commandLine.getOptionValue("key-freq"), minFreq);
			}


			searcher.interactive();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		//java -cp dist/jservice.jar org.fbk.cit.hlt.thewikimachine.index.PageTopicSearcher
	}

}
