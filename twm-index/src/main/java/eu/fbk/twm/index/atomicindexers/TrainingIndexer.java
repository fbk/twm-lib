package eu.fbk.twm.index.atomicindexers;

import eu.fbk.twm.index.util.AbstractIndexer;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 2/28/13
 * Time: 9:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class TrainingIndexer extends AbstractIndexer {

	static Logger logger = Logger.getLogger(TrainingIndexer.class.getName());

	public static String FIELD_ID_NAME = "id";
	public static String FIELD_WIKIID_NAME = "wikiID";
	public static String FIELD_VALUE_NAME = "value";

	public TrainingIndexer(String indexName) throws IOException {
		super(indexName);
	}

	public void add(String index, String wikiID, byte[] features) {

		if (features == null) {
			logger.trace("Feature array is null, skipping");
			return;
		}

		Document d = new Document();

		d.add(new Field(FIELD_ID_NAME, index, Field.Store.YES, Field.Index.NOT_ANALYZED));
		d.add(new Field(FIELD_WIKIID_NAME, wikiID, Field.Store.YES, Field.Index.NOT_ANALYZED));
		d.add(new Field(FIELD_VALUE_NAME, features, Field.Store.YES));

		try {
			indexWriter.addDocument(d);
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}
}
