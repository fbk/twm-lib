/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.lsa.LSM;
import eu.fbk.utils.math.Vector;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/5/13
 * Time: 2:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class OneExamplePerSenseExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OneExamplePerSenseExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(OneExamplePerSenseExtractor.class.getName());
	public static final int ID_FORM_INDEX = 0;

	public static final int ID_PAGE_INDEX = 1;

	public static final int FORM_INDEX = 2;

	public static final int PAGE_INDEX = 3;

	public static final int SOURCE_INDEX = 4;

	public static final int TYPE_INDEX = 5;

	public static final int ID_INDEX = 6;

	public static final int LEFT_CONTEXT_INDEX = 7;

	public static final int RIGHT_CONTEXT_INDEX = 8;

	public static final int COLUMN_NUMBER = 9;

	protected LSM lsm;

	private int numForms;

	private int numThreads;

	private ExecutorService myExecutor;

	public final static int DEFAULT_THREADS_NUMBER = 1;

	public final static int DEFAULT_NOTIFICATION_POINT = 100000;

	public final static int DEFAULT_LSM_DIM = 100;

	public static final int DEFAULT_MINIMUM_FORM_FREQ = 1;

	public static final int DEFAULT_MINIMUM_PAGE_FREQ = 1;

	public static final boolean DEFAULT_NORMALIZE = false;

	private int notificationPoint;

	PrintWriter senseWriter;

	public static final int DEFAULT_NUM_FORMS = Integer.MAX_VALUE;

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	private static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public final static int DEFAULT_QUEUE_SIZE = 10000;

	private int minimumFormFreq;

	private int minimumPageFreq;

	private boolean normalized;

	private int tfType;

	public OneExamplePerSenseExtractor(LSM lsm, File outputFile) throws IOException {
		this(lsm, outputFile, DEFAULT_THREADS_NUMBER);
	}

	public OneExamplePerSenseExtractor(LSM lsm, String outputFileName) throws IOException {
		this(lsm, new File(outputFileName), DEFAULT_THREADS_NUMBER);
	}

	public OneExamplePerSenseExtractor(LSM lsm, String outputFileName, int numThreads) throws IOException {
		this(lsm, new File(outputFileName), numThreads);
	}

	public OneExamplePerSenseExtractor(LSM lsm, File outputFile, int numThreads) throws IOException {
		this.lsm = lsm;
		this.numThreads = numThreads;
		tfType = BOW.DEFAULT_TERM_FREQUENCY_TYPE;
		normalized = DEFAULT_NORMALIZE;
		minimumFormFreq = DEFAULT_MINIMUM_FORM_FREQ;
		minimumPageFreq = DEFAULT_MINIMUM_PAGE_FREQ;
		notificationPoint = DEFAULT_NOTIFICATION_POINT;
		logger.info("creating the thread executor (" + numThreads + ")");
		//myExecutor = Executors.newFixedThreadPool(numThreads);
		int blockQueueSize = DEFAULT_QUEUE_SIZE;
		BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(blockQueueSize);
		RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();
		myExecutor = new ThreadPoolExecutor(numThreads, numThreads, 1, TimeUnit.MINUTES, blockingQueue, rejectedExecutionHandler);

		senseWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8")));
	}

	public int getTfType() {
		return tfType;
	}

	public void setTfType(int tfType) {
		this.tfType = tfType;
	}

	public void setNormalized(boolean normalized) {
		this.normalized = normalized;
	}

	public boolean isNormalized() {
		return normalized;
	}

	public int getMinimumFormFreq() {
		return minimumFormFreq;
	}

	public void setMinimumFormFreq(int minimumFormFreq) {
		this.minimumFormFreq = minimumFormFreq;
	}

	public int getMinimumPageFreq() {
		return minimumPageFreq;
	}

	public void setMinimumPageFreq(int minimumPageFreq) {
		this.minimumPageFreq = minimumPageFreq;
	}

	public int getNumForms() {
		return numForms;
	}

	public void setNumForms(int numForms) {
		this.numForms = numForms;
	}

	public int getNumThreads() {
		return numThreads;
	}

	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
	}

	public int getNotificationPoint() {
		return notificationPoint;
	}

	public void setNotificationPoint(int notificationPoint) {
		this.notificationPoint = notificationPoint;
	}

	public void extract(String name) throws IOException {
		extract(new File(name));
	}

	public void extract(File in) throws IOException {
		logger.info("reading " + in + "...");

		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		String line;
		int count = 0, part = 0, tot = 0;
		String previousForm = "";
		//Map<String, BOW> map = new HashMap<String, BOW>();
		String[] t = null;
		List<String[]> list = new ArrayList<String[]>();
		logger.info("totalFreq\tcount\ttime\tdate");
		// read the first line
		if ((line = lnr.readLine()) != null) {
			try {
				t = tabPattern.split(line);

				if (t.length == COLUMN_NUMBER) {
					list.add(t);
					previousForm = t[FORM_INDEX];
					//logger.info(part + "\t\"" + t[3] + "\"");
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + count);
				logger.error(e);
			} finally {
				tot++;
			}
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {

			if (count > numForms) {
				logger.info("Exit after " + count + " forms (" + numForms + ")");
				break;
			}
			try {
				t = tabPattern.split(line);
				if (t.length == COLUMN_NUMBER) {
					if (!t[FORM_INDEX].equals(previousForm)) {
						//logger.debug("executing " + previousForm + " (" + senseList.size() + ")...");
						//todo: filter forms with less than minimumFormFreq
						//todo: add topic label
						myExecutor.execute(new SenseExtractor(list, previousForm));
						list = new ArrayList<String[]>();
						count++;
						part = 0;
					}
					list.add(t);
					previousForm = t[FORM_INDEX];
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + tot);
				logger.error(e);
			} finally {
				tot++;
			}
			//if (count > 500) break;

			if ((tot % notificationPoint) == 0) {
				//senseWriter.flush();
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		} // end while
		lnr.close();

		// add the last line
		list.add(t);
		logger.debug("executing " + previousForm + " (" + list.size() + ")...");
		myExecutor.execute(new SenseExtractor(list, previousForm));

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());

		try {
			myExecutor.shutdown();
			logger.info("waiting for execution...");
			myExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			logger.error(e);
		}
		logger.info("closing the streams...");

		senseWriter.close();
		logger.info("done it");
	}

	class SenseExtractor extends Thread implements Runnable {
		List<String[]> senseList;

		String form;

		int totalFreq;

		SenseExtractor(List<String[]> senseList, String form) {
			this.form = form;
			this.senseList = senseList;
			totalFreq = 0;
		}

		public void run() {
			//logger.debug(Thread.currentThread().getName() + " is extracting senses for " + form + " (" + senseList.size() + ")...");
			Map<String, List<String[]>> exampleMap = createExampleListMap();
			//extract bow and ls
			Example[] examples = createExampleArray(exampleMap);
			//sort by prior (freq)
			Arrays.sort(examples, new Comparator<Example>() {
				@Override
				public int compare(Example example1, Example example2) {
					return example2.getFreq() - example1.getFreq();
				}
			});
			writeExampleArray(examples);

		}

		class Example implements Comparable<Example> {
			private BOW bow;

			private eu.fbk.utils.math.Vector bowVector;

			private eu.fbk.utils.math.Vector lsVector;

			private int freq;

			private String page;

			Example(String page, List<String[]> list) {
				this.page = page;
				freq = list.size();
				totalFreq += freq;
				StringBuilder sb = new StringBuilder();
				bow = new BOW();
				String[] s;
				String[] leftContext;
				String[] rightContext;
				for (int i = 0; i < list.size(); i++) {
					s = list.get(i);
					try {
						leftContext = spacePattern.split(s[LEFT_CONTEXT_INDEX].toLowerCase());
						rightContext = spacePattern.split(s[RIGHT_CONTEXT_INDEX].toLowerCase());
						bow.addAll(leftContext);
						bow.addAll(rightContext);
					} catch (Exception e) {
						logger.error(e);
					}
				}
				bowVector = lsm.mapDocument(bow);
				if (normalized) {
					bowVector.normalize();
				}

				lsVector = lsm.mapPseudoDocument(bowVector);
				//bowVector.normalize();
				if (normalized) {
					lsVector.normalize();
				}

			}

			public String getPage() {
				return page;
			}

			public BOW getBow() {
				return bow;
			}

			public Vector getBowVector() {
				return bowVector;
			}

			public Vector getLsVector() {
				return lsVector;
			}

			public int getFreq() {
				return freq;
			}

			@Override
			public String toString() {
				StringBuilder sb = new StringBuilder();
				sb.append(page);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append((double) freq / totalFreq);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(lsVector);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(bowVector);
				return sb.toString();
			}

			@Override
			public int compareTo(Example example) {
				return example.getFreq() - freq;
			}
		}

		private void writeExampleArray(Example[] examples) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < examples.length; i++) {
				sb.append(form);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(examples[i]);
				sb.append(CharacterTable.LINE_FEED);
			}
			synchronized (this) {
				senseWriter.print(sb.toString());
			}
		}

		/**
		 * Returns a map in which the keys are senses and values are senseList of examples.
		 *
		 * @return a map in which the keys are senses and values are senseList of examples.
		 */
		Map<String, List<String[]>> createExampleListMap() {
			Map<String, List<String[]>> map = new HashMap<String, List<String[]>>();
			String[] line;
			String key;
			for (int i = 0; i < senseList.size(); i++) {
				line = senseList.get(i);
				key = line[PAGE_INDEX];
				List<String[]> list = map.get(key);
				if (list == null) {
					list = new ArrayList<String[]>();
					map.put(key, list);
				}
				list.add(line);
			}
			return map;
		}

		//todo: filter pages with less than minimumPageFreq
		Example[] createExampleArray(Map<String, List<String[]>> map) {
			Example[] examples = new Example[map.size()];
			Iterator<String> it = map.keySet().iterator();
			String page;
			List<String[]> list;
			for (int i = 0; it.hasNext(); i++) {
				page = it.next();
				list = map.get(page);
				examples[i] = new Example(page, list);
			}
			return examples;
		}
	}

	public static void main(String[] args) {
		// java com.ml.test.net.HttpServerDemo
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option inputFileNameOpt = OptionBuilder.withArgName("file").hasArg().withDescription("sorted form/page file").isRequired().withLongOpt("input").create("i");
			Option outputFileNameOpt = OptionBuilder.withArgName("file").hasArg().withDescription("one sense per example file").isRequired().withLongOpt("output").create("o");
			Option tfOpt = OptionBuilder.withArgName("FUNC").hasArg().withDescription("term frequency function; FUNC is " + BOW.RAW_TERM_FREQUENCY + "=`" + BOW.labels[BOW.RAW_TERM_FREQUENCY] + BOW.BOOLEAN_TERM_FREQUENCY + "=`" + BOW.labels[BOW.BOOLEAN_TERM_FREQUENCY] + "'," + BOW.LOGARITHMIC_TERM_FREQUENCY + "=`" + BOW.labels[BOW.LOGARITHMIC_TERM_FREQUENCY] + "'," + BOW.AUGMENTED_TERM_FREQUENCY + "=`" + BOW.labels[BOW.AUGMENTED_TERM_FREQUENCY] + " (default is " + BOW.DEFAULT_TERM_FREQUENCY_TYPE + ")").withLongOpt("tf").create();

			Option numFormOpt = OptionBuilder.withArgName("int").hasArg().withDescription("maximum number of forms to process (default is all)").withLongOpt("num-forms").create("f");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option lsmDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("lsm dir").isRequired().withLongOpt("lsm").create("l");
			Option lsmDimOpt = OptionBuilder.withArgName("int").hasArg().withDescription("lsm dim").withLongOpt("dim").create("d");
			Option normalizedOpt = OptionBuilder.withDescription("normalize vectors (default is " + DEFAULT_NORMALIZE + ")").withLongOpt("normalized").create("n");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(inputFileNameOpt);
			options.addOption(tfOpt);
			options.addOption(outputFileNameOpt);
			options.addOption(numThreadOpt);
			options.addOption(notificationPointOpt);
			options.addOption(numFormOpt);
			options.addOption(lsmDirOpt);
			options.addOption(lsmDimOpt);
			options.addOption(normalizedOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			logger.debug(options);

			logger.debug(line.getOptionValue("output") + "\t" + line.getOptionValue("input") + "\t" + line.getOptionValue("lsm"));

			String lsmDirName = line.getOptionValue("lsm");
			if (!lsmDirName.endsWith(File.separator)) {
				lsmDirName += File.separator;
			}

			File fileUt = new File(lsmDirName + "X-Ut");
			File fileSk = new File(lsmDirName + "X-S");
			File fileR = new File(lsmDirName + "X-row");
			File fileC = new File(lsmDirName + "X-col");
			File fileDf = new File(lsmDirName + "X-df");
			int dim = DEFAULT_LSM_DIM;
			if (line.hasOption("dim")) {
				dim = Integer.parseInt(line.getOptionValue("dim"));
			}
			logger.debug(line.getOptionValue("lsm") + "\t" + line.getOptionValue("dim"));
			boolean normalized = false;
			if (line.hasOption("normalized")) {
				normalized = true;
			}
			LSM lsm = new LSM(fileUt, fileSk, fileR, fileC, fileDf, dim, true, normalized);

			int numThreads = DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int minimumFormFreq = DEFAULT_MINIMUM_FORM_FREQ;
			if (line.hasOption("min-freq")) {
				minimumFormFreq = Integer.parseInt(line.getOptionValue("min-freq"));
			}

			int minimumPageFreq = DEFAULT_MINIMUM_PAGE_FREQ;
			if (line.hasOption("min-page")) {
				minimumPageFreq = Integer.parseInt(line.getOptionValue("min-page"));
			}

			int numForms = DEFAULT_NUM_FORMS;
			if (line.hasOption("num-forms")) {
				numForms = Integer.parseInt(line.getOptionValue("num-forms"));
			}

			int notificationPoint = DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			int tfType = BOW.DEFAULT_TERM_FREQUENCY_TYPE;
			if (line.hasOption("tf")) {
				tfType = Integer.parseInt(line.getOptionValue("tf"));
			}
			logger.info("extracting one example per sense using " + numThreads + " threads");
			OneExamplePerSenseExtractor oneExamplePerSenseExtractor = new OneExamplePerSenseExtractor(lsm, line.getOptionValue("output"), numThreads);
			oneExamplePerSenseExtractor.setTfType(tfType);
			oneExamplePerSenseExtractor.setNormalized(normalized);
			oneExamplePerSenseExtractor.setNotificationPoint(notificationPoint);
			oneExamplePerSenseExtractor.setNumForms(numForms);
			oneExamplePerSenseExtractor.extract(line.getOptionValue("input"));


		} catch (ParseException e) {
			// oops, something went wrong
			System.err.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.OneExamplePerSenseExtractor", "\n", options, "\n", true);
		} catch (IOException e) {
			logger.error(e);
		}
	}

}
