package eu.fbk.twm.index.csv;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/14/14
 * Time: 12:05 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractCategoryExtractor extends CSVExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AbstractCategoryExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(AbstractCategoryExtractor.class.getName());

	public static final int DEFAULT_MAX_CATEGORY_DEPTH = 7;

	private int maxDepth;

	protected AbstractCategoryExtractor(int numThreads) {
		super(numThreads);
		maxDepth = DEFAULT_MAX_CATEGORY_DEPTH;
	}

	protected AbstractCategoryExtractor(int numThreads, int numPages) {
		super(numThreads, numPages);
		maxDepth = DEFAULT_MAX_CATEGORY_DEPTH;

	}

	public int getMaxDepth() {
		return maxDepth;
	}

	public void setMaxDepth(int maxDepth) {
		this.maxDepth = maxDepth;
	}
}
