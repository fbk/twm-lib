/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.utils.PageMap;
import eu.fbk.twm.utils.WikipediaExtractor;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.lsa.LSM;
import eu.fbk.utils.math.Node;
import eu.fbk.utils.math.Vector;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.OptionBuilder;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 11/8/13
 * Time: 11:35 AM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * java -cp dist/thewikimachine.jar eu.fbk.twm.index.PageCategorization -i /data/models/wikipedia/it/20130908/itwiki-20130908-page-vector-index/ -l /data/models/lsa/it/current/ -c Tecnologia,Politica,Scienza,Economia,Sport,Turismo,Musica,Autovettura,Salute,Cucina,Cinema,Moda,Architettura,Religione,Arte -t
 * <p/>
 * java -cp dist/thewikimachine.jar eu.fbk.twm.index.PageCategorization -i /data/mods/wikipedia/en/20130904/enwiki-20130904-page-vector-index/ -l /data/models/lsa/en/current/ -c Technology,Politics,Science,Economy,Sport,Tourism,Music,Automobile,Health,Food,Cinematography,Fashion,Religion,Art,Terrorism,History,Entertainment -t
 * <p/>
 * java -cp dist/thewikimachine.jar eu.fbk.twm.index.PageCategorization -i /data/models/wikipedia/it/20130908/itwiki-20130908-page-vector-index/ -l /data/models/lsa/it/current/ -r /data/models/wikipedia/it/20130908/itwiki-20130908-redirect.csv -c "Informatica,Bibliografia,Biblioteconomia,Enciclopedia,Museologia,Giornalismo,Editoria,Giornale,Manoscritto,Filosofia,Metafisica,Epistemologia,Paranormale,Psicologia,Logica,Etica,Filosofia_antica,Filosofia_medievale,Filosofie_orientali,Filosofia_moderna,Religione,Bibbia,Teologia,Teologia_morale,Chiesa,Ordine_religioso,Scienze_sociali,Sociologia,Statistica,Scienza_politica,Economia,Diritto,Amministrazione_pubblica,Arte_militare,Assistenza_sociale,Educazione,Commercio,Comunicazione,trasporti,Galateo_(costume),folclore,Linguaggio,Linguistica,Lingua_inglese,Lingua_anglosassone,Lingue_germaniche,Lingua_tedesca,Lingue_romanze,Lingua_francese,Lingua_italiana,Lingua_rumena,Lingua_spagnola,Lingua_portoghese,Lingue_italiche,Lingua_latina,Lingua_greca,Matematica,Astronomia,Fisica,Chimica,Scienze_della_terra,Paleontologia,Paleozoologia,Biologia,Botanica,Zoologia,Tecnologia,Medicina,Ingegneria,Agricoltura,Casa,Famiglia,Ingegneria_chimica,Edilizia,Arte,Urbanistica,Architettura,Arti_plastiche,scultura,Disegno,Pittura,Grafica,Tipografia,Fotografia,Musica,Arti_performative,Letteratura,Letteratura_americana,Letteratura_tedesca,Letteratura_francese,Letteratura_italiana,letteratura_rumena,letteratura_spagnola,letteratura_portoghese,Letteratura_latina,Letteratura_greca,Geografia,Storia,Geografia,Biografia,Genealogia,Araldica,Storia_antica,Storia_dell'Europa,Storia_dell'Africa" -t
 */
public class PageCategorization {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageCategorization</code>.
	 */
	static Logger logger = Logger.getLogger(PageCategorization.class.getName());
	private LSM lsm;
	private PageVectorSearcher pageVectorSearcher;
	private PageSeeAlsoSearcher pageSeeAlsoSearcher;
	//private String[] categories;
	private PageMap redirectPageMap;
	Tokenizer tokenizer;

	protected static DecimalFormat df = new DecimalFormat("###,###,##0.000");

	//Node[][][] nodes;
	Map<String, Entry> pageCategoryMap;

	public PageCategorization(LSM lsm, PageVectorSearcher pageVectorSearcher, File pageCategoryFile, PageMap redirectPageMap, PageSeeAlsoSearcher pageSeeAlsoSearcher) {
		this.lsm = lsm;
		this.pageVectorSearcher = pageVectorSearcher;
		this.pageSeeAlsoSearcher = pageSeeAlsoSearcher;
		//this.categories = new String[0];
		this.redirectPageMap = redirectPageMap;
		tokenizer = HardTokenizer.getInstance();
		try {
			init(pageCategoryFile);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	class Entry {
		private String page;
		private String category;
		private Node[][] nodes;

		Entry(String page, String category, Node[][] nodes) {
			this.page = page;
			this.category = category;
			this.nodes = nodes;
		}

		String getCategory() {
			return category;
		}

		Node[][] getNodes() {
			return nodes;
		}
	}

	public void init(File f) throws IOException {
		pageCategoryMap = new HashMap<String, Entry>();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;
		String page = null, category = null;
		while ((line = lnr.readLine()) != null) {
			if (line.length() > 0 && !line.startsWith("#")) {
				String[] s = line.split("=");
				if (s.length == 2) {
					page = s[0];
					category = s[1];
				}
				else {
					page = s[0];
					category = s[0];
				}
				logger.debug(page + "\t" + category);
				Node[][] nodes = pageVectorSearcher.search(page);
				if (nodes != null) {
					//logger.debug("\t" + nodes[0].length + "\t" + nodes[1].length);
					Entry entry = new Entry(page, category, nodes);
					pageCategoryMap.put(page, entry);
					String[] pages = pageSeeAlsoSearcher.search(page);
					for (int i = 0; i < pages.length; i++) {
						logger.debug(i + "\t" + pages[i] + "\t" + category);
						nodes = pageVectorSearcher.search(pages[i]);
						if (nodes != null) {
							//logger.debug("\t" + nodes[0].length + "\t" + nodes[1].length);
							entry = new Entry(pages[i], category, nodes);
							//pageCategoryMap.put(pages[i], entry);
						}
					}
				}
			}
		}

	}

	public Category[] classify(String text) {
		String tokenizedText = tokenizer.tokenizedString(text);

		BOW bow = new BOW(tokenizedText.toLowerCase());
		Vector d = lsm.mapDocument(bow);

		Vector pd = lsm.mapPseudoDocument(d);
		d.normalize();
		pd.normalize();
		Node[] nd = d.toNodeArray();
		Node[] npd = pd.toNodeArray();
		//logger.debug(Node.toString(nd));
		//logger.debug(Node.toString(npd));

		Category[] categoryArray = new Category[pageCategoryMap.size()];
		Iterator<String> it = pageCategoryMap.keySet().iterator();
		for (int i = 0; it.hasNext(); i++) {
			String page = it.next();
			Entry entry = pageCategoryMap.get(page);
			Node[][] nodes = entry.getNodes();
			Node[] cd = nodes[1];
			Node[] cpd = nodes[0];
			double cbow = Node.dot(nd, cd);
			double clsa = Node.dot(npd, cpd);
			double combo = (cbow + clsa) / 2;
			categoryArray[i] = new Category(entry.getCategory(), cbow, clsa);

			//logger.debug(i + "\t" + categories[i] + "\t" + cbow + "\t" + clsa + "\t" + combo);
		}
		Arrays.sort(categoryArray);
		return categoryArray;
	}

	public void interactive() throws Exception {
		InputStreamReader indexReader = null;
		BufferedReader myInput = null;
		long begin = 0, end = 0;
		while (true) {
			System.out.println("\nPlease write a query and type <return> to continue (CTRL C to exit):");

			indexReader = new InputStreamReader(System.in);
			myInput = new BufferedReader(indexReader);

			String text = myInput.readLine().toString();
			Category[] categoryArray = null;
			File f = new File(text);
			if (f.exists()) {
				categoryArray = classify(readFile(f));
			}
			else {
				categoryArray = classify(text);
			}
			if (categoryArray != null) {
				int start = categoryArray.length - 10;
				if (start < 0) {
					start = 0;
				}
				for (int i = start; i < categoryArray.length; i++) {
					logger.debug(i + "\t" + categoryArray[i]);
				}
			}
		}
	}

	private String readFile(File f) throws IOException {
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		StringBuilder sb = new StringBuilder();
		String line;
		int tot = 0;
		while ((line = lnr.readLine()) != null && tot < 2) {
			sb.append(line);
			sb.append("\n");
			tot++;
		}
		logger.debug(tot + " lines read");
		return sb.toString();
	}

	public class Category implements Comparable<Category> {
		private double bow;

		private double ls;

		private double combo;

		private String category;

		Category(String category, double bow, double ls) {
			this.category = category;
			this.bow = bow;
			this.ls = ls;
			combo = (bow + ls) / 2;
		}

		public String getLabel() {
			return category;
		}

		public double getCombo() {
			return combo;
		}

		public double getBow() {
			return bow;
		}

		public double getLs() {
			return ls;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (!(o instanceof Category)) {
				return false;
			}

			Category category1 = (Category) o;
			return category1.equals(category);
			/*if (category != null ? !category.equals(category1.getLabel()) : category1.getLabel() != null) {
				return false;
			}

			return true; */
		}

		@Override
		public int hashCode() {
			return category.hashCode();
		}

		@Override
		public int compareTo(Category sense) {
			double diff = combo - sense.getCombo();
			//double diff = bow - sense.getBow();
			//double diff = ls - sense.getLs();
			if (diff > 0) {
				return 1;
			}
			else if (diff < 0) {
				return -1;
			}

			return 0;
		}

		@Override
		public String toString() {
			return df.format(bow) + "\t" + df.format(ls) + "\t" + df.format(combo) + "\t" + category;
		}
	}

	public void test(File dir) throws IOException {
		double weight = 0;
		int[] correct = new int[3];
		String[] method = {"best", "weighted vote", "vote"};
		int total = 0;


		File[] categories = dir.listFiles();
		for (int i = 0; i < categories.length; i++) {
			logger.info(i + "\t" + categories[i]);
			File[] files = categories[i].listFiles();
			for (int j = 0; j < files.length; j++) {
				weight++;
				logger.debug(weight + "\t" + j + "\t" + files[j]);
				Category[] predictedCategories = classify(readFile(files[j]));
				if (predictedCategories != null) {
					int start = predictedCategories.length - 10;
					if (start < 0) {
						start = 0;
					}
					Voting voting = new Voting();
					Voting weightedVoting = new Voting();
					for (int k = start; k < predictedCategories.length; k++) {
						voting.add(predictedCategories[k].getLabel());
						weightedVoting.add(predictedCategories[k].getLabel(), predictedCategories[k].getCombo());

						logger.debug("[" + k + "\t" + predictedCategories[k] + "]");
					}
					SortedMap<Double, List<String>> weightedVotingMap = weightedVoting.toSortedMap();
					logger.info(weightedVotingMap);
					SortedMap<Double, List<String>> votingMap = weightedVoting.toSortedMap();
					logger.info(votingMap);


					if (predictedCategories[predictedCategories.length - 1].getLabel().equalsIgnoreCase(categories[i].getName())) {
						logger.debug("1\t" + predictedCategories[predictedCategories.length - 1].getLabel() + " == " + categories[i].getName());
						correct[0]++;
					}
					else {
						logger.debug("0\t" + predictedCategories[predictedCategories.length - 1].getLabel() + " != " + categories[i].getName());
					}

					if (weightedVotingMap.get(weightedVotingMap.firstKey()).get(0).equalsIgnoreCase(categories[i].getName())){
						logger.debug("1\t" + weightedVotingMap.get(weightedVotingMap.firstKey()).get(0) + " == " + categories[i].getName());
						correct[1]++;
					}
					else{
						logger.debug("0\t" + weightedVotingMap.get(weightedVotingMap.firstKey()).get(0) + " != " + categories[i].getName());
					}

					if (votingMap.get(votingMap.firstKey()).get(0).equalsIgnoreCase(categories[i].getName())){
						logger.debug("1\t" + votingMap.get(votingMap.firstKey()).get(0) + " == " + categories[i].getName());
						correct[2]++;
					}
					else{
						logger.debug("0\t" + votingMap.get(votingMap.firstKey()).get(0) + " != " + categories[i].getName());
					}
				}
				total++;
			}
		}
		double[] accuracy = new double[3];
		for (int i = 0; i < accuracy.length; i++) {
			accuracy[i] = (double) correct[i] / total;
			logger.info(df.format(accuracy[i]) + " = " + correct[i] + " / " + total + "\t" + method[i]);
		}



	}

	class Voting {
		protected Map<String, Weight> map;

		protected int total;

		public Voting() {
			map = new TreeMap<String, Weight>();
		}

		public void add(String category) {
			add(category, 1.0);
		}

		public void add(String category, double weight) {
			total += weight;
			Weight currentWeight = map.get(category);
			if (currentWeight == null) {
				map.put(category, new Weight(weight));
			}
			else {
				currentWeight.inc(weight);
			}
		}

		public SortedMap<Double, List<String>> toSortedMap() {
			SortedMap<Double, List<String>> smap = new TreeMap<Double, List<String>>(new Comparator<Double>() {
				public int compare(Double e1, Double e2) {
					return e2.compareTo(e1);
				}
			});

			Iterator<String> it = map.keySet().iterator();
			while (it.hasNext()) {
				String c = it.next();
				Weight w = map.get(c);
				List<String> list = smap.get(w.get());
				if (list == null) {
					list = new ArrayList<String>();
					list.add(c);
					smap.put(w.get(), list);
				}
				else {
					list.add(c);
				}
			}

			return smap;
		}

		@Override
		public String toString() {
			return map.toString();
		}

		class Weight {
			double weight;

			public Weight(double weight) {
				this.weight = weight;
			}

			public void inc() {
				weight++;
			}

			public void inc(double l) {
				weight += l;
			}

			public double get() {
				return weight;
			}

			public String toString() {
				return Double.toString(weight);
			}
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option pageVectorIndexNameOpt = new OptionBuilder().withArgName("dir").hasArg().withDescription("open a page vector index with the specified name").isRequired().withLongOpt("vector-index").toOption();
			Option pageSeeAlsoIndexNameOpt = new OptionBuilder().withArgName("dir").hasArg().withDescription("open a page see also index with the specified name").isRequired().withLongOpt("see-also-index").toOption();
			Option testSetOpt = new OptionBuilder().withArgName("dir").hasArg().withDescription("dir from which to read the test set").withLongOpt("test-dir").toOption();

			Option redirectMapOpt = new OptionBuilder().withArgName("file").hasArg().withDescription("redirect pages file").isRequired().withLongOpt("redirect").toOption("r");
			Option interactiveModeOpt = new OptionBuilder().withDescription("enter in the interactive mode").withLongOpt("interactive-mode").toOption("t");
			//Option instanceFileOpt = new OptionBuilder().withArgName("file").hasArg().withDescription("read the instances to classify from the specified file").withLongOpt("instance-file").toOption("f");
			Option lsmDirOpt = new OptionBuilder().withArgName("dir").hasArg().withDescription("lsm dir").isRequired().withLongOpt("lsm").toOption("l");
			Option lsmDimOpt = new OptionBuilder().withArgName("int").hasArg().withDescription("lsm dim").withLongOpt("dim").toOption("d");
			Option categoryFileOpt = new OptionBuilder().withArgName("file").hasArg().withDescription("category file").isRequired().withLongOpt("categories").toOption("c");
			Option normalizedOpt = new OptionBuilder().withDescription("normalize vectors (default is " + WikipediaExtractor.DEFAULT_NORMALIZE + ")").withLongOpt("normalized").toOption();

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(pageVectorIndexNameOpt);
			options.addOption(redirectMapOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(categoryFileOpt);
			options.addOption(pageSeeAlsoIndexNameOpt);
			options.addOption(lsmDirOpt);
			options.addOption(lsmDimOpt);
			options.addOption(normalizedOpt);
			options.addOption(testSetOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = OneExamplePerSenseSearcher.DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			String lsmDirName = line.getOptionValue("lsm");
			if (!lsmDirName.endsWith(File.separator)) {
				lsmDirName += File.separator;
			}

			boolean normalized = WikipediaExtractor.DEFAULT_NORMALIZE;
			if (line.hasOption("normalized")) {
				normalized = true;
			}

			File fileUt = new File(lsmDirName + "X-Ut");
			File fileSk = new File(lsmDirName + "X-S");
			File fileR = new File(lsmDirName + "X-row");
			File fileC = new File(lsmDirName + "X-col");
			File fileDf = new File(lsmDirName + "X-df");
			int dim = 100;
			if (line.hasOption("dim")) {
				dim = Integer.parseInt(line.getOptionValue("dim"));
			}
			File pageCategoryFile = null;
			if (line.hasOption("categories")) {
				pageCategoryFile = new File(line.getOptionValue("categories"));
			}
			logger.debug(line.getOptionValue("lsm") + "\t" + line.getOptionValue("dim"));

			LSM lsm = new LSM(fileUt, fileSk, fileR, fileC, fileDf, dim, true, normalized);

			PageVectorSearcher pageVectorSearcher = new PageVectorSearcher(line.getOptionValue("vector-index"));
			pageVectorSearcher.setNotificationPoint(notificationPoint);
			if (line.hasOption("key-freq")) {
				pageVectorSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
			}

			PageMap redirectPageMap = new PageMap(new File(line.getOptionValue("redirect")));
			//logger.info(redirectPageMap.size() + " redirect pages");


			PageSeeAlsoSearcher pageSeeAlsoSearcher = new PageSeeAlsoSearcher(line.getOptionValue("see-also-index"));

			PageCategorization pageCategorization = new PageCategorization(lsm, pageVectorSearcher, pageCategoryFile, redirectPageMap, pageSeeAlsoSearcher);

			if (line.hasOption("test-dir")) {
				pageCategorization.test(new File(line.getOptionValue("test-dir")));
			}

			if (line.hasOption("interactive-mode")) {
				pageCategorization.interactive();
			}


		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar eu.fbk.twm.index.PageCategorization", "\n", options, "\n", true);
		}
	}
}

