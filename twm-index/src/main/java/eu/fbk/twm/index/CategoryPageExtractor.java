/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.utils.core.core.HashIndexSet;
import eu.fbk.utils.math.Node;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.WikipediaCategory;
import eu.fbk.twm.utils.Defaults;

import java.io.*;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/24/13
 * Time: 11:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class CategoryPageExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>SubCategoryPageSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(CategoryPageExtractor.class.getName());
	public final static int DEFAULT_MIN_FREQUENCY = 0;
	public final static int DEFAULT_MAX_FREQUENCY = Integer.MAX_VALUE;
	CategorySubCategorySearcher categorySubCategorySearcher;
	CategoryPageSearcher categoryPageSearcher;
	PageVectorSearcher pageVectorSearcher;
	PageFreqSearcher pageFreqSearcher;
	private int maxDepth;
	private int minFrequency;
	private int maxFrequency;
	DecimalFormat nf = new DecimalFormat("###,###,###");
	public CategoryPageExtractor(String categorySubCategoryIndex, String categoryPageIndex, String pageFrequencyIndex, int maxDepth, int minFrequency, int maxFrequency) throws IOException {
		this.maxDepth = maxDepth;
		this.minFrequency = minFrequency;
		this.maxFrequency = maxFrequency;
		categorySubCategorySearcher = new CategorySubCategorySearcher(categorySubCategoryIndex);
		categoryPageSearcher = new CategoryPageSearcher(categoryPageIndex);
		pageFreqSearcher = new PageFreqSearcher(pageFrequencyIndex);
		pageFreqSearcher.loadCache(10);
		//pageVectorSearcher = new PageVectorSearcher(vectorIndex);
	}

	private void process(String in, String out) throws IOException {

		String prefix = out + "-depth-" + maxDepth;
		PrintWriter bowWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(prefix + ".bow"), "UTF-8")));
		PrintWriter lsaWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(prefix + ".lsa"), "UTF-8")));
		PrintWriter textWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(prefix + ".txt"), "UTF-8")));
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		String line;
		int count = 0;
		HashIndexSet<String> indexSet = new HashIndexSet<>();
		int lineCount = 0;
		while ((line = lnr.readLine()) != null) {
			logger.debug(line);
			String[] s = line.split("\t");

			indexSet.add(s[1]);
			int label = indexSet.getIndex(s[1]);
			Set<WikipediaCategory> result = new HashSet<>();
			categorySubCategorySearcher.search(s[0], result, maxDepth);
			Iterator<WikipediaCategory> it = result.iterator();
			for (int i = 0; it.hasNext(); i++) {
				WikipediaCategory category = it.next();


				String[] pages = categoryPageSearcher.search(category.getLabel());
				for (int j = 0; j < pages.length; j++) {

					logger.info(lineCount + "\t" + i + "/" + j + "\t" + category + "\t" + pages[j] + "\t" + count++);
					Node[][] vectors = pageVectorSearcher.search(pages[j]);
					bowWriter.println(label + " " + Node.toString(vectors[1]));
					lsaWriter.println(label + " " + Node.toString(vectors[0]));
					textWriter.println(line + "\t" + label + "\t" + pages[j]);
				}
			}
			lineCount++;
		}
		lnr.close();
		bowWriter.close();
		lsaWriter.close();
		textWriter.close();
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			int pageCount = 0, totalPageCount = 0;
			Set<WikipediaCategory> result = new HashSet<>();
			categorySubCategorySearcher.search(query, result, maxDepth);
			Iterator<WikipediaCategory> it = result.iterator();
			int categoryCount = 0;
			long begin = System.currentTimeMillis();
			for (; it.hasNext(); categoryCount++) {
				WikipediaCategory category = it.next();
				String[] pages = categoryPageSearcher.search(category.getLabel());
				for (int j = 0; j < pages.length; j++) {
					int freq = pageFreqSearcher.search(pages[j]);
					if (freq >= minFrequency && freq < maxFrequency) {
						logger.debug(categoryCount + "\t" + j + "\t" + category + "\t" + pages[j] + "\t" + freq + "\t" + pageCount++);
					}
					totalPageCount++;
				}
			}
			long end = System.currentTimeMillis();
			long time = end - begin;
			logger.info(nf.format(pageCount) + " (" + nf.format(totalPageCount) + ") pages found in " + nf.format(time) + " ms");
			logger.info(nf.format(categoryCount) + " categories found in " + nf.format(time) + " ms");

		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			options.addOption(OptionBuilder.withArgName("dir").hasArg().withDescription("open the category/subcategory index with the specified name").isRequired().withLongOpt("category-index").create("c"));
			options.addOption(OptionBuilder.withArgName("dir").hasArg().withDescription("open the category/page index with the specified name").isRequired().withLongOpt("page-index").create("p"));
			options.addOption(OptionBuilder.withArgName("dir").hasArg().withDescription("open the page/frequency index with the specified name").isRequired().withLongOpt("frequency-index").create("f"));
			//Option pageVectorIndexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open the page/vector index with the specified name").isRequired().withLongOpt("vector-index").create("v");

			options.addOption(OptionBuilder.withArgName("int").hasArg().withDescription("max depth").isRequired().withLongOpt("max-depth").create("d"));
			options.addOption(OptionBuilder.withArgName("int").hasArg().withDescription("max page frequency (default is " + DEFAULT_MAX_FREQUENCY + ")").withLongOpt("max-frequency").create("M"));
			options.addOption(OptionBuilder.withArgName("int").hasArg().withDescription("min page frequency (default is " + DEFAULT_MIN_FREQUENCY + ")").withLongOpt("min-frequency").create("m"));
			options.addOption(OptionBuilder.withArgName("interactive-mode").withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t"));
			options.addOption(OptionBuilder.withArgName("search").hasArg().withDescription("search for the specified key").withLongOpt("search").create("s"));
			options.addOption(OptionBuilder.withDescription("trace mode").withLongOpt("trace").create());
			options.addOption(OptionBuilder.withDescription("debug mode").withLongOpt("debug").create());
			options.addOption(OptionBuilder.withDescription("info mode").withLongOpt("info").create());

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			Properties defaultProps = new Properties();
			try {
				defaultProps.load(new InputStreamReader(new FileInputStream(logConfig), "UTF-8"));
			} catch (Exception e) {
				defaultProps.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
				defaultProps.setProperty("log4j.appender.stdout.layout.ConversionPattern", "[%t] %-5p (%F:%L) - %m %n");
				defaultProps.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
				defaultProps.setProperty("log4j.appender.stdout.Encoding", "UTF-8");
			}
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("trace")) {
				defaultProps.setProperty("log4j.rootLogger", "trace,stdout");
			}
			else if (line.hasOption("debug")) {
				defaultProps.setProperty("log4j.rootLogger", "debug,stdout");
			}
			else if (line.hasOption("info")) {
				defaultProps.setProperty("log4j.rootLogger", "info,stdout");
			}
			else {
				if (defaultProps.getProperty("log4j.rootLogger") == null) {
					defaultProps.setProperty("log4j.rootLogger", "info,stdout");
				}
			}
			PropertyConfigurator.configure(defaultProps);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}


			int maxDepth = Integer.parseInt(line.getOptionValue("max-depth"));
			int maxFrequency = DEFAULT_MAX_FREQUENCY;
			if (line.hasOption("max-frequency")) {
				maxFrequency = Integer.parseInt(line.getOptionValue("max-frequency"));
			}
			int minFrequency = DEFAULT_MIN_FREQUENCY;
			if (line.hasOption("min-frequency")) {
				minFrequency = Integer.parseInt(line.getOptionValue("min-frequency"));
			}

			String pageIndex = line.getOptionValue("page-index");
			String categoryIndex = line.getOptionValue("category-index");
			String frequencyIndex = line.getOptionValue("frequency-index");
			//String vectorIndex= line.getOptionValue("vector-index");
			//String input= line.getOptionValue("input");
			//String output= line.getOptionValue("output");
			CategoryPageExtractor categoryPageExtractor = new CategoryPageExtractor(categoryIndex, pageIndex, frequencyIndex, maxDepth, minFrequency, maxFrequency);
			//categoryPageExtractor.process(input,output);
			if (line.hasOption("interactive-mode")) {
				categoryPageExtractor.interactive();
			}


		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.CategoryPageExtractor", "\n", options, "\n", true);
		}
	}


}
