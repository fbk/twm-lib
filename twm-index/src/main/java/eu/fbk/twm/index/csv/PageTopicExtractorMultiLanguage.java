package eu.fbk.twm.index.csv;

import eu.fbk.twm.index.PageTopicIndexer;
import eu.fbk.twm.index.util.WeightedSetIndexer;
import eu.fbk.twm.utils.*;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 23/01/14
 * Time: 16:49
 * To change this template use File | Settings | File Templates.
 */
public class PageTopicExtractorMultiLanguage {

	static Logger logger = Logger.getLogger(PageTopicExtractor.class.getName());
	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");
	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public static void interactive(Map<String, LangTopicModel> models) throws IOException {
		while (true) {
			System.out.println("\nPlease write a LANG and a PAGE and type <return> to continue, <return> with empty string to exit:");

			InputStreamReader reader = new InputStreamReader(System.in);
			BufferedReader myInput = new BufferedReader(reader);
			String query = myInput.readLine();

			if (query == null || query.length() == 0) {
				break;
			}

			String[] parts = query.split("\\s+");
			if (parts.length == 1) {
				logger.info("You must insert a LANG and a PAGE");
				continue;
			}

			String lang = parts[0];
			String page = query.substring(lang.length()).trim();
			page = page.replace(' ', CharacterTable.LOW_LINE);
			logger.debug("Language: " + lang);
			logger.debug("Page: " + page);

			if (models.get(lang) == null) {
				logger.info("Language " + lang + " is not loaded");
				continue;
			}

			long begin = System.nanoTime();
			WeightedSet weightedSet = new WeightedSet();
			try {
				models.get(lang).search(page, weightedSet);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			long end = System.nanoTime();
			Map<Double, List<String>> sortedMap = weightedSet.toSortedMap();
			logger.info(sortedMap + "\t" + tf.format(end - begin));
		}
	}

	public static void main(String[] args) {

		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		commandLineWithLogger.addOption(OptionBuilder.withDescription("Languages mappings").isRequired().hasArgs().withArgName("iso-codes").withLongOpt("languages").create("l"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("Mapping folder").isRequired().hasArg().withArgName("folder").withLongOpt("mapping-dir").create("f"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("Base folder").isRequired().hasArg().withArgName("folder").withLongOpt("base-dir").create("b"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("Wikidata schema").hasArg().withArgName("folder").withLongOpt("wikidata").create("w"));

		commandLineWithLogger.addOption(OptionBuilder.withDescription("Output folder").hasArg().withArgName("folder").withLongOpt("output-dir").create("o"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("Topic ontology file").hasArg().withArgName("file").withLongOpt("ontology").create("t"));

		commandLineWithLogger.addOption(OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("I"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}

		String[] langs = commandLine.getOptionValues("languages");
		String baseFolder = commandLine.getOptionValue("base-dir");
		String mappingFolder = commandLine.getOptionValue("mapping-dir");
		String outputFolder = commandLine.getOptionValue("output-dir");
		String wikidataSchema = commandLine.getOptionValue("wikidata");

		TopicOntology ontology = null;
		if (commandLine.hasOption("ontology")) {
			try {
				logger.info("Loading ontology");
				ontology = new TopicOntology(commandLine.getOptionValue("ontology"));
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		if ((wikidataSchema == null && outputFolder != null) ||
				(wikidataSchema != null && outputFolder == null)) {
			logger.info("wikidata and output-dir options must be set both");
			System.exit(1);
		}

		boolean interactive = commandLine.hasOption("interactive-mode");

		if (!baseFolder.endsWith(File.separator)) {
			baseFolder += File.separator;
		}
		if (!mappingFolder.endsWith(File.separator)) {
			mappingFolder += File.separator;
		}

		Map<String, LangTopicModel> models = new HashMap<String, LangTopicModel>();

		for (String l : langs) {
			FakeExtractorParameters parameters = null;
			try {
				parameters = new FakeExtractorParameters(l, baseFolder);
			} catch (Exception e) {
				logger.error(e.getMessage());
				continue;
			}
			if (parameters.getLang() == null) {
				logger.debug(l + " skipped");
				continue;
			}

			LangTopicModel langTopicModel = new LangTopicModel();
			langTopicModel.setOntology(ontology);

			// Categories
			langTopicModel.setCatFile(parameters.getWikipediaPageCategoryFileName());
			langTopicModel.setCatSuperFile(parameters.getWikipediaCategorySuperCategoryFileName());
			langTopicModel.setCatMapFile(mappingFolder + l + "wiki-cat2topic.properties");

			// Navigation templates
			langTopicModel.setNavFile(parameters.getWikipediaPageNavigationTemplateFileName());
			langTopicModel.setNavMapFile(mappingFolder + l + "wiki-navtpl2topic.properties");

			// Portals
			langTopicModel.setPortalFile(parameters.getWikipediaPagePortalFileName());
			langTopicModel.setPortalMapFile(mappingFolder + l + "wiki-portal2topic.properties");

			// Suffixes
			langTopicModel.setSuffixMapFile(mappingFolder + l + "wiki-suff2topic.properties");

			langTopicModel.load();
			models.put(l, langTopicModel);
		}

		if (interactive) {
			try {
				interactive(models);
				System.exit(1);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		if (outputFolder != null) {
			if (!outputFolder.endsWith(File.separator)) {
				outputFolder += File.separator;
			}

			// create folder with date
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Calendar cal = Calendar.getInstance();
			String today = dateFormat.format(cal.getTime());
			outputFolder += today + File.separator;

			File output = new File(outputFolder);
			if (!output.exists()) {
				output.mkdirs();
			}

			Map<String, BufferedWriter> languagesCSV = new HashMap<String, BufferedWriter>();
			Map<String, WeightedSetIndexer> languagesIndexes = new HashMap<String, WeightedSetIndexer>();

			File baseFolderFile = new File(baseFolder);
			File[] files = baseFolderFile.listFiles();
			for (File f : files) {
				String csvFile = output + File.separator + f.getName() + ".csv";
				String indexFolder = output + File.separator + f.getName();
				try {
					BufferedWriter writer = new BufferedWriter(new FileWriter(csvFile));
					WeightedSetIndexer indexer = new PageTopicIndexer(indexFolder, true);
					languagesCSV.put(f.getName(), writer);
					languagesIndexes.put(f.getName(), indexer);
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
			logger.info("Languages: " + languagesCSV.size());

			//todo: attenzione!!! se le pagine non esistono nella lingua con i mapping, il sistema fa casino.
			// guardare quello dell'estrazione di Airpedia per sistemare.
			try {
				IndexReader clschema = IndexReader.open(FSDirectory.open(new File(wikidataSchema)));
				int totalNumber = clschema.numDocs();
				for (int i = 0; i < totalNumber; i++) {
					Document d = clschema.document(i);

					if ((i + 1) % 1000 == 0) {
						System.out.print(".");
					}
					if ((i + 1) % 100000 == 0) {
						System.out.println(" " + (i + 1) + "/" + totalNumber);
					}

					logger.trace("wikiID: " + d.get("wikiID"));
					WeightedSet ws = new WeightedSet();
					for (String l : models.keySet()) {
						String page = d.get(l);
						if (page != null) {
							models.get(l).search(page, ws);
						}
					}

					if (ws.size() == 0) {
						continue;
					}

					TopicOntology.convertWeightedSet(ws, ontology);

					String rowWS = ws.toString(true);
					for (String l : languagesCSV.keySet()) {
						String page = d.get(l);
						if (page != null) {
							StringBuffer row = new StringBuffer();
							row.append(page).append("\t").append(rowWS).append("\n");

							// CSV file
							languagesCSV.get(l).append(row);

							// Index
							languagesIndexes.get(l).add(page, ws);
						}
					}

				}
				System.out.println();
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			try {
				logger.info("Closing buffers and indexes");
				for (String l : languagesCSV.keySet()) {
					languagesCSV.get(l).close();
				}
				for (String l:languagesIndexes.keySet()) {
					languagesIndexes.get(l).close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
	}
}
