/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.StringIndexer;
import eu.fbk.twm.utils.CommandLineWithLogger;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 1/24/13
 * Time: 6:47 PM
 * Extract the indexer for navigation templates.
 * You can extract the clean list of these templates, sorted by frequency, with the command:
 */

// cut -f 2 [template-page-navigation.csv] | sort | uniq -c | sort -nr | sed -e 's/^[ ]*//' | cut -f2- -d' '

public class PageRedirectIndexer extends StringIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageNavigationTemplateIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(PageRedirectIndexer.class.getName());

	public static final String PAGE_FIELD_NAME = "page";

	public static final String REDIRECTED_PAGE_FIELD_NAME = "links";

	public static final int REDIRECTED_PAGE_COLUMN_INDEX = 1;

	public static final int PAGE_COLUMN_INDEX = 0;


	public PageRedirectIndexer(String indexName) throws IOException {
		super(indexName, PAGE_FIELD_NAME, REDIRECTED_PAGE_FIELD_NAME);
	}

	public PageRedirectIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		super(indexName, keyFieldName, valueFieldName);
	}

	@Override
	public void index(String fileName, boolean compress) throws IOException {
		index(fileName, PAGE_COLUMN_INDEX, REDIRECTED_PAGE_COLUMN_INDEX, compress);
	}

	@Override
	public void index(File file, boolean compress) throws IOException {
		index(file, PAGE_COLUMN_INDEX, REDIRECTED_PAGE_COLUMN_INDEX, compress);
	}


	public static void main(String args[]) throws Exception {
		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		commandLineWithLogger.addOption(OptionBuilder.withArgName("index").hasArg().withDescription("create an index with the specified name").withLongOpt("index").create("i"));
		commandLineWithLogger.addOption(OptionBuilder.withArgName("file").hasArg().withDescription("read the key/value pairs to index from the specified file").withLongOpt("file").create("f"));

		commandLineWithLogger.addOption(OptionBuilder.withArgName("file").hasArg().withDescription("Wikipedia XML dump file").withLongOpt("dump").create("d"));
		commandLineWithLogger.addOption(OptionBuilder.withArgName("file").hasArg().withDescription("base Folder").withLongOpt("base").create("b"));

		commandLineWithLogger.addOption(OptionBuilder.withDescription("set compression to true (default is " + WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT + ")").withLongOpt("compress").create("c"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}

		String index = commandLine.getOptionValue("index");
		String file = commandLine.getOptionValue("file");

		String dump = commandLine.getOptionValue("dump");
		String base = commandLine.getOptionValue("base");

		boolean fileNull = (index == null || file == null);
		boolean dumpNull = (dump == null || base == null);

		if (fileNull && dumpNull) {
			throw new ParseException("You must specify either index/file or dump/base parameters.");
		}

		boolean compress = WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT;
		if (commandLine.hasOption("compress")) {
			compress = true;
		}

		PageRedirectIndexer pageFileIndexer = null;
		if (dump != null && base != null) {
			ExtractorParameters extractorParameters = new ExtractorParameters(dump, base, true);
			pageFileIndexer = new PageRedirectIndexer(extractorParameters.getWikipediaRedirIndexName());
			pageFileIndexer.index(extractorParameters.getWikipediaRedirFileName(), compress);
		}
		else {
			pageFileIndexer = new PageRedirectIndexer(index);
			pageFileIndexer.index(file, compress);
		}
		pageFileIndexer.close();
	}
}
