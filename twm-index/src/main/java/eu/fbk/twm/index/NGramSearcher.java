/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractSearcher;
import eu.fbk.twm.index.util.SerialUtils;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import eu.fbk.twm.utils.Defaults;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class NGramSearcher extends AbstractSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>NGramSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(NGramSearcher.class.getName());

	public static final int DEFAULT_MIN_FREQ = 1000;

	public static final boolean DEFAULT_THREAD_SAFE = false;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	//
	private static DecimalFormat rf = new DecimalFormat("0.000000");


	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected boolean threadSafe;

	private Map<String, Entry> cache;

	private Term keyTerm;

	public NGramSearcher(String indexName) throws IOException {
		this(indexName, DEFAULT_THREAD_SAFE);
	}

	public NGramSearcher(String indexName, boolean threadSafe) throws IOException {
		super(indexName);
		this.threadSafe = threadSafe;

		keyTerm = new Term(NGramIndexer.FORM_FIELD_NAME, "");
		logger.debug(keyTerm);
	}

	public void loadCache() throws IOException {
		loadCache(DEFAULT_MIN_FREQ);
	}

	public void loadCache(int minFreq) throws IOException {
		logger.info("loading cache (freq >" + minFreq + ")...");

		long begin = System.nanoTime();

		if (threadSafe) {
			logger.info(this.getClass().getName() + "'s cache is thread safe");
			cache = Collections.synchronizedMap(new HashMap<String, Entry>());
		}
		else {
			logger.warn(this.getClass().getName() + "'s cache isn't thread safe");
			cache = new HashMap<String, Entry>();
		}

		String form;
		int lfreq = 0, dfreq = 0;
		Entry result;
		Document doc;
		for (int i = 1; i < indexReader.numDocs(); i++) {
			doc = indexReader.document(i);
			form = doc.get(NGramIndexer.FORM_FIELD_NAME);
			lfreq = SerialUtils.toInt(doc.getBinaryValue(NGramIndexer.LINK_FREQ_FIELD_NAME));
			dfreq = SerialUtils.toInt(doc.getBinaryValue(NGramIndexer.DOCUMENT_FREQUENCY_FIELD_NAME));
			result = new Entry(lfreq, dfreq);
			if (dfreq > minFreq) {
				cache.put(form, result);
			}
			else {
				break;
			}

			if ((i % notificationPoint) == 0) {
				//System.out.print(CharacterTable.FULL_STOP);
				logger.debug(i + " keys read (" + cache.size() + ") " + new Date());
			}
		}

		System.out.print(CharacterTable.LINE_FEED);

		long end = System.nanoTime();
		logger.info(df.format(cache.size()) + " (" + df.format(indexReader.numDocs()) + ") keys cached in " + tf.format(end - begin) + " ns");
	}

	public Entry search(String key) {
		Entry result = null;
		if (cache != null) {
			result = cache.get(key);
		}

		//long end = System.nanoTime();
		if (result != null) {
			//logger.debug("found in cache in " + tf.format(end - begin) + " ns");
			return result;
		}

		try {
			//begin = System.nanoTime();
			TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(key));
			//end = System.nanoTime();
			//logger.debug("\"" + form + "\" sought in index in " + tf.format(end - begin) + " ns");

			int lf = 0, df = 0;
			if (termDocs.next()) {
				//begin = System.nanoTime();
				Document doc = indexReader.document(termDocs.doc());
				lf = SerialUtils.toInt(doc.getBinaryValue(NGramIndexer.LINK_FREQ_FIELD_NAME));
				df = SerialUtils.toInt(doc.getBinaryValue(NGramIndexer.DOCUMENT_FREQUENCY_FIELD_NAME));
				result = new Entry(lf, df);
				//end = System.nanoTime();
				//logger.debug("\"" + form + "\" deserialized in " + tf.format(end - begin) + " ns");

				return result;
			}
		} catch (IOException e) {
			logger.error(e);
		}

		return new Entry();
	}

	public void interactive() throws Exception {
		InputStreamReader indexReader = null;
		BufferedReader myInput = null;
		long begin = 0, end = 0;
		while (true) {
			System.out.println("\nPlease write a query and type <return> to continue (CTRL C to exit):");

			indexReader = new InputStreamReader(System.in);
			myInput = new BufferedReader(indexReader);
			String query = myInput.readLine().toString();
			begin = System.nanoTime();
			Entry e = search(query);
			end = System.nanoTime();
			logger.info("query\tdf\tlf\tratio\ttime");
			logger.info(query + "\t" + df.format(e.df()) + "\t" + df.format(e.lf()) + "\t" + rf.format((double) e.lf() / e.df()) + "\t" + tf.format(end - begin) + " ns");
		}
	}

	public class Entry {
		//
		private int df, lf;

		//
		public Entry() {
			this(0, 0);
		} // end constructor

		//
		public Entry(int lf, int df) {
			this.lf = lf;
			this.df = df;
		} // end constructor

		/**
		 * Returns the inverse document frequency
		 */
		public int df() {
			return df;
		} // end df

		/**
		 * Returns the frequency
		 */
		public int lf() {
			return lf;
		} // end lf

		//
		public String toString() {
			return String.valueOf(df + CharacterTable.HORIZONTAL_TABULATION + lf);
		} // end toString
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode").withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option searchOpt = OptionBuilder.withArgName("search").hasArg().withDescription("search for the specified key").withLongOpt("search").create("s");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			Option minimumKeyFreqOpt = OptionBuilder.withArgName("minimum-freq").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			//options.addOption(keyFieldNameOpt);
			//options.addOption(valueFieldNameOpt);
			options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			NGramSearcher ngramSearcher = new NGramSearcher(line.getOptionValue("index"));
			ngramSearcher.setNotificationPoint(notificationPoint);
			/*logger.debug(line.getOptionValue("key-field-name") + "\t" + line.getOptionValue("value-field-name"));
			if (line.hasOption("key-field-name"))
			{
				ngramSearcher.setKeyFieldName(line.getOptionValue("key-field-name"));
			}
			if (line.hasOption("value-field-name"))
			{
				ngramSearcher.setValueFieldName(line.getOptionValue("value-field-name"));
			} */
			if (line.hasOption("minimum-freq")) {
				ngramSearcher.loadCache(minFreq);
			}
			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				NGramSearcher.Entry result = ngramSearcher.search(line.getOptionValue("search"));
				logger.info(result);
			}
			if (line.hasOption("interactive-mode")) {
				ngramSearcher.interactive();
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.NGramSearcher", "\n", options, "\n", true);
		}
	}

}
