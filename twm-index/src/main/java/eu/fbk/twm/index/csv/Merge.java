/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xerial.snappy.SnappyInputStream;
import org.xerial.snappy.SnappyOutputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/19/13
 * Time: 11:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class Merge {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Merge</code>.
	 */
	static Logger logger = Logger.getLogger(Merge.class.getName());

	private Pattern tabPattern = Pattern.compile("\t");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private int notificationPoint;

	public static final int DEFAULT_NOTIFICATION_POINT = 1000000;

	public Merge(File file1, File file2, File out, int col) throws IOException {
		this(file1, file2, out, col, false);
	}

	public Merge(File file1, File file2, File out, int col, boolean compress) throws IOException {
		long begin = System.currentTimeMillis(), beginTotal = System.currentTimeMillis(), end = 0;
		long length1 = file1.length();
		long length2 = file2.length();
		logger.info("merging 2 " + (compress ? "compressed" : "uncompressed") + " files...");
		logger.info("file1:\t" + file1 + " (" + df.format(length1) + ")");
		logger.info("file2:\t" + file2 + " (" + df.format(length2) + ")");
		logger.info("out:\t" + out);
		notificationPoint = DEFAULT_NOTIFICATION_POINT;

		PrintWriter pw = null;
		LineNumberReader reader1 = null;
		LineNumberReader reader2 = null;

		//PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
		//LineNumberReader reader1 = new LineNumberReader(new InputStreamReader(new FileInputStream(file1), "UTF-8"));
		//LineNumberReader reader2 = new LineNumberReader(new InputStreamReader(new FileInputStream(file2), "UTF-8"));
		if (compress) {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new SnappyOutputStream(new FileOutputStream(out)), "UTF-8")));
			reader1 = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file1)), "UTF-8"));
			reader2 = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file2)), "UTF-8"));

		}
		else {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
			reader1 = new LineNumberReader(new InputStreamReader(new FileInputStream(file1), "UTF-8"));
			reader2 = new LineNumberReader(new InputStreamReader(new FileInputStream(file2), "UTF-8"));
		}
		int step = 1;
		String l1 = null, l2 = null;
		String[] s1 = null, s2 = null;

		int r1 = 0, r2 = 0, z = 0, r3 = 0;
		l1 = reader1.readLine();
		l2 = reader2.readLine();
		logger.info("step\tl1\tl2\tl3\tsum\ttime\tdate");
		while (true) {
			//logger.info(step + "\t[" + l1 + "]\t[" + l2 + "]");
			if (l1 != null && l2 != null) {
				s1 = tabPattern.split(l1);
				s2 = tabPattern.split(l2);

				if (Math.min(s1.length, s2.length) > col) {
					if (s1[col].compareTo(s2[col]) == 0) {
						pw.println(l1);
						pw.println(l2);
						l1 = reader1.readLine();
						l2 = reader2.readLine();
						r1++;
						r2++;
						r3 += 2;
					}
					else if (s1[col].compareTo(s2[col]) > 0) {
						pw.println(l2);
						l2 = reader2.readLine();
						r2++;
						r3++;
					}
					else {
						pw.println(l1);
						l1 = reader1.readLine();
						r1++;
						r3++;
					}
				}
				else {
					logger.error(s1.length + " or " + s2.length + " > " + col);
					System.exit(0);
					z++;
				}
			}
			else if (l1 != null && l2 == null) {
				pw.println(l1);
				l1 = reader1.readLine();
				r1++;
				r3++;
			}
			else if (l1 == null && l2 != null) {
				pw.println(l2);
				l2 = reader2.readLine();
				r2++;
				r3++;
			}
			else {
				break;
			}
			step++;

			if ((step % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(step) + "\t" + df.format(r1) + "\t" + df.format(r2) + "\t" + df.format(r3) + "\t" + df.format(r1 + r2) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}

			/*if ((step % notificationPoint) == 0)
			{
				System.out.print(".");
			}*/
		}
		//System.out.print("\n");
		//logger.info(df.format(step) + " steps done (" + r1 + ", " + r2 + ")");
		end = System.currentTimeMillis();
		logger.info(df.format(step) + "\t" + df.format(r1) + "\t" + df.format(r2) + "\t" + df.format(r3) + "\t" + df.format(r1 + r2) + "\t" + df.format(end - begin) + "\t" + new Date());

		if (z > 0) {
			logger.warn(z + " lines where the number of tabs < " + col);
		}
		reader1.close();
		reader2.close();
		//pw.flush();
		pw.close();
		long endTotal = System.currentTimeMillis();
		logger.info("merge done in " + df.format(endTotal - beginTotal) + " ms");
		logger.info("file1:\t" + file1 + " (" + df.format(file1.length()) + ")");
		logger.info("file2:\t" + file2 + " (" + df.format(file2.length()) + ")");
		logger.info("out:\t" + out + " (" + df.format(out.length()) + ", " + (length1 + length2) + ")");
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 5) {
			System.err.println("Wrong number of parameters " + args.length);
			System.err.println("Usage: java -mx512M org.fbk.cit.hlt.thewikimachine.csv.Merge file1 file2 out col compress");

			System.exit(-1);
		}

		new Merge(new File(args[0]), new File(args[1]), new File(args[2]), Integer.parseInt(args[3]), Boolean.parseBoolean(args[4]));

	}


}
