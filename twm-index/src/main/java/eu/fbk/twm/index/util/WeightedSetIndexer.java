/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import eu.fbk.twm.utils.WeightedSet;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class WeightedSetIndexer extends AbstractIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>FreqSetIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(WeightedSetIndexer.class.getName());

	public final static String DEFAULT_KEY_FIELD_NAME = "KEY";

	public final static String DEFAULT_VALUE_FIELD_NAME = "VALUE";

	protected String keyFieldName;

	protected String valueFieldName;

	protected static Pattern tabPattern = Pattern.compile("\t");

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public WeightedSetIndexer(String indexName) throws IOException {
		this(indexName, false);
	}

	public WeightedSetIndexer(String indexName, boolean clean) throws IOException {
		this(indexName, DEFAULT_KEY_FIELD_NAME, DEFAULT_VALUE_FIELD_NAME, clean);
	}

	public WeightedSetIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		this(indexName, keyFieldName, valueFieldName, false);
	}

	public WeightedSetIndexer(String indexName, String keyFieldName, String valueFieldName, boolean clean) throws IOException {
		super(indexName, clean);
		this.keyFieldName = keyFieldName;
		this.valueFieldName = valueFieldName;
	}

	public String getKeyFieldName() {
		return keyFieldName;
	}

	public void setKeyFieldName(String keyFieldName) {
		this.keyFieldName = keyFieldName;
	}

	public String getValueFieldName() {
		return valueFieldName;
	}

	public void setValueFieldName(String valueFieldName) {
		this.valueFieldName = valueFieldName;
	}

	public void add(String key, WeightedSet value) {
		Document doc = new Document();
		try {
			doc.add(new Field(keyFieldName, key, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field(valueFieldName, value.toByte(), Field.Store.YES));
			indexWriter.addDocument(doc);
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
