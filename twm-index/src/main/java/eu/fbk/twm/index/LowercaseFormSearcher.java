/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.SetSearcher;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.Defaults;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:05 PM
 * To change this template use File | Settings | File Templates.
 *
 * java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.LowercaseFormSearcher -i /data/models/wikipedia/en/20130604/enwiki-20130604-form-page-index/ -t
 *
 */
public class LowercaseFormSearcher extends SetSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>LowercaseFormSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(LowercaseFormSearcher.class.getName());

	public LowercaseFormSearcher(String indexName) throws IOException {
		super(indexName, LowercaseFormIndexer.LOWERCASE_FORM_FIELD_NAME, LowercaseFormIndexer.ENTRY_FIELD_NAME);
		logger.trace(toString(10));
	}

	@Override
	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString().toLowerCase();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				String[] result = search(s[0]);
				long end = System.nanoTime();

				if (result != null && result.length > 0) {
					for (int i = 0; i < result.length; i++) {
						logger.info(i + "\t" + result[i]);
					}
					logger.info(s[0] + " found in " + tf.format(end - begin) + " ns");

				}
				else {
					logger.info(s[0] + " not found in " + tf.format(end - begin) + " ns");
				}
			}
		}
	}
	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode").withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option searchOpt = OptionBuilder.withArgName("search").hasArg().withDescription("search for the specified key").withLongOpt("search").create("s");
			Option freqFileOpt = OptionBuilder.withArgName("key-freq").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			Option minimumKeyFreqOpt = OptionBuilder.withArgName("minimum-freq").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			options.addOption(freqFileOpt);
			//options.addOption(keyFieldNameOpt);
			//options.addOption(valueFieldNameOpt);
			options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			LowercaseFormSearcher pageFormSearcher = new LowercaseFormSearcher(line.getOptionValue("index"));
			pageFormSearcher.setNotificationPoint(notificationPoint);
			/*logger.debug(line.getOptionValue("key-field-name") + "\t" + line.getOptionValue("value-field-name"));
			if (line.hasOption("key-field-name"))
			{
				pageFormSearcher.setKeyFieldName(line.getOptionValue("key-field-name"));
			}
			if (line.hasOption("value-field-name"))
			{
				pageFormSearcher.setValueFieldName(line.getOptionValue("value-field-name"));
			} */
			if (line.hasOption("key-freq")) {
				pageFormSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
			}
			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				String[] result = pageFormSearcher.search(line.getOptionValue("search"));
				logger.info(Arrays.toString(result));
			}
			if (line.hasOption("interactive-mode")) {
				pageFormSearcher.interactive();
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.LowercaseFormSearcher", "\n", options, "\n", true);
		}
	}

}
