package eu.fbk.twm.index.util;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.FSDirectory;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 2/19/13
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShowAllInIndex {
	public static void main(String[] args) {
		String index = args[0];
		try {
			IndexReader indexReader = IndexReader.open(FSDirectory.open(new File(index)));
			for (int i = 0; i < indexReader.numDocs(); i++) {
				Document d = indexReader.document(i);
				System.out.println(d);
			}
			indexReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
