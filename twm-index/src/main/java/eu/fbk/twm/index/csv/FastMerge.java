/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.CharacterTable;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xerial.snappy.SnappyInputStream;
import org.xerial.snappy.SnappyOutputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/28/13
 * Time: 8:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class FastMerge {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>FastMerge</code>.
	 */
	static Logger logger = Logger.getLogger(FastMerge.class.getName());

	private Pattern tabPattern = Pattern.compile("\t");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private int notificationPoint;

	public static final int DEFAULT_NOTIFICATION_POINT = 1000000;

	private char separatorChar;

	private int col;

	private int size;

	public static final int DEFAULT_SEPARATOR_CHAR = CharacterTable.HORIZONTAL_TABULATION;

	public FastMerge(File file1, File file2, File out, int col, int size, boolean compress) throws IOException {
		this.col = col;
		this.size = size;

		long begin = System.currentTimeMillis(), beginTotal = System.currentTimeMillis(), end = 0;
		long length1 = file1.length();
		long length2 = file2.length();
		logger.info("merging 2 " + (compress ? "compressed" : "uncompressed") + " files...");
		logger.info("file1:\t" + file1 + " (" + df.format(length1) + ")");
		logger.info("file2:\t" + file2 + " (" + df.format(length2) + ")");
		logger.info("out:\t" + out);
		notificationPoint = DEFAULT_NOTIFICATION_POINT;
		separatorChar = DEFAULT_SEPARATOR_CHAR;
		PrintWriter pw = null;
		LineNumberReader reader1 = null;
		LineNumberReader reader2 = null;

		if (compress) {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new SnappyOutputStream(new FileOutputStream(out)), "UTF-8")));
			reader1 = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file1)), "UTF-8"));
			reader2 = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file2)), "UTF-8"));

		}
		else {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
			reader1 = new LineNumberReader(new InputStreamReader(new FileInputStream(file1), "UTF-8"));
			reader2 = new LineNumberReader(new InputStreamReader(new FileInputStream(file2), "UTF-8"));
		}
		int step = 0;
		List<Line> list1 = read(reader1);
		List<Line> list2 = read(reader2);
		List<Line> list3 = new ArrayList<Line>();
		//StringBuilder sb = new StringBuilder();
		int i1 = 0;
		int i2 = 0;
		int i3 = 0;
		Line l1;
		Line l2;
		int c;
		while (list1.size() > 0 && list2.size() > 0) {
			l1 = list1.get(i1);
			l2 = list2.get(i2);
			c = l1.compareTo(l2);
			if (c == 0) {
				/*sb.append(l1);
				sb.append(CharacterTable.LINE_FEED);
				sb.append(l2);
				sb.append(CharacterTable.LINE_FEED); */
				list3.add(l1);
				list3.add(l2);
				i1++;
				i2++;
				i3 += 2;
			}
			else if (c > 0) {
				/*sb.append(l2);
				sb.append(CharacterTable.LINE_FEED);*/
				list3.add(l2);
				i2++;
				i3++;
			}
			else {
				/*sb.append(l1);
				sb.append(CharacterTable.LINE_FEED);*/
				list3.add(l1);
				i1++;
				i3++;
			}

			if (i1 >= list1.size()) {
				list1 = read(reader1);
				i1 = 0;
			}

			if (i2 >= list2.size()) {
				list2 = read(reader2);
				i2 = 0;
			}

			//if (i3 > size)
			if (list3.size() > size) {
				//logger.info("writing " + df.format(i3) + " lines...");
				/*pw.print(sb.toString());
				sb = new StringBuilder();*/
				write(list3, pw);
				list3.clear();
				i3 = 0;
			}

			step++;

			if ((step % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(step) + "\t" + df.format(i1) + "\t" + df.format(i2) + "\t" + df.format(i3) + "\t" + df.format(i1 + i2) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}

		//if (i3 > size)
		if (list3.size() > 0) {
			write(list3, pw);
		}

		while ((list1 = read(reader1)).size() > 0) {
			write(list1, pw);
		}

		while ((list2 = read(reader2)).size() > 0) {
			write(list2, pw);
		}
		pw.close();
		reader1.close();
		reader2.close();

		long endTotal = System.currentTimeMillis();
		logger.info("merge done in " + df.format(endTotal - beginTotal) + " ms");
		logger.info("file1:\t" + file1 + " (" + df.format(file1.length()) + ")");
		logger.info("file2:\t" + file2 + " (" + df.format(file2.length()) + ")");
		logger.info("out:\t" + out + " (" + df.format(out.length()) + ", " + df.format(length1 + length2) + ")");

	}

	private void write(List<Line> list, PrintWriter pw) throws IOException {
		logger.info("writing " + list.size() + " lines...");
		for (int i = 0; i < list.size(); i++) {
			pw.println(list.get(i));
		}
	}

	private List<Line> read(LineNumberReader reader) throws IOException {
		long begin = System.currentTimeMillis(), beginTotal = System.currentTimeMillis(), end = 0;
		logger.info("reading " + df.format(size) + " lines starting from " + df.format(reader.getLineNumber()) + "... " + new Date());

		List<Line> list = new ArrayList<Line>();
		String line;
		int j = 0;
		logger.info("lines\tsize\ttime\tdate");
		while (j < size && (line = reader.readLine()) != null) {
			list.add(new Line(line));
			j++;

			if ((j % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(j) + "\t" + df.format(list.size()) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}
		long endTotal = System.currentTimeMillis();
		logger.info(df.format(list.size()) + " lines read in " + (endTotal - beginTotal) + " ms");
		return list;
	}

	class Line implements Comparable<Line> {
		String s;

		String key;

		Line(String s) {
			this.s = s;
			int start = findStart(s);
			int end = findEnd(s, start + 1);
			key = s.substring(start, end);
		}


		int findStart(String s) {
			if (col == 0) {
				return 0;
			}
			int count = 1;
			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == separatorChar) {
					if (count == col) {
						return i + 1;
					}
					count++;
				}
			}
			return 0;
		}

		int findEnd(String s, int start) {
			for (int i = start; i < s.length(); i++) {
				if (s.charAt(i) == separatorChar) {
					return i;
				}
			}
			return s.length();
		}

		public String getKey() {
			return key;
		}

		@Override
		public String toString() {
			return s;
		}

		@Override
		public int compareTo(Line line) {
			return key.compareTo(line.getKey());
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 6) {
			System.err.println("Wrong number of parameters " + args.length);
			System.err.println("Usage: java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.FastMerge file1 file2 out col size compress");

			System.exit(-1);
		}

		new FastMerge(new File(args[0]), new File(args[1]), new File(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), Boolean.parseBoolean(args[5]));

	}

		/*Line split(String s, char separatorChar)
	{
		List<String> senseList = new ArrayList<String>();
		char ch;
		int start = 0;
		for (int i = 0; i < s.length(); i++) {
			ch = s.charAt(i);
			if (ch == separatorChar)
			{
				senseList.add(s.substring(start, i));
				start = i + 1;
			}
		}
		return senseList.toArray(new String[senseList.size()]);
	} */

}
