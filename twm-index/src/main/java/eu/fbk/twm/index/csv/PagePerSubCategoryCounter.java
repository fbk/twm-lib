package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.*;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/2/13
 * Time: 6:53 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * This class extracts for each page the categories up to the specified depth.
 * It uses the in memory mapping between pages and categories and categories and super categories (read from file)
 * The depth threshold must be fixed in advance (7?)
 */
public class PagePerSubCategoryCounter extends AbstractCategoryExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PagePerSubCategoryCounter</code>.
	 */
	static Logger logger = Logger.getLogger(PagePerSubCategoryCounter.class.getName());

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	public static final int DEFAULT_DEPTH = 10;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	Map<String, Set<String>> pageCategoryMap;

	Map<String, Set<String>> categorySubCategoryMap;

	PrintWriter pageBottonCategoryWriter;

	FreqSet freqSet;

	public PagePerSubCategoryCounter(int numThreads) {
		super(numThreads);
		freqSet = new FreqSet();
	}


	void search(Set<String> categories, Set<String> set, int depth) {
		//logger.debug("searching: " + categories + "\t" + depth + "...");

		//if (depth > 5 && freqSet.size() > 0) {
		if (categories == null || depth > getMaxDepth()) {
			//logger.debug("{{{" + depth + "}}}");
			//logger.debug("stop1 " + depth);
			return;
		}

		Iterator<String> it = categories.iterator();
		for (int i = 0; it.hasNext(); i++) {
			//logger.debug(i + "\t" + categories[i]);
			//String label = topMap.get(categories[i]);
			String category = it.next();
			try {
				Set<String> superCategories = categorySubCategoryMap.get(category);
				if (superCategories != null) {
					//logger.debug(depth + "\t" + category + ": " + superCategories);
					//synchronized (this) {
					set.addAll(superCategories);
					//}

					search(superCategories, set, depth + 1);
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
		//logger.debug("stop2 " + depth);
		return;
	}

	@Override
	public void notification(int tot, long begin, long end) {
		//super.notification(tot, begin, end);    //To change body of overridden methods use File | Settings | File Templates.
		logger.info(df.format(freqSet.size()) + "\t" + df.format(tot) + "\t" + df.format(end - begin) + "\t" + new Date());
	}

	@Override
	public void processLine(String line) {
		//logger.debug("begin " +line);
		String[] t = tabPattern.split(line);
		Set<String> categories = pageCategoryMap.get(t[0]);
		/*if (categories != null) {
			synchronized (this) {
				freqSet.addAll(categories);
			}
		}
    */
		Set<String> allCategories = new HashSet<String>();
		if (categories != null) {
			allCategories.addAll(categories);
			search(categories, allCategories, 1);

			synchronized (this) {
				freqSet.addAll(allCategories);
			}
		}

	}

	Map<String, Set<String>> readMap(String name) throws IOException {
		logger.info("reading " + name + "...");
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));

		String line = null;
		int count = 0, key = 0, value = 1, part = 0, tot = 0;
		String oldKey = "";
		Set<String> set = new HashSet<String>();
		String[] t;
		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				if (!t[key].equals(oldKey)) {
					result.put(oldKey, set);
					count++;
					set = new HashSet<String>();
					part = 0;
				}
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last line
		result.put(oldKey, set);
		lnr.close();
		return result;
	}


	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			pageCategoryMap = readMap(extractorParameters.getWikipediaPageCategoryFileName());
			categorySubCategoryMap = readMap(extractorParameters.getWikipediaCategorySuperCategoryFileName());
			pageBottonCategoryWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaPagePerCategoryCountFileName()), "UTF-8")));

			//read a categories per line from a file and call for each category processLine
			read(extractorParameters.getWikipediaCategoryFileName());

		} catch (IOException e) {
			logger.error(e);
		}

	}

	@Override
	public void end() {
		logger.debug("writing " + freqSet.size() + " categories...");
		Iterator<String> it = freqSet.iterator();
		for (int i = 0; it.hasNext(); i++) {
			String category = it.next();
			int freq = freqSet.get(category);
			pageBottonCategoryWriter.print(category);
			pageBottonCategoryWriter.print(CharacterTable.HORIZONTAL_TABULATION);
			pageBottonCategoryWriter.println(freq);

			if ((i % DEFAULT_NOTIFICATION_POINT) == 0) {
				logger.debug(i + "/" + freqSet.size());
			}

		}
		pageBottonCategoryWriter.close();
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			//Option categoryMappingDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("directory from which to read the category mapping files").isRequired().withLongOpt("category-mapping-dir").create("c");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			Option maxDepthOpt = OptionBuilder.withArgName("int").hasArg().withDescription("recursion maximum category depth (default is " + AbstractCategoryExtractor.DEFAULT_MAX_CATEGORY_DEPTH + ")").withLongOpt("max-depth").create();
			Option interactiveModeOpt = OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create();

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(interactiveModeOpt);
			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			//options.addOption(categoryMappingDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(maxDepthOpt);
			options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);
			PagePerSubCategoryCounter pagePerCategoryCounter = new PagePerSubCategoryCounter(numThreads);
			if (line.hasOption("max-depth")) {
				pagePerCategoryCounter.setMaxDepth(Integer.parseInt(line.getOptionValue("max-depth")));
			}
			pagePerCategoryCounter.setNotificationPoint(notificationPoint);
			pagePerCategoryCounter.start(extractorParameters);


		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.PagePerSubCategoryCounter", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
