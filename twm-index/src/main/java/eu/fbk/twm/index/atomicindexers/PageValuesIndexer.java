package eu.fbk.twm.index.atomicindexers;

import eu.fbk.twm.index.util.SetIndexer;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 2/6/13
 * Time: 6:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageValuesIndexer extends SetIndexer {

	int keyCol = 0;
	int valueCol = 1;

	public PageValuesIndexer(String indexName) throws IOException {
		super(indexName, "page", "values");
	}

	public PageValuesIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		super(indexName, keyFieldName, valueFieldName);
	}

	@Override
	public void index(String fileName, boolean compress) throws IOException {
		index(new File(fileName), compress);
	}

	@Override
	public void index(File file, boolean compress) throws IOException {
		index(file, keyCol, valueCol, compress);
	}
}
