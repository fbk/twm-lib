package eu.fbk.twm.index.atomicindexers;

import eu.fbk.twm.index.util.AbstractIndexer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 2/28/13
 * Time: 9:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class FeatureSplitSchemaIndexer extends AbstractIndexer {

	public static String FIELD_ID_NAME = "id";
	public static String FIELD_VALUE_NAME = "value";
	public static String FIELD_TYPE_NAME = "type";

	public FeatureSplitSchemaIndexer(String indexName) throws IOException {
		super(indexName);
	}

	public void add(int id, String type, byte[] features) {

		Document d = new Document();

		d.add(new Field(FIELD_ID_NAME, Integer.toString(id), Field.Store.YES, Field.Index.NOT_ANALYZED));
		d.add(new Field(FIELD_TYPE_NAME, type, Field.Store.YES, Field.Index.NOT_ANALYZED));
		d.add(new Field(FIELD_VALUE_NAME, features, Field.Store.YES));
		try {
			synchronized (this) {
				indexWriter.addDocument(d);
			}
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}
}
