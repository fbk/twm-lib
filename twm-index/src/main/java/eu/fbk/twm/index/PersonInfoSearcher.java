/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractSearcher;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import eu.fbk.twm.utils.Defaults;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonInfoSearcher extends AbstractSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PersonInfoSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(PersonInfoSearcher.class.getName());

	public static final int DEFAULT_MIN_FREQ = 1000;

	public static final boolean DEFAULT_THREAD_SAFE = false;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	private static DecimalFormat rf = new DecimalFormat("0.000000");

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected boolean threadSafe;

	private Map<String, Entry> cache;

	private Term keyTerm;

	public PersonInfoSearcher(String indexName) throws IOException {
		this(indexName, DEFAULT_THREAD_SAFE);
	}

	public PersonInfoSearcher(String indexName, boolean threadSafe) throws IOException {
		super(indexName);

		this.threadSafe = threadSafe;

		keyTerm = new Term(PersonInfoIndexer.PAGE_FIELD_NAME, "");
		logger.debug(keyTerm);
		logger.trace(toString(10));
	}

	public Map<String, Entry> getCache() {
		return cache;
	}

	public void loadCache(String name) throws IOException {
		loadCache(new File(name));
	}

	public void loadCache(String name, int minFreq) throws IOException {
		loadCache(new File(name), minFreq);
	}

	public void loadCache(File f) throws IOException {
		loadCache(f, DEFAULT_MIN_FREQ);
	}

	public void loadCache(File f, int minFreq) throws IOException {
		logger.info("loading cache from " + f + " (freq>" + minFreq + ")...");
		long begin = System.nanoTime();

		if (threadSafe) {
			logger.info(this.getClass().getName() + "'s cache is thread safe");
			cache = Collections.synchronizedMap(new HashMap<String, Entry>());
		}
		else {
			logger.warn(this.getClass().getName() + "'s cache isn't thread safe");
			cache = new HashMap<String, Entry>();
		}

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;
		int i = 1;
		String[] t;
		int freq = 0;
		Entry entry;
		Document doc;
		TermDocs termDocs;
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				freq = Integer.parseInt(t[0]);
				if (freq < minFreq) {
					break;
				}
				termDocs = indexReader.termDocs(keyTerm.createTerm(t[1]));
				if (termDocs.next()) {
					doc = indexReader.document(termDocs.doc());
					entry = new Entry(doc.get(PersonInfoIndexer.PAGE_FIELD_NAME));
					entry.setFirstName(doc.get(PersonInfoIndexer.FIRST_NAME_FIELD_NAME));
					entry.setFamilyName(doc.get(PersonInfoIndexer.FAMILY_NAME_FIELD_NAME));
					entry.setBirthDate(doc.get(PersonInfoIndexer.BIRTH_DATE_FIELD_NAME));
					entry.setDeathDate(doc.get(PersonInfoIndexer.DEATH_DATE_FIELD_NAME));
					cache.put(t[1], entry);
				}
			}
			if ((i % notificationPoint) == 0) {
				//System.out.print(CharacterTable.FULL_STOP);
				logger.debug(i + " keys read (" + cache.size() + ") " + new Date());
			}
			i++;
		}
		System.out.print(CharacterTable.LINE_FEED);
		lnr.close();
		long end = System.nanoTime();
		logger.info(df.format(cache.size()) + " (" + df.format(indexReader.numDocs()) + ") keys cached in " + tf.format(end - begin) + " ns");
	}


	public Entry search(String key) {
		Entry entry = null;
		if (cache != null) {
			entry = cache.get(key);
		}

		//long end = System.nanoTime();
		if (entry != null) {
			//logger.debug("found in cache in " + tf.tokenizedFirstNameat(end - begin) + " ns");
			return entry;
		}

		try {
			//begin = System.nanoTime();
			TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(key));
			//end = System.nanoTime();
			//logger.debug("\"" + tokenizedFirstName + "\" sought in index in " + tf.tokenizedFirstNameat(end - begin) + " ns");

			if (termDocs.next()) {
				//begin = System.nanoTime();
				Document doc = indexReader.document(termDocs.doc());
				entry = new Entry(key);
				entry.setFirstName(doc.get(PersonInfoIndexer.FIRST_NAME_FIELD_NAME));
				entry.setFamilyName(doc.get(PersonInfoIndexer.FAMILY_NAME_FIELD_NAME));
				entry.setBirthDate(doc.get(PersonInfoIndexer.BIRTH_DATE_FIELD_NAME));
				entry.setDeathDate(doc.get(PersonInfoIndexer.DEATH_DATE_FIELD_NAME));
				//end = System.nanoTime();
				//logger.debug("\"" + tokenizedFirstName + "\" deserialized in " + tf.tokenizedFirstNameat(end - begin) + " ns");

				return entry;
			}
		} catch (IOException e) {
			logger.error(e);
		}

		return new Entry(key);
	}

	public boolean contains(String key) {
		Entry entry = null;
		if (cache != null) {
			entry = cache.get(key);
		}

		//long end = System.nanoTime();
		if (entry != null) {
			//logger.debug("found in cache in " + tf.tokenizedFirstNameat(end - begin) + " ns");
			return true;
		}

		try {
			//begin = System.nanoTime();
			TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(key));
			//end = System.nanoTime();
			//logger.debug("\"" + tokenizedFirstName + "\" sought in index in " + tf.tokenizedFirstNameat(end - begin) + " ns");

			if (termDocs.next()) {

				return true;
			}
		} catch (IOException e) {
			logger.error(e);
		}

		return false;
	}

	public class Entry {

		String page;

		String firstName;

		String familyName;

		String birthDate;

		String deathDate;

		public final static String UNKNOWN = "UNKNOWN";

		public Entry(String page) {
			this.page = page;
		}

		public boolean isEmpty() {
			return page == null;
		}

		public Entry(String page, String firstName, String familyName, String birthDate, String deathDate) {
			this.page = page;
			this.firstName = firstName;
			this.familyName = familyName;
			this.birthDate = birthDate;
			this.deathDate = deathDate;
		}

		public String getPage() {
			return page;
		}

		public void setPage(String page) {
			this.page = page;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getFamilyName() {
			return familyName;
		}

		public void setFamilyName(String familyName) {
			this.familyName = familyName;
		}

		public String getBirthDate() {
			return birthDate;
		}

		public void setBirthDate(String birthDate) {
			this.birthDate = birthDate;
		}

		public String getDeathDate() {
			return deathDate;
		}

		public void setDeathDate(String deathDate) {
			this.deathDate = deathDate;
		}

		@Override
		public String toString() {
			return "Entry{" +
							"page='" + page + '\'' +
							", firstName='" + firstName + '\'' +
							", familyName='" + familyName + '\'' +
							", birthDate='" + birthDate + '\'' +
							", deathDate='" + deathDate + '\'' +
							'}';
		}
	}

	public void interactive() throws Exception {
		InputStreamReader indexReader = null;
		BufferedReader myInput = null;
		long begin = 0, end = 0;
		while (true) {
			System.out.println("\nPlease write a query and type <return> to continue (CTRL C to exit):");

			indexReader = new InputStreamReader(System.in);
			myInput = new BufferedReader(indexReader);
			String query = myInput.readLine().toString();
			begin = System.nanoTime();
			Entry e = search(query);
			end = System.nanoTime();

			if (e.isEmpty()) {
				logger.info(query + " is not a person");
			}
			else {
				logger.info("query\tfirst\tfamily\tbirth\tdeath\ttime");
				logger.info(query + "\t" + e.getFirstName() + "\t" + e.getFamilyName() + "\t" + e.getBirthDate() + "\t" + e.getDeathDate() + "\t" + tf.format(end - begin) + " ns");

			}

		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode").withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option searchOpt = OptionBuilder.withArgName("search").hasArg().withDescription("search for the specified key").withLongOpt("search").create("s");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			Option minimumKeyFreqOpt = OptionBuilder.withArgName("minimum-freq").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option freqFileOpt = OptionBuilder.withArgName("key-freq").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version intokenizedFirstNameation and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			options.addOption(freqFileOpt);
			//options.addOption(valueFieldNameOpt);
			options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			PersonInfoSearcher personInfoSearcher = new PersonInfoSearcher(line.getOptionValue("index"));
			personInfoSearcher.setNotificationPoint(notificationPoint);
			/*logger.debug(line.getOptionValue("key-field-name") + "\t" + line.getOptionValue("value-field-name"));
			if (line.hasOption("key-field-name"))
			{
				ngramSearcher.setKeyFieldName(line.getOptionValue("key-field-name"));
			}
			if (line.hasOption("value-field-name"))
			{
				ngramSearcher.setValueFieldName(line.getOptionValue("value-field-name"));
			} */
			if (line.hasOption("key-freq")) {
				personInfoSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
			}
			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				Entry entry = personInfoSearcher.search(line.getOptionValue("search"));
				if (entry.isEmpty()) {
					logger.info(line.getOptionValue("search") + " is not a person");
				}
				else {
					logger.info(entry);
				}

			}
			if (line.hasOption("interactive-mode")) {
				personInfoSearcher.interactive();
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter tokenizedFirstNameatter = new HelpFormatter();
			tokenizedFirstNameatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PersonInfoSearcher", "\n", options, "\n", true);
		}
	}

}
