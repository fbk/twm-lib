/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.StringTable;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xerial.snappy.SnappyInputStream;
import org.xerial.snappy.SnappyOutputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/19/13
 * Time: 11:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class Sort {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Sort</code>.
	 */
	static Logger logger = Logger.getLogger(Sort.class.getName());

	private static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private File in;

	private File out;

	private int size;

	public static final int DEFAULT_SIZE = 20000000;

	private int col;

	private int notificationPoint;

	public static final int DEFAULT_NOTIFICATION_POINT = 1000000;

	private boolean compress;

	public Sort(String inName, String outName, int col, int size) throws IOException {
		this(new File(inName), new File(outName), col, size, false);
	}

	public Sort(String inName, String outName, int col, int size, boolean compress) throws IOException {
		this(new File(inName), new File(outName), col, size, compress);
	}

	public Sort(File in, File out, int col, int size) throws IOException {
		this(in, out, col, size, false);
	}

	public Sort(File in, File out, int col, int size, boolean compress) throws IOException {
		this.in = in;
		this.out = out;
		this.size = size;
		this.col = col;
		this.compress = compress;
		notificationPoint = DEFAULT_NOTIFICATION_POINT;
	}

	public int getNotificationPoint() {
		return notificationPoint;
	}

	public void setNotificationPoint(int notificationPoint) {
		this.notificationPoint = notificationPoint;
	}

	public void run() {
		long begin = System.currentTimeMillis();
		logger.info("sorting a " + (compress ? "compressed" : "uncompressed") + " file (" + new Date() + ")...");
		logger.info("size:\t" + df.format(size));
		logger.info("in:\t" + in + " (" + df.format(in.length()) + ")");
		logger.info("out:\t" + out);
		Stack<File> stack = new Stack<File>();
		LineNumberReader reader = null;
		try {
			//todo: check the reader
			//reader = new LineNumberReader(new FileReader(in));
			if (compress) {
				reader = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(in)), "UTF-8"));
			}
			else {
				reader = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
			}

			int step = 0;
			Map<String, List<String>> map = null;

			while ((map = read(reader, size, col, step)).size() > 0) {
				//File f = File.createTempFile(out.getName(), Integer.toString(step), out.getParentFile());
				//File f = File.createTempFile("sort", Integer.toString(step), out.getParentFile());
				String path = out.getParentFile().getAbsolutePath();
				if (!path.endsWith(File.separator)) {
					path += File.separator;
				}
				File f = new File(path + "sort-" + step);
				write(map, f);
				map = null;
				System.gc();
				stack.push(f);
				if (stack.size() >= 5) {

					merge(stack, col, step);
				}
				step++;

			}
			map = null;
			System.gc();
			merge(stack, col, step);
			File f = stack.pop();
			// ATTENZIONE NON VIENE RINOMINATO SE IL PATH DI OUT
			// NON E' ASSOLUTO.
			logger.info("renaming " + f + " to " + out + "...");
			f.renameTo(out);
			reader.close();
		} catch (IOException e) {
			logger.error(e);
		}

		long end = System.currentTimeMillis();
		logger.info("sorting done in " + df.format(end - begin) + " ms " + new Date());
		logger.info("in:\t" + in + " (" + df.format(in.length()) + ")");
		logger.info("out:\t" + out + " (" + df.format(out.length()) + ")");
	}

	private void merge(Stack<File> stack, int col, int step) throws IOException {
		long begin = System.currentTimeMillis();
		logger.info("merging " + stack.size() + " " + (compress ? "compressed" : "uncompressed") + " files... " + new Date());
		while (stack.size() > 1) {
			File f1 = stack.pop();
			File f2 = stack.pop();
			File f3 = File.createTempFile("merge", Integer.toString(step), f1.getParentFile());
			new Merge(f1, f2, f3, col, compress);
			logger.info("deleting " + f1 + "(" + f1.length() + "...");
			f1.delete();
			logger.info("deleting " + f2 + "(" + f2.length() + "...");
			f2.delete();
			logger.info("pushing " + f3 + "(" + f3.length() + "...");
			stack.push(f3);
		}

		long end = System.currentTimeMillis();
		logger.info("merge done in " + df.format(end - begin) + " ms " + new Date());
	}

	private Map<String, List<String>> read(LineNumberReader reader, int size, int col, int step) throws IOException {
		long begin = System.currentTimeMillis(), beginTotal = System.currentTimeMillis(), end = 0;
		logger.info("(" + step + ") sorting " + df.format(size) + " lines starting from " + df.format(reader.getLineNumber()) + "... " + new Date());

		Map<String, List<String>> map = new TreeMap<String, List<String>>();
		String line;
		int j = 0, z = 0;
		logger.info("lines\tsize\ttime\tdate");
		while (j < size && (line = reader.readLine()) != null) {
			//logger.info(j + "\t" + line);
			String[] s = tabPattern.split(line);

			if (s.length > col) {
				List<String> list = map.get(s[col]);
				if (list == null) {
					list = new ArrayList<String>();
					list.add(line);
					map.put(s[col], list);
				}
				else {
					list.add(line);
				}
			}
			else {
				z++;
			}

			j++;

			if ((j % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(j) + "\t" + df.format(map.size()) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}

			/*if ((j % notificationPoint) == 0)
			{
				System.out.print(".");
			}*/
		}
		//System.out.print("\n");
		//end = System.currentTimeMillis();
		//logger.info(df.format(j) + "\t" + df.format(map.size()) + "\t" + df.format(end - begin) + "\t" + new Date());

		if (z > 0) {
			logger.warn(df.format(z) + " lines where the number of \\t is lower than " + col);
		}
		long endTotal = System.currentTimeMillis();
		logger.info(df.format(size) + " lines sorted in " + df.format(endTotal - beginTotal) + " ms");
		return map;
	}

	private void write(Map<String, List<String>> map, File out) throws IOException {
		long begin = System.currentTimeMillis(), beginTotal = System.currentTimeMillis(), end = 0;
		logger.info("writing " + df.format(map.size()) + " unique keys in " + out + "...");
		//PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(out)));
		PrintWriter pw = null;
		if (compress) {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new SnappyOutputStream(new FileOutputStream(out)), "UTF-8")));
		}
		else {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
		}
		logger.info("lines\ttime\tdate");
		Iterator<String> it = map.keySet().iterator();
		int count = 0;
		while (it.hasNext()) {
			String term = it.next();
			List<String> list = map.get(term);
			for (int i = 0; i < list.size(); i++) {
				pw.println(list.get(i));
				count++;

				if ((count % notificationPoint) == 0) {
					end = System.currentTimeMillis();
					logger.info(df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
					begin = System.currentTimeMillis();
				}

				/*if ((count % notificationPoint) == 0)
				{
					System.out.print(".");
				}*/
			}

		}
		//pw.flush();
		pw.close();

		long endTotal = System.currentTimeMillis();
		logger.info(df.format(size) + " (" + df.format(count) + ") lines wrote in " + (endTotal - beginTotal) + " ms");
		logger.info("out:\t" + out + " (" + df.format(out.length()) + ")");

	}

	public static void main(String[] args) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 5) {
			logger.info("java -mx1G org.fbk.cit.hlt.thewikimachine.csv.Sort in-file out-file col size compress");
			System.exit(1);
		}

		File in = new File(args[0]);
		File out = new File(args[1]);
		int col = Integer.parseInt(args[2]);
		int size = Integer.parseInt(args[3]);
		boolean compress = Boolean.parseBoolean(args[4]);
		new Sort(in, out, col, size, compress).run();



		/*Thread t = new Sort(in, out, size, col);
		t.start();
		logger.debug("prima");
		t.join();
		logger.debug("dopo");
		//Thread.currentThread().join();   */
	}
}
