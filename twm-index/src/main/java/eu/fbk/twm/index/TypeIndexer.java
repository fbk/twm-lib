/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractIndexer;
import eu.fbk.twm.index.util.SerialUtils;
import eu.fbk.twm.utils.StringTable;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/24/13
 * Time: 11:14 PM
 * To change this template use File | Settings | File Templates.
 * @deprecated
 * @see PageTypeIndexer
 */
@Deprecated public class TypeIndexer extends AbstractIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TypeIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(TypeIndexer.class.getName());

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	public static final int NAM = 0;

	public static final int NOM = 1;

	public static final int FORM_COLUMN_INDEX = 2;

	public static final int PAGE_COLUMN_INDEX = 3;

	public static final String PAGE_FIELD_NAME = "page";

	public static final String FREQ_FIELD_NAME = "freq";

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public TypeIndexer(String indexName) throws IOException {
		super(indexName);
	}

	public void index(String fileName) throws IOException {
		index(new File(fileName));
	}

	public void index(File in) throws IOException {
		logger.info("reading " + in + "...");
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new FileReader(in));
		String line;
		int count = 0, tot = 0, part = 0;
		String oldKey = "";
		int[] frequencyArray = new int[2];
		String[] t;
		// read the first line
		if ((line = lnr.readLine()) != null) {
			try {
				t = tabPattern.split(line);
				if (t.length > 2) {
					if (isNam(t[FORM_COLUMN_INDEX])) {
						frequencyArray[NAM]++;
					}
					else {
						frequencyArray[NOM]++;
					}
					oldKey = t[PAGE_COLUMN_INDEX];
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + count);
				logger.error(e);
			} finally {
				tot++;
			}
		} // end if

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			try {
				t = tabPattern.split(line);
				if (t.length > 2) {
					if (!t[PAGE_COLUMN_INDEX].equals(oldKey)) {
						add(oldKey, frequencyArray);
						count++;

						frequencyArray = new int[2];
						part = 0;
					}
					if (isNam(t[FORM_COLUMN_INDEX])) {
						frequencyArray[NAM]++;
					}
					else {
						frequencyArray[NOM]++;
					}

					oldKey = t[PAGE_COLUMN_INDEX];
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + tot);
				logger.error(e);
			} finally {
				tot++;
			}

			//if (tot > 2000000)
			//	break;

			if ((tot % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + " lines indexed: " + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());
				begin = System.currentTimeMillis();
			}
		} // end while

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + " lines indexed: " + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last document
		add(oldKey, frequencyArray);
		lnr.close();
	}

	protected static boolean isNam(String s) {
		if (s.length() == 0) {
			return false;
		}
		char ch = s.charAt(0);
		if (Character.isLetter(ch)) {
			return Character.isUpperCase(ch);
		}

		return true;
	}

	private void add(String key, int[] frequencyArray) throws IOException {
		if (frequencyArray[NOM] > frequencyArray[NAM]) {
			Document doc = new Document();
			doc.add(new Field(PAGE_FIELD_NAME, key, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field(FREQ_FIELD_NAME, SerialUtils
                    .toByteArray((double) frequencyArray[NOM] / (frequencyArray[NOM] + frequencyArray[NAM])), Field.Store.YES));
			indexWriter.addDocument(doc);
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}


		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("create an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option inputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the key/value pairs to index from the specified file").withLongOpt("file").create("f");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(inputFileOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			TypeIndexer typeIndexer = new TypeIndexer(line.getOptionValue("index"));
			typeIndexer.index(line.getOptionValue("file"));
			typeIndexer.close();
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.TypeIndexer", "\n", options, "\n", true);
		}
	}
}
