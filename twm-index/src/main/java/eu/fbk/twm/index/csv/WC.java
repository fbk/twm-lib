/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xerial.snappy.Snappy;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/28/13
 * Time: 11:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class WC {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WC</code>.
	 */
	static Logger logger = Logger.getLogger(WC.class.getName());

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public WC(File in, boolean compress) throws IOException {
		long begin = System.currentTimeMillis(), beginTotal = System.currentTimeMillis(), end;
		logger.info("counting a " + (compress ? "compressed" : "uncompressed") + " file (" + new Date() + ")...");
		logger.info("in:\t" + in + " (" + df.format(in.length()) + ")");
		LineNumberReader reader = null;

		if (compress) {
			reader = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(in)), "UTF-8"));
		}
		else {
			reader = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		}
		String line;
		long l = 0;
		long w = 0;
		long b = 0;
		long c = 0;
		byte[] decompressed;
		byte[] compressed;
		while ((line = reader.readLine()) != null) {
			decompressed = line.getBytes("UTF-8");
			b += decompressed.length;
			compressed = Snappy.compress(decompressed);
			c += compressed.length;
			w += line.length();
			l++;

			if ((l % 10000000) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(l) + "\t" + df.format(w) + "\t" + df.format(b) + "\t" + df.format(c) + "\t" + df.format((double) c / b * 100) + "%\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}

		}
		end = System.currentTimeMillis();
		logger.info(df.format(l) + "\t" + df.format(w) + "\t" + df.format(b) + "\t" + df.format(c) + "\t" + df.format((double) c / b * 100) + "%\t" + df.format(end - begin) + "\t" + new Date());
		reader.close();
		long endTotal = System.currentTimeMillis();
		logger.info("counting done in " + df.format(endTotal - beginTotal) + " ms " + new Date());
		logger.info(df.format(l) + "\t" + df.format(w));


	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 2) {
			System.err.println("Wrong number of parameters " + args.length);
			System.err.println("Usage: java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.WC in compress");

			System.exit(-1);
		}

		new WC(new File(args[0]), Boolean.parseBoolean(args[1]));

	}

}

