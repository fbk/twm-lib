/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.FreqSet;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import org.apache.log4j.Logger;
import org.xerial.snappy.SnappyInputStream;
import org.xerial.snappy.SnappyOutputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/20/13
 * Time: 4:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileUtils {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>FileUtils</code>.
	 */
	static Logger logger = Logger.getLogger(FileUtils.class.getName());

	private static Pattern tabPattern = Pattern.compile("\t");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public static final int DEFAULT_NOTIFICATION_POINT = 1000000;

	public static void csort(String in, String out, int col, int size) throws IOException {
		csort(new File(in), new File(out), col, size);
	}

	public static void csort(File in, File out, int col, int size) throws IOException {
		new CSort(in, out, col, size).run();
	}

	public static void sort(String in, String out, int col, int size, boolean compress) throws IOException {
		sort(new File(in), new File(out), col, size, compress);
	}

	public static void sort(String in, String out, int col, int size) throws IOException {
		sort(new File(in), new File(out), col, size);
	}

	public static void sort(File in, File out, int col, int size) throws IOException {
		new Sort(in, out, col, size).run();
	}

	public static void sort(File in, File out, int col, int size, boolean compress) throws IOException {
		new Sort(in, out, col, size, compress).run();
	}


	public static void uniq(String in, String out, int col) throws IOException {
		uniq(new File(in), new File(out), col);
	}

	public static void uniq(File in, File out, int col) throws IOException {
		logger.info("reading " + in + "...");

		long begin = System.currentTimeMillis(), end = 0;
		FreqSet freqSet = new FreqSet();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		//Tokenizer tokenizer = HardTokenizer.getInstance();
		String line;
		int tot = 0;
		while ((line = lnr.readLine()) != null) {
			try {
				String[] t = tabPattern.split(line);
				if (t.length > col) {
					freqSet.add(t[col]);
				}

			} catch (Exception e) {
				logger.error("Error at line " + tot);
				logger.error(e);
			} finally {
				tot++;
			}

			if ((tot % DEFAULT_NOTIFICATION_POINT) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(freqSet.size()) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}
		lnr.close();
		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(freqSet.size()) + "\t" + df.format(end - begin) + "\t" + new Date());

		logger.info("sorting " + in + "...");
		begin = System.currentTimeMillis();
		SortedMap<Integer, List<String>> sortedMap = freqSet.toSortedMap();
		writeSortedMap(sortedMap, out);
		end = System.currentTimeMillis();
		logger.info(sortedMap.size() + " lines sorted in " + df.format(end - begin) + " ms " + new Date());
	}

	public static void df(File in, File out) throws IOException {
		logger.info("reading " + in + "...");

		long begin = System.currentTimeMillis(), end = 0;
		FreqSet freqSet = new FreqSet();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		Tokenizer tokenizer = HardTokenizer.getInstance();
		String line;
		int tot = 0;
		while ((line = lnr.readLine()) != null) {
			try {
				String[] t = tabPattern.split(line);
				if (t.length > 0) {

					String[] tokenArray = tokenizer.stringArray(t[0]);
					for (int j = 0; j < tokenArray.length; j++) {
						freqSet.add(tokenArray[j]);
					}

				}

			} catch (Exception e) {
				logger.error("Error at line " + tot);
				logger.error(e);
			} finally {
				tot++;
			}

			if ((tot % DEFAULT_NOTIFICATION_POINT) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(freqSet.size()) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}
		lnr.close();
		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(freqSet.size()) + "\t" + df.format(end - begin) + "\t" + new Date());

		logger.info("sorting " + in + "...");
		begin = System.currentTimeMillis();
		SortedMap<Integer, List<String>> sortedMap = freqSet.toSortedMap();
		writeSortedMap(sortedMap, out);
		end = System.currentTimeMillis();
		logger.info(df.format(sortedMap.size()) + " distinct frequencies found in " + df.format(end - begin) + " ms " + new Date());
	}

	private static void writeSortedMap(SortedMap<Integer, List<String>> sortedMap, File out) throws IOException {
		logger.info("writing " + out + "...");
		long begin = System.currentTimeMillis();
		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
		Iterator<Integer> it = sortedMap.keySet().iterator();
		for (int i = 0; it.hasNext(); i++) {
			Integer freq = it.next();
			List<String> list = sortedMap.get(freq);
			for (int j = 0; j < list.size(); j++) {
				pw.print(freq);
				pw.print(CharacterTable.HORIZONTAL_TABULATION);
				pw.println(list.get(j));
			}
		}
		long end = System.currentTimeMillis();
		logger.info(df.format(sortedMap.size()) + " distinct frequencies in " + df.format(end - begin) + " ms " + new Date());
		pw.close();
	}

	public static void filter(String in, String out, FreqSet formFreqSet, int col, int minimumFreq) throws IOException {
		filter(new File(in), new File(out), formFreqSet, col, minimumFreq, false);
	}

	public static void filter(String in, String out, FreqSet formFreqSet, int col, int minimumFreq, boolean compress) throws IOException {
		filter(new File(in), new File(out), formFreqSet, col, minimumFreq, compress);
	}

	public static void filter(File in, File out, FreqSet formFreqSet, int col, int minimumFreq) throws IOException {
		filter(in, out, formFreqSet, col, minimumFreq, false);
	}

	public static void filter(File in, File out, FreqSet formFreqSet, int col, int minimumFreq, boolean compress) throws IOException {
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		PrintWriter pw = null;
		//LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		//PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));

		if (compress) {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new SnappyOutputStream(new FileOutputStream(out)), "UTF-8")));
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(in)), "UTF-8"));
		}
		else {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		}

		int totalFreq;
		String line;
		int tot = 0;
		int count = 0;
		String[] t;
		logger.info("total\tcount\ttime (ms)\tdate");
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length >= col) {
				totalFreq = formFreqSet.get(t[col]);
				if (totalFreq >= minimumFreq) {
					pw.println(line);
					count++;
				}
			}
			tot++;
			if ((tot % DEFAULT_NOTIFICATION_POINT) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}

		}
		lnr.close();
		pw.close();
		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
	}
}
