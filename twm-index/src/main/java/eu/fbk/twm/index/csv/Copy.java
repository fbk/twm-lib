/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xerial.snappy.SnappyInputStream;
import org.xerial.snappy.SnappyOutputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/28/13
 * Time: 1:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class Copy {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Copy</code>.
	 */
	static Logger logger = Logger.getLogger(Copy.class.getName());

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public Copy(File in, File out, int from, int to, boolean compress) throws IOException {
		long begin = System.currentTimeMillis(), beginTotal = System.currentTimeMillis(), end;
		logger.info("counting " + (to - from) + " (" + from + ", " + to + ") lines from  a " + (compress ? "compressed" : "uncompressed") + " file (" + new Date() + ")...");
		logger.info("in:\t" + in + " (" + df.format(in.length()) + ")");
		logger.info("out:\t" + out);
		LineNumberReader reader = null;
		PrintWriter pw = null;

		if (compress) {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new SnappyOutputStream(new FileOutputStream(out)), "UTF-8")));
			reader = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(in)), "UTF-8"));
		}
		else {
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
			reader = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		}
		String line;
		long l = 0;
		while ((line = reader.readLine()) != null) {
			if (l >= from) {
				pw.println(line);
			}

			l++;

			if (l >= to) {
				break;
			}

			if ((l % 10000000) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(l) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}

		}
		end = System.currentTimeMillis();
		logger.info(df.format(l) + "\t" + df.format(end - begin) + "\t" + new Date());

		reader.close();
		pw.close();
		long endTotal = System.currentTimeMillis();
		logger.info("copied " + l + " lines (" + from + ", " + to + ") in " + df.format(endTotal - beginTotal) + " ms " + new Date());
		logger.info("in:\t" + in + " (" + df.format(in.length()) + ")");
		logger.info("out:\t" + out + " (" + df.format(in.length()) + ")");
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 5) {
			System.err.println("Wrong number of parameters " + args.length);
			System.err.println("Usage: java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.Copy in out from to compress");

			System.exit(-1);
		}

		new Copy(new File(args[0]), new File(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Boolean.parseBoolean(args[4]));

	}
}
