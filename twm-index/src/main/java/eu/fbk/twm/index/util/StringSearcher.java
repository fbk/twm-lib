/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import eu.fbk.twm.utils.StringTable;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import java.io.*;
import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:11 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class StringSearcher extends AbstractSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>IntSetSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(StringSearcher.class.getName());

	public static final int DEFAULT_MIN_FREQ = 1000;

	public static final boolean DEFAULT_THREAD_SAFE = false;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected boolean threadSafe;

	protected Term keyTerm;

	protected String keyFieldName;

	protected String valueFieldName;

	protected StringSearcher(String indexName) throws IOException {
		this(indexName, FreqSetIndexer.DEFAULT_KEY_FIELD_NAME, FreqSetIndexer.DEFAULT_VALUE_FIELD_NAME, DEFAULT_THREAD_SAFE);
	}

	protected StringSearcher(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		this(indexName, keyFieldName, valueFieldName, false);
	}

	protected StringSearcher(String indexName, String keyFieldName, String valueFieldName, boolean threadSafe) throws IOException {
		super(indexName);
		this.threadSafe = threadSafe;
		this.keyFieldName = keyFieldName;
		this.valueFieldName = valueFieldName;
		logger.debug(keyFieldName + "\t" + valueFieldName);

		keyTerm = new Term(keyFieldName, "");
		logger.debug(keyTerm);
	}

	public void setKeyFieldName(String keyFieldName) {
		keyTerm = new Term(keyFieldName, "");
		this.keyFieldName = keyFieldName;
	}

	public String getKeyFieldName() {
		return keyFieldName;
	}

	public String getValueFieldName() {
		return valueFieldName;
	}

	public void setValueFieldName(String valueFieldName) {
		this.valueFieldName = valueFieldName;
	}


	public String search(String key) {
		String result = null;

		try {
			TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(key));

			if (termDocs.next()) {
				Document doc = indexReader.document(termDocs.doc());
				result = doc.get(valueFieldName);
			}
		} catch (IOException e) {
			logger.error(e);
		}

		return result;
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				String result = search(s[0]);
				long end = System.nanoTime();

				if (result != null) {
					logger.info("Result: " + result);
					logger.info(s[0] + " found in " + tf.format(end - begin) + " ns");

				}
				else {
					logger.info(s[0] + " not found in " + tf.format(end - begin) + " ns");
				}
			}
		}
	}

	private int[] fromByte(byte[] byteArray) throws IOException {
		ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);
		DataInputStream dataStream = new DataInputStream(byteStream);

		int size = dataStream.readInt();
		int[] entryArray = new int[size];
		for (int j = 0; j < size; j++) {
			entryArray[j] = dataStream.readInt();
		}

		return entryArray;
	}

}
