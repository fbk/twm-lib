/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/19/13
 * Time: 11:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class CSort {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CSort</code>.
	 */
	static Logger logger = Logger.getLogger(CSort.class.getName());

	private static Pattern spacePattern = Pattern.compile(" ");

	private static Pattern tabPattern = Pattern.compile("\t");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public static final int BUFFER_SIZE = 1024;

	public static final int DEFAULT_SIZE = 10000000;

	private File in;

	private File out;

	private int size;

	private int col;

	private int notificationPoint;

	public static final int DEFAULT_NOTIFICATION_POINT = 1000000;

	public CSort(String inName, String outName, int col, int size) throws IOException {
		this(new File(inName), new File(outName), col, size);
	}

	public CSort(File in, File out, int col, int size) throws IOException {
		this.in = in;
		this.out = out;
		this.size = size;
		this.col = col;
		notificationPoint = DEFAULT_NOTIFICATION_POINT;
	}

	public int getNotificationPoint() {
		return notificationPoint;
	}

	public void setNotificationPoint(int notificationPoint) {
		this.notificationPoint = notificationPoint;
	}

	public void run() {
		long begin = System.currentTimeMillis();
		logger.info("sorting " + in + " (" + out + ", " + df.format(size) + ", " + col + " " + new Date() + ")...");

		Stack<File> stack = new Stack<File>();
		LineNumberReader reader = null;
		try {
			reader = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
			int step = 0;
			Map<String, List<byte[]>> map = null;

			while ((map = read(reader, size, col)).size() > 0) {
				File f = File.createTempFile(out.getName(), Integer.toString(step), out.getParentFile());
				write(map, f);

				stack.push(f);
				if (stack.size() > 5) {
					map = null;
					merge(stack, col);
				}
				step++;
			}

			map = null;
			merge(stack, col);
			File f = stack.pop();
			// ATTENZIONE NON VIENE RINOMINATO SE IL PATH DI OUT
			// NON E' ASSOLUTO.
			logger.info("renaming " + f + " to " + out + "...");
			f.renameTo(out);
			reader.close();
		} catch (IOException e) {
			logger.error(e);
		}

		long end = System.currentTimeMillis();
		logger.info("process done in " + df.format(end - begin) + " ms " + new Date());
	}

	private void merge(Stack<File> stack, int col) throws IOException {
		long begin = System.currentTimeMillis();
		logger.info("merging " + stack.size() + " files... " + new Date());
		while (stack.size() > 1) {
			File f1 = stack.pop();
			File f2 = stack.pop();
			File f3 = File.createTempFile("merge", "", f1.getParentFile());
			new Merge(f1, f2, f3, col);
			logger.debug("deleting " + f1 + "...");
			f1.delete();
			logger.debug("deleting " + f2 + "...");
			f2.delete();
			logger.debug("pushing " + f3 + "...");
			stack.push(f3);
		}

		long end = System.currentTimeMillis();
		logger.info("merge done in " + df.format(end - begin) + " ms " + new Date());

	}

	private Map<String, List<byte[]>> read(LineNumberReader reader, int size, int col) throws IOException {
		long begin = System.currentTimeMillis();
		logger.info("sorting " + df.format(size) + " lines starting from " + reader.getLineNumber() + "... " + new Date());

		Map<String, List<byte[]>> map = new TreeMap<String, List<byte[]>>();
		String line;
		int j = 0, z = 0;

		while (j < size && (line = reader.readLine()) != null) {
			//logger.info(j + "\t" + line);
			String[] s = tabPattern.split(line);

			if (s.length > col) {
				List<byte[]> list = map.get(s[col]);
				if (list == null) {
					list = new ArrayList<byte[]>();
					//senseList.add(line.getBytes("UTF-8"));
					map.put(s[col], list);
				}
				list.add(zip(line));
			}
			else {
				z++;
			}

			j++;

			if ((j % notificationPoint) == 0) {
				System.out.print(".");
			}
		}
		System.out.print("\n");
		long end = System.currentTimeMillis();
		logger.info("sorted " + df.format(j) + " lines in " + df.format(end - begin) + " ms " + new Date());
		logger.info("found " + df.format(map.size()) + " unique keys");
		if (z > 0) {
			logger.warn(df.format(z) + " lines where the number of \\t is lower than " + col);
		}
		return map;
	}

	private void write(Map<String, List<byte[]>> map, File out) throws IOException {
		long begin = System.currentTimeMillis();
		logger.info("writing " + df.format(map.size()) + " unique keys in " + out + "...");
		//PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(out)));
		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
		Iterator<String> it = map.keySet().iterator();
		int count = 0;
		while (it.hasNext()) {
			String term = it.next();
			List<byte[]> list = map.get(term);

			for (int i = 0; i < list.size(); i++) {
				//pw.println(new String(senseList.get(i), "UTF-8"));
				pw.println(new String(unzip(list.get(i))));
				count++;

				if ((count % notificationPoint) == 0) {
					System.out.print(".");
				}
			}

		}
		pw.flush();
		pw.close();
		long end = System.currentTimeMillis();
		logger.info("wrote " + df.format(count) + " lines in " + df.format(end - begin) + " ms");

	}

	public static byte[] zip(String s) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		GZIPOutputStream gzip = new GZIPOutputStream(out);
		byte[] b = s.getBytes("UTF-8");
		gzip.write(b, 0, b.length);
		gzip.close();

		return out.toByteArray();
	}

	public static String unzip(byte[] b) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayInputStream in = new ByteArrayInputStream(b);
		GZIPInputStream gzip = new GZIPInputStream(in);
		byte[] buffer = new byte[BUFFER_SIZE];
		int length;
		while ((length = gzip.read(buffer, 0, BUFFER_SIZE)) != -1) {
			out.write(buffer, 0, length);
		}
		gzip.close();
		out.close();

		return out.toString("UTF-8");
	}

	public static void main(String[] args) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 4) {
			logger.info("java -mx1G org.fbk.cit.hlt.thewikimachine.csv.CSort in-file out-file col size");
			System.exit(1);
		}

		File in = new File(args[0]);
		File out = new File(args[1]);
		int size = Integer.parseInt(args[3]);
		int col = Integer.parseInt(args[2]);
		new CSort(in, out, col, size).run();

	}
}
