package eu.fbk.twm.index;


import eu.fbk.twm.index.atomicindexers.IDValueIndexer;
import eu.fbk.twm.index.atomicindexers.PageValuesIndexer;
import eu.fbk.twm.index.atomicindexers.ValuePagesIndexer;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 2/7/13
 * Time: 8:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class GenericValuesIndexer {

	public GenericValuesIndexer(String fileOrderPage, String fileOrderValue, String indexP2VFileName, String indexV2PFileName, String indexIDFileName) throws IOException {
		PageValuesIndexer index1 = new PageValuesIndexer(indexP2VFileName);
		index1.index(fileOrderPage, false);
		index1.close();

		ValuePagesIndexer index2 = new ValuePagesIndexer(indexV2PFileName);
		index2.index(fileOrderValue, false);
		index2.close();

		IDValueIndexer index3 = new IDValueIndexer(indexIDFileName);
		index3.index(fileOrderValue);
		index3.close();
	}
}
