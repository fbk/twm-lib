/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.SetIndexer;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 4:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class LowercaseFormIndexer extends SetIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>LowercaseFormIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(LowercaseFormIndexer.class.getName());

	public static final int LOWERCASE_FORM_COLUMN_INDEX = 0;

	public static final int FORM_COLUMN_INDEX = 1;

	public static final String LOWERCASE_FORM_FIELD_NAME = "form";

	public static final String ENTRY_FIELD_NAME = "entry";

	public LowercaseFormIndexer(String indexName) throws IOException {
		super(indexName, LOWERCASE_FORM_FIELD_NAME, ENTRY_FIELD_NAME);
	}

	public LowercaseFormIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		super(indexName, keyFieldName, valueFieldName);
	}

	@Override
	public void index(String fileName, boolean compress) throws IOException {
		index(fileName, LOWERCASE_FORM_COLUMN_INDEX, FORM_COLUMN_INDEX, compress);
	}

	@Override
	public void index(File file, boolean compress) throws IOException {
		index(file, LOWERCASE_FORM_COLUMN_INDEX, FORM_COLUMN_INDEX, compress);
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}


		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("create an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option inputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the key/value pairs to index from the specified file").withLongOpt("file").create("f");
			Option compressOpt = OptionBuilder.withArgName("compress").withDescription("the input file is compressed (default is " + WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT + ")").withLongOpt("compress").create("c");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			//Option freqFileOpt = OptionBuilder.withArgName("key-freq").withDescription("key frequency file").withLongOpt("key-freq").create("f");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(inputFileOpt);
			options.addOption(compressOpt);
			//options.addOption(valueFieldNameOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			boolean compress = WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT;
			if (line.hasOption("compress")) {
				compress = true;
			}

			LowercaseFormIndexer formPageIndexer = new LowercaseFormIndexer(line.getOptionValue("index"));
			/*if (line.hasOption("key-field-name")) {
				pageFormIndexer.setKeyFieldName(line.getOptionValue("key-field-name"));
			}
			if (line.hasOption("value-field-name")) {
				pageFormIndexer.setKeyFieldName(line.getOptionValue("value-field-name"));
			}*/
			formPageIndexer.index(line.getOptionValue("file"), compress);
			formPageIndexer.close();
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.LowercaseFormIndexer", "\n", options, "\n", true);
		}
	}
}
