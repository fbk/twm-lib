/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.similarity;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.index.PageIncomingSearcher;
import eu.fbk.twm.index.PageOutgoingSearcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/31/13
 * Time: 11:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class IncomingOutgoingSimilarity {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>IncomingOutgoingSimilarity</code>.
	 */
	static Logger logger = Logger.getLogger(IncomingOutgoingSimilarity.class.getName());

	private PageIncomingSearcher incomingSearcher;

	private PageOutgoingSearcher outgoingSearcher;

	private static DecimalFormat decimalFormat = new DecimalFormat("0.00");

	private static Pattern tabPattern = Pattern.compile("\t");

	private long st, it;

	private static DecimalFormat nf = new DecimalFormat("000,000,000.#");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###.000000");

	//
	private static final double LOG_2 = Math.log(2);

	//
	private static final double NUMBER_OF_PAGES = 10000000.0;


	public IncomingOutgoingSimilarity(String outgoingIndexDir, String incomingIndexDir, boolean ram) throws IOException {
		this(outgoingIndexDir, incomingIndexDir, ram, false);
	}

	public IncomingOutgoingSimilarity(String outgoingIndexDir, String incomingIndexDir, boolean ram, boolean threadSafe) throws IOException {
		outgoingSearcher = new PageOutgoingSearcher(outgoingIndexDir);
		incomingSearcher = new PageIncomingSearcher(incomingIndexDir);
	}

	public int[] outgoingLinks(String q) {
		return outgoingSearcher.search(q);
	} // end outgoingLinks

	//
	public int[] incomingLinks(String q) {
		return incomingSearcher.search(q);
	} // end incomingLinks

	/**
	 * Returns the number of items the two array shares.
	 * To speed up the execution <code>a1</code> <b>must</b>
	 * be shorter than <code>a2</code> and the <b>must</b>
	 * be sorted.
	 *
	 * @return the number of items the two sets shares.
	 */
	private static int intersectCount(int[] a1, int[] a2) {
		//long begin = System.nanoTime();
		int count = 0;
		int l = 0;
		int s = -a2.length - 1;
		//logger.info("l=" + l + "\ts=" + s);

		for (int i = 0; i < a1.length; i++) {
			//logger.info("i=" + i + "\tai=" + a1[i]);

			l = Arrays.binarySearch(a2, a1[i]);
			if (l >= 0) {
				count++;
			}
			else if (l <= s) {
				break;
			}
		} // end for i

		//long end = System.nanoTime();
		//logger.info(iter + " interation ("+ (a1.length * a2.length) + ") done in " + nf.format(end - begin) + " ns");

		//logger.debug("intersection " + arrayToString(a1));
		//logger.debug("intersection " + arrayToString(a2));

		return count;
	} // end intersectCount

	/**
	 * Returns the number of items the two array shares.
	 * Baseline implementation.
	 *
	 * @return the number of items the two sets shares.
	 */
	private int basicIntersectCount(int[] a1, int[] a2) {
		//long begin = System.nanoTime();
		logger.debug("intersection " + arrayToString(a1));
		logger.debug("intersection " + arrayToString(a2));
		int count = 0;
		//int s = 0;
		for (int i = 0; i < a1.length; i++) {
			for (int j = 0; j < a2.length; j++) {
				if (a1[i] == a2[j]) {
					count++;
					logger.info(count + "\t" + a1[i] + "\t" + a2[j]);
				}


			} // end for j

		} // end for i

		//long end = System.nanoTime();
		//logger.info("intersection between " + set1.size() + ", " + set2.size() + " done in " + nf.format(end - begin) + " ns");

		return count;
	} // end intersectCount


	//
	private int contains(int[] a, int i) {
		return contains(a, 0, i);
	} // end contains

	//
	private static String arrayToString(int[] a) {
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < a.length; j++) {
			if (j > 0) {
				sb.append(", ");
			}
			sb.append(a[j]);
		}
		return sb.toString();
	} // end arrayToString

	//
	private int contains(int[] a, int s, int i) {
		for (int j = s; j < a.length; j++) {
			if (a[j] == i) {
				return j;
			}
			else if (i < a[j]) {
				return (-j - 1);
			}

		} // end for j
		return 0;
	} // end contains

	/**
	 * Returns the correlation of a page with the other
	 * pages in the list. The correlation between two pages is
	 * the number of incoming and outgoing links they share.
	 * The resulting correlation is the sum of the correlation
	 * with all pages excluding itself.
	 * Note the page could be repetead in the list, this could cause
	 * problems. To be fix.
	 *
	 * @param list the list of pages.
	 * @return the array of correlations.
	 */
	public double[] sumCorrelation(List<String> list) throws IOException//, ParseException
	{
		String[] s = new String[list.size()];
		for (int i = 0; i < s.length; i++) {
			s[i] = list.get(i);
		}
		return sumCorrelation(s);
	} // end sumCorrelation

	/**
	 * Returns the correlation of a page with the other
	 * pages in the array. The correlation between two pages is
	 * the number of incoming and outgoing links they share.
	 * The resulting correlation is the sum of the correlation
	 * with all pages excluding itself.
	 * Note the page could be repetead in the array, this could cause
	 * problems. To be fix.
	 *
	 * @return the array of correlations.
	 */
	public double[] sumCorrelation(String[] s) throws IOException//, ParseException
	{
		double[][] result = compare(s);
		logger.debug("Correlation Matrix:\n" + matrixToString(s, result));
		double[] sum = new double[s.length];

		for (int i = 0; i < s.length; i++) {
			sum[i] = 0;
			for (int j = 0; j < s.length; j++) {
				if (result[i][j] != 1) {
					sum[i] += result[i][j];
				}

			} // end for j

		} // end for i
		return sum;
	} // end sumCorrelation

	//
	public double[] maxCorrelation(List<String> list) throws IOException//, ParseException
	{
		String[] s = new String[list.size()];
		for (int i = 0; i < s.length; i++) {
			s[i] = list.get(i);
		}
		return maxCorrelation(s);
	} // end maxCorrelation

	//
	public double[] maxCorrelation(String[] s) throws IOException//, ParseException
	{
		double[][] result = compare(s);
		double[] max = new double[s.length];

		for (int i = 0; i < s.length; i++) {
			max[i] = 0;
			for (int j = 0; j < s.length; j++) {

				if (i != j) {
					if (result[i][j] != 1 && max[i] < result[i][j]) {
						max[i] = result[i][j];
					}
				} // end if

			} // end for j

		} // end for i

		return max;
	} // end maxCorrelation

	//
	public double[] meanCorrelation(List<String> list) throws IOException//, ParseException
	{
		String[] s = new String[list.size()];
		for (int i = 0; i < s.length; i++) {
			s[i] = list.get(i);
		}
		return meanCorrelation(s);
	} // end meanCorrelation

	//
	public double[] meanCorrelation(String[] s) throws IOException//, ParseException
	{
		double[][] result = compare(s);
		double[] avg = new double[s.length];
		int sum = 0;
		for (int i = 0; i < s.length; i++) {
			sum = 0;
			for (int j = 0; j < s.length; j++) {
				if (i != j) {
					if (result[i][j] != 1) {
						sum += result[i][j];
					}

				} // end for j
			} // end if
			avg[i] = (double) sum / (s.length - 1);
		} // end for i
		return avg;
	} // end meanCorrelation

	/**
	 * Computes the pairwise comparison of the pages in the
	 * specified array. The result is a symmetric matrix.
	 */
	public double[][] compare(String[] s) throws IOException//, ParseException
	{
		double[][] result = new double[s.length][s.length];
		double[] avg = new double[s.length];
		int c = 0;
		for (int i = 0; i < s.length; i++) {
			//long begin = System.nanoTime();
			st = 0;
			it = 0;
			result[i][i] = 1;
			for (int j = i + 1; j < s.length; j++) {
				result[i][j] = result[j][i] = compare(s[i], s[j]);
				//logger.debug((++c) + "\t" + i + ", " + j + "\t" + s[i] + " * " + s[j] + " = " + result[i][j]);
			} // end for j
			//long end = System.nanoTime();
			//logger.debug(s[i] + " compared with " + (s.length - i + 1) + " pages in " + nf.format(end - begin) + " ns (" + st + ", " + it + ")");

		} // end for i
		return result;
	} // end compare

	//
	public String matrixToString(String[] s, double[][] result) {
		StringBuilder sb = new StringBuilder();
		double[] avg = new double[s.length];
		for (int i = 0; i < s.length; i++) {
			sb.append("\t" + i);
		}

		sb.append("\t\tmax\tsum\tavr\tpage\ti\n");
		//sb.append("\n");

		for (int i = 0; i < s.length; i++) {
			sb.append(i);
			double sum = 0;
			double max = -1;
			int size = 0;
			for (int j = 0; j < s.length; j++) {

				sb.append("\t" + decimalFormat.format(result[i][j]));
				if (i != j && result[i][j] != 1) {
					size++;
					sum += result[i][j];
					//logger.info(i + "," + j + "\t" + s[i] + " * " + s[j] + " = " + result[i][j] + " (" + sum + ")");
					if (max < result[i][j]) {
						max = result[i][j];
					}

				}
			} // end for j
			avg[i] = (double) sum / size;

			//logger.info(i + "\t" + s[i] + " = " + avg[i]);

			sb.append("\t\t" + decimalFormat.format(max) + "\t" + decimalFormat.format(sum) + "\t" + decimalFormat.format(avg[i]) + "\t" + s[i] + "\t" + i + "\n");
		} // end for i
		return sb.toString();
	} // end matrixToString

	/**
	 * Returns the similarity between the two specified pages.
	 * The similarity is proportional to number of incoming
	 * and outgoing links the two pages shares.
	 *
	 * @param p1 the first page
	 * @param p2 the first page
	 * @return the number of incoming and outgoing links
	 *         the two pages shares.
	 */
	public double intersection(String p1, String p2) throws IOException//, ParseException
	{
		long begin, end;

		//begin = System.nanoTime();
		int[] out1 = outgoingSearcher.search(p1);
		//end = System.nanoTime();
		//logger.debug(p1 + " has " + out1.length + " outgoing links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int[] out2 = outgoingSearcher.search(p2);
		//end = System.nanoTime();
		//logger.debug(p2 + " has " + out2.length + " outgoing links found in " + nf.format(end - begin) + " ns");

		//logger.debug("comparing " + p1 + " and " + p2 + "...");
		//begin = System.nanoTime();
		int[] in1 = incomingSearcher.search(p1);
		//end = System.nanoTime();
		//logger.debug(p1 + " has " + in1.length + " incoming links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int[] in2 = incomingSearcher.search(p2);
		//end = System.nanoTime();
		///logger.debug(p2 + " has " + in2.length + " incoming links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		//int cin = basicIntersectCount(in1, in2);
		int cin = (in1.length < in2.length ? intersectCount(in1, in2) : intersectCount(in2, in1));
		//end = System.nanoTime();
		//logger.debug(cin + " shared incoming links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		//int cout = basicIntersectCount(out1, out2);
		int cout = (out1.length < out2.length ? intersectCount(out1, out2) : intersectCount(out2, out1));
		//end = System.nanoTime();
		//logger.info(cout + " shared outgoing links found in " + nf.format(end - begin) + " ns");

		//logger.info("cin " + cin);
		//logger.info("cout " + cout);
		double ctot = (double) cin + cout;
		if (ctot == 0) {
			return 0;
		}

		double tot1 = (double) in1.length + out1.length;
		double tot2 = (double) in2.length + out2.length;

		double ncomp = ctot / (tot1 + (tot2 - ctot));
		//logger.info(ctot + "\t" + tot1 + "\t" + tot2 + "\t" + ncomp);
		return ncomp;

	} // end intersection

	/**
	 * Returns the similarity between the two specified pages.
	 * The similarity is proportional to number of incoming
	 * and outgoing links the two pages shares.
	 *
	 * @param p1 the first page
	 * @param p2 the first page
	 * @return the number of incoming and outgoing links
	 *         the two pages shares.
	 */
	public double compare(String p1, String p2) throws IOException//, ParseException
	{
		long begin, end;
		//begin = System.nanoTime();
		int[] out1 = outgoingSearcher.search(p1);
		//end = System.nanoTime();
		//logger.debug(p1 + " has " + out1.length + " outgoing links found in " + nf.format(end - begin) + " ns");

		/*if (out1.contains(p2))
		{
			logger.warn(p2 + " is contained in out1 (" + p1 + ")");
			return 1;
		}*/

		//begin = System.nanoTime();
		int[] out2 = outgoingSearcher.search(p2);
		//end = System.nanoTime();
		//logger.debug(p2 + " has " + out2.length + " outgoing links found in " + nf.format(end - begin) + " ns");

		/*if (out2.contains(p1))
		{
			logger.warn(p1 + " is contained in out2 (" + p2 + ")");
			return 1;
		}*/

		//logger.debug("comparing " + p1 + " and " + p2 + "...");
		//begin = System.nanoTime();
		int[] in1 = incomingSearcher.search(p1);
		//end = System.nanoTime();
		//logger.debug(p1 + " has " + in1.length + " incoming links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int[] in2 = incomingSearcher.search(p2);
		//end = System.nanoTime();
		//logger.debug(p2 + " has " + in2.length + " incoming links found in " + nf.format(end - begin) + " ns");

		//end = System.nanoTime();
		//st += end - begin;
		//logger.debug(" search time " + st + " ns");

		//begin = System.nanoTime();
		int cin = (in1.length < in2.length ? intersectCount(in1, in2) : intersectCount(in2, in1));
		//end = System.nanoTime();
		//logger.debug(cin + " shared incoming links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int cout = (out1.length < out2.length ? intersectCount(out1, out2) : intersectCount(out2, out1));
		//end = System.nanoTime();
		//logger.info(cout + " shared outgoing links found in " + nf.format(end - begin) + " ns");

		//end = System.nanoTime();
		//it += end - begin;

		//logger.info("intersection time " + it + " ns");
		//logger.info("cin " + cin);
		//logger.info("cout " + cout);

		double ctot = (double) cin + cout;

		//return ctot;

		//if (cin == 0 && cout == 0)
		if (ctot <= 2) {
			return 0;
		}

		//logger.info(ctot + "\t" + in1.length + "+" + out1.length + "\t" + in2.length + "+" + out2.length);
		double tot1 = ((double) in1.length + out1.length) / NUMBER_OF_PAGES;
		double tot2 = ((double) in2.length + out2.length) / NUMBER_OF_PAGES;
		double pro = tot1 * tot2;

		double sum = ctot / NUMBER_OF_PAGES;

		double comp = log2(sum / pro);
		//logger.info(sum + "\t" + tot1 + "\t" + tot2);
		// normalized between 0 and 1
		double ncomp = (comp / -(2 * log2(Math.max(tot1, tot2)))) + 0.5;
		//logger.debug(comp + " = log(" + sum + " / " + tot1 + " * " + tot2 + ") = log(" + sum + " / " + pro + ")");
		//logger.debug(ncomp + " = " + ncomp + "/" + "-(2 * log(Max(" + tot1 + ", " + tot2 + "))) + 0.5");
		return ncomp;

	} // end compare

	public double relatedness(String p1, String p2) throws IOException//, ParseException
	{
		long begin, end;

		//logger.debug("comparing " + p1 + " and " + p2 + "...");
		//begin = System.nanoTime();
		int[] in1 = incomingSearcher.search(p1);
		//end = System.nanoTime();
		//logger.debug(p1 + " has " + in1.length + " incoming links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int[] in2 = incomingSearcher.search(p2);
		//end = System.nanoTime();
		//logger.debug(p2 + " has " + in2.length + " incoming links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int cin = (in1.length < in2.length ? intersectCount(in1, in2) : intersectCount(in2, in1));
		//end = System.nanoTime();
		//logger.debug(cin + " shared incoming links found in " + nf.format(end - begin) + " ns");

		if (cin == 0) {
			return 0;
		}
		int W = 3764657;
		double max = Math.max(in1.length, in2.length);
		//logger.info("max = " + max);
		double min = Math.min(in1.length, in2.length);
		//logger.info("min = " + min);
		double rel = (Math.log(max) - Math.log(cin)) / (Math.log(W) - Math.log(min));
		//logger.info("rel = " + rel);
		return rel;

	} // end compare

	//
	protected double log2(double x) {
		return Math.log(x) / LOG_2;
	} // end log2

	//
	public void close() throws IOException {
		outgoingSearcher.close();
		incomingSearcher.close();
	} // end close

	//
	public void interactive() throws Exception {
		InputStreamReader fullReader = null;
		BufferedReader myInput = null;
		System.out.println("\nquery = [page] | [page1 \t page2] | [page1 \t page2 \t ...]");

		while (true) {
			System.out.println("\nPlease write a query and type <return> to continue (CTRL C to exit):");

			fullReader = new InputStreamReader(System.in);
			myInput = new BufferedReader(fullReader);
			//String[] query = myInput.readLine().toString().replace(":", "\\:").split("\t");
			String[] query = myInput.readLine().toString().replace(" ", "_").split("\t");
			for (int i = 0; i < query.length; i++) {
				if (query[i].startsWith("http://en.wikipedia.org/wiki/")) {
					int len = "http://en.wikipedia.org/wiki/".length();
					query[i] = query[i].substring(len, query[i].length());
				}

			} // end for i
			if (query.length == 1) {

				long begin = System.nanoTime();
				int[] in = incomingLinks(query[0]);
				long end = System.nanoTime();
				logger.info(in.length + " incoming links found in " + nf.format(end - begin) + " ns)");
				//logger.debug(arrayToString(in));

				long begin1 = System.nanoTime();
				int[] out = outgoingLinks(query[0]);
				long end1 = System.nanoTime();
				logger.info(out.length + " outgoing links found in " + nf.format(end - begin) + " ns)");
				logger.info((in.length + out.length) + " total links found in " + nf.format(end1 - begin) + " ns)");
				//logger.debug(arrayToString(out));
			}
			else if (query.length == 2) {
				st = 0;
				it = 0;

				long begin = System.nanoTime();
				//double count = compare(query[0], query[1]);

				double count2 = compare(query[0], query[1]);

				long end = System.nanoTime();
				//logger.info(count + " shared links found in " + nf.format(end - begin) + " ns (" + st + ", " + it + ")");
				logger.info("sim = " + df.format(count2) + " done in " + nf.format(end - begin) + " ns (" + st + ", " + it + ")");

				begin = System.nanoTime();
				double rel = relatedness(query[0], query[1]);
				end = System.nanoTime();
				logger.info("rel = " + df.format(rel) + " done in " + nf.format(end - begin) + " ns (" + st + ", " + it + ")");


				begin = System.nanoTime();
				double intr = intersection(query[0], query[1]);
				end = System.nanoTime();
				logger.info("intr = " + df.format(intr) + " done in " + nf.format(end - begin) + " ns (" + st + ", " + it + ")");

			}
			else if (query.length > 2) {
				long begin = System.nanoTime();
				double[][] result = compare(query);
				long end = System.nanoTime();
				logger.info(query.length + " pages compared in " + nf.format(end - begin) + " ns");

				logger.info("\n" + matrixToString(query, result));

				StringBuilder sb = new StringBuilder();
				sb.append(query[0]);
				//sb.append("\t");
				//sb.append(sumc[0]);

				for (int i = 1; i < query.length; i++) {
					sb.append("\t");
					sb.append(query[i]);
					//sb.append("\t");
					//sb.append(sumc[i]);
				}
				logger.info(sb.toString());

				//logger.info("compare " + count);

			}
			//logger.info(pages);

		} // end while
	} // end

	//
	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 3) {
			System.err.println("Wrong number of parameters " + args.length);
			System.err.println("Usage: java -mx1G org.fbk.cit.hlt.thewikimachine.similarity.IncomingOutgoingSimilarity outgoing-index-dir incoming-index-dir ram");

			System.exit(-1);
		}


		/*int[] a = new int[args.length];
		for (int i=0;i<args.length;i++)
			a[i]=Integer.parseInt(args[i]);
		int[] b = {
			2, 5, 7, 8, 9, 10, 11
		};
		logger.info(intersectCount(a, b));

		System.exit(0);*/

		String outgoingIndexDir = args[0];
		String incomingIndexDir = args[1];
		boolean ram = Boolean.parseBoolean(args[2]);
		//String[] query = new String[args.length - 1];
		//System.arraycopy(args, 1, query, 0, query.length);

		//long beginTotal = System.nanoTime();
		IncomingOutgoingSimilarity searcher = new IncomingOutgoingSimilarity(outgoingIndexDir, incomingIndexDir, ram);
		searcher.interactive();
		//searcher.search(query);
		searcher.close();

		//long endTotal = System.nanoTime(), endPartial = System.nanoTime();
		//logger.info(query.length + " terms found in " + (endTotal - beginTotal) + " ns");

	} // end main

}
