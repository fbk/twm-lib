/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.index.TypeSearcher;
import eu.fbk.twm.utils.*;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Token;
import eu.fbk.twm.utils.analysis.Tokenizer;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/20/13
 * Time: 9:13 PM
 * To change this template use File | Settings | File Templates.
 *
 * This class extracts all n-grams from a Wikipedia page
 *
 */
//todo: check why the ngram 5-Star Movement is missing
//todo: add the weighted lf: use the traffic
public class PageNGramExtractor extends CSVExtractor {
	private int n;

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	private Map<String, Integer> freqFormMap;

	private Tokenizer tokenizer;

	//private Map<String, String> redirectPageMap;

	private SynchronizedCounter synchronizedCounter;

	private PrintWriter ngramWriter;

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public static final int DEFAULT_N_GRAM = 10;

	public PageNGramExtractor(int numThreads, int numPages, int n) {
		super(numThreads, numPages);
		this.n = n;
		tokenizer = new HardTokenizer();
		synchronizedCounter = new SynchronizedCounter();

	}

	public PageNGramExtractor(int numThreads, int n) {
		super(numThreads);
		this.n = n;
		tokenizer = new HardTokenizer();
		synchronizedCounter = new SynchronizedCounter();

	}

	@Override
	public void processLine(String line) {
		//logger.debug(line.substring(0, 1));
		Set<String> ngramSet = createNGramSet(line);
		synchronizedCounter.addAll(ngramSet);
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			freqFormMap = createFreqFormMap(new File(extractorParameters.getWikipediaFormFreqFileName()));
			// the ngrams from the redirect have been added into the text
			/*TypeSearcher typeSearcher= new TypeSearcher(extractorParameters.getWikipediaTypeIndexName());
			typeSearcher.loadCache();
			addRedirectForms(typeSearcher, new File(extractorParameters.getWikipediaRedirFileName()));
			typeSearcher.close();
			System.exit(0);*/


			ngramWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaNGramFileName()), "UTF-8")));
			//ngramWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream("prova.csv"), "UTF-8")));
			read(extractorParameters.getWikipediaTextFileName());
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public void end() {
		logger.info("sorting...");
		long begin = System.currentTimeMillis();
		SortedMap<AtomicInteger, List<String>> sortedMap = synchronizedCounter.getSortedMap();
		try {
			writeSortedMap(sortedMap);
			ngramWriter.close();
		} catch (IOException e) {
			logger.error(e);
		}
		long end = System.currentTimeMillis();
		logger.info(sortedMap.size() + " lines sorted in " + df.format(end - begin) + " ms " + new Date());
	}

	private void writeSortedMap(SortedMap<AtomicInteger, List<String>> sortedMap) throws IOException {
		logger.info("writing...");
		long begin = System.currentTimeMillis();
		Integer linkFreq;
		String form;
		Iterator<AtomicInteger> it = sortedMap.keySet().iterator();
		StringBuilder sb;
		AtomicInteger documentFreq;
		List<String> list;
		for (int i = 0; it.hasNext(); i++) {
			documentFreq = it.next();
			list = sortedMap.get(documentFreq);
			sb = new StringBuilder();
			for (int j = 0; j < list.size(); j++) {
				form = list.get(j);
				linkFreq = freqFormMap.get(form);
				if (linkFreq != null) {
					sb.append(documentFreq);
					sb.append(CharacterTable.HORIZONTAL_TABULATION);
					sb.append(linkFreq);
					sb.append(CharacterTable.HORIZONTAL_TABULATION);
					sb.append(form);
					sb.append(CharacterTable.LINE_FEED);

				}
			}
			ngramWriter.print(sb.toString());
		}
		long end = System.currentTimeMillis();
		logger.info(df.format(sortedMap.size()) + " lines wrote in " + df.format(end - begin) + "\t" + new Date());

	}

	private Set<String> createNGramSet(String line) {
		Set<String> ngramSet = new HashSet<String>();
		Token[] tokenArray = tokenizer.tokenArray(line);
		int start = 0, end = 0;
		String form;
		Integer freq;
		Token firstToken, lastToken;
		int m = 0;
		for (int i = 1; i < tokenArray.length; i++) {
			firstToken = tokenArray[i];
			start = firstToken.getStart();
			m = i + n + 1;
			if (m > tokenArray.length) {
				m = tokenArray.length;
			}
			for (int j = i; j < m; j++) {
				lastToken = tokenArray[j];
				end = lastToken.getEnd();
				form = line.substring(start, end);
				freq = freqFormMap.get(form);
				if (freq != null) {
					ngramSet.add(form);
				}
			}
		}
		return ngramSet;
	}

	/**
	 * This method try to solve the following problem: redirect forms could not appear as ngrams in text
	 * but they appear in examples
	 *
	 * @param typeSearcher
	 * @param f
	 * @throws IOException
	 */
	private void addRedirectForms(TypeSearcher typeSearcher, File f) throws IOException {
		logger.info("reading redirect pairs " + f + "...");

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		Map<String, String > map = new HashMap<String, String>();
		String line = null;
		int count = 0, tot = 0;
		String[] t = null;
		// read the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				ParsedPageTitle redirect = new ParsedPageTitle(t[0]);
				ParsedPageTitle target = new ParsedPageTitle(t[1]);

				if (!redirect.getForm().equals(target.getForm())) {
					String tokenizedRedirectForm = tokenizer.tokenizedString(redirect.getForm());
					String tokenizedTargetForm = tokenizer.tokenizedString(target.getForm());
					Integer freqTargetForm = freqFormMap.get(tokenizedTargetForm);
					Integer freqRedirectForm = freqFormMap.get(redirect.getForm());
					TypeSearcher.Entry entry = typeSearcher.search(t[1]);
					if (entry.getType().equals(TypeSearcher.NOM_LABEL)) {
						if (tokenizedRedirectForm.length() > 1) {
							//System.out.println("NOM\t" + line + "\t[" + tokenizedRedirectForm + "\t" + freqRedirectForm + "]\t(" + tokenizedTargetForm + "\t" + freqTargetForm + ")");
							synchronizedCounter.add(tokenizedRedirectForm);
						}

					}


					//System.out.println("NAM\t" + line + "\t[" + tokenizedRedirectForm + "\t" + freqRedirectForm + "]\t(" + tokenizedTargetForm + "\t" + freqTargetForm + ")");
					synchronizedCounter.add(tokenizedRedirectForm);
					count++;
				}
				tot++;
			}
		}
		logger.info(count + "/" + tot + " redirect forms read (" + synchronizedCounter.size() + ")");

	}


	private Map<String, Integer> createFreqFormMap(File f) throws IOException {
		logger.info("reading form/freq pairs from " + f + "...");

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		Map<String, Integer> map = new HashMap<String, Integer>();
		String line = null;
		int count = 0, tot = 0;
		String[] t = null;
		// read the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				map.put(t[1], new Integer(t[0]));
			}
		}
		logger.info(map.size() + " forms read");
		return map;
	}


	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			Option ngramSizeOpt = OptionBuilder.withArgName("n-gram").hasArg().withDescription("n-grams size (default is " + PageNGramExtractor.DEFAULT_N_GRAM + ")").withLongOpt("n-gram").create("n");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");


			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(ngramSizeOpt);
			options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			int ngramSize = PageNGramExtractor.DEFAULT_N_GRAM;
			if (line.hasOption("n-gram")) {
				ngramSize = Integer.parseInt(line.getOptionValue("n-gram"));
			}

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);
			CSVExtractor ngramExtractor = new PageNGramExtractor(numThreads, ngramSize);
			ngramExtractor.setNotificationPoint(notificationPoint);
			ngramExtractor.start(extractorParameters);


		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.PageNGramExtractor", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
