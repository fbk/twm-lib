/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import org.apache.commons.lang.SerializationUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class MapIndexer extends AbstractIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>SetIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(MapIndexer.class.getName());

	public final static String DEFAULT_KEY_FIELD_NAME = "KEY";

	public final static String DEFAULT_VALUE_FIELD_NAME = "VALUE";

	protected String keyFieldName;

	protected String valueFieldName;

	protected static Pattern tabPattern = Pattern.compile("\t");

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected MapIndexer(String indexName) throws IOException {
		this(indexName, DEFAULT_KEY_FIELD_NAME, DEFAULT_VALUE_FIELD_NAME);
	}

	protected MapIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		super(indexName);
		this.keyFieldName = keyFieldName;
		this.valueFieldName = valueFieldName;
	}

	public String getKeyFieldName() {
		return keyFieldName;
	}

	public void setKeyFieldName(String keyFieldName) {
		this.keyFieldName = keyFieldName;
	}

	public String getValueFieldName() {
		return valueFieldName;
	}

	public void setValueFieldName(String valueFieldName) {
		this.valueFieldName = valueFieldName;
	}

	public abstract void index(String fileName, boolean compress) throws IOException;

	public abstract void index(File file, boolean compress) throws IOException;

	protected void index(String fileName, int key, int mapKey, int value, boolean compress) throws IOException {
		index(new File(fileName), mapKey, key, value, compress);
	}

	protected void index(File file, int key, int mapKey, int value, boolean compress) throws IOException {
		logger.info("indexing " + file + "...");
		int max = Math.max(key, value);
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		if (compress) {
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file)), "UTF-8"));
		}
		else {
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		}

		String line;
		int count = 0, tot = 0;
//		int part = 0;

		String oldKey = "";
//		Set<String> set = new TreeSet<String>();
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		String[] t;
		logger.info("tot\tcount\ttime\tdate");

		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length > max && t[key] != null && t[value] != null && t[mapKey] != null) {
//				set.add(t[value]);
				map.put(t[mapKey], Integer.parseInt(t[value]));
				oldKey = t[key];
//				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length > max && t[key] != null && t[value] != null) {
				if (!t[key].equals(oldKey)) {
					add(oldKey, map);
					count++;
					map = new HashMap<String, Integer>();
//					set = new TreeSet<String>();
//					part = 0;
				}
//				set.add(t[value]);
				map.put(t[mapKey], Integer.parseInt(t[value]));
				oldKey = t[key];
//				part++;
			}
			tot++;

			if ((tot % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + " lines read, key indexed: " + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last document
		add(oldKey, map);
		lnr.close();
	} // end read

	public void add(String key, HashMap value) {
		Document doc = new Document();
		try {
			doc.add(new Field(keyFieldName, key, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field(valueFieldName, SerializationUtils.serialize(value), Field.Store.YES));
			indexWriter.addDocument(doc);
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
