/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.IntSetSearcher;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.Defaults;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:05 PM
 * To change this template use File | Settings | File Templates.
 *
 * @deprecated
 * @see PageIncomingOutgoingWeightedSearcher
 */
@Deprecated public class PageIncomingOutgoingSearcher extends IntSetSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageIncomingOutgoingSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(PageIncomingOutgoingSearcher.class.getName());

	private static final double LOG_2 = Math.log(2);

	private static final double NUMBER_OF_PAGES = 10000000.0;

	public PageIncomingOutgoingSearcher(String indexName) throws IOException {
		super(indexName, PageOutgoingIndexer.PAGE_FIELD_NAME, PageOutgoingIndexer.LINK_FIELD_NAME);
		logger.trace(toString(10));
	}

	protected double log2(double x) {
		return Math.log(x) / LOG_2;
	}

	private static Set<Integer> intersectionSet(int[] a1, int[] a2) {
		//long begin = System.nanoTime();
		Set<Integer> set = new TreeSet<Integer>();
		//int count = 0;
		int l = 0;
		int s = -a2.length - 1;
		//logger.info("l=" + l + "\ts=" + s);

		for (int i = 0; i < a1.length; i++) {
			//logger.info("i=" + i + "\tai=" + a1[i]);
			l = Arrays.binarySearch(a2, a1[i]);
			if (l >= 0) {
				//logger.warn(a1[i]);
				set.add(a1[i]);
				//count++;
			}
			else if (l <= s) {
				break;
			}
		}

		//long end = System.nanoTime();
		//logger.info(iter + " interation ("+ (a1.length * a2.length) + ") done in " + nf.format(end - begin) + " ns");
		return set;
	}

	/**
	 * Returns the number of elements the two array share.
	 * To speed up the execution <code>a1</code> <b>must</b>
	 * be shorter than <code>a2</code> and the <b>must</b>
	 * be sorted.
	 *
	 * @return the number of items the two arrays share
	 */
	private static int intersectCount(int[] a1, int[] a2) {
		//long begin = System.nanoTime();
		int count = 0;
		int l = 0;
		int s = -a2.length - 1;
		//logger.info("l=" + l + "\ts=" + s);

		for (int i = 0; i < a1.length; i++) {
			//logger.info("i=" + i + "\tai=" + a1[i]);
			l = Arrays.binarySearch(a2, a1[i]);
			if (l >= 0) {
				//logger.warn(a1[i]);
				count++;
			}
			else if (l <= s) {
				break;
			}
		}

		//long end = System.nanoTime();
		//logger.info(iter + " interation ("+ (a1.length * a2.length) + ") done in " + nf.format(end - begin) + " ns");
		return count;
	}

	/**
	 * Returns the similarity between the two specified pages.
	 * The similarity is proportional to number of incoming
	 * and outgoing links the two pages share.
	 *
	 * @param p1 the first page
	 * @param p2 the second page
	 * @return the number of incoming and outgoing links
	 *         the two pages share.
	 */
	public double intersection(String p1, String p2) //throws IOException//, ParseException
	{
		int[] links1 = search(p1);
		int[] links2 = search(p2);
		return (links1.length < links2.length ? intersectCount(links1, links2) : intersectCount(links2, links1));
	}

	public double normalizedIntersection(String p1, String p2) {
		//long begin = System.nanoTime(), end = 0;
		int[] links1 = search(p1);
		//end = System.nanoTime();
		//logger.debug(p1 + " has " + out1.length + " outgoing links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int[] links2 = search(p2);
		//end = System.nanoTime();
		//logger.debug(p2 + " has " + out2.length + " outgoing links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int count = (links1.length < links2.length ? intersectCount(links1, links2) : intersectCount(links2, links1));
		//end = System.nanoTime();
		//logger.debug(cin + " shared incoming links found in " + nf.format(end - begin) + " ns");

		if (count == 0) {
			return 0;
		}

		return (double) count / (links1.length + (links2.length - count));

	}

	/**
	 * Returns the similarity between the two specified pages.
	 * The similarity is proportional to number of incoming
	 * and outgoing links the two pages share.
	 *
	 * @param p1 the first page
	 * @param p2 the second page
	 * @return the number of incoming and outgoing links
	 *         the two pages share.
	 */
	public double compare(String p1, String p2) //throws IOException//, ParseException
	{
		//long begin = System.nanoTime(), end = 0;
		int[] links1 = search(p1);
		//end = System.nanoTime();
		//logger.debug(p1 + " has " + out1.length + " outgoing links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int[] links2 = search(p2);
		//end = System.nanoTime();
		//logger.debug(p2 + " has " + out2.length + " outgoing links found in " + nf.format(end - begin) + " ns");

		//begin = System.nanoTime();
		int count = (links1.length < links2.length ? intersectCount(links1, links2) : intersectCount(links2, links1));
		//end = System.nanoTime();
		//logger.debug(cin + " shared incoming links found in " + nf.format(end - begin) + " ns");

		if (count == 0) {
			return 0;
		}

		//logger.info(ctot + "\t" + in1.length + "+" + out1.length + "\t" + in2.length + "+" + out2.length);
		double tot1 = (double) links1.length / NUMBER_OF_PAGES;
		double tot2 = (double) links2.length / NUMBER_OF_PAGES;
		double pro = tot1 * tot2;

		double sum = (double) count / NUMBER_OF_PAGES;

		double comp = log2(sum / pro);
		//double comp = log2((double) (count * NUMBER_OF_PAGES) / (links1.length * links2.length));
		//logger.info(sum + "\t" + tot1 + "\t" + tot2);
		// normalized between 0 and 1
		return (comp / -(2 * log2(Math.max(tot1, tot2)))) + 0.5;
	}

	@Override
	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				int[] result = search(s[0]);
				long end = System.nanoTime();

				if (result != null) {
					for (int i = 0; i < result.length; i++) {
						logger.info(i + "\t" + result[i]);
					}
					logger.info(s[0] + " found in " + tf.format(end - begin) + " ns");

				}
				else {
					logger.info(s[0] + " not found in " + tf.format(end - begin) + " ns");
				}
			}
			else if (s.length == 2) {

				int[] s1 = search(s[0]);
				int[] s2 = search(s[1]);
				logger.info(s1.length + "\t" + s2.length);

				Set<Integer> set = intersectionSet(s1, s2);
				logger.debug("set\t" + set);

				long begin = System.nanoTime();
				double sim = compare(s[0], s[1]);
				long end = System.nanoTime();
				logger.info("sim\t" + sim + "\t" + tf.format(end - begin));

				begin = System.nanoTime();
				double intersection = intersection(s[0], s[1]);
				end = System.nanoTime();
				logger.info("int\t" + intersection + "\t" + tf.format(end - begin));

				begin = System.nanoTime();
				double normalizedIntersection = normalizedIntersection(s[0], s[1]);
				end = System.nanoTime();
				logger.info("nint\t" + normalizedIntersection + "\t" + tf.format(end - begin));
			}
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode").withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option searchOpt = OptionBuilder.withArgName("search").hasArg().withDescription("search for the specified key").withLongOpt("search").create("s");
			Option freqFileOpt = OptionBuilder.withArgName("key-freq").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			Option minimumKeyFreqOpt = OptionBuilder.withArgName("minimum-freq").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			options.addOption(freqFileOpt);
			//options.addOption(keyFieldNameOpt);
			//options.addOption(valueFieldNameOpt);
			options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher = new PageIncomingOutgoingSearcher(line.getOptionValue("index"));
			pageIncomingOutgoingSearcher.setNotificationPoint(notificationPoint);

			if (line.hasOption("key-freq")) {
				pageIncomingOutgoingSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
			}
			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				int[] result = pageIncomingOutgoingSearcher.search(line.getOptionValue("search"));
				logger.info(Arrays.toString(result));
			}
			if (line.hasOption("interactive-mode")) {
				pageIncomingOutgoingSearcher.interactive();
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PageIncomingOutgoingSearcher", "\n", options, "\n", true);
		}
	}

}
