/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.similarity;

import eu.fbk.twm.utils.Defaults;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.index.PageIncomingOutgoingSearcher;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;


import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/31/13
 * Time: 12:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class IncomingOutgoingSimilarityExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>IncomingOutgoingSimilarityExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(IncomingOutgoingSimilarityExtractor.class.getName());

	public final static int DEFAULT_THREADS_NUMBER = 1;

	public final static int DEFAULT_QUEUE_SIZE = 10000;

	public final static int DEFAULT_NOTIFICATION_POINT = 10000;

	private int notificationPoint;

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private int numPages;

	public static final int DEFAULT_NUM_PAGES = Integer.MAX_VALUE;

	private ExecutorService myExecutor;

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	public static final int DEFAULT_MIN_FREQ = 10000;

	public static final int DEFAULT_OUTPUT_NUM_PAGES = 100;

	Map<String, int[]> cache;

	PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher;

	PrintWriter similarityWriter;

	int outputNumPages;

	AtomicInteger pageCount;

	public IncomingOutgoingSimilarityExtractor(int numThreads, int numPages, PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher, String name) throws IOException {
		this(numThreads, numPages, pageIncomingOutgoingSearcher, new File(name));
	}

	public IncomingOutgoingSimilarityExtractor(int numThreads, int numPages, PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher, File out) throws IOException {
		this.numPages = numPages;
		this.pageIncomingOutgoingSearcher = pageIncomingOutgoingSearcher;
		notificationPoint = DEFAULT_NOTIFICATION_POINT;
		outputNumPages = DEFAULT_OUTPUT_NUM_PAGES;
		cache = pageIncomingOutgoingSearcher.getCache();
		pageCount = new AtomicInteger(0);
		similarityWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));

		int blockQueueSize = DEFAULT_QUEUE_SIZE;
		BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(blockQueueSize);
		RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();
		myExecutor = new ThreadPoolExecutor(numThreads, numThreads, 1, TimeUnit.MINUTES, blockingQueue, rejectedExecutionHandler);
	}

	public int getNotificationPoint() {
		return notificationPoint;
	}

	public void setNotificationPoint(int notificationPoint) {
		this.notificationPoint = notificationPoint;
	}

	public void start() {
		long begin = System.currentTimeMillis(), end = 0;
		Iterator<String> it = cache.keySet().iterator();
		String p;
		int i;
		logger.info("pages\ttime\tdate");
		for (i = 1; it.hasNext(); i++) {
			p = it.next();
			if (i > numPages) {
				break;
			}
			myExecutor.execute(new PageSimilarity(p));

			if ((i % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(i) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}
		end = System.currentTimeMillis();
		logger.info(df.format(i) + "\t" + df.format(end - begin) + "\t" + new Date());

		boolean b = true;
		try {
			myExecutor.shutdown();
			logger.debug("waiting to end " + new Date() + "...");
			b = myExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			logger.error(e);
		}
		similarityWriter.close();
		logger.info("ending process " + b + " " + new Date() + "...");
	}

	private void write(String page, Map<Double, List<String>> map) {
		StringBuilder sb = new StringBuilder();
		Iterator<Double> it = map.keySet().iterator();
		List<String> list;
		double d;
		//sb.append(page);
		int tot = 0;
		for (int i = 0; it.hasNext(); i++) {
			d = it.next();
			list = map.get(d);
			for (int j = 0; j < list.size(); j++) {
				if (tot > outputNumPages) {
					write(sb.toString());
					return;
				}
				sb.append(page);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(d);

				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(list.get(j));
				sb.append(CharacterTable.LINE_FEED);
				tot++;
			}
		}

		write(sb.toString());
	}

	private void write(String s) {
		pageCount.incrementAndGet();
		synchronized (this) {
			similarityWriter.print(s);
		}
	}

	class PageSimilarity implements Runnable {
		String page;

		PageSimilarity(String page) {
			this.page = page;
		}

		@Override
		public void run() {
			Iterator<String> it = cache.keySet().iterator();
			String p;
			SortedMap<Double, List<String>> map = new TreeMap<Double, List<String>>(new Comparator<Double>() {
				public int compare(Double e1, Double e2) {
					return e2.compareTo(e1);
				}
			});
			List<String> list;
			double d;
			for (int i = 0; it.hasNext(); i++) {
				p = it.next();
				//d = pageIncomingOutgoingSearcher.compare(page, p);
				d = pageIncomingOutgoingSearcher.normalizedIntersection(page, p);
				list = map.get(d);
				if (list == null) {
					list = new ArrayList<String>();
					map.put(d, list);
				}
				list.add(p);
			}
			write(page, map);
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option freqFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
			Option similarityFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the keys' frequencies from the specified file").isRequired().withLongOpt("similarity-file").create("s");
			Option minimumKeyFreqOpt = OptionBuilder.withArgName("int").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("maximum number of pages to process (default is all)").withLongOpt("num-pages").create("p");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(similarityFileOpt);
			options.addOption(freqFileOpt);
			options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}
			PageIncomingOutgoingSearcher pageIncomingOutgoingSearcher = new PageIncomingOutgoingSearcher(line.getOptionValue("index"));
			pageIncomingOutgoingSearcher.setNotificationPoint(notificationPoint);
			if (line.hasOption("key-freq")) {
				pageIncomingOutgoingSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
			}

			IncomingOutgoingSimilarityExtractor incomingOutgoingSimilarityExtractor = new IncomingOutgoingSimilarityExtractor(numThreads, numPages, pageIncomingOutgoingSearcher, line.getOptionValue("similarity-file"));
			incomingOutgoingSimilarityExtractor.setNotificationPoint(notificationPoint);
			incomingOutgoingSimilarityExtractor.start();
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.similarity.IncomingOutgoingSimilarityExtractor", "\n", options, "\n", true);
		}
	}
}
