/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xerial.snappy.SnappyInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/28/13
 * Time: 10:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class Decompress {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Decompress</code>.
	 */
	static Logger logger = Logger.getLogger(Decompress.class.getName());

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public Decompress(File in, File out) {
		long begin = System.currentTimeMillis();
		logger.info("decompressing" + new Date() + "...");
		logger.info("in:\t" + in + " (" + df.format(in.length()) + ")");
		logger.info("out:\t" + out);

		byte[] buffer = new byte[1024];

		try {

			SnappyInputStream sis = new SnappyInputStream(new FileInputStream(in));

			FileOutputStream fos = new FileOutputStream(out);

			int len;
			while ((len = sis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
			sis.close();
			fos.close();
		} catch (IOException e) {
			logger.error(e);
		}
		long end = System.currentTimeMillis();
		logger.info("decompression done in " + df.format(end - begin) + " ms (" + new Date() + ")");
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 2) {
			System.err.println("Wrong number of parameters " + args.length);
			System.err.println("Usage: java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.Decompress in out");

			System.exit(-1);
		}

		new Decompress(new File(args[0]), new File(args[1]));

	}
}
