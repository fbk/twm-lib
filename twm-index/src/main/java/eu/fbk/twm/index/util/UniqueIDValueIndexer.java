/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 5:23 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class UniqueIDValueIndexer extends AbstractIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>SingleValueIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(UniqueIDValueIndexer.class.getName());

	public final static String DEFAULT_KEY_FIELD_NAME = "KEY";

	public final static String DEFAULT_VALUE_FIELD_NAME = "VALUE";

	protected String keyFieldName;

	protected String valueFieldName;

	protected static Pattern tabPattern = Pattern.compile("\t");

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected UniqueIDValueIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		super(indexName);
		this.keyFieldName = keyFieldName;
		this.valueFieldName = valueFieldName;
	}

	public void index(String fileName, int key) throws IOException {
		index(new File(fileName), key);
	}

	public void index(File file, int valueCol) throws IOException {
		logger.info("indexing " + file + "...");
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		String line;
		int tot = 0, count = 0;
		String[] t;
		logger.info("tot\tcount\ttime\tdate");
		String last = "";

		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length > valueCol) {
				if (!t[valueCol].equals(last)) {
					add(count++, t[valueCol]);
					last = t[valueCol];
				}
			}
			tot++;

			if ((tot % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + " lines read: " + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());
		lnr.close();
	}

	protected void add(int id, String v) throws IOException {
		Document doc = new Document();
		try {
			doc.add(new Field(keyFieldName, SerialUtils.toByteArray(id), Field.Store.YES));
			doc.add(new Field(valueFieldName, v, Field.Store.YES, Field.Index.NOT_ANALYZED));
			indexWriter.addDocument(doc);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public void index(String fileName) throws IOException {
		index(new File(fileName));
	}

	public abstract void index(File file) throws IOException;

}
