/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 6/17/13
 * Time: 11:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class QIDPageIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>QIDPageIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(QIDPageIndexer.class.getName());


}
