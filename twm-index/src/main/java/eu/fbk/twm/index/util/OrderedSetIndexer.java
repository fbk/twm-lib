/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.util;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class OrderedSetIndexer extends AbstractIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OrderedSetIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(OrderedSetIndexer.class.getName());

	public final static String DEFAULT_KEY_FIELD_NAME = "KEY";

	public final static String DEFAULT_VALUE_FIELD_NAME = "VALUE";

	protected String keyFieldName;

	protected String valueFieldName;

	protected static Pattern tabPattern = Pattern.compile("\t");

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected OrderedSetIndexer(String indexName) throws IOException {
		this(indexName, DEFAULT_KEY_FIELD_NAME, DEFAULT_VALUE_FIELD_NAME);
	}

	protected OrderedSetIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		super(indexName);
		this.keyFieldName = keyFieldName;
		this.valueFieldName = valueFieldName;
	}

	public String getKeyFieldName() {
		return keyFieldName;
	}

	public void listKeyFieldName(String keyFieldName) {
		this.keyFieldName = keyFieldName;
	}

	public String getValueFieldName() {
		return valueFieldName;
	}

	public void listValueFieldName(String valueFieldName) {
		this.valueFieldName = valueFieldName;
	}

	public abstract void index(String fileName, boolean compress) throws IOException;

	public abstract void index(File file, boolean compress) throws IOException;

	protected void index(String fileName, int key, int value, boolean compress) throws IOException {
		index(new File(fileName), key, value, compress);
	}

	protected void index(File file, int key, int value, boolean compress) throws IOException {
		logger.info("indexing " + file + "...");
		int max = Math.max(key, value);
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		if (compress) {
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(file)), "UTF-8"));
		}
		else {
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		}

		String line = null;
		int count = 0, part = 0, tot = 0;
		String oldKey = "";
		List<String> list = new ArrayList<String>();
		String[] t;
		logger.info("tot\tcount\ttime\tdate");
		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length > max && t[key] != null && t[value] != null) {
				// remove repetitions
				if (!list.contains(t[value])) {
					list.add(t[value]);
				}

				oldKey = t[key];
				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length > max && t[key] != null && t[value] != null) {
				if (!t[key].equals(oldKey)) {
					add(oldKey, list);
					count++;
					list = new ArrayList<String>();
					part = 0;
				}
				list.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;

			//if (tot > 1000000)
			//	break;

			if ((tot % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + " lines read, key indexed: " + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last document
		add(oldKey, list);
		lnr.close();
	} // end read

	public void add(String key, List<String> list) {
		Document doc = new Document();
		try {
			doc.add(new Field(keyFieldName, key, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field(valueFieldName, toByte(list), Field.Store.YES));
			indexWriter.addDocument(doc);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	protected byte[] toByte(List<String> list) throws IOException {
		int freq = 0;
		String form = null;
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(1024);
		DataOutputStream dataStream = new DataOutputStream(byteStream);
		// number of distinct forms
		dataStream.writeInt(list.size());

		for (int i = 0; i < list.size(); i++) {
			dataStream.writeUTF(list.get(i));
		}
		return byteStream.toByteArray();
	}
}
