/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.lsa.LSM;
import eu.fbk.utils.math.Vector;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/26/13
 * Time: 5:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleTrainingExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>SimpleTrainingExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(SimpleTrainingExtractor.class.getName());

	protected LSM lsm;

	private int numThreads;

	private ExecutorService myExecutor;

	public final static int DEFAULT_NUMBER_OF_THREAD = 1;

	public final static int DEFAULT_NOTIFICATION_POINT = 10000;

	public final static int DEFAULT_LSM_DIM = 100;

	private int notificationPoint;

	PrintWriter trainingWriter;

	PrintWriter vectorWriter;

	PrintWriter bowWriter;

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	private static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public SimpleTrainingExtractor(LSM lsm, String outputFileName) throws IOException {
		this(lsm, new File(outputFileName), DEFAULT_NUMBER_OF_THREAD);
	}

	public SimpleTrainingExtractor(LSM lsm, String outputFileName, int numThreads) throws IOException {
		this(lsm, new File(outputFileName), DEFAULT_NUMBER_OF_THREAD);
	}

	public SimpleTrainingExtractor(LSM lsm, File outputFile, int numThreads) throws IOException {
		this.lsm = lsm;
		this.numThreads = numThreads;
		notificationPoint = DEFAULT_NOTIFICATION_POINT;
		logger.info("creating the thread executor (" + numThreads + ")");
		myExecutor = Executors.newFixedThreadPool(numThreads);
		trainingWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8")));
		bowWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile + ".bow"), "UTF-8")));
		vectorWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile + ".vec"), "UTF-8")));
	}

	public int getNumThreads() {
		return numThreads;
	}

	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
	}

	public int getNotificationPoint() {
		return notificationPoint;
	}

	public void train(String name) throws IOException {
		train(new File(name));
	}

	public void train(File in) throws IOException {
		logger.info("reading " + in + "...");

		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		String line;
		int count = 0, part = 0, tot = 0;
		String previousForm = "";
		//Map<String, BOW> map = new HashMap<String, BOW>();
		String[] t = null;
		List<String[]> list = new ArrayList<String[]>();
		logger.info("totalFreq\tcount\ttime\tdate");
		// read the first line
		if ((line = lnr.readLine()) != null) {
			try {
				t = tabPattern.split(line);

				if (t.length == OneExamplePerSenseExtractor.COLUMN_NUMBER) {
					list.add(t);
					previousForm = t[OneExamplePerSenseExtractor.FORM_INDEX];
					//logger.info(part + "\t\"" + t[3] + "\"");
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + count);
				logger.error(e);
			} finally {
				tot++;
			}
		} // end if

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			try {
				t = tabPattern.split(line);
				if (t.length == OneExamplePerSenseExtractor.COLUMN_NUMBER) {
					if (!t[OneExamplePerSenseExtractor.FORM_INDEX].equals(previousForm)) {
						logger.debug("executing " + previousForm + " (" + list.size() + ")...");
						myExecutor.execute(new Training(list, previousForm));
						list = new ArrayList<String[]>();
						count++;
						part = 0;
					}
					list.add(t);
					previousForm = t[OneExamplePerSenseExtractor.FORM_INDEX];
					part++;
				}
			} catch (Exception e) {
				logger.error("Error at line " + tot);
				logger.error(e);
			} finally {
				tot++;
			}
			//if (count > 500) break;

			if ((tot % notificationPoint) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
		} // end while
		lnr.close();
		// add the last line
		list.add(t);

		logger.debug("executing " + previousForm + " (" + list.size() + ")...");
		myExecutor.execute(new Training(list, previousForm));

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + "\t" + new Date());

		try {
			myExecutor.shutdown();
			logger.info("waiting for execution...");
			myExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			logger.error(e);
		}
		logger.info("closing the streams...");

		trainingWriter.close();
		vectorWriter.close();
		bowWriter.close();
		logger.info("done it");

	}


	private void write(Map<String, BOW> map, String form) {
		logger.debug(Thread.currentThread().getName() + " is writing " + form + "...");
		Iterator<String> it = map.keySet().iterator();
		String label;
		BOW bow;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; it.hasNext(); i++) {
			label = it.next();
			bow = map.get(label);
			sb.append(form);
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
			sb.append(label);
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
			sb.append(bow.toSingleLine());
			sb.append(CharacterTable.LINE_FEED);
		}
		synchronized (this) {
			trainingWriter.println(sb.toString());
		}
	}

	synchronized void write(Set<String> set) {
		//logger.debug(Thread.currentThread().getName() + " is writing " +  form + "...");
		Iterator<String> it = set.iterator();
		String line;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; it.hasNext(); i++) {
			line = it.next();
			sb.append(line);
			sb.append(CharacterTable.LINE_FEED);
		}
		trainingWriter.print(sb.toString());
		//trainingWriter.flush();
	}

	class Training extends Thread implements Runnable {
		List<String[]> list;

		String form;

		Training(List<String[]> list, String form) {
			this.form = form;
			this.list = list;
		}

		public void run() {
			logger.debug(Thread.currentThread().getName() + " is training " + form + " (" + list.size() + ")...");
			//Map<String, Example> map = createBowMap();
			//Set<String> set = createVectorSet(map);
			//write(set);
			Map<String, List<String[]>> map = createMap();
			Map<String, BOW> bowMap = createBowMap(map);
			Map<String, Vector> vectorMap = createVectorMap(map);
			writeMap(map);
			writeBowMap(bowMap);
			writeVectorMap(vectorMap);
		}

		class Example {
			BOW bow;

			int freq;

			Example() {
				bow = new BOW();
				freq = 0;
			}

			void inc() {
				freq++;
			}

			void add(String[] tokens) {
				bow.addAll(tokens);
			}

			public BOW getBow() {
				return bow;
			}

			public int getFreq() {
				return freq;
			}
		}

		private void writeMap(Map<String, List<String[]>> map) {
			Iterator<String> it = map.keySet().iterator();
			List<String[]> list;
			String key;
			StringBuilder sb = new StringBuilder();
			String[] s;
			for (int i = 0; it.hasNext(); i++) {
				key = it.next();
				list = map.get(key);
				sb.append(form);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(key);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(list.size());
				sb.append(CharacterTable.HORIZONTAL_TABULATION);

				for (int j = 0; j < list.size(); j++) {
					s = list.get(j);
					if (j > 0) {
						sb.append(CharacterTable.SPACE);
					}
					sb.append("(");
					sb.append(j);
					sb.append(") ");

					sb.append(s[OneExamplePerSenseExtractor.LEFT_CONTEXT_INDEX]);
					sb.append(CharacterTable.SPACE);
					sb.append("<form>");
					sb.append(s[OneExamplePerSenseExtractor.FORM_INDEX]);
					sb.append("</form>");
					if (s.length > OneExamplePerSenseExtractor.RIGHT_CONTEXT_INDEX) {
						sb.append(CharacterTable.SPACE);
						sb.append(list.get(j)[OneExamplePerSenseExtractor.RIGHT_CONTEXT_INDEX]);
					}
				}
				sb.append(CharacterTable.LINE_FEED);
			}
			synchronized (this) {
				trainingWriter.print(sb.toString());
			}

		}

		private void writeBowMap(Map<String, BOW> map) {
			Iterator<String> it = map.keySet().iterator();
			BOW bow;
			String key;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; it.hasNext(); i++) {
				key = it.next();
				bow = map.get(key);
				sb.append(form);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(key);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(bow.toSingleLine());
				sb.append(CharacterTable.LINE_FEED);
			}
			synchronized (this) {
				bowWriter.print(sb.toString());
			}
		}

		private void writeVectorMap(Map<String, Vector> map) {
			Iterator<String> it = map.keySet().iterator();
			Vector vector;
			String key;
			StringBuilder sb = new StringBuilder();
			for (int i = 0; it.hasNext(); i++) {
				key = it.next();
				vector = map.get(key);
				sb.append(form);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(key);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(vector.toString());
				sb.append(CharacterTable.LINE_FEED);
			}
			synchronized (this) {
				vectorWriter.print(sb.toString());
			}
		}

		Map<String, List<String[]>> createMap() {
			Map<String, List<String[]>> map = new HashMap<String, List<String[]>>();
			String[] line;
			String key;
			for (int i = 0; i < list.size(); i++) {
				line = list.get(i);
				key = line[OneExamplePerSenseExtractor.PAGE_INDEX];
				List<String[]> list = map.get(key);
				if (list == null) {
					list = new ArrayList<String[]>();
					map.put(key, list);
				}
				list.add(line);
			}
			return map;
		}

		/*
			Vector d = lsm.mapDocument(bow);
	Vector pd = lsm.mapPseudoDocument(d);
	d.normalize();
	pd.normalize();

 */
		Vector createVector(List<String[]> list) {
			BOW bow = new BOW();
			String[] s;
			String[] leftContext;
			String[] rightContext;
			for (int i = 0; i < list.size(); i++) {
				s = list.get(i);
				leftContext = spacePattern.split(s[OneExamplePerSenseExtractor.LEFT_CONTEXT_INDEX].toLowerCase());
				rightContext = spacePattern.split(s[OneExamplePerSenseExtractor.RIGHT_CONTEXT_INDEX].toLowerCase());
				bow.addAll(leftContext);
				bow.addAll(rightContext);
			}
			Vector d = lsm.mapDocument(bow);
			Vector pd = lsm.mapPseudoDocument(d);
			d.normalize();
			pd.normalize();

			return pd;
		}


		BOW createBow(List<String[]> list) {
			BOW bow = new BOW();
			String[] s;
			String[] leftContext;
			String[] form;
			String[] rightContext;
			for (int i = 0; i < list.size(); i++) {
				s = list.get(i);
				leftContext = spacePattern.split(s[OneExamplePerSenseExtractor.LEFT_CONTEXT_INDEX].toLowerCase());
				form = spacePattern.split(s[OneExamplePerSenseExtractor.FORM_INDEX].toLowerCase());
				rightContext = spacePattern.split(s[OneExamplePerSenseExtractor.RIGHT_CONTEXT_INDEX].toLowerCase());
				bow.addAll(leftContext);
				bow.addAll(form);
				bow.addAll(rightContext);
			}
			logger.debug(bow);
			return bow;
		}

		Map<String, Vector> createVectorMap(Map<String, List<String[]>> map) {
			Map<String, Vector> vectorMap = new HashMap<String, Vector>();
			Iterator<String> it = map.keySet().iterator();
			String page;
			List<String[]> list;
			while (it.hasNext()) {
				page = it.next();
				list = map.get(page);
				vectorMap.put(page, createVector(list));
			}
			return vectorMap;
		}

		Map<String, BOW> createBowMap(Map<String, List<String[]>> map) {
			Map<String, BOW> bowMap = new HashMap<String, BOW>();
			Iterator<String> it = map.keySet().iterator();
			String page;
			List<String[]> list;
			while (it.hasNext()) {
				page = it.next();
				list = map.get(page);
				bowMap.put(page, createBow(list));
			}
			return bowMap;
		}

		Map<String, Example> createBowMap() {
			Map<String, Example> map = new HashMap<String, Example>();
			String[] line;
			for (int i = 0; i < list.size(); i++) {
				line = list.get(i);
				Example example = map.get(line[OneExamplePerSenseExtractor.PAGE_INDEX]);
				if (example == null) {
					//bow = new BOW();
					example = new Example();
					//map.put(line[PAGE], bow);
					map.put(line[OneExamplePerSenseExtractor.PAGE_INDEX], example);
				}
				example.inc();
				example.add(spacePattern.split(line[OneExamplePerSenseExtractor.LEFT_CONTEXT_INDEX].toLowerCase()));
				example.add(spacePattern.split(line[OneExamplePerSenseExtractor.RIGHT_CONTEXT_INDEX].toLowerCase()));
				//bow.addAll(spacePattern.split(line[LEFT].toLowerCase()));
				//bow.addAll(spacePattern.split(line[RIGHT].toLowerCase()));
			}
			return map;
		}

		Set<String> createVectorSet(Map<String, Example> map) {
			Set<String> set = new HashSet<String>();
			Iterator<String> it = map.keySet().iterator();
			StringBuilder sb;
			String label;
			BOW bow;
			Example example;
			for (int i = 0; it.hasNext(); i++) {
				label = it.next();
				//bow = map.get(label);
				example = map.get(label);
				//Vector vec = lsm.mapDocument(bow);
				bow = example.getBow();
				Vector vec = lsm.mapDocument(bow);
				vec.normalize();

				Vector pseudoVec = lsm.mapPseudoDocument(vec);
				pseudoVec.normalize();
				sb = new StringBuilder();
				sb.append(form);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(example.getFreq());
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(label);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(bow.toSortedLine());
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(pseudoVec.toString());
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(vec.toString(lsm.getDimension()));
				set.add(sb.toString());
			}
			return set;
		}

	}

	public static void main(String[] args) {
		// java com.ml.test.net.HttpServerDemo
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option inputFileNameOpt = OptionBuilder.withArgName("input").hasArg().withDescription("input file").isRequired().withLongOpt("input").create("i");
			Option outputFileNameOpt = OptionBuilder.withArgName("output").hasArg().withDescription("output file").isRequired().withLongOpt("output").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("nt").hasArg().withDescription("number of threads").withLongOpt("nt").create("n");
			//Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("maximum number of pages to process (default is all)").withLongOpt("num-pages").create("p");
			Option lsmDirOpt = OptionBuilder.withArgName("lsm").hasArg().withDescription("lsm dir").isRequired().withLongOpt("lsm").create("l");
			Option lsmDimOpt = OptionBuilder.withArgName("dim").hasArg().withDescription("lsm dim").withLongOpt("dim").create("d");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(inputFileNameOpt);
			options.addOption(outputFileNameOpt);
			options.addOption(numThreadOpt);
			//options.addOption(numPageOpt);
			options.addOption(lsmDirOpt);
			options.addOption(lsmDimOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			logger.debug(options);

			int numThread = DEFAULT_NUMBER_OF_THREAD;
			if (line.hasOption("nt")) {
				numThread = Integer.parseInt(line.getOptionValue("nt"));
			}

			logger.debug(line.getOptionValue("nt") + "\t" + line.getOptionValue("qs"));
			logger.debug(line.getOptionValue("output") + "\t" + line.getOptionValue("input") + "\t" + line.getOptionValue("lsm"));

			String lsmDirName = line.getOptionValue("lsm");
			if (!lsmDirName.endsWith(File.separator)) {
				lsmDirName += File.separator;
			}

			File fileUt = new File(lsmDirName + "X-Ut");
			File fileSk = new File(lsmDirName + "X-S");
			File fileR = new File(lsmDirName + "X-row");
			File fileC = new File(lsmDirName + "X-col");
			File fileDf = new File(lsmDirName + "X-df");
			int dim = DEFAULT_LSM_DIM;
			if (line.hasOption("dim")) {
				dim = Integer.parseInt(line.getOptionValue("dim"));
			}
			logger.debug(line.getOptionValue("lsm") + "\t" + line.getOptionValue("dim"));

			LSM lsm = new LSM(fileUt, fileSk, fileR, fileC, fileDf, dim, true);
			//LSM lsm = null;
			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			SimpleTrainingExtractor trainingExtractor = new SimpleTrainingExtractor(lsm, line.getOptionValue("output"), numThread);
			trainingExtractor.train(line.getOptionValue("input"));


		} catch (ParseException e) {
			// oops, something went wrong
			System.err.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.SimpleTrainingExtractor", "\n", options, "\n", true);
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
