/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.SetIndexer;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 1/24/13
 * Time: 6:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageClassIndexer extends SetIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageCategoryIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(PageClassIndexer.class.getName());

	public static final String KEY_FIELD_NAME = "page";
	public static final String VALUE_FIELD_NAME = "class";

	public static final int VALUE_COLUMN_INDEX = 1;
	public static final int KEY_COLUMN_INDEX = 0;


	public PageClassIndexer(String indexName) throws IOException {
		this(indexName, false);
	}

	public PageClassIndexer(String indexName, boolean clean) throws IOException {
		super(indexName, KEY_FIELD_NAME, VALUE_FIELD_NAME, clean);
	}

	public PageClassIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		this(indexName, keyFieldName, valueFieldName, false);
	}

	public PageClassIndexer(String indexName, String keyFieldName, String valueFieldName, boolean clean) throws IOException {
		super(indexName, keyFieldName, valueFieldName, clean);
	}

	@Override
	public void index(String fileName, boolean compress) throws IOException {
		index(fileName, KEY_COLUMN_INDEX, VALUE_COLUMN_INDEX, compress);
	}

	@Override
	public void index(File file, boolean compress) throws IOException {
		index(file, KEY_COLUMN_INDEX, VALUE_COLUMN_INDEX, compress);
	}

}
