package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.FreqSet;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/30/13
 * Time: 5:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class NGramCaseVariantBuilder {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>NGramCaseVariantBuilder</code>.
	 */
	static Logger logger = Logger.getLogger(NGramCaseVariantBuilder.class.getName());

	private static Pattern tabPattern = Pattern.compile("\t");

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");

	public static final int DEFAULT_NOTIFICATION_POINT = 1000000;

	public NGramCaseVariantBuilder(File in, File out) throws IOException {
		Map<String, Set<String>> map = read(in);
		writeSortedMap(map, out);
	}

	public Map<String, Set<String>> read(File in) throws IOException {
		logger.info("reading " + in + "...");
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();
		long begin = System.currentTimeMillis(), end = 0;
		FreqSet freqSet = new FreqSet();
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(in), "UTF-8"));
		//Tokenizer tokenizer = HardTokenizer.getInstance();
		String line;
		int tot = 1;
		while ((line = lnr.readLine()) != null) {
			String[] t = tabPattern.split(line);
			if (t.length == 3) {
				String key = t[2].toLowerCase();
				Set<String> set = map.get(key);
				if (set == null) {
					set = new HashSet<String>();
					map.put(key, set);
				}
				set.add(t[2]);
			}
			if ((tot % DEFAULT_NOTIFICATION_POINT) == 0) {
				end = System.currentTimeMillis();
				logger.info(df.format(tot) + "\t" + df.format(freqSet.size()) + "\t" + df.format(end - begin) + "\t" + new Date());
				begin = System.currentTimeMillis();
			}
			tot++;
		}
		lnr.close();
		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(freqSet.size()) + "\t" + df.format(end - begin) + "\t" + new Date());
		return map;
	}

	private static void writeSortedMap(Map<String, Set<String>> map, File out) throws IOException {
		logger.info("writing " + out + "...");
		long begin = System.currentTimeMillis();
		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));
		Iterator<String> it = map.keySet().iterator();
		StringBuilder sb = new StringBuilder();
		int count = 0;
		for (int i = 0; it.hasNext(); i++) {
			String key = it.next();
			Iterator<String> it1 = map.get(key).iterator();
			for (int j = 0; it1.hasNext(); j++) {
				pw.print(key);
				pw.print(CharacterTable.HORIZONTAL_TABULATION);
				pw.println(it1.next());
				count++;
			}
		}
		long end = System.currentTimeMillis();
		logger.info(df.format(map.size()) + " of " + df.format(count) + " distinct forms saved in " + df.format(end - begin) + " ms " + new Date());
		pw.close();
	}
/*public String toCapitalized() {
		if (form.length() == 0) {
			return form;
		}
		return Character.toUpperCase(form.charAt(0)) + form.substring(1, form.length());

	}

	public String toUpperCase() {
		return form.toUpperCase();
	}

	public String toLowerCase() {
		return form.toLowerCase();
	}

	public boolean isCapitalized() {
		if (form.length() == 0) {
			return false;
		}
		if (Character.isUpperCase(form.charAt(0))) {
			return true;
		}
		return false;
	}

	public boolean isUpperCase() {
		for (int i = 0; i < form.length(); i++) {
			if (Character.isLowerCase(form.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public boolean isLowerCase() {
		for (int i = 0; i < form.length(); i++) {
			if (Character.isUpperCase(form.charAt(i))) {
				return false;
			}
		}
		return true;
	}*/

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}


		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option inputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("ngram file").isRequired().withLongOpt("input").create("i");
			Option outputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("lowercase ngram file").withLongOpt("output").create("o");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(inputFileOpt);
			options.addOption(outputFileOpt);
			//options.addOption(keyFieldNameOpt);
			//options.addOption(valueFieldNameOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			File in = new File(line.getOptionValue("input"));
			File out = new File(line.getOptionValue("output"));

			NGramCaseVariantBuilder nGramCaseVariantBuilder = new NGramCaseVariantBuilder(in, out);
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.NGramCaseVariantBuilder", "\n", options, "\n", true);
		}
	}
}
