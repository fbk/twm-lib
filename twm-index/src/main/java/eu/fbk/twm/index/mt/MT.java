/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.mt;

import eu.fbk.twm.index.PageFormSearcher;
import eu.fbk.twm.index.util.FreqSetSearcher;
import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.ParsedPageTitle;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.index.csv.CSVExtractor;


import java.io.*;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 12/18/13
 * Time: 4:36 PM
 * To change this template use File | Settings | File Templates.
 *
 * Used with Marco Turchi, Acl 2014
 *
 * cd ~/IdeaProjects/jservice
 * java -Dfile.encoding=UTF-8 -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient --disambiguation -o localhost -m annotate -p 9000 --output-format=csv --min-weight 0.01 --overlapping-filter 0 --cross 1 --topic 1 --target-lang it --input-file tmp/gnome/en/ --output-file tmp/gnome/annotation/en/
 * java -Dfile.encoding=UTF-8 -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient --disambiguation -o localhost -m annotate -p 9000 --output-format=csv --min-weight 0.01 --overlapping-filter 0 --cross 1 --topic 1 --target-lang it --input-file tmp/kde_annotated/en/ --output-file tmp/kde_annotated/annotation/en/
 *
 * java -Dfile.encoding=UTF-8 -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient --disambiguation -o localhost -m annotate -p 9000 --output-format=csv --min-weight 0.01 --overlapping-filter 0 --cross 1 --topic 1 --target-lang en --input-file tmp/gnome/it/ --output-file tmp/gnome/annotation/it/
 * java -Dfile.encoding=UTF-8 -cp dist/jservice.jar com.machinelinking.server.SimpleHttpClient --disambiguation -o localhost -m annotate -p 9000 --output-format=csv --min-weight 0.01 --overlapping-filter 0 --cross 1 --topic 1 --target-lang en --input-file tmp/kde_annotated/it/ --output-file tmp/kde_annotated/annotation/it/
 *
 * cd claudio@wikifiki.apnetwork.ch:~/thewikimachine
 *
 * java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.experiments.mt.MT -d /data/corpora/wikipedia/itwiki-20131125-pages-articles.xml -m it -a tmp/kde_annotated//annotation/en/ -t 1
 * java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.experiments.mt.MT -d /data/corpora/wikipedia/itwiki-20131125-pages-articles.xml -m it -a tmp/gnome/annotation/en/ -t 1
 *
 * java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.experiments.mt.MT -d /data/corpora/wikipedia/enwiki-20130904-pages-articles.xml -m en -a tmp/kde_annotated//annotation/it/ -t 1
 * java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.experiments.mt.MT -d /data/corpora/wikipedia/enwiki-20130904-pages-articles.xml -m en -a tmp/gnome/annotation/it/ -t 1
 *
 * rsync -vaz claudio@wikifiki.apnetwork.ch:~/thewikimachine/tmp/kde_annotated/annotation/en/ ~/Dropbox/MachineLinking4SMT/kde_annotated/annotation/en/
 * rsync -vaz claudio@wikifiki.apnetwork.ch:~/thewikimachine/tmp/kde_annotated/annotation/it/ ~/Dropbox/MachineLinking4SMT/kde_annotated/annotation/it/
 * rsync -vaz claudio@wikifiki.apnetwork.ch:~/thewikimachine/tmp/gnome/annotation/it/ ~/Dropbox/MachineLinking4SMT/gnome/annotation/it/
 * rsync -vaz claudio@wikifiki.apnetwork.ch:~/thewikimachine/tmp/gnome/annotation/en/ ~/Dropbox/MachineLinking4SMT/gnome/annotation/en/
 *
 */
public class MT extends CSVExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>MT</code>.
	 */
	static Logger logger = Logger.getLogger(MT.class.getName());
	private String annotatedFileName;

	PrintWriter termWriter, termsWriter;
	AtomicInteger count;

	PageFormSearcher pageFormSearcher;

	public MT(String annotatedFileName, int numThreads) throws IOException {
		super(numThreads);
		this.annotatedFileName = annotatedFileName;
		String termOutputFile = annotatedFileName + ".term";
		String termsOutputFile = annotatedFileName + ".terms";
		termWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(termOutputFile), "UTF-8")));
		termsWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(termsOutputFile), "UTF-8")));
		count = new AtomicInteger();
	}


	@Override
	public void end() {
		termsWriter.close();
		termWriter.close();
	}

	@Override
	public void processLine(String line) {
		//To change body of implemented methods use File | Settings | File Templates.

		String[] s = line.split("\t");
		//logger.debug(s.length + "\t" + line);
		int v = count.incrementAndGet();
		//todo: s[4].cotains(s[5])
		//if (s.length > 6 && s[4].equals(s[5])) {
		if (s.length > 6) {
			FreqSetSearcher.Entry[] targetTerms = pageFormSearcher.search(s[6]);
			String targetTerm = rules(s[1], s[2], s[6], targetTerms);
			if (targetTerm != null) {
				//System.out.println(s[1] + "\t" + s[2] + "\t" + s[6] + "\t>" +targetTerm + "\t" + Arrays.toString(targetTerms));
				synchronized (this) {
					System.out.println(v + "\t" + s[1] + "\t" + s[2] + "\t" + s[6] + "\t" + targetTerm);
					termWriter.println(line + "\t" + targetTerm);
					termsWriter.print(line);
					if (targetTerms.length > 0) {
						termsWriter.print("\t");
						for (int i = 0; i < targetTerms.length; i++) {
							if (i>0){
								termsWriter.print(",");
							}
							termsWriter.print(targetTerms[i].getValue().replace(" - ", "-") + "=" + targetTerms[i].getFreq());
						}
						termsWriter.print("\n");
					}
				}
			}


		}

	}

	String rules(String sourceTerm, String sourcePage, String targetPage, FreqSetSearcher.Entry[] targetTerms) {
		//logger.debug(sourceTerm + "\t" + sourcePage + "\t" + targetPage + "\t" + Arrays.toString(targetTerms));
		ParsedPageTitle parsedSourcePageTitle = new ParsedPageTitle(sourcePage);
		ParsedPageTitle parsedTargetPageTitle = new ParsedPageTitle(targetPage);
		String targetTerm = parsedTargetPageTitle.getForm();

		//logger.debug(sourceTerm + " =?" + parsedSourcePageTitle.getForm());

		if (sourceTerm.equalsIgnoreCase(parsedSourcePageTitle.getForm())) {
			if (Character.isUpperCase(sourceTerm.charAt(0))) {
				return targetTerm + "=1";
			}
			else {
				return Character.toLowerCase(targetTerm.charAt(0)) + targetTerm.substring(1, targetTerm.length()) + "=1";
			}
		}
		if (sourceInTarget(sourceTerm, targetTerms)) {
			return sourceTerm + "=1";
		}
		if (targetTerms.length>0) {
			return targetTerms[0].getValue().replace(" - ", "-") + "=" + targetTerms[0].getFreq();
		}
		return null;



	}

	boolean sourceInTarget(String sourceTerm, FreqSetSearcher.Entry[] targetTerms) {
		for (int i = 0; i < targetTerms.length; i++) {
			if (sourceTerm.equalsIgnoreCase(targetTerms[i].getValue())) {
				return true;
			}
		}
		return false;
	}
	//String

	@Override
	public void start(ExtractorParameters extractorParameters) {
		//To change body of implemented methods use File | Settings | File Templates.
		try {
			pageFormSearcher = new PageFormSearcher(extractorParameters.getWikipediaPageFormIndexName());

			read(annotatedFileName);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option modelDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("directory from which to read the model files").isRequired().withLongOpt("model-dir").create("m");
			Option annotatedFileOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("file from which to read the annotated text").isRequired().withLongOpt("annotated-file").create("a");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(wikipediaDumpOpt);
			options.addOption(annotatedFileOpt);
			options.addOption(modelDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("model-dir"));
			logger.debug(extractorParameters);
			File f = new File(line.getOptionValue("annotated-file"));
			if (f.isFile()) {
				MT mt = new MT(line.getOptionValue("annotated-file"), numThreads);
				mt.setNotificationPoint(notificationPoint);
				mt.start(extractorParameters);

			}
			else {
				File[] files = f.listFiles(new FilenameFilter() {
					public boolean accept(File directory, String fileName) {
						return fileName.endsWith(".ml");
					}
				});
				for (int i = 0; i < files.length; i++) {
					try {
						//File outputFile = new File(f.getAbsolutePath() + File.separator + files[i].getName() + ".terms");
						MT mt = new MT(files[i].getAbsolutePath(), numThreads);
						mt.setNotificationPoint(notificationPoint);
						mt.start(extractorParameters);
					} catch (Exception ex) {
						logger.error("Error at " + files[i]);
						logger.error(ex);
					}
				}
			}
		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.experiments.mt.MT", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
