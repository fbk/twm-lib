/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.SetSearcher;
import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.WikipediaCategory;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/24/13
 * Time: 11:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class CategorySubCategorySearcher extends SetSearcher {

    /**
     * Define a static logger variable so that it references the
     * Logger instance named <code>CategorySubCategorySearcher</code>.
     */
    static Logger logger = Logger.getLogger(CategorySubCategorySearcher.class.getName());

    public static final int DEFAULT_IMAGE_SIZE = 100;

    public CategorySubCategorySearcher(String indexName) throws IOException {
        super(indexName, CategorySubCategoryIndexer.CATEGORY_FIELD_NAME,
                CategorySubCategoryIndexer.SUB_CATEGORY_FIELD_NAME);
        logger.trace(toString(10));
    }

    public String[] search(String category, int d) {
        logger.info("searching label " + category + " at depth " + d + "...");
        //long beginTotal = System.currentTimeMillis(), beginPartial = 0, endPartial = 0;
        String[] result = search(category);

        if (d <= 0) {
            return result;
        }

        //Set<String> completeResult = new HashSet<String>();
        //Iterator<String> it = result.iterator();
        String cat = null;
        for (int i = 0; i < result.length; i++) {

            String[] s = search(result[i], d - 1);
            //completeResult.addAll(set);
        }

        //long endTotal = System.currentTimeMillis();
        return null;
    }

    public void search(WikipediaCategory category, Set<WikipediaCategory> subcategories) {
        String[] categories = search(category.getLabel());
        //logger.info(category.getLabel() + "\t" + categories.length);
        for (int i = 0; i < categories.length; i++) {
            String path = (category.getPath().length() == 0 ?
                    category.getLabel() :
                    category.getPath() + ">" + category.getLabel());
            WikipediaCategory subcategory = new WikipediaCategory(categories[i], category.getDepth() + 1, path);
            boolean b = subcategories.add(subcategory);
            if (b) {
                search(subcategory, subcategories);
            }

        }
    }

    public void search(WikipediaCategory category, Set<WikipediaCategory> subcategories, Set<String> blackList) {
        String[] categories = search(category.getLabel());
        //logger.info(category.getLabel() + "\t" + categories.length);
        for (int i = 0; i < categories.length; i++) {
            if (blackList.contains(categories[i])) {
                continue;
            }
            String path = (category.getPath().length() == 0 ?
                    category.getLabel() :
                    category.getPath() + ">" + category.getLabel());
            WikipediaCategory subcategory = new WikipediaCategory(categories[i], category.getDepth() + 1, path);
            boolean b = subcategories.add(subcategory);
            if (b) {
                search(subcategory, subcategories, blackList);
            }

        }
    }

    public void search(String category, Set<WikipediaCategory> subcategories, int max, Set<String> blackList) {
        search(new WikipediaCategory(category), subcategories, max, blackList);
    }

    public void search(String category, Set<WikipediaCategory> subcategories, int max) {
        search(new WikipediaCategory(category), subcategories, max);
    }

    public void search(WikipediaCategory category, Set<WikipediaCategory> subcategories, int max) {
        if (category.getDepth() >= max) {
            subcategories.add(category);
            return;
        }
        String[] categories = search(category.getLabel());
        //logger.debug(category + "\t" + categories.length);
        for (int i = 0; i < categories.length; i++) {
            String path = (category.getPath().length() == 0 ?
                    category.getLabel() :
                    category.getPath() + ">" + category.getLabel());
            WikipediaCategory subcategory = new WikipediaCategory(categories[i], category.getDepth() + 1, path);
            boolean b = subcategories.add(subcategory);
            if (b) {
                search(subcategory, subcategories, max);
            }

        }
    }

    public void search(WikipediaCategory category, Set<WikipediaCategory> subcategories, int max,
            Set<String> blackList) {
        //logger.debug(blackList.size() + "\t"+ category.getLabel());
        //if (blackList.contains(category.getLabel())){
        //	logger.error("FOUND " + category.getLabel());
        //	return;
        //}
        if (category.getDepth() >= max) {
            subcategories.add(category);
            return;
        }
        String[] categories = search(category.getLabel());
        //logger.debug(category + "\t" + categories.length);
        for (int i = 0; i < categories.length; i++) {
            if (blackList.contains(categories[i])) {
                continue;
            }
            String path = (category.getPath().length() == 0 ?
                    category.getLabel() :
                    category.getPath() + ">" + category.getLabel());
            WikipediaCategory subcategory = new WikipediaCategory(categories[i], category.getDepth() + 1, path);
            boolean b = subcategories.add(subcategory);
            if (b) {
                search(subcategory, subcategories, max, blackList);
            }

        }
    }

    public void search(String category, Set<WikipediaCategory> subcategories) {
        search(new WikipediaCategory(category), subcategories);
    }

    public static void main(String args[]) throws Exception {
        String logConfig = System.getProperty("log-config");
        if (logConfig == null) {
            logConfig = "configuration/log-config.txt";
        }

        PropertyConfigurator.configure(logConfig);
        Options options = new Options();
        try {
            Option indexNameOpt = OptionBuilder.withArgName("index").hasArg()
                    .withDescription("open an index with the specified name").isRequired().withLongOpt("index")
                    .create("i");
            Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode")
                    .withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
            Option searchOpt = OptionBuilder.withArgName("search").hasArg()
                    .withDescription("search for the specified key").withLongOpt("search").create("s");
            //Option freqFileOpt = OptionBuilder.withArgName("key-freq").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
            //Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
            Option maxDepthOpt = OptionBuilder.withArgName("int").hasArg().withDescription("max depth").isRequired()
                    .withLongOpt("max-depth").create("m");
            options.addOption(OptionBuilder.withArgName("list").hasArg()
                    .withDescription("comma separated list of forbidden categories").withLongOpt("black-list")
                    .create("b"));
            //Option minimumKeyFreqOpt = OptionBuilder.withArgName("minimum-freq").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
            //Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
            options.addOption("h", "help", false, "print this message");
            options.addOption("v", "version", false, "output version information and exit");

            options.addOption(indexNameOpt);
            options.addOption(interactiveModeOpt);
            options.addOption(searchOpt);
            //options.addOption(freqFileOpt);
            //options.addOption(keyFieldNameOpt);
            options.addOption(maxDepthOpt);
            //options.addOption(minimumKeyFreqOpt);
            //options.addOption(notificationPointOpt);

            CommandLineParser parser = new PosixParser();
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("help") || line.hasOption("version")) {
                throw new ParseException("");
            }

            Set<String> blackList = new HashSet<>();
            if (line.hasOption("black-list")) {
                String[] s = line.getOptionValue("black-list").split(",");

                for (int i = 0; i < s.length; i++) {
                    blackList.add(s[i]);
                }
            }

            int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
            if (line.hasOption("notification-point")) {
                notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
            }
            CategorySubCategorySearcher categorySuperCategorySearcher = new CategorySubCategorySearcher(
                    line.getOptionValue("index"));
            categorySuperCategorySearcher.setNotificationPoint(notificationPoint);

            if (line.hasOption("max-depth")) {
                int maxDepth = Integer.parseInt(line.getOptionValue("max-depth"));
                if (line.hasOption("search")) {
                    logger.debug("searching " + line.getOptionValue("search") + "...");
                    Set<WikipediaCategory> result = new HashSet<>();
                    categorySuperCategorySearcher.search(line.getOptionValue("search"), result, maxDepth, blackList);
                    Iterator<WikipediaCategory> it = result.iterator();
                    for (int i = 0; it.hasNext(); i++) {
                        logger.info(i + "\t" + it.next());
                    }
                    logger.info(result.size());
                }
            } else {

                if (line.hasOption("search")) {
                    logger.debug("searching " + line.getOptionValue("search") + "...");
                    Set<WikipediaCategory> result = new HashSet<>();
                    categorySuperCategorySearcher.search(line.getOptionValue("search"), result);

                    ////logger.info(result);

                }

            }
            if (line.hasOption("interactive-mode")) {
                categorySuperCategorySearcher.interactive();
            }
        } catch (ParseException e) {
            // oops, something went wrong
            if (e.getMessage().length() > 0) {
                System.out.println("Parsing failed: " + e.getMessage() + "\n");
            }
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(400,
                    "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.CategorySubCategorySearcher",
                    "\n", options, "\n", true);
        }
    }
}
