package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.*;
import eu.fbk.utils.math.Node;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/2/13
 * Time: 6:53 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * This class extracts for each page the categories up to the specified depth.
 * It uses the in memory mapping between pages and categories and categories and super categories (read from file)
 * The depth threshold must be fixed in advance (7?)
 * The output is a set of categories with the number of pages the contain.
 *
 */
public class PageAllCategoryExtractor extends AbstractCategoryExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageAllCategoryExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(PageAllCategoryExtractor.class.getName());

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected static DecimalFormat wf = new DecimalFormat("###,###,###,###.000");

	Map<String, Set<String>> pageCategoryMap;

	Map<String, Set<String>> categorySuperCategoryMap;

	SynchronizedIndexer categoryIndex;

	PrintWriter pageTopCategoryWriter;

	Writer categoryIndexWriter;

	Map<String, Double> categoryWeight;

	public PageAllCategoryExtractor(int numThreads) {
		super(numThreads);
		categoryIndex = new SynchronizedIndexer<String>();
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				WeightedSet weightedSet = search(s[0]);
				long end = System.nanoTime();
				Map<Double, List<String>> sortedMap = weightedSet.toSortedMap();
				logger.debug(sortedMap + "\t" + tf.format(end - begin));
				logger.debug("print\n" + print(s[0], weightedSet));

			}
			else if (s.length == 2) {
				long begin = System.nanoTime();
				WeightedSet weightedSet0 = search(s[0]);
				WeightedSet weightedSet1 = search(s[1]);
				Node[] node0 = toNodeArray(weightedSet0);
				Node[] node1 = toNodeArray(weightedSet1);
				double dot01 = dot(node0, node1);
				double dot00 = dot(node0, node0);
				double dot11 = dot(node1, node1);
				long end = System.nanoTime();
				double dot = dot01 /Math.sqrt(dot00*dot11);
				logger.debug(s[0] + "\t" + s[1] + "\t" + wf.format(dot) + "\t" + wf.format(dot01) + "\t" + tf.format(end - begin));


			}
		}
	}

	@Override
	public void notification(int tot, long begin, long end) {
		//super.notification(tot, begin, end);    //To change body of overridden methods use File | Settings | File Templates.
		logger.info(df.format(categoryIndex.size()) + "\t" + df.format(tot) + "\t" + df.format(end - begin) + "\t" + new Date());
	}

	public double dot(Node[] x, Node[] y) {
		Map<Integer, String> reverseMap = categoryIndex.reverseIndex();
		double sum = 0;
		int xlen = x.length;
		int ylen = y.length;
		int i = 0;
		int j = 0;
		int count=0;
		while (i < xlen && j < ylen) {
			if (x[i].index == y[j].index) {
				logger.debug((count++)+"/"+x[i].index + "\t" + reverseMap.get(x[i].index) + "\t" + y[j].index + "\t" + x[i].value + "\t" + y[j].value);
				sum += x[i++].value * y[j++].value;
			}
			else {
				if (x[i].index > y[j].index) {
					++j;
				}
				else {
					++i;
				}
			}
		}
		return sum;
	}

	public static String normalizePageName(String s) {
		if (s.length() == 0) {
			return s;
		}

		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		}

		return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());

	}

	private String tabulator(int l) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < l; i++) {
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
		}
		return sb.toString();
	}

	void search(Set<String> categories, WeightedSet weightedSet, int depth) {
		//logger.debug("searching: " + categories + "\t" + depth + "...");

		//if (depth > 5 && weightedSet.size() > 0) {
		if (categories == null || depth > getMaxDepth()) {
			//logger.debug("{{{" + depth + "}}}");
			//logger.debug("stop1 " + depth);
			return;
		}

		Iterator<String> it = categories.iterator();
		for (int i = 0; it.hasNext(); i++) {
			String normalizedCategory = normalizePageName(it.next());
			if (!weightedSet.contains(normalizedCategory)) {
				double w =  categoryWeight.get(normalizedCategory);
				//weightedSet.add(normalizedCategory, (double) w / depth);
				weightedSet.add(normalizedCategory, w);
				try {
					Set<String> superCategories = categorySuperCategoryMap.get(normalizedCategory);
					if (superCategories != null) {
						//logger.trace(tabulator(depth) + "{" + normalizedCategory + ", " + depth + ", " + wf.format(w)+ ", " + superCategories.size() + ", " + superCategories + "}");
						search(superCategories, weightedSet, depth + 1);
					}
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
		//logger.debug("stop2 " + depth);
		return;
	}

	WeightedSet search(String page) {
		WeightedSet weightedSet = new WeightedSet();
		Set<String> categories = pageCategoryMap.get(page);
		//logger.trace(page + "\t" + categories);
		if (categories!=null){
			//

			//logger.debug(page + ": " + categories);
			//long begin = System.nanoTime();
			//logger.debug(page);
			search(categories, weightedSet, 1);
			//long end = System.nanoTime();
			//long time = end - begin;
			//logger.info(weightedSet.size() + " topics found in " + tf.format(time) + " ns");
		}

		return weightedSet;
	}

	@Override
	public void processLine(String line) {
		//logger.debug("begin " +line);
		String[] t = tabPattern.split(line);
		WeightedSet weightedSet = search(t[0]);
		Node[] nodes = toNodeArray(weightedSet);
		if (nodes.length>0){
			String s = t[0] + "\t" + Node.toString(nodes);
			//String s = print(t[0], weightedSet);
			synchronized (this) {
				//pageTopCategoryWriter.print(s);
				pageTopCategoryWriter.println(s);
			}

		}
	}

	Map<String, Set<String>> readMap(String name) throws IOException {
		logger.info("reading " + name + "...");
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));

		String line = null;
		int count = 0, key = 0, value = 1, part = 0, tot = 0;
		String oldKey = "";
		Set<String> set = new HashSet<String>();
		String[] t;
		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				if (!t[key].equals(oldKey)) {
					result.put(oldKey, set);
					count++;
					set = new HashSet<String>();
					part = 0;
				}
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last line
		result.put(oldKey, set);
		lnr.close();
		return result;
	}


	Map<String, Double> readCategoryFrequency(String name) throws IOException {
		logger.info("reading " + name + "...");
		Map<String, Double> result = new HashMap<>();
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));

		String line = null;

		double max = 0;
		int tot=0;
		Set<String> set = new HashSet<String>();
		String[] t;
		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				max = Double.parseDouble(t[1]);
				result.put(t[0],0.0);
			}
			tot++;
		}

		double val = 0;
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				val = Double.parseDouble(t[1]);

				result.put(t[0], Math.log10(max/val));
			}
			tot++;
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(end - begin) + " ms " + new Date());

		lnr.close();
		return result;
	}

	private Node[] toNodeArray(WeightedSet weightedSet) {
		SortedSet<Node> set = toSortedSet(weightedSet);
		return set.toArray(new Node[set.size()]);
	}

	private SortedSet<Node> toSortedSet(WeightedSet weightedSet) {
		SortedSet<Node> set = new TreeSet<>();
		Iterator<String> it = weightedSet.iterator();
		for (int i = 0; it.hasNext(); i++) {
			String key = it.next();
			int index = categoryIndex.get(key);
			double value = weightedSet.get(key);
			//logger.trace(key + "\t" + index + "\t" + value);
			Node node = new Node(index, value);
			set.add(node);

		}
		return set;
	}

	private String print(String page, WeightedSet weightedSet) {
		StringBuilder sb = new StringBuilder();
		Iterator<String> it = weightedSet.iterator();
		for (int i = 0; it.hasNext(); i++) {
			String key = it.next();
			int index = categoryIndex.get(key);
			double value = weightedSet.get(key);
			sb.append(page);
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
			sb.append(key);
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
			sb.append(value);
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
			sb.append(index);
			sb.append("\n");
			//logger.debug(t[0] + "\t" + key + "\t" + value);
			/*for (int j = 0; j < value; j++) {
				sb.append(page);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(key);
				
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(index);
				sb.append("\n");

			} */
			//sb.append(t[0]);
			//sb.append(CharacterTable.HORIZONTAL_TABULATION);
			//sb.append(key);
			//sb.append(CharacterTable.HORIZONTAL_TABULATION);
			//sb.append(value);
			//sb.append("\n");
		}
		//logger.debug(t[0] + ">>\n" + sb.toString());
		return sb.toString();
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			pageCategoryMap = readMap(extractorParameters.getWikipediaPageCategoryFileName());
			categorySuperCategoryMap = readMap(extractorParameters.getWikipediaCategorySuperCategoryFileName());
			categoryWeight = readCategoryFrequency(extractorParameters.getWikipediaSortedPagePerCategoryCountFileName());
			pageTopCategoryWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaPageAllCategoryFileName()), "UTF-8")));
			categoryIndexWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediCategoryIndexFileName()), "UTF-8"));
			read(extractorParameters.getWikipediaTitleIdFileName());
		} catch (IOException e) {
			logger.error(e);
		}
	}

	@Override
	public void end() {
		pageTopCategoryWriter.close();
		try {
			categoryIndex.write(categoryIndexWriter);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		//PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			//Option categoryMappingDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("directory from which to read the category mapping files").isRequired().withLongOpt("category-mapping-dir").create("c");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			Option maxDepthOpt = OptionBuilder.withArgName("int").hasArg().withDescription("recursion maximum category depth (default is " + AbstractCategoryExtractor.DEFAULT_MAX_CATEGORY_DEPTH + ")").withLongOpt("max-depth").create();
			Option interactiveModeOpt = OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create();

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");
			options.addOption(OptionBuilder.withDescription("trace mode").withLongOpt("trace").create());
			options.addOption(OptionBuilder.withDescription("debug mode").withLongOpt("debug").create());

			options.addOption(interactiveModeOpt);
			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			//options.addOption(categoryMappingDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(maxDepthOpt);
			options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			Properties defaultProps = new Properties();
			try {
				defaultProps.load(new InputStreamReader(new FileInputStream(logConfig), "UTF-8"));
			} catch (Exception e) {
				defaultProps.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
				defaultProps.setProperty("log4j.appender.stdout.layout.ConversionPattern", "[%t] %-5p (%F:%L) - %m %n");
				defaultProps.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
				defaultProps.setProperty("log4j.appender.stdout.Encoding", "UTF-8");
			}

			if (line.hasOption("trace")) {
				defaultProps.setProperty("log4j.rootLogger", "trace,stdout");
			}
			else if (line.hasOption("debug")) {
				defaultProps.setProperty("log4j.rootLogger", "debug,stdout");
			}
			else {
				if (defaultProps.getProperty("log4j.rootLogger") == null) {
					defaultProps.setProperty("log4j.rootLogger", "info,stdout");
				}
			}
			PropertyConfigurator.configure(defaultProps);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);
			PageAllCategoryExtractor pageAllCategoryExtractor = new PageAllCategoryExtractor(numThreads);
			if (line.hasOption("max-depth")) {
				pageAllCategoryExtractor.setMaxDepth(Integer.parseInt(line.getOptionValue("max-depth")));
			}
			pageAllCategoryExtractor.setNotificationPoint(notificationPoint);

			if (line.hasOption("interactive-mode")) {

				try {
					pageAllCategoryExtractor.interactive();
				} catch (Exception e) {
					logger.error(e);
				}
			} else {
				pageAllCategoryExtractor.start(extractorParameters);
			}

		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.PageAllCategoryExtractor", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
