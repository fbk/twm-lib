/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index.csv;

import eu.fbk.twm.utils.*;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/25/13
 * Time: 5:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class FirstNameExtractor extends CSVExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>FirstNameExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(FirstNameExtractor.class.getName());

	private static final Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);
	
	public static final int FIRST_NAME_COLUMN = 1;
	
	private Map<String, Integer> freqFormMap;

	private Tokenizer tokenizer;

	private SynchronizedCounter synchronizedCounter;

	private PrintWriter firstNameWriter;

	private static DecimalFormat df = new DecimalFormat("###,###,###,###");


	public FirstNameExtractor(int numThreads, int numPages) {
		super(numThreads, numPages);
		tokenizer = new HardTokenizer();
		synchronizedCounter = new SynchronizedCounter();

	}

	public FirstNameExtractor(int numThreads) {
		super(numThreads);
		tokenizer = new HardTokenizer();
		synchronizedCounter = new SynchronizedCounter();

	}

	@Override
	public void processLine(String line) {
		//logger.debug(line.substring(0, 1));
		String[] t = tabPattern.split(line);
		if (t.length > FIRST_NAME_COLUMN) {
			String tokenizedFirstName = tokenizer.tokenizedString(t[FIRST_NAME_COLUMN]);
			synchronizedCounter.add(tokenizedFirstName);

		}
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			
			firstNameWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaFirstNameFileName()), "UTF-8")));
			read(extractorParameters.getWikipediaPersonInfoFileName());
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public void end() {
		logger.info("sorting...");
		long begin = System.currentTimeMillis();
		SortedMap<AtomicInteger, List<String>> sortedMap = synchronizedCounter.getSortedMap();
		try {
			writeSortedMap(sortedMap);
			firstNameWriter.close();
		} catch (IOException e) {
			logger.error(e);
		}
		long end = System.currentTimeMillis();
		logger.info(sortedMap.size() + " lines sorted in " + df.format(end - begin) + " ms " + new Date());	}

	private void writeSortedMap(SortedMap<AtomicInteger, List<String>> sortedMap) throws IOException {
		logger.info("writing...");
		long begin = System.currentTimeMillis();
		
		String tokenizedFirstName;
		Iterator<AtomicInteger> it = sortedMap.keySet().iterator();
		StringBuilder sb;
		AtomicInteger firstNameFreq;
		List<String> list;
		for (int i = 0; it.hasNext(); i++) {
			firstNameFreq = it.next();
			list = sortedMap.get(firstNameFreq);
			sb = new StringBuilder();
			for (int j = 0; j < list.size(); j++) {
				tokenizedFirstName = list.get(j);
				
				
				
				sb.append(firstNameFreq);
				sb.append(CharacterTable.HORIZONTAL_TABULATION);
				sb.append(tokenizedFirstName);
				sb.append(CharacterTable.LINE_FEED);
			}
			firstNameWriter.print(sb.toString());
		}
		long end = System.currentTimeMillis();
		logger.info(df.format(sortedMap.size()) + " lines wrote in " + df.format(end - begin) + "\t" + new Date());
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");


			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			//options.addOption(ngramSizeOpt);
			options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			/*int ngramSize = FirstNameExtractor.DEFAULT_N_GRAM;
			if (line.hasOption("n-gram")) {
				ngramSize = Integer.parseInt(line.getOptionValue("n-gram"));
			} */

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);
			CSVExtractor firstNameExtractor = new FirstNameExtractor(numThreads);
			firstNameExtractor.setNotificationPoint(notificationPoint);
			firstNameExtractor.start(extractorParameters);


		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.csv.FirstNameExtractor", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}

}
