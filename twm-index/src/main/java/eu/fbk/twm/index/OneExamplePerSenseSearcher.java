/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.csv.OneExamplePerSenseExtractor;
import eu.fbk.twm.index.util.AbstractSearcher;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.lsa.LSI;
import eu.fbk.utils.math.Node;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import eu.fbk.twm.utils.Defaults;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:11 PM
 * To change this template use File | Settings | File Templates.
 *
 * This the best configuration for disambiguation
 */
public class OneExamplePerSenseSearcher extends AbstractSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OneExamplePerSenseSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(OneExamplePerSenseSearcher.class.getName());

	public static final int DEFAULT_MIN_FREQ = 1000;

	public static final boolean DEFAULT_THREAD_SAFE = false;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	protected static DecimalFormat rf = new DecimalFormat("###,###,##0.000000");

	protected static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected boolean threadSafe;

	protected Map<String, Entry[]> cache;

	protected Term keyTerm;

	private boolean normalized;

	public OneExamplePerSenseSearcher(String indexName) throws IOException {
		this(indexName, false);
	}

	public OneExamplePerSenseSearcher(String indexName, boolean threadSafe) throws IOException {
		super(indexName);
		this.threadSafe = threadSafe;
		normalized = OneExamplePerSenseExtractor.DEFAULT_NORMALIZE;
		keyTerm = new Term(OneExamplePerSenseIndexer.PAGE_FIELD_NAME, "");
		logger.debug(keyTerm);
	}

	public Map<String, Entry[]> getCache() {
		return cache;
	}

	public void loadCache(String name) throws IOException {
		loadCache(new File(name));
	}

	public void loadCache(String name, int minFreq) throws IOException {
		loadCache(new File(name), minFreq);
	}

	public void loadCache(File f) throws IOException {
		loadCache(f, DEFAULT_MIN_FREQ);
	}

	public void loadCache(File f, int minFreq) throws IOException {
		logger.info("loading cache from " + f + " (freq>" + minFreq + ")...");
		long begin = System.nanoTime();

		if (threadSafe) {
			logger.info(this.getClass().getName() + "'s cache is thread safe");
			cache = Collections.synchronizedMap(new HashMap<String, Entry[]>());
		}
		else {
			logger.warn(this.getClass().getName() + "'s cache isn't thread safe");
			cache = new HashMap<String, Entry[]>();
		}

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;
		int i = 1;
		String[] t;
		int freq = 0;
		Entry[] result;
		Document doc;
		TermDocs termDocs;
		setNotificationPoint(100);
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				freq = Integer.parseInt(t[0]);
				if (freq < minFreq) {
					break;
				}
				termDocs = indexReader.termDocs(keyTerm.createTerm(t[1]));
				if (termDocs.next()) {
					doc = indexReader.document(termDocs.doc());
					result = fromByte(doc.getBinaryValue(OneExamplePerSenseIndexer.ENTRY_FIELD_NAME));
					cache.put(t[1], result);
				}
			}
			if ((i % notificationPoint) == 0) {
				//System.out.print(CharacterTable.FULL_STOP);
				logger.debug(i + " keys read (" + cache.size() + ") " + new Date());
			}
			i++;
		}
		System.out.print(CharacterTable.LINE_FEED);
		lnr.close();
		long end = System.nanoTime();
		logger.info(df.format(cache.size()) + " (" + df.format(indexReader.numDocs()) + ") keys cached in " + tf.format(end - begin) + " ns");
	}

	public boolean isNormalized() {
		return normalized;
	}

	public void setNormalized(boolean normalized) {
		this.normalized = normalized;
	}

	public Entry[] search(String key) {
		//logger.debug("searching " + key + "...");
		//long begin = 0, end = 0;

		//begin = System.nanoTime();
		Entry[] result = null;
		if (cache != null) {
			result = cache.get(key);
		}
		//end = System.nanoTime();

		if (result != null) {
			//logger.debug("found in cache in " + tf.format(end - begin) + " ns");
			return result;
		}

		try {
			//begin = System.nanoTime();
			TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(key));
			//end = System.nanoTime();
			//logger.debug("found in index in " + tf.format(end - begin) + " ns");

			if (termDocs.next()) {
				//begin = System.nanoTime();
				Document doc = indexReader.document(termDocs.doc());
				result = fromByte(doc.getBinaryValue(OneExamplePerSenseIndexer.ENTRY_FIELD_NAME));
				//end = System.nanoTime();
				//logger.debug(termDocs.freq() + " deserialized in " + tf.format(end - begin) + " ns");

				return result;
			}
		} catch (IOException e) {
			logger.error(e);
		}
		return new Entry[0];
	}

	public void interactive(LSI lsi) throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);
			Tokenizer tokenizer = HardTokenizer.getInstance();

			BOW bow = new BOW();

			if (s.length > 1) {
				long begin = System.nanoTime();
				Entry[] entries = search(s[1]);
				long end = System.nanoTime();
				String[] left = tokenizer.stringArray(s[0].toLowerCase());
				bow.addAll(left);
				if (s.length > 2) {
					String[] right = tokenizer.stringArray(s[2].toLowerCase());
					bow.addAll(right);
				}
				logger.debug(bow);
				Node[] bowVector = lsi.mapDocument(bow);
				logger.debug("bow\t" + Node.toString(bowVector));

				if (normalized) {
					Node.normalize(bowVector);
				}
				Node[] lsVector = lsi.mapPseudoDocument(bowVector);
				//bowVector.normalize();
				if (normalized) {
					Node.normalize(lsVector);
				}
				//logger.debug("bow\t" + bowVector);
				//logger.debug("lsi\t" + lsVector);
				double normBow, maxBow = 0;
				double normLs, maxLs = 0;
				Sense[] senses = new Sense[entries.length];
				for (int i = 0; i < entries.length; i++) {
					normBow = Node.norm(entries[i].getBowVector());
					normLs = Node.norm(entries[i].getLsVector());
					//logger.debug(i + "\t" + Node.toString(entries[i].getLsVector()));
					if (normBow > maxBow)
					{
						maxBow = normBow;
					}
					if (normLs > maxLs)
					{
						maxLs = normLs;
					}
					//logger.debug(i + "\t" + normBow + "\t" + normLs);
				}
				logger.debug("-\t" + maxBow + "\t" + maxLs);
				/*for (int i = 0; i < entries.length; i++) {

					Node.normalize(entries[i].getBowVector(), maxBow);
					Node.normalize(entries[i].getLsVector(), maxLs);
					//logger.debug(i + "\t" + Node.toString(entries[i].getLsVector()));
				} */

				//Node.normalize(bowVector, maxBow);
				logger.debug("*\t" + Node.toString(lsVector));
				Node.normalize(bowVector);
				Node.normalize(lsVector, maxLs);
				logger.debug("+\t" + Node.toString(lsVector));
				for (int i = 0; i < entries.length; i++) {
					Node.normalize(entries[i].getBowVector(), maxBow);
					Node.normalize(entries[i].getLsVector(), maxLs);

					//logger.debug(i + "\t" + Node.toString(entries[i].getBowVector()));
					//logger.debug(i + "\t" + Node.toString(entries[i].getLsVector()));
					if (normalized) {
						Node.normalize(entries[i].getBowVector());
						Node.normalize(entries[i].getLsVector());
					}
					double bowKernel = Node.dot(bowVector, entries[i].getBowVector());
					double lsKernel = Node.dot(lsVector, entries[i].getLsVector());
					//logger.debug(i + "\t" + entries[i].getPage() + "\t" + rf.format(bowKernel) + "\t" + rf.format(lsKernel) + "\t" + rf.format(entries[i].getFreq()));
					senses[i] = new Sense(entries[i].getPage(), bowKernel, lsKernel, entries[i].getFreq());
				}
				Arrays.sort(senses, new Comparator<Sense>() {
					@Override
					public int compare(Sense sense, Sense sense2) {
						double diff = sense.getCombo() - sense2.getCombo();
						if (diff > 0) {
							return -1;
						}
						else if (diff < 0) {
							return 1;
						}
						return 0;
					}
				});
				logger.info("i\tprior\tbow\tls\tcombo\tpage");
				for (int i = 0; i < senses.length; i++) {
					logger.info(i + "\t" + rf.format(senses[i].getPrior()) + "\t" + rf.format(senses[i].getBow()) + "\t" + rf.format(senses[i].getLs()) + "\t" + rf.format(senses[i].getCombo()) + "\t" + rf.format(senses[i].getCombo() * senses[i].getPrior())+  "\t" + senses[i].getPage());
				}
			}
		}
	}


	public class Sense implements Comparable<Sense> {
		private double bow;

		private double ls;

		private double prior;

		private double combo;

		public String page;

		Sense(String page, double bow, double ls, double prior) {
			this.page = page;
			this.bow = bow;
			this.ls = ls;
			this.prior = prior;
			combo = (bow + ls) / 2;
		}

		public String getPage() {
			return page;
		}

		public double getCombo() {
			return combo;
		}

		public double getBow() {
			return bow;
		}

		public double getLs() {
			return ls;
		}

		public double getPrior() {
			return prior;
		}

		@Override
		public int compareTo(Sense sense) {
			double diff = combo - sense.getCombo();
			if (diff > 0) {
				return -1;
			}
			else if (diff < 0) {
				return 1;
			}

			return 0;
		}
	}

	private Entry[] fromByte(byte[] byteArray) throws IOException {
		ByteArrayInputStream byteStream = new ByteArrayInputStream(byteArray);
		DataInputStream dataStream = new DataInputStream(byteStream);

		int size = dataStream.readInt();
		Entry[] entries = new Entry[size];
		int vectorSize;
		for (int j = 0; j < size; j++) {
			entries[j] = new Entry();
			entries[j].setPage(dataStream.readUTF());
			entries[j].setFreq(dataStream.readDouble());
			entries[j].setLsVector(readVector(dataStream));
			entries[j].setBowVector(readVector(dataStream));
		}
		return entries;
	}

	private Node[] readVector(DataInputStream dataStream) throws IOException {
		int m = dataStream.readInt();
		Node[] vector = new Node[m];
		for (int i = 0; i < m; i++) {
			vector[i] = new Node(dataStream.readInt(), dataStream.readDouble());
		}
		return vector;
	}

	public class Entry {
		private double freq;

		private String page;

		private Node[] bowVector;

		private Node[] lsVector;

		public double getFreq() {
			return freq;
		}

		public void setFreq(double freq) {
			this.freq = freq;
		}

		public String getPage() {
			return page;
		}

		public void setPage(String page) {
			this.page = page;
		}

		public Node[] getLsVector() {
			return lsVector;
		}

		public void setLsVector(Node[] lsVector) {
			this.lsVector = lsVector;
		}

		public Node[] getBowVector() {
			return bowVector;
		}

		public void setBowVector(Node[] bowVector) {
			this.bowVector = bowVector;
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option searchOpt = OptionBuilder.hasArg().withDescription("search for the specified key").withLongOpt("search").create("s");
			Option freqFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
			Option minimumKeyFreqOpt = OptionBuilder.withArgName("int").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			Option lsmDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("lsi dir").isRequired().withLongOpt("lsi").create("l");
			Option lsmDimOpt = OptionBuilder.withArgName("int").hasArg().withDescription("lsi dim").withLongOpt("dim").create("d");
			Option normalizedOpt = OptionBuilder.withDescription("normalize vectors (default is " + OneExamplePerSenseExtractor.DEFAULT_NORMALIZE + ")").withLongOpt("normalized").create("n");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			options.addOption(freqFileOpt);
			options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);
			options.addOption(lsmDirOpt);
			options.addOption(lsmDimOpt);
			options.addOption(normalizedOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			OneExamplePerSenseSearcher oneExamplePerSenseSearcher = new OneExamplePerSenseSearcher(line.getOptionValue("index"));
			oneExamplePerSenseSearcher.setNotificationPoint(notificationPoint);

			if (line.hasOption("key-freq")) {
				oneExamplePerSenseSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
			}
			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				Entry[] result = oneExamplePerSenseSearcher.search(line.getOptionValue("search"));
				logger.info(Arrays.toString(result));
			}
			if (line.hasOption("interactive-mode")) {

				String lsmDirName = line.getOptionValue("lsi");
				if (!lsmDirName.endsWith(File.separator)) {
					lsmDirName += File.separator;
				}

				File fileUt = new File(lsmDirName + "X-Ut");
				File fileSk = new File(lsmDirName + "X-S");
				File fileR = new File(lsmDirName + "X-row");
				File fileC = new File(lsmDirName + "X-col");
				File fileDf = new File(lsmDirName + "X-df");
				int dim = 100;
				if (line.hasOption("dim")) {
					dim = Integer.parseInt(line.getOptionValue("dim"));
				}
				logger.debug(line.getOptionValue("lsi") + "\t" + line.getOptionValue("dim"));
				boolean normalized = false;
				if (line.hasOption("normalized")) {
					normalized = true;
				}
				LSI lsi = new LSI(fileUt, fileSk, fileR, fileC, fileDf, dim, true, normalized);

				oneExamplePerSenseSearcher.interactive(lsi);
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.OneExamplePerSenseSearcher", "\n", options, "\n", true);
		}
	}
}
