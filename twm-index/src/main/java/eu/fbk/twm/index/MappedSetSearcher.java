package eu.fbk.twm.index;

import eu.fbk.twm.index.util.SetSearcher;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 15/01/14
 * Time: 13:04
 * To change this template use File | Settings | File Templates.
 */
public abstract class MappedSetSearcher extends SetSearcher {
	protected Properties mappings;
	static Logger logger = Logger.getLogger(MappedSetSearcher.class.getName());

	public Properties getMappings() {
		return mappings;
	}

	public void setMappings(Properties mappings) {
		this.mappings = mappings;
	}

	protected MappedSetSearcher(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		super(indexName, keyFieldName, valueFieldName);
	}

	protected MappedSetSearcher(String indexName, String keyFieldName, String valueFieldName, boolean threadSafe) throws IOException {
		super(indexName, keyFieldName, valueFieldName, threadSafe);
	}

	protected MappedSetSearcher(String indexName) throws IOException {
		super(indexName);

	}

	@Override
	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				String[] result = search(s[0]);
				long end = System.nanoTime();

				if (result != null && result.length > 0) {
					for (int i = 0; i < result.length; i++) {
						logger.info(i + "\t" + result[i]);
						if (mappings != null) {
							logger.info("Topic: " + mappings.getProperty(result[i]));
						}
					}
					logger.info(s[0] + " found in " + tf.format(end - begin) + " ns");

				}
				else {
					logger.info(s[0] + " not found in " + tf.format(end - begin) + " ns");
				}
			}
		}
	}

}
