/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.twm.utils.WeightedSet;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/2/13
 * Time: 6:53 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * This class extracts for each page the top categories
 * It uses the in memory mapping between pages and categories and categories and super categories (read from file)
 * The depth threshold must be fixed in advance (7?)
 * The mapping category - top-category is loaded from a file in  /data/resources/category-mapping/[lang].properties
 * To verify if the top categories must be mapped to an integer
 */
public class TopCategorySearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>TopCategorySearcher</code>.
	 */
	static Logger logger = Logger.getLogger(TopCategorySearcher.class.getName());

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	public static final int DEFAULT_DEPTH = 10;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	PageCategorySearcher pageCategorySearcher;

	CategorySuperCategorySearcher categorySuperCategorySearcher;

	Properties properties;

	int maxDepth;

	public TopCategorySearcher(PageCategorySearcher pageCategorySearcher, CategorySuperCategorySearcher categorySuperCategorySearcher, int maxDepth, Properties properties) {
		this.pageCategorySearcher = pageCategorySearcher;
		this.categorySuperCategorySearcher = categorySuperCategorySearcher;
		this.maxDepth = maxDepth;
		this.properties = properties;
	}

	public void interactive() throws Exception {
		InputStreamReader reader = null;
		BufferedReader myInput = null;
		while (true) {
			System.out.println("\nPlease write a key and type <return> to continue (CTRL C to exit):");

			reader = new InputStreamReader(System.in);
			myInput = new BufferedReader(reader);
			String query = myInput.readLine().toString();
			String[] s = tabPattern.split(query);

			if (s.length == 1) {
				long begin = System.nanoTime();
				String page = s[0].replace(' ', CharacterTable.LOW_LINE);
				WeightedSet weightedSet = search(page);
				long end = System.nanoTime();
				Map<Double, List<String>> sortedMap = weightedSet.toSortedMap();
				logger.debug(page + "\t" + sortedMap + "\t" + tf.format(end - begin));

			}
		}
	}

	//todo: this must be done when the mapping is created
	public static String normalizePageName(String s) {
		if (s.length() == 0) {
			return s;
		}

		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		}

		return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
	}

	private String tabulator(int l) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < l; i++) {
			sb.append(CharacterTable.HORIZONTAL_TABULATION);
		}
		return sb.toString();
	}

	Set<String> search(String[] categories, WeightedSet weightedSet, int depth, Set<String> visitedSet) {
		//System.out.println(tabulator(depth) + "{" + depth + ", " + Arrays.toString(categories) + "}");

		if (categories == null || depth > maxDepth) {
			//logger.debug("{{{" + depth + "}}}");
			//logger.debug("stop1 " + depth);
			return new HashSet<String>();
		}

		for (int i = 0; i < categories.length; i++) {

			//String label = topMap.get(categories[i]);
			String normalizedCategory = normalizePageName(categories[i]);
			if (!visitedSet.contains(normalizedCategory)) {
				visitedSet.add(normalizedCategory);
				String label = properties.getProperty(normalizedCategory);
				//logger.debug(i + "\t" + category + "\t" + label);
				//String label = categories[i];

				if (label != null) {
					System.out.println(tabulator(depth) + "<" + normalizedCategory + ", '" + label + "', " + depth + ">");
					//WeightedSet.add(categories[i]);
					if (label.length() == 0) {
						logger.warn("stop category " + normalizedCategory);
					}
					else if (!label.equals("Factotum")) {
						weightedSet.add(label, (double) 1 / depth);
						//weightedSet.add(label, 1.0);
					}

				}
				else {
					try {


						String[] superCategories = categorySuperCategorySearcher.search(normalizedCategory);
						if (superCategories != null) {
							//logger.debug(i + "\t" + depth + "\t" + category + "\t" + superCategories.length + "\t" + Arrays.toString(superCategories));
							System.out.println(tabulator(depth) + "{" + normalizedCategory + ", " + depth + ", " + superCategories.length + ", " + Arrays.toString(superCategories) + "}");
							search(superCategories, weightedSet, depth + 1, visitedSet);
						}

					} catch (Exception e) {
						logger.error(e);
					}
				}
			}
			//else {
			//	logger.warn("SKIP " + category);
			//}
		}
		//logger.debug("stop2 " + depth);
		return new HashSet<String>();
	}

	WeightedSet search(String page) {
		WeightedSet weightedSet = new WeightedSet();
		String[] categories = pageCategorySearcher.search(page);
		//logger.debug(page + "\t" + categories.length +"\t" + Arrays.toString(categories));
		System.out.println("{" + page + ", 1," + categories.length + ", " + Arrays.toString(categories) + "}");
		//long begin = System.nanoTime();
		//logger.debug(page);
		Set<String> visitedSet = new HashSet<String>();
		search(categories, weightedSet, 1, visitedSet);
		//long end = System.nanoTime();
		//long time = end - begin;
		//logger.info(WeightedSet.size() + " topics found in " + nf.format(time) + " ns");
		return weightedSet;
	}

	Map<String, Set<String>> readMap(String name) throws IOException {
		logger.info("reading " + name + "...");
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(name), "UTF-8"));

		String line = null;
		int count = 0, key = 0, value = 1, part = 0, tot = 0;
		String oldKey = "";
		Set<String> set = new HashSet<String>();
		String[] t;
		// read the first line
		if ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		// read the rest of the file
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				if (!t[key].equals(oldKey)) {
					result.put(oldKey, set);
					count++;
					set = new HashSet<String>();
					part = 0;
				}
				set.add(t[value]);
				oldKey = t[key];
				part++;
			}
			tot++;
		}

		end = System.currentTimeMillis();
		logger.info(df.format(tot) + "\t" + df.format(count) + "\t" + df.format(end - begin) + " ms " + new Date());

		// and the last line
		result.put(oldKey, set);
		lnr.close();
		return result;
	}


	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option modelDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("model-dir").create("m");
			Option categoryMappingDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("directory from which to read the category mapping files").isRequired().withLongOpt("category-mapping-dir").create("c");
			//Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + AbstractWikipediaXmlDumpParser.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			//Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			//Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			Option depthOpt = OptionBuilder.withArgName("int").hasArg().withDescription("recursion depth (default is " + TopCategorySearcher.DEFAULT_DEPTH + ")").withLongOpt("depth").create();
			Option interactiveModeOpt = OptionBuilder.withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			logger.debug(options);
			//
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(interactiveModeOpt);
			options.addOption(wikipediaDumpOpt);
			options.addOption(modelDirOpt);
			options.addOption(categoryMappingDirOpt);
			//options.addOption(numThreadOpt);
			//options.addOption(numPageOpt);
			options.addOption(depthOpt);
			//options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}


			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("model-dir"));

			Properties properties = new Properties();
			if (line.hasOption("category-mapping-dir")) {
				String categoryMappingDir = line.getOptionValue("category-mapping-dir");
				if (!categoryMappingDir.endsWith(File.separator)) {
					categoryMappingDir += File.separator;
				}
				File file = new File(categoryMappingDir + extractorParameters.getLang() + ".properties");
				logger.info("reading category mappings from " + file + "...");
				properties.load(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
				logger.debug(properties.size() + " mappings read");
			}

			int depth = DEFAULT_DEPTH;
			if (line.hasOption("depth")) {
				depth = Integer.parseInt(line.getOptionValue("depth"));
			}

			//logger.debug(extractorParameters);

			PageCategorySearcher pageCategorySearcher = new PageCategorySearcher(extractorParameters.getWikipediaPageCategoryIndexName());
			CategorySuperCategorySearcher categorySuperCategorySearcher = new CategorySuperCategorySearcher(extractorParameters.getWikipediaCategorySuperCategoryIndexName());

			TopCategorySearcher TopCategorySearcher = new TopCategorySearcher(pageCategorySearcher, categorySuperCategorySearcher, depth, properties);
			if (line.hasOption("interactive-mode")) {

				try {
					TopCategorySearcher.interactive();
				} catch (Exception e) {
					logger.error(e);
				}
			}

		} catch (ParseException e) {
			// oops, something went wrong
			logger.error("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(200, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.TopCategorySearcher", "\n", options, "\n", true);
		} finally {
			logger.info("extraction ended " + new Date());
		}
	}
}
