/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.index;

import eu.fbk.twm.index.util.OrderedSetIndexer;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.twm.utils.ExtractorParameters;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 1/24/13
 * Time: 6:47 PM
 * Extract the indexer for navigation templates.
 * You can extract the clean list of these templates, sorted by frequency, with the command:
 */

// cut -f 2 [template-page-navigation.csv] | sort | uniq -c | sort -nr | sed -e 's/^[ ]*//' | cut -f2- -d' '

public class PageNavigationTemplateIndexer extends OrderedSetIndexer {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageNavigationTemplateIndexer</code>.
	 */
	static Logger logger = Logger.getLogger(PageNavigationTemplateIndexer.class.getName());

	public static final String PAGE_FIELD_NAME = "page";

	public static final String NAVIGATION_TEMPLATE_FIELD_NAME = "template";

	public static final int NAVIGATION_TEMPLATE_COLUMN_INDEX = 1;

	public static final int PAGE_COLUMN_INDEX = 0;


	public PageNavigationTemplateIndexer(String indexName) throws IOException {
		super(indexName, PAGE_FIELD_NAME, NAVIGATION_TEMPLATE_FIELD_NAME);
	}

	public PageNavigationTemplateIndexer(String indexName, String keyFieldName, String valueFieldName) throws IOException {
		super(indexName, keyFieldName, valueFieldName);
	}

	@Override
	public void index(String fileName, boolean compress) throws IOException {
		index(fileName, PAGE_COLUMN_INDEX, NAVIGATION_TEMPLATE_COLUMN_INDEX, compress);
	}

	@Override
	public void index(File file, boolean compress) throws IOException {
		index(file, PAGE_COLUMN_INDEX, NAVIGATION_TEMPLATE_COLUMN_INDEX, compress);
	}


	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		CommandLine line = null;

		String index = null;
		String file = null;
		String dump = null;
		String base = null;

		boolean fileNull = true;
		boolean dumpNull = true;

		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("create an index with the specified name").withLongOpt("index").create("i");
			Option inputFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("read the key/value pairs to index from the specified file").withLongOpt("file").create("f");

			Option dumpFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("Wikipedia XML dump file").withLongOpt("dump").create("d");
			Option baseFileOpt = OptionBuilder.withArgName("file").hasArg().withDescription("base Folder").withLongOpt("base").create("b");

			Option compressOpt = OptionBuilder.withDescription("set compression to true (default is " + WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT + ")").withLongOpt("compress").create("c");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(inputFileOpt);
			options.addOption(compressOpt);
			options.addOption(baseFileOpt);
			options.addOption(dumpFileOpt);

			CommandLineParser parser = new PosixParser();
			line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			index = line.getOptionValue("index");
			file = line.getOptionValue("file");

			dump = line.getOptionValue("dump");
			base = line.getOptionValue("base");

			fileNull = (index == null || file == null);
			dumpNull = (dump == null || base == null);

			if (fileNull && dumpNull) {
				throw new ParseException("You must specify either index/file or dump/base parameters.");
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PageNavigationTemplateIndexer", "\n", options, "\n", true);
			System.exit(1);
		}

		boolean compress = WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT;
		if (line.hasOption("compress")) {
			compress = true;
		}

		PageNavigationTemplateIndexer pageFileIndexer = null;
		if (dump != null && base != null) {
			ExtractorParameters extractorParameters = new ExtractorParameters(dump, base, true);
			pageFileIndexer = new PageNavigationTemplateIndexer(extractorParameters.getWikipediaPageNavigationTemplateIndexName());
			pageFileIndexer.index(extractorParameters.getWikipediaPageNavigationTemplateFileName(), compress);
		}
		else {
			pageFileIndexer = new PageNavigationTemplateIndexer(index);
			pageFileIndexer.index(file, compress);
		}
		pageFileIndexer.close();
	}
}
