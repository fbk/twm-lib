package eu.fbk.twm.index;

import eu.fbk.twm.index.util.AbstractSearcher;
import eu.fbk.twm.index.util.SerialUtils;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.StringTable;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import eu.fbk.twm.utils.Defaults;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 8/8/13
 * Time: 4:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageIdSearcher extends AbstractSearcher {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageIdSearcher</code>.
	 */
	static Logger logger = Logger.getLogger(PageIdSearcher.class.getName());

	public static final int DEFAULT_MIN_FREQ = 1000;

	public static final boolean DEFAULT_THREAD_SAFE = false;

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	private static DecimalFormat tf = new DecimalFormat("000,000,000.#");

	//
	private static DecimalFormat rf = new DecimalFormat("0.000000");


	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected boolean threadSafe;

	private Map<String, Integer> cache;

	private Term keyTerm;

	public PageIdSearcher(String indexName) throws IOException {
		this(indexName, DEFAULT_THREAD_SAFE);
	}

	public PageIdSearcher(String indexName, boolean threadSafe) throws IOException {
		super(indexName);
		this.threadSafe = threadSafe;

		keyTerm = new Term(PageIdIndexer.PAGE_FIELD_NAME, "");
		logger.debug(keyTerm);
	}

	public void loadCache(String name) throws IOException {
		loadCache(new File(name));
	}

	public void loadCache(String name, int minFreq) throws IOException {
		loadCache(new File(name), minFreq);
	}

	public void loadCache(File f) throws IOException {
		loadCache(f, DEFAULT_MIN_FREQ);
	}

	public void loadCache(File f, int minFreq) throws IOException {
		logger.info("loading cache from " + f + " (freq>" + minFreq + ")...");
		long begin = System.nanoTime();

		if (threadSafe) {
			logger.info(this.getClass().getName() + "'s cache is thread safe");
			cache = Collections.synchronizedMap(new HashMap<String, Integer>());
		}
		else {
			logger.warn(this.getClass().getName() + "'s cache isn't thread safe");
			cache = new HashMap<String, Integer>();
		}

		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		String line;
		int i = 1;
		String[] t;
		int freq = 0;
		Integer result;
		Document doc;
		TermDocs termDocs;
		while ((line = lnr.readLine()) != null) {
			t = tabPattern.split(line);
			if (t.length == 2) {
				freq = Integer.parseInt(t[0]);
				if (freq < minFreq) {
					break;
				}
				termDocs = indexReader.termDocs(keyTerm.createTerm(t[1]));
				if (termDocs.next()) {
					doc = indexReader.document(termDocs.doc());
					result = SerialUtils.toInt(doc.getBinaryValue(PageIdIndexer.ID_FIELD_NAME));
					cache.put(t[1], result);
				}
			}
			if ((i % notificationPoint) == 0) {
				//System.out.print(CharacterTable.FULL_STOP);
				logger.debug(i + " keys read (" + cache.size() + ") " + new Date());
			}
			i++;
		}
		System.out.print(CharacterTable.LINE_FEED);
		lnr.close();
		long end = System.nanoTime();
		logger.info(df.format(cache.size()) + " (" + df.format(indexReader.numDocs()) + ") keys cached in " + tf.format(end - begin) + " ns");
	}


	public Integer search(String key) {
		//long begin = System.nanoTime();
		Integer result = null;
		if (cache != null) {
			result = cache.get(key);
		}

		//long end = System.nanoTime();
		if (result != null) {
			//logger.debug("found in cache in " + tf.format(end - begin) + " ns");
			return result;
		}

		try {
			//begin = System.nanoTime();
			TermDocs termDocs = indexReader.termDocs(keyTerm.createTerm(key));
			//end = System.nanoTime();
			//logger.debug("\"" + key + "\" sought in index in " + tf.format(end - begin) + " ns");

			
			if (termDocs.next()) {
				//begin = System.nanoTime();
				Document doc = indexReader.document(termDocs.doc());
				result= SerialUtils.toInt(doc.getBinaryValue(PageIdIndexer.ID_FIELD_NAME));
				
				
				//end = System.nanoTime();
				//logger.debug("\"" + key + "\" deserialized in " + tf.format(end - begin) + " ns");
				return result;
				
			}
		} catch (IOException e) {
			logger.error(e);
		}

		return new Integer(-1);
	}

	public void interactive() throws Exception {
		InputStreamReader indexReader = null;
		BufferedReader myInput = null;
		long begin = 0, end = 0;
		while (true) {
			System.out.println("\nPlease write a query and type <return> to continue (CTRL C to exit):");

			indexReader = new InputStreamReader(System.in);
			myInput = new BufferedReader(indexReader);
			String query = myInput.readLine().toString();
			begin = System.nanoTime();
			Integer e = search(query);
			end = System.nanoTime();
			logger.info("query\tdf\tlf\tratio\ttime");
			logger.info(query + "\t" + e + "\t" + tf.format(end - begin) + " ns");
		}
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode").withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option searchOpt = OptionBuilder.withArgName("search").hasArg().withDescription("search for the specified key").withLongOpt("search").create("s");
			Option freqFileOpt = OptionBuilder.withArgName("key-freq").hasArg().withDescription("read the keys' frequencies from the specified file").withLongOpt("key-freq").create("f");
			//Option keyFieldNameOpt = OptionBuilder.withArgName("key-field-name").hasArg().withDescription("use the specified name for the field key").withLongOpt("key-field-name").create("k");
			//Option valueFieldNameOpt = OptionBuilder.withArgName("value-field-name").hasArg().withDescription("use the specified name for the field value").withLongOpt("value-field-name").create("v");
			Option minimumKeyFreqOpt = OptionBuilder.withArgName("minimum-freq").hasArg().withDescription("minimum key frequency of cached values (default is " + DEFAULT_MIN_FREQ + ")").withLongOpt("minimum-freq").create("m");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			options.addOption(freqFileOpt);
			//options.addOption(keyFieldNameOpt);
			//options.addOption(valueFieldNameOpt);
			options.addOption(minimumKeyFreqOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			PageIdSearcher pageIdSearcher = new PageIdSearcher(line.getOptionValue("index"));
			pageIdSearcher.setNotificationPoint(notificationPoint);
			if (line.hasOption("key-freq")) {
				pageIdSearcher.loadCache(line.getOptionValue("key-freq"), minFreq);
			}
			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				Integer result = pageIdSearcher.search(line.getOptionValue("search"));
				logger.info(result);
			}
			if (line.hasOption("interactive-mode")) {
				pageIdSearcher.interactive();
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.index.PageIdSearcher", "\n", options, "\n", true);
		}
	}

}
