/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump.util;

import eu.fbk.twm.wiki.xmldump.AbstractWikipediaExtractor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

public class WikiTemplateParser {

	public static ArrayList<WikiTemplate> parse(String content, boolean stopOnText) {

		// Get templates before text
		ArrayList<WikiTemplate> listOfTemplates = new ArrayList<WikiTemplate>();
		ArrayList<Integer> start = new ArrayList<Integer>();
		ArrayList<Integer> count = new ArrayList<Integer>();
		HashMap<Integer, ArrayList<WikiTemplate>> templatesOnLevel = new HashMap<Integer, ArrayList<WikiTemplate>>();

		templatesOnLevel.put(0, new ArrayList<WikiTemplate>());

		int max = content.length();
		for (int i = 0; i < max - 1; i++) {
			char c1, c2;
			int j = i;

			while ((Character.toString(content.charAt(j)).trim()).length() == 0) {
				j++;
				i++;
				if (j > max - 1) {
					break;
				}
			}
			if (j > max - 1) {
				break;
			}
			c1 = content.charAt(j);
			j++;
			if (j > max - 1) {
				break;
			}
			while ((Character.toString(content.charAt(j)).trim()).length() == 0) {
				j++;
				i++;
				if (j > max - 1) {
					break;
				}
			}
			if (j > max - 1) {
				break;
			}
			c2 = content.charAt(j);

			if (c1 == '{' && c2 == '{') {
				start.add(i);
				int s = count.size();
				if (s > 0) {
					count.set(s - 1, count.get(s - 1) + 1);
				}
				count.add(0);
				templatesOnLevel.put(s + 1, new ArrayList<WikiTemplate>());
				i++;
			}
			if (c1 == '}' && c2 == '}') {
				if (start.size() > 0) {
					int myStart = start.get(start.size() - 1);
					int myEnd = i + 1;
					int includedTemplates = count.get(count.size() - 1);
					String myContent = content.substring(myStart + 2, myEnd - 1).trim();

					WikiTemplate t = new WikiTemplate(myStart, myEnd, myContent, includedTemplates);
					t.setChildren(templatesOnLevel.get(count.size()));

					if (count.size() == 1) {
						t.isRoot = true;
					}
					listOfTemplates.add(t);
					templatesOnLevel.get(count.size() - 1).add(t);

/*
					System.out.println(start);
                    System.out.println(countPageCounter);
                    System.out.println(myStart);
                    System.out.println(myEnd);
                    System.out.println(myContent);
                    System.out.println(templatesOnLevel);
                    System.out.println(t);
                    System.exit(0);
*/

					start.remove(start.size() - 1);
					count.remove(count.size() - 1);
				}
				i++;
			}

			String myChar = Character.toString(c1);
			if (stopOnText && myChar.trim().length() > 0 && start.size() == 0 && c1 != '}') {
				break;
			}
			// System.out.print(c);
		}

		for (WikiTemplate c : listOfTemplates) {
			String templateContent = c.getContent();
			int level = 0;
			int fromChar = 0;
			ArrayList<String> parts = new ArrayList<String>();

			for (int i = 0; i < templateContent.length() - 1; i++) {
				char c1 = templateContent.charAt(i);
				char c2 = templateContent.charAt(i + 1);
				if (c1 == '{' && c2 == '{') {
					level++;
					i++;
				}
				if (c1 == '[' && c2 == '[') {
					level++;
					i++;
				}
				if (c1 == '}' && c2 == '}') {
					level--;
					i++;
				}
				if (c1 == ']' && c2 == ']') {
					level--;
					i++;
				}
				if (c1 == '|' && level == 0) {
					if (i > fromChar) {
						parts.add(templateContent.substring(fromChar, i).trim());
					}
					fromChar = i + 1;
				}
			}
			if (templateContent.substring(fromChar).trim().length() > 0) {
				parts.add(templateContent.substring(fromChar));
			}
			c.setParts(parts);
		}

		return listOfTemplates;
	}

	public static ArrayList<WikiTemplate> getTemplates(String content) {
		return parse(content, true);
	}

	public static ArrayList<WikiTemplate> getTemplatesFromFile(String filename) {
		return getTemplatesFromFile(filename, true);
	}

	public static ArrayList<WikiTemplate> getTemplatesFromFile(String filename, boolean stopOnText) {
		StringBuilder strbuf = new StringBuilder();

		// Read filePageCounter
		try {
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				strbuf.append(line).append("\n");
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return parse(strbuf.toString(), stopOnText);
	}

	public static void main(String[] args) {
		String filename = args[0];
		boolean stopOnText = false;
		ArrayList<WikiTemplate> listOfTemplates = getTemplatesFromFile(filename, stopOnText);

		for (WikiTemplate t : listOfTemplates) {
			String name = t.getFirstPart();
			if (name == null) {
				System.out.println("Name null!");
				continue;
			}
			String nName = AbstractWikipediaExtractor.normalizePageName(name.trim()).replace(' ', '_').toLowerCase();
			if (!nName.equals("film")) {
				continue;
			}

			HashMap<String, String> parts = t.getHashMapOfParts();
			// logger.info(title + " -- " + name + " -- " + parts.size());
			for (String p : parts.keySet()) {
				if (p.equals(name)) {
					continue;
				}

				String value = parts.get(p);
				value = value.replaceAll("<ref>.*?</ref>", "");

				System.out.println(p);
				System.out.println(value);
				System.out.println();
				// logger.info("   " + p + " -- " + parts.get(p));
			}
		}

		// System.out.println(listOfTemplates);
	}
}