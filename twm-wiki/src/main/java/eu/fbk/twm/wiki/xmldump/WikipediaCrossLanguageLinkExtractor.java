/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WikipediaCrossLanguageLinkExtractor extends AbstractWikipediaExtractor implements WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaCrossLanguageLinkExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaCrossLanguageLinkExtractor.class.getName());

	private PrintWriter crossLanguageWriter;

	protected Pattern crossLanguagePattern;

	public WikipediaCrossLanguageLinkExtractor(int numThreads, int numPages, Locale locale) {
		super(numThreads, numPages, locale);
		crossLanguagePattern = Pattern.compile("\\[\\[(\\w\\w:[^\\]]+)\\]\\]");
		logger.info("crossLanguagePattern: " + crossLanguagePattern);
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			crossLanguageWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaCrossLanguageLinkFileName()), "UTF-8")));
		} catch (IOException e) {
			logger.error(e);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {
		StringBuilder buffer = new StringBuilder();
		Matcher m = crossLanguagePattern.matcher(text);
		buffer.append(title);
		while (m.find()) {
			int s = m.start(1);
			int e = m.end(1);
			String foreignPage = text.substring(s, e).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);

			buffer.append(CharacterTable.HORIZONTAL_TABULATION);
			buffer.append(foreignPage);

		}

		synchronized (this) {
			crossLanguageWriter.println(buffer.toString());
		}
	}

	@Override
	public void endProcess() {
		super.endProcess();
		crossLanguageWriter.flush();
		crossLanguageWriter.close();
	}

	/*public static void main(String argv[]) throws IOException
	{
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) logConfig = "configuration/log-config.txt";

		PropertyConfigurator.configure(logConfig);


		if (argv.length < 2) {
			System.out.println("");
			System.out.println("USAGE:");
			System.out.println("");
			System.out.println("java -mx6G org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaCrossLanguageLinkExtractor\n" +
							" in-wiki-xml -- Input file\n" +
							" out-cross-csv -- Output template file\n" +
							" locale -- Locale (used for resources)\n" +
							" [threads=1] -- Number of threads to use\n" +
							" [count=0] -- Lines to stop, 0 means never stop");
			System.out.println("");
			System.exit(1);
		}

		int parID = 0;

		String xin = argv[parID++];
		String xout = argv[parID++];
		Locale locale = new Locale(argv[parID++]);

		int threads = 1;
		if (argv.length > parID) {
			threads = Integer.parseInt(argv[parID++]);
		}

		int size = 0;
		if (argv.length > parID) {
			size = Integer.parseInt(argv[parID++]);
		}

		WikipediaCrossLanguageLinkExtractor crossLanguageWriter = new WikipediaCrossLanguageLinkExtractor(threads, size, locale);
		crossLanguageWriter.start(xin, xout);
	}  */

}