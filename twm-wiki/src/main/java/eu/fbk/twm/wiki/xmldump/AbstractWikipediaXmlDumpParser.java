/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.CharacterTable;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.log4j.Logger;
import org.xml.sax.*;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 08/01/13
 * Time: 11:50
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractWikipediaXmlDumpParser extends DefaultHandler implements LexicalHandler {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>AbstractWikipediaXmlDumpParser</code>.
	 */
	static Logger logger = Logger.getLogger(AbstractWikipediaXmlDumpParser.class.getName());

	protected static final String LEXICAL_HANDLER_PROPERTY_ID = "http://xml.org/sax/properties/lexical-handler";
	protected static final String DEFAULT_PARSER_NAME = "org.apache.xerces.parsers.SAXParser";

	// Variables
	protected int fElementDepth;
	protected boolean fXML11;
	protected boolean fInCDATA;
	protected long begin, end;
	public AtomicLong genBegin = new AtomicLong(), genEnd = new AtomicLong();
	protected DecimalFormat decimalFormat;
	protected String xpath;

	public AtomicInteger generalCount = new AtomicInteger();

	// Wikipedia variables
	private String currentTitle = "";
	private int currentWikiID = 0;
	private String currentRedirect = null;
	private String currentText = "";
	protected StringBuilder content = new StringBuilder();

	// Multithread stuff
	private int numThreads;

	private ExecutorService myExecutor;

	public final static int DEFAULT_QUEUE_SIZE = 10000;

	public AbstractWikipediaXmlDumpParser(int numThreads) {
		this.numThreads = numThreads;
		logger.info("creating the thread executor (" + numThreads + ")");
		//myExecutor = Executors.newFixedThreadPool(numThreads);
		int blockQueueSize = DEFAULT_QUEUE_SIZE;
		BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(blockQueueSize);
		RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();
		myExecutor = new ThreadPoolExecutor(numThreads, numThreads, 1, TimeUnit.MINUTES, blockingQueue, rejectedExecutionHandler);
	}

	public int getNumThreads() {
		return numThreads;
	}

	/**
	 * Receive notification of the begin of a process.
	 */
	public void startProcess(String xin) {
		//this.wikipediaFileName = wikipediaFileName;
		XMLReader parser;

		xpath = "";
		decimalFormat = new DecimalFormat("###,###,###,###");

		try {
			parser = XMLReaderFactory.createXMLReader(DEFAULT_PARSER_NAME);

			// set parser
			parser.setContentHandler(this);
			parser.setErrorHandler(this);
			parser.setProperty(LEXICAL_HANDLER_PROPERTY_ID, this);

			if (xin.endsWith(".bz2")) {
				logger.info("parsing a bz2 file");
				FileInputStream in = new FileInputStream(xin);
				BZip2CompressorInputStream bin = new BZip2CompressorInputStream(in);
				parser.parse(new InputSource(bin));
			} else {
				parser.parse(xin);
			}


		} catch (SAXParseException e) {
			logger.error("SAXParseException at " + currentTitle + " " + e);
		} catch (Exception e) {
			logger.error("Error at " + currentTitle + " " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void printSituation() {
		String s = String.format("Pages: %10s - %7s ms - %s", decimalFormat.format(generalCount.intValue()), decimalFormat.format(end - begin), new Date());
		logger.info(s);
	}

	public void printLog() {

	}

	public abstract void getPage(String text, String title, int wikiID, String redirect);

	/**
	 * This class analyzes the page
	 */
	public class AnalyzePage implements Runnable {
		private String text;
		private String title;
		private int wikiID;
		private String redirect;

		//protected Tokenizer tokenizer;

		public AnalyzePage(String text, String title, int wikiID, String redirect) {
			//logger.debug("creating a page analyzer " + Thread.currentThread().getName() + "...");
			this.text = text;
			this.title = title;
			this.wikiID = wikiID;
			this.redirect = redirect;

		}

		public void run() {
			//logger.debug("running thread " + Thread.currentThread().getName() + "...");
			getPage(text, title, wikiID, redirect);
		}
	}

	public void endProcess() {
		//todo: check
	}

	public void startDocument() throws SAXException {
		logger.info("Process started at " + new Date());
		begin = System.currentTimeMillis();
		genBegin.set(System.currentTimeMillis());
		fElementDepth = 0;
		fXML11 = false;
		fInCDATA = false;
	}

	public void endDocument() throws SAXException {
		end = System.currentTimeMillis();

		printSituation();
		logger.info("Finished to read the document " + new Date());

		boolean b = true;
		try {
			myExecutor.shutdown();
			logger.debug("waiting to end " + new Date() + "...");
			b = myExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			logger.error(e);
		}

		logger.info("ending process " + b + " " + new Date() + "...");
		endProcess();
	}

	/**
	 * Characters.
	 */
	public void characters(char ch[], int start, int length) throws SAXException {
		try {
			content.append(ch, start, length);
		} catch (Exception e) {
			logger.error("Error at characters " + currentTitle);
		}
	}

	public void ignorableWhitespace(char ch[], int start, int length) throws SAXException {
		try {
			characters(ch, start, length);
		} catch (Exception e) {
			logger.error("Error at ignorableWhitespace " + currentTitle);
		}
	}

	//todo: replace with constants the wiki paths
	public void startElement(String uri, String local, String raw, Attributes attrs) throws SAXException {
		xpath += "/" + raw;
		fElementDepth++;

		content.setLength(0);
		/*logger.debug(xpath);
		if (xpath.equals("/mediawiki")) {

			logger.debug("setting locale " + attrs);
			locale = new Locale(attrs.getValue("xml:lang"));
			logger.info("locale set to " + locale);
		}*/

		if (xpath.equals("/mediawiki/page")) {
			currentRedirect = null;
		}

		if (xpath.equals("/mediawiki/page/redirect")) {
			String title = attrs.getValue("title");
			if (title.trim().length() > 0) {
				currentRedirect = title.replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
			}
		}
	}

	public void endElement(String uri, String local, String raw) throws SAXException {

		fElementDepth--;
		if (xpath.equals("/mediawiki/page/title")) {
			currentTitle = content.toString().replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
		}
		else if (xpath.equals("/mediawiki/page/id")) {
			currentWikiID = Integer.parseInt(content.toString());
		}
		else if (xpath.equals("/mediawiki/page/revision/text")) {
			// currentText = StringEscapeUtils.unescapeXml(content.toString());
			currentText = content.toString();
		}

		if (xpath.equals("/mediawiki/page")) {
			try {
				myExecutor.execute(new AnalyzePage(currentText, currentTitle, currentWikiID, currentRedirect));
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
		xpath = xpath.substring(0, xpath.length() - raw.length() - 1);

	}

	public void startDTD(String name, String publicId, String systemId) throws SAXException {
	}

	public void endDTD() throws SAXException {
	}

	public void startEntity(String name) throws SAXException {
	}

	public void endEntity(String name) throws SAXException {
	}

	public void startCDATA() throws SAXException {
	}

	public void endCDATA() throws SAXException {
	}

	public void comment(char ch[], int start, int length) throws SAXException {
	}
}