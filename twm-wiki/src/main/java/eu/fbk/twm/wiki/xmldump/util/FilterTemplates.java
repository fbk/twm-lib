/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump.util;

import java.io.*;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 10/01/13
 * Time: 10:19
 * To change this template use File | Settings | File Templates.
 */
public class FilterTemplates {

	public static void main(String argv[]) {

		if (argv.length < 3) {
			System.out.println("");
			System.out.println("USAGE:");
			System.out.println("");
			System.out.println("java -mx6G org.fbk.cit.hlt.thewikimachine.xmldump.util.FilterTemplates\n" +
					" in-page-tpl -- Input good-template file\n" +
					" in-infoboxes -- Input filtered file\n" +
					" out -- Output clean good-template file");
			System.out.println("");
			System.exit(1);
		}

		int parID = 0;

		String filePageTemplates = argv[parID++];
		String fileInfoboxes = argv[parID++];

		String out = argv[parID++];

		try {

			HashSet<String> infoboxes = new HashSet<String>();
			BufferedReader in;
			String line;

			System.out.print("Loading infoboxes file...");
			in = new BufferedReader(new FileReader(fileInfoboxes));
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.length() > 0) {
					infoboxes.add(line);
				}
			}
			System.out.println(" done!");

			PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));

			in = new BufferedReader(new FileReader(filePageTemplates));
			while ((line = in.readLine()) != null) {
				String[] parts = line.split("\\t");
				if (parts.length < 2) {
					continue;
				}

				String template = parts[0];
				// String template = parts[1];

				if (infoboxes.contains(template.toLowerCase())) {
					// writer.println(page + "\t" + template);
					writer.println(template);
				}
			}

			writer.flush();
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
