/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.index.CrossLanguageIndexer;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.ExtractorParameters;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.FSDirectory;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WikiDataExtractor extends AbstractWikipediaExtractor {
	Pattern q = Pattern.compile("^Q([0-9]+)$");
	private String outputDir, clSchema;
	IndexWriter clSchemaWriter = null;
	HashMap<String, BufferedWriter> writers = new HashMap<String, BufferedWriter>();
	HashSet<String> langs = null;

	public WikiDataExtractor(int numThreads, int numPages, Locale locale) {
		super(numThreads, numPages, locale);
	}

	public void start(String fileName, String outputDir, String clSchema) {
		start(fileName, outputDir, clSchema, null);
	}

	public void start(String fileName, String outputDir, String clSchema, HashSet<String> langs) {
		this.langs = langs;
		this.outputDir = outputDir;
		this.clSchema = clSchema;
		writers = new HashMap<String, BufferedWriter>();
		if (clSchema != null) {
			try {
				clSchemaWriter = new IndexWriter(FSDirectory.open(new File(clSchema)), new WhitespaceAnalyzer(), IndexWriter.MaxFieldLength.LIMITED);
			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}
		startProcess(fileName);
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {

		Matcher m = q.matcher(title);
		if (!m.find()) {
			logger.trace("Invalid title: " + title);
			return;
		}
		String id = m.group(1);
		logger.trace("ID: " + id);

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> pageData = null;
		try {
			pageData = mapper.readValue(text, Map.class);
		} catch (Exception ignored) {
			return;
		}

		Map<String, Object> links = null;
		try {
			links = (Map<String, Object>) pageData.get(new String("links"));
			if (links == null) {
				links = (Map<String, Object>) pageData.get(new String("sitelinks"));
			}
		} catch (Exception ignored) {
			return;
		}

		StringBuffer sb = new StringBuffer();

		// wikiID
		sb.append("wikiID");
		sb.append(":");
		sb.append(id);
		sb.append("\t");

		try {
			for (String lang : links.keySet()) {

				String foreignPage = null;
				try {
					foreignPage = ((String) links.get(lang)).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
				} catch (Exception e) {
					try {
						foreignPage = ((String) ((LinkedHashMap) links.get(lang)).get("name")).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
					} catch (Exception e2) {
						foreignPage = ((String) ((LinkedHashMap) links.get(lang)).get("title")).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
					}
				}

				String shortLang = lang.replaceAll("wiki", "");
				sb.append(shortLang);
				sb.append(":");
				sb.append(foreignPage);
				sb.append("\t");
			}
		} catch (Exception e) {
			e.printStackTrace();
//			System.out.println(text);
//			System.exit(1);
		}

		if (clSchemaWriter != null) {
			Document doc = new Document();
			doc.add(new Field("wikiID", id, Field.Store.YES, Field.Index.NOT_ANALYZED));
			for (String lang : links.keySet()) {
				String shortLang = lang.replaceAll("wiki", "");
				String foreignPage = null;
				try {
					foreignPage = ((String) links.get(lang)).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
				} catch (Exception e) {
					try {
						foreignPage = ((String) ((LinkedHashMap) links.get(lang)).get("name")).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
					} catch (Exception e2) {
						foreignPage = ((String) ((LinkedHashMap) links.get(lang)).get("title")).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
					}
				}
				doc.add(new Field(shortLang, foreignPage, Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			try {
				clSchemaWriter.addDocument(doc);
			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}


		synchronized (this) {
			try {

				for (String lang : links.keySet()) {
					String shortLang = lang.replaceAll("wiki", "");

					if (langs != null && !langs.contains(shortLang)) {
						continue;
					}

					if (writers.get(lang) == null) {
						String fileName = outputDir + shortLang + ".csv";
						logger.trace("Creating " + fileName);
						writers.put(lang, new BufferedWriter(new FileWriter(fileName)));
					}

					String thisPage = null;
					try {
						thisPage = ((String) links.get(lang)).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
					} catch (Exception e) {
						try {
							thisPage = ((String) ((LinkedHashMap) links.get(lang)).get("name")).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
						} catch (Exception e2) {
							thisPage = ((String) ((LinkedHashMap) links.get(lang)).get("title")).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
						}
					}

//					String thisPage;
//					try {
//						thisPage = ((String) links.get(lang)).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
//					} catch (Exception e) {
//						thisPage = ((String) ((LinkedHashMap) links.get(lang)).get("name")).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
//					}

					writers.get(lang).write(thisPage);
					writers.get(lang).write("\t");
					writers.get(lang).write(sb.toString().trim());
					writers.get(lang).write("\n");
				}

			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}
	}

	@Override
	public void endProcess() {
		super.endProcess();
		for (String lang : writers.keySet()) {
			try {
				writers.get(lang).close();
			} catch (Exception ignored) {

			}
		}
		if (clSchemaWriter != null) {
			try {
				logger.info("Optimizing and closing index");
				clSchemaWriter.optimize();
				clSchemaWriter.close();
			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}
	}

	public static void main(String args[]) throws IOException {

		Options options = new Options();
		options.addOption(OptionBuilder.withArgName("filename").hasArg().withDescription("WikiData xml dump file").isRequired().withLongOpt("wikidata-dump").create("w"));
		options.addOption(OptionBuilder.withArgName("dirname").hasArg().withDescription("Output dir").isRequired().withLongOpt("output").create("o"));

		options.addOption("S", "skip-cl", false, "Skip cross-language schema");
		options.addOption(OptionBuilder.withArgName("ISO codes").hasArgs().withDescription("ISO codes of languages to extract (default: all)").withLongOpt("languages").create());

		options.addOption(OptionBuilder.withArgName("num").hasArg().withDescription("Number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t"));
		options.addOption(OptionBuilder.withArgName("num").hasArg().withDescription("Number of pages").withLongOpt("num-pages").create("p"));
		options.addOption(OptionBuilder.withLongOpt("debug").withDescription("Activate debug mode").create("d"));
		options.addOption("h", "help", false, "print this message");

		CommandLineParser parser = new PosixParser();
		CommandLine commandLine = null;
		try {
			commandLine = parser.parse(options, args);
			if (commandLine.hasOption("help")) {
				throw new ParseException("");
			}
		} catch (ParseException exp) {
			System.out.println();
			if (exp.getMessage().length() > 0) {
				System.out.println("ERR: " + exp.getMessage());
				System.out.println();
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -mx4g " + Thread.currentThread().getStackTrace()[1].getClassName(), "\n", options, "\n", true);
			System.out.println();
			System.exit(0);
		}

		Properties defaultProps = new Properties();
		defaultProps.setProperty("log4j.rootLogger", "info,stdout");
		defaultProps.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
		defaultProps.setProperty("log4j.appender.stdout.layout.ConversionPattern", "[%t] %-5p (%F:%L) - %m %n");
		defaultProps.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");

		if (commandLine.hasOption('d')) {
			defaultProps.setProperty("log4j.rootLogger", "trace,stdout");
		}

		int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
		if (commandLine.hasOption("num-threads")) {
			numThreads = Integer.parseInt(commandLine.getOptionValue("num-threads"));
		}

		String wikidataFile = commandLine.getOptionValue("w");
		String outputDir = commandLine.getOptionValue("o");

		boolean skipCL = commandLine.hasOption("skip-cl");
		String[] langs = commandLine.getOptionValues("languages");
		HashSet<String> languages = new HashSet<>();
		for (String lang : langs) {
			languages.add(lang);
		}


		if (!outputDir.endsWith(File.separator)) {
			outputDir += File.separator;
		}
		File outputFile = new File(outputDir);
		outputFile.mkdirs();

		String clSchema = null;
		if (!skipCL) {
			clSchema = outputDir + "clschema" + File.separator;
			outputFile = new File(clSchema);
			outputFile.mkdirs();
		}

		String langsDir = outputDir + "langs" + File.separator;
		outputFile = new File(langsDir);
		outputFile.mkdirs();

		Logger logger = Logger.getLogger(WikiDataExtractor.class.getName());
		PropertyConfigurator.configure(defaultProps);

		int numPages = Integer.MAX_VALUE;
		if (commandLine.hasOption("p")) {
			numPages = Integer.parseInt(commandLine.getOptionValue("p"));
		}

		WikiDataExtractor wd = new WikiDataExtractor(numThreads, numPages, Locale.ENGLISH);
		wd.start(wikidataFile, langsDir, clSchema, languages);

		File[] listOfFiles = outputFile.listFiles();
		for (File f : listOfFiles) {
			if (f.getName().endsWith(".csv")) {
				String newOutput = langsDir + f.getName().replaceFirst("[.][^.]+$", "") + File.separator;
				logger.info("Indexing " + newOutput);
				CrossLanguageIndexer crossLanguageIndexer = new CrossLanguageIndexer(newOutput);
				crossLanguageIndexer.index(f);
				crossLanguageIndexer.close();
			}
		}

	}
}