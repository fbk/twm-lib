/*
 * Copyright (2010) Fondazione Bruno Kessler (FBK)
 * 
 * FBK reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced
 * in any form whatsoever except as provided by license
 * without the written consent of FBK.  A license under FBK's
 * rights in the Program may be available directly from FBK.
 */

package eu.fbk.twm.wiki.xmldump.util;

import eu.fbk.twm.utils.StringTable;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * This class represents a map between people pages and basic people
 * information extracted from the infobox using regex.
 * The people map is read from a filePageCounter with format:
 * <p/>
 * <code>(person_page \t name \t surname \t birth_date \t death_day \n)+</code>
 *
 * @author Claudio Giuliano
 * @version 1.0
 * @since 1.0
 */
public class PersonInfoMap {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PersonInfoMap</code>.
	 */
	static Logger logger = Logger.getLogger(PersonInfoMap.class.getName());

	//
	protected Map<String, Person> map;

	//
	protected static Pattern tabPattern = Pattern.compile("\t");

	//
	protected static Pattern underlinePattern = Pattern.compile("_");

	//
	public PersonInfoMap() throws IOException {
		map = new HashMap<String, Person>();
	} // end

	//
	public PersonInfoMap(File peopleFile) throws IOException {
		long begin = System.currentTimeMillis(), end = 0;
		logger.info("reading " + peopleFile + "...");
		map = read(peopleFile);
		end = System.currentTimeMillis();
		logger.info(map.size() + " pages read in " + (end - begin) + " ms");
	} // end 

	//
	private Map<String, Person> read(File peopleFile) throws IOException {
		Map<String, Person> map = new HashMap<String, Person>();
		if (!peopleFile.exists()) {
			return map;
		}

		LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(peopleFile), "UTF-8"));

		String line = null;
		int j = 1;

		// read links
		while ((line = reader.readLine()) != null) {
			Person p = new Person(line);

			map.put(p.getPage(), new Person(line));

			if ((j % 100000) == 0) {
				System.out.print(".");
			}
			j++;
		} // end while
		reader.close();

		if (j > 100000) {
			System.out.print("\n");
		}
		//logger.info(j + " pages read");
		return map;
	} // end read

	//
	public Person get(String page) {
		return map.get(page);
	} // end get

	public Iterator<Person> people() {
		return map.values().iterator();
	} // people

	//
	public int size() {
		return map.size();
	} // end size

	//
	public class Person {
		//
		private String[] s;


		//
		public Person(String line) {
			s = tabPattern.split(line);

		} // end constructor

		//
		public String getPage() {
			if (s.length > 0) {
				return s[0];
			}

			return StringTable.EMPTY_STRING;
		} // end getPage

		//
		public String getName() {
			if (s.length > 1) {
				return s[1];
			}

			return StringTable.EMPTY_STRING;
		} // end getName

		//
		public String getSurname() {
			if (s.length > 2) {
				return s[2];
			}

			return StringTable.EMPTY_STRING;
		} // end getSurname

		//
		public String getBirthDate() {
			if (s.length > 3) {
				return s[3];
			}

			return StringTable.EMPTY_STRING;
		} // end getBirthDate

		//
		public String getDeathDate() {
			if (s.length > 4) {
				return s[4];
			}

			return StringTable.EMPTY_STRING;
		} // end getDeathDate

		//
		public String toString() {
			//logger.debug(s.length + "\t" + Arrays.toString(s));
			return getPage() + "\t" + getName() + "\t" + getSurname() + "\t" + getBirthDate() + "\t" + getDeathDate();
		} // end getDeathDate

	} // end class Person

	//
	public static void main(String[] args) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length == 0) {
			logger.info("java -mx1024M org.fbk.cit.hlt.wm.wiki.xmldump.PersonInfoMap people-filePageCounter [page]");
			System.exit(1);
		}

		File peopleFile = new File(args[0]);
		PersonInfoMap map = new PersonInfoMap(peopleFile);
		logger.info(map.size());
		if (args.length == 1) {
			Iterator<Person> it = map.people();
			while (it.hasNext()) {
				logger.info(it.next());
			}
		}
		for (int i = 1; i < args.length; i++) {
			Person p = map.get(args[i]);
			logger.info(i + "\t" + p);
		}
	} // end main

} // end PersonInfoMap
