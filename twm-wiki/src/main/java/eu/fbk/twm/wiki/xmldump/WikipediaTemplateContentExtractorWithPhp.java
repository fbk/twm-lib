/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.GenericFileUtils;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.util.Locale;
import java.util.UUID;

@Deprecated public class WikipediaTemplateContentExtractorWithPhp extends AbstractWikipediaExtractor implements
		WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaTemplateExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaTemplateContentExtractorWithPhp.class.getName());

	private static final String tempFile = "/tmp/ramdisk/parseme.wiki";
	private static final String tempDir = "/tmp/ramdisk/";
	private static final String phpCommand = "/usr/bin/php";
	private static final String phpScript = "/home/alessio/phpWiki/MediaWikiWrapper/parse.php";

	private IndexWriter templateWriter;

	public WikipediaTemplateContentExtractorWithPhp(int numThreads, int numPages, Locale locale) {
		super(numThreads, numPages, locale, "configuration/");
	}

	public String getUniqueFileName(String directory, String extension) {
		File t = new File(directory, new StringBuilder().append("prefix")
				.append(UUID.randomUUID())
				.append(".").append(extension).toString());
		return t.getAbsolutePath();
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			String dirName = extractorParameters.getWikipediaTemplateFilePrefixName() + "xml-index/";
			dirName = GenericFileUtils.checkWriteableFolder(dirName, true);
			templateWriter = new IndexWriter(FSDirectory.open(new File(dirName)), new WhitespaceAnalyzer(), IndexWriter.MaxFieldLength.LIMITED);

		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}


	@Override
	public void templatePage(String text, String title, int wikiID) {
		// System.out.println(title);
		String fileName = getUniqueFileName(tempDir, "wiki");

		try {

			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
			writer.write("" + text);
			writer.close();
			writer = null;

			String[] commands = {phpCommand, phpScript, fileName};
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(commands);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			// BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
			String s;
			String res = "";

			// read the output from the command
			while ((s = stdInput.readLine()) != null) {
				res += s;
				res += "\n";
			}
			stdInput.close();
			proc.destroy();

			// todo: remove <ignore> tags from res
			// todo: remove prefix in title

			Document doc = new Document();

			doc.add(new Field("page", title, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("xml", res.getBytes(), Field.Store.YES));
			synchronized (this) {
				templateWriter.addDocument(doc);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			File t = new File(fileName);
			if (t.exists()) {
				t.delete();
			}
			t = null;
		}
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {
	}

	@Override
	public void endProcess() {
		super.endProcess();
		try {
			templateWriter.optimize();
			templateWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String xmlFile = args[0];
		String outFile = args[1];

		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		ExtractorParameters e = new ExtractorParameters(xmlFile, outFile);

		WikipediaTemplateContentExtractorWithPhp writer = new WikipediaTemplateContentExtractorWithPhp(12, Integer.MAX_VALUE, e.getLocale());
		writer.start(e);
	}

	/*public static void main(String argv[]) throws IOException
	{
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) logConfig = "configuration/log-config.txt";

		PropertyConfigurator.configure(logConfig);

		if (argv.length < 2) {
			System.out.println("");
			System.out.println("USAGE:");
			System.out.println("");
			System.out.println("java -mx6G org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaTemplateExtractor\n" +
							" in-wiki-xml -- Input filePageCounter\n" +
							" root-templatePageCounter-files -- Output templatePageCounter filePageCounter\n" +
							" locale -- Locale (used for resources)\n" +
							" [threads=1] -- Number of threads to use\n" +
							" [countPageCounter=0] -- Lines to stop, 0 means never stop");
			System.out.println("");
			System.exit(1);
		}

		int parID = 0;

		String xin = argv[parID++];
		String xout = argv[parID++];
		Locale locale = new Locale(argv[parID++]);

		int threads = 1;
		if (argv.length > parID) {
			threads = Integer.parseInt(argv[parID++]);
		}

		int size = 0;
		if (argv.length > parID) {
			size = Integer.parseInt(argv[parID++]);
		}

		WikipediaTemplateExtractor writer = new WikipediaTemplateExtractor(threads, size, locale);
		writer.start(xin, xout);
	}*/

}