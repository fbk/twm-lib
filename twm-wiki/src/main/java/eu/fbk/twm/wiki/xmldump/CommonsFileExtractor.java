/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/21/13
 * Time: 2:46 PM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated
public class CommonsFileExtractor extends AbstractWikipediaExtractor implements WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>CommonsFileExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(CommonsFileExtractor.class.getName());

	private PrintWriter fileWriter;

	public static final String COMMONS_URL = "http://commons.wikimedia.org/wiki/";

	public CommonsFileExtractor(int numThreads, int numPages, Locale locale) {
		super(numThreads, numPages, locale);
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			fileWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getCommonsFileName()), "UTF-8")));
		} catch (IOException e) {
			logger.error(e);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//logger.debug(title);
		writeFileName(title);
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	private void writeFileName(String title) {
		byte[] bytesOfMessage = new byte[0];
		try {
			bytesOfMessage = title.substring(5, title.length()).getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		}

		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
		}
		byte[] digest = md.digest(bytesOfMessage);
		BigInteger bigInt = new BigInteger(1, digest);
		String hash = bigInt.toString(16);
		// Now we need to zero pad it if you actually want the full 32 chars.
		//while (hash.length() < 32) {
		//	hash = "0" + hash;
		//}
		StringBuilder sb = new StringBuilder();
		sb.append(title);
		sb.append(CharacterTable.HORIZONTAL_TABULATION);
		//sb.append(COMMONS_URL);
		sb.append(hash.substring(0, 1));
		sb.append(File.separator);
		sb.append(hash.substring(0, 2));
		synchronized (this) {
			fileWriter.println(sb.toString());
		}
		/*synchronized (this) {
			fileWriter.println(title.substring(5, title.length()));
		}*/
	}

	@Override
	public void endProcess() {
		super.endProcess();
		fileWriter.close();
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("commonswiki-dump").hasArg().withDescription("commonswiki xml dump file").isRequired().withLongOpt("commonswiki-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");


			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);


			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("commonswiki-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);

			logger.debug("extracting files (" + extractorParameters.getCommonsFileName() + ")...");
			CommonsFileExtractor commonsFileExtractor = new CommonsFileExtractor(numThreads, numPages, extractorParameters.getLocale());
			commonsFileExtractor.setNotificationPoint(notificationPoint);
			commonsFileExtractor.start(extractorParameters);

			logger.info("extraction ended " + new Date());

		} catch (ParseException e) {
			// oops, something went wrong
			System.out.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.xmldump.CommonsFileExtractor", "\n", options, "\n", true);
		}
	}

}
