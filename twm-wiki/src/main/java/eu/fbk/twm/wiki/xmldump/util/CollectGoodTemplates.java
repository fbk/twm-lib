/*
 * Copyright 2012 FBK (http://www.fbk.eu)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.fbk.twm.wiki.xmldump.util;

import eu.fbk.twm.utils.ExtractorParameters;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Date;
import java.util.HashSet;
import java.util.concurrent.*;

public class CollectGoodTemplates {

	static Logger logger = Logger.getLogger(CollectGoodTemplates.class.getName());
	private HashSet<String> goodTemplates = null;
	int threshold = 0;

	public final static int DEFAULT_THREADS_NUMBER = 1;
	private int numThreads;
	public final static int DEFAULT_QUEUE_SIZE = 10000;

	public CollectGoodTemplates(String in, String goodTemplatesFile, String out, int threshold) throws IOException {
		this(in, goodTemplatesFile, out, threshold, DEFAULT_THREADS_NUMBER, null);
	}

	public CollectGoodTemplates(String in, String goodTemplatesFile, String out, int threshold, int numThreads, String simpleOut) throws IOException {
		this.numThreads = numThreads;
		File outFile = new File(out);
		if (outFile.exists()) {
			outFile.delete();
		}
		if (!outFile.createNewFile()) {
			logger.error("File " + out + " not writeable!");
			System.exit(1);
		}

		if (!(new File(in)).exists()) {
			logger.error("File " + in + " does not exist!");
			System.exit(1);
		}

		if (!(new File(goodTemplatesFile)).exists()) {
			logger.error("Template file " + goodTemplatesFile + " does not exist!");
			System.exit(1);
		}

		HashSet<String> ret = new HashSet<String>();

		String hash = goodTemplatesFile;
		logger.info("Loading templates...");

		try {
			BufferedReader inF = new BufferedReader(new FileReader(hash));
			String line;
			while ((line = inF.readLine()) != null) {
				String[] parts = line.split("\t");
				if (parts.length >= 2) {
					ret.add(parts[0]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		start(in, out, ret, threshold, simpleOut);
	}

	/**
	 * Default constructor.
	 */
	private void start(String in, String out, HashSet<String> tpls, int threshold, String simpleOut) throws IOException {
		goodTemplates = tpls;
		this.threshold = threshold;

		logger.info("Loading file in memory and writing it to disk...");
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out)));
		BufferedWriter simpleWriter = null;
		HashSet<String> veryGoodTemplates = new HashSet<String>();

		BufferedReader reader = new BufferedReader(new FileReader(in));
		String line = null;

		long i = 0;

		logger.info("creating the thread executor (" + numThreads + ")");
		int blockQueueSize = DEFAULT_QUEUE_SIZE;
		BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(blockQueueSize);
		RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();
		ExecutorService myExecutor = new ThreadPoolExecutor(numThreads, numThreads, 1, TimeUnit.MINUTES, blockingQueue, rejectedExecutionHandler);

		while ((line = reader.readLine()) != null) {

			try {
				myExecutor.execute(new AnalyzeString(line, writer, veryGoodTemplates));
			} catch (Exception e) {
				e.printStackTrace();
			}

			i++;
			if ((i % 100000) == 0) {
				System.out.print(".");
			}
			if ((i % 5000000) == 0) {
				System.out.println(" " + i);
			}
		}
		System.out.println("");

		try {
			myExecutor.shutdown();
			logger.debug("waiting to end " + new Date() + "...");
			myExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			logger.error(e);
		}

		writer.flush();
		writer.close();
		logger.info("Finished writing!");

		if (simpleOut != null) {
			logger.info("Write good templates to file");
			simpleWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(simpleOut)));
			for (String t : veryGoodTemplates) {
				simpleWriter.write(t + "\n");
			}
			simpleWriter.close();
		}

	}

	private class AnalyzeString implements Runnable {
		private String toBeAnalyzed;
		BufferedWriter w;
		HashSet<String> veryGoodTemplates;

		public AnalyzeString(String s, BufferedWriter writer, HashSet<String> vGT) {
			toBeAnalyzed = s;
			w = writer;
			veryGoodTemplates = vGT;
		}

		public void run() {
			String[] parts = toBeAnalyzed.split("\t");

			if (parts.length <= 4) {
				return;
			}

			String page = parts[0];
			String template = parts[1];

			int labelCount = 0;
			int nlCount = 0;
			int equalsCount = 0;
			try {
				labelCount = Integer.parseInt(parts[2]);
				nlCount = Integer.parseInt(parts[3]);
				equalsCount = Integer.parseInt(parts[4]);
			} catch (Exception e) {
				return;
			}

			if (labelCount - equalsCount > 2) {
				return;
			}
			if (!goodTemplates.contains(template)) {
				return;
			}
			if (nlCount <= threshold) {
				return;
			}
			if (page.length() <= 0) {
				return;
			}
			if (template.length() <= 0) {
				return;
			}

			synchronized (veryGoodTemplates) {
				veryGoodTemplates.add(template);
			}

			synchronized (w) {
				try {
					w.append(page).append('\t').append(template).append('\n');
				} catch (IOException e) {
					e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
				}
			}
		}
	}

	public static void main(String args[]) throws Exception {

		if (args.length < 3) {
			logger.error("Wrong number of parameters " + args.length);
			System.out.println("java -mx6G org.fbk.cit.hlt.thewikimachine.xmldump.util.CollectGoodTemplates\n" +
					" in-wiki-xml -- Input file\n" +
					" out-global-folder -- Output global folder\n" +
					" threshold -- Threshold\n" +
					"");
			System.exit(-1);
		}

		int parID = 0;

		String xin = args[parID++];
		String xout = args[parID++];

		int threshold = 0;
		if (args.length > parID) {
			threshold = Integer.parseInt(args[parID++]);
		}

		if (!xout.endsWith(System.getProperty("file.separator"))) {
			xout += System.getProperty("file.separator");
		}

		ExtractorParameters extractorParameters = new ExtractorParameters(xin, xout);
		String outInfoboxes = extractorParameters.getWikipediaTemplateFileNames().get("infoboxes");
		String goodTemplatesFile = extractorParameters.getWikipediaTemplateFileNames().get("good");
		String tplPruned = extractorParameters.getWikipediaTemplateFileNames().get("pruned");
		String tplMapRep = extractorParameters.getWikipediaTemplateFileNames().get("map-rep");

		new CollectGoodTemplates(tplMapRep, goodTemplatesFile, tplPruned, threshold, 12, outInfoboxes);
	}
}
