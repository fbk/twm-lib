/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import de.tudarmstadt.ukp.wikipedia.parser.Content;
import de.tudarmstadt.ukp.wikipedia.parser.ParsedPage;
import de.tudarmstadt.ukp.wikipedia.parser.Section;
import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import eu.fbk.twm.wiki.xmldump.util.WikiMarkupParser;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.lsa.LSM;
import eu.fbk.utils.math.Vector;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.List;
import java.util.Locale;

/**
 * @see org.fbk.cit.hlt.thewikimachine.csv.VectorExtractor
 */
@Deprecated
public class WikipediaVectorExtractor extends AbstractWikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaVectorExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaVectorExtractor.class.getName());

	//private PrintWriter textWriter;

	private PrintWriter vectorWriter;

	private LSM lsm;

	public WikipediaVectorExtractor(int numThreads, int numPages, Locale locale, String lsaRoot) throws IOException {
		super(numThreads, numPages, locale);

		if (!lsaRoot.endsWith(File.separator)) {
			lsaRoot += File.separator;
		}
		File Ut = new File(lsaRoot + "X-Ut");
		File Sk = new File(lsaRoot + "X-S");
		File r = new File(lsaRoot + "X-row");
		File c = new File(lsaRoot + "X-col");
		File df = new File(lsaRoot + "X-df");
		int dim = 100;
		boolean rescaleIdf = true;

		lsm = new LSM(Ut, Sk, r, c, df, dim, rescaleIdf);

	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			vectorWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaVectorFileName()), "UTF-8")));
		} catch (IOException e) {
			logger.error(e);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {

		try {
			WikiMarkupParser wikiMarkupParser = WikiMarkupParser.getInstance();
			//logger.debug(title + "\t" + wikiID);
			String[] prefixes = {filePrefix, imagePrefix};
			ParsedPage parsedPage = wikiMarkupParser.parsePage(text, prefixes);
			String page = tokenizedText(parsedPage, title);
			BOW bow = new BOW(page.toLowerCase());
			Vector d = lsm.mapDocument(bow);
			Vector pd = lsm.mapPseudoDocument(d);
			d.normalize();
			pd.normalize();

			synchronized (this) {
				vectorWriter.print(title);
				vectorWriter.print(CharacterTable.HORIZONTAL_TABULATION);
				vectorWriter.print(pd.toString());
				vectorWriter.print(CharacterTable.HORIZONTAL_TABULATION);
				vectorWriter.println(d.toString());

			}
		} catch (Exception e) {
			logger.error("Error processing page " + title + " (" + wikiID + ")");
		}
	}

	/**
	 * Returns the whole content of the page tokenized in a single line.
	 * The first token is the page title (with underscores)
	 */
	private String tokenizedText(ParsedPage parsedPage, String title) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(title);
		sb.append(" ");
		Tokenizer tokenizer = HardTokenizer.getInstance();
		String tokenizedTitle = tokenizer.tokenizedString(title.replace('_', ' '));
		sb.append(tokenizedTitle);
		String rawContent;
		String tokenizedContent;
		List<Content> list;
		for (Section section : parsedPage.getSections()) {
			list = section.getContentList();
			for (int i = 0; i < list.size(); i++) {
				rawContent = list.get(i).getText();
				//logger.debug(i + "\t'" + rawContent + "'");
				if (rawContent.length() > 0) {
					tokenizedContent = tokenizer.tokenizedString(rawContent);
					/*if (tokenizedContent.indexOf('\n') != -1)
						logger.debug("\\n " + tokenizedContent.length() + "\t" + tokenizedContent.indexOf('\n'));
					if (tokenizedContent.indexOf('\r') != -1)
						logger.debug("\\r " + tokenizedContent.length() + "\t" + tokenizedContent.indexOf('\r'));   */

					sb.append(" ");
					sb.append(tokenizedContent);

				}
			}
		}
		return sb.toString();
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	public void endProcess() {
		super.endProcess();
		vectorWriter.flush();
		vectorWriter.close();
	}

	/*public static void main(String argv[]) throws IOException
	{
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) logConfig = "configuration/log-config.txt";

		PropertyConfigurator.configure(logConfig);

		if (argv.length < 2) {
			System.out.println("");
			System.out.println("USAGE:");
			System.out.println("");
			System.out.println("java -mx6G org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaVectorExtractor\n" +
							" in-wiki-xml -- Input file\n" +
							" out-root-file -- Output template file\n" +
							" locale -- Locale (used for resources)\n" +
							" in-lsa-root -- lsa model \n" +
							" [threads=1] -- Number of threads to use\n" +
							" [count=0] -- Lines to stop, 0 means never stop");
			System.out.println("");
			System.exit(1);
		}

		int parID = 0;

		String xin = argv[parID++];
		String xout = argv[parID++];
		Locale locale = new Locale(argv[parID++]);
		String lsmRoot = argv[parID++];
		int threads = 1;
		if (argv.length > parID) {
			threads = Integer.parseInt(argv[parID++]);
		}

		int size = 0;
		if (argv.length > parID) {
			size = Integer.parseInt(argv[parID++]);
		}

		WikipediaVectorExtractor textWriter = new WikipediaVectorExtractor(threads, size, locale, lsmRoot);
		textWriter.start(xin, xout);
	} */

}