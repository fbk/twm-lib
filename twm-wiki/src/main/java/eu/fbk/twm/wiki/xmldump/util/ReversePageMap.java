/*
 * Copyright (2010) Fondazione Bruno Kessler (FBK)
 * 
 * FBK reserves all rights in the Program as delivered.
 * The Program or any portion thereof may not be reproduced
 * in any form whatsoever except as provided by license
 * without the written consent of FBK.  A license under FBK's
 * rights in the Program may be available directly from FBK.
 */

package eu.fbk.twm.wiki.xmldump.util;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * This class map a redirectPageCounter page to the original page.
 * The redirectPageCounter map is read from a filePageCounter with format:
 * <p/>
 * <code>(page \t redirect_page\n)+</code>
 *
 * @author Claudio Giuliano
 * @version 1.0
 * @see PageMap, WikipediaRedirectExtractor
 * @since 1.0
 */
public class ReversePageMap {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageMap</code>.
	 */
	static Logger logger = Logger.getLogger(ReversePageMap.class.getName());

	//
	private Map<String, Set<String>> map;

	//
	private static Pattern tabPattern = Pattern.compile("\t");

	//
	public ReversePageMap() throws IOException {
		map = new HashMap<String, Set<String>>();
	} // end

	//
	public ReversePageMap(File redir) throws IOException {
		long begin = System.currentTimeMillis(), end = 0;
		map = read(redir);
		end = System.currentTimeMillis();
		logger.info(map.size() + " reverse redirectPageCounter pages read in " + (end - begin) + " ms");
	} // end 

	//
	private Map<String, Set<String>> read(File redir) throws IOException {
		logger.info("reading " + redir + "...");
		Map<String, Set<String>> map = new HashMap<String, Set<String>>();
		if (!redir.exists()) {
			return map;
		}

		LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(redir), "UTF-8"));
		String line = null;
		String[] s = null;
		int j = 1;

		// read links
		while ((line = reader.readLine()) != null) {
			if ((j % 100000) == 0) {
				System.out.print(".");
			}

			s = tabPattern.split(line);

			if (s.length == 2) {
				Set<String> set = map.get(s[1]);
				if (set == null) {
					set = new HashSet<String>();
					set.add(s[0]);
					map.put(s[1], set);
				}
				else {
					set.add(s[0]);
				}
			}
			j++;
		} // end while
		reader.close();

		if (j > 100000) {
			System.out.print("\n");
		}

		return map;
	} // end read


	public int size() {
		return map.size();
	}

	//
	public Set<String> get(String page) {
		return map.get(page);
	} // end get

	//
	public static void main(String[] args) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length == 0) {
			logger.info("java -mx1024M org.fbk.cit.hlt.wm.wiki.xmldump.ReversePageMap redirectPageCounter-filePageCounter [page]");
			System.exit(1);
		}

		File redir = new File(args[0]);
		ReversePageMap map = new ReversePageMap(redir);
		logger.info(map.size());

		if (args.length == 2) {
			logger.info(map.get(args[1]));
		}
	} // end main

} // end PageMap
