/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.CommandLineWithLogger;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.Locale;
import java.util.regex.Matcher;

public class WikipediaPageCategoryMainExtractor extends AbstractWikipediaExtractor implements WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaPageCategoryMainExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaPageCategoryMainExtractor.class.getName());

	private PrintWriter pageCategoryWriter;

	private boolean delCatLabel;

	public WikipediaPageCategoryMainExtractor(int numThreads, int numPages, Locale locale, boolean delCatLabel) {
		super(numThreads, numPages, locale);
		this.delCatLabel = delCatLabel;
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			pageCategoryWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaPageCategoryMainFileName()), "UTF-8")));
		} catch (IOException e) {
			logger.error(e);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {
		//todo: make uppercase the first letter
		Matcher m = categoryMainPattern.matcher(text);
		StringBuilder buff = new StringBuilder();
		int index = delCatLabel ? 2 : 1;
		while (m.find()) {
			int s = m.start(index);
			int e = m.end(index);
			String category = text.substring(s, e).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);
			buff.append(title);
			buff.append(CharacterTable.HORIZONTAL_TABULATION);
			buff.append(normalizePageName(category));
			buff.append(CharacterTable.LINE_FEED);
		}

		synchronized (this) {
			pageCategoryWriter.print(buff);
		}
	}

	@Override
	public void endProcess() {
		super.endProcess();
		pageCategoryWriter.flush();
		pageCategoryWriter.close();
	}

	public static void main(String[] args) {

		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();
		
		commandLineWithLogger.addOption(OptionBuilder.isRequired().withDescription("Base folder").hasArg().withArgName("folder").withLongOpt("base-folder").create("b"));
		commandLineWithLogger.addOption(OptionBuilder.isRequired().withDescription("Wikipedia dump").hasArg().withArgName("file").withLongOpt("wiki-dump").create("d"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}
		
		String xmlFileName = commandLine.getOptionValue("wiki-dump");
		String baseDir = commandLine.getOptionValue("base-folder");

		ExtractorParameters extractorParameters = new ExtractorParameters(xmlFileName, baseDir, true);
		WikipediaPageCategoryMainExtractor w = new WikipediaPageCategoryMainExtractor(12, Integer.MAX_VALUE, extractorParameters.getLocale(), true);
		w.start(extractorParameters);
	}

}