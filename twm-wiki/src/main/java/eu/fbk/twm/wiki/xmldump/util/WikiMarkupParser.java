/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump.util;

import de.tudarmstadt.ukp.wikipedia.parser.ParsedPage;
import de.tudarmstadt.ukp.wikipedia.parser.Section;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.FlushTemplates;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParser;
import de.tudarmstadt.ukp.wikipedia.parser.mediawiki.MediaWikiParserFactory;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.twm.wiki.xmldump.util.clean.CleanWikipedia;
import eu.fbk.utils.core.io.FileUtils;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/15/13
 * Time: 8:10 PM
 * To change this templatePageCounter use File | Settings | File Templates.
 */
public class WikiMarkupParser {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikiMarkupParser</code>.
	 */
	static Logger logger = Logger.getLogger(WikiMarkupParser.class.getName());

	public final static String NBSP = "&nbsp;";

	//public final static String SPACE = " ";

	public final static String EMPTY_STRING = "";

	//protected Pattern refPattern = Pattern.compile("<ref[^>]*>[^<]+</ref>", Pattern.MULTILINE);
	protected Pattern refPattern = Pattern.compile("<ref[^>]*>[^<]+</ref>");

	//protected Pattern commentPattern = Pattern.compile("<!--[^-]+-->", Pattern.MULTILINE);
	protected Pattern commentPattern = Pattern.compile("<!--[^-]+-->");

	protected MediaWikiParser parser;

	//protected static Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	//protected static Pattern underScorePattern = Pattern.compile(StringTable.LOW_LINE);


	private static WikiMarkupParser ourInstance = null;

	public static synchronized WikiMarkupParser getInstance() {
		if (ourInstance == null) {
			ourInstance = new WikiMarkupParser();
		}
		return ourInstance;
	}

	private WikiMarkupParser() {
		logger.info("WikiMarkupParser.WikiMarkupParser");
		MediaWikiParserFactory pf = new MediaWikiParserFactory();
		pf.setTemplateParserClass(new FlushTemplates().getClass());
		//pf.setTemplateParserClass(new ShowTemplateNamesAndParameters().getClass());
		//pf.setTemplateParserClass(new GermanTemplateParser().getClass());

		logger.info("getShowImageText: " + pf.getShowImageText());
		logger.info("getDeleteTemplates: " + pf.getDeleteTemplates());
		logger.info("getLineSeparator: " + pf.getLineSeparator());
		logger.info("getParseTemplates: " + pf.getParseTemplates());
		logger.info("getLanguageIdentifers: " + pf.getLanguageIdentifers());
		logger.info("getCategoryIdentifers: " + pf.getCategoryIdentifers());
		logger.info("getImageIdentifers: " + pf.getImageIdentifers());
		logger.info("getShowImageText: " + pf.getShowImageText());
		logger.info("getDeleteTags: " + pf.getDeleteTags());
		logger.info("getImageIdentifers: " + pf.getImageIdentifers());
		logger.info("getShowMathTagContent: " + pf.getShowMathTagContent());
		logger.info("getCalculateSrcSpans: " + pf.getCalculateSrcSpans());
		parser = pf.createParser();
	} // end constructor

	/**
	 * Returns the string capitalized if it is not.
	 */
	protected String normalizePageName(String s) {
		if (s.length() == 0) {
			return s;
		}

		if (Character.isUpperCase(s.charAt(0))) {
			return s;
		}

		return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
	}

	/**
	 * Removes footnotes
	 */
	protected String removeRef(String text) {
		return refPattern.matcher(text).replaceAll(EMPTY_STRING);
	}

	/**
	 * Removes HTML comments
	 */
	protected String removeHtmlComments(String text) {
		return commentPattern.matcher(text).replaceAll(EMPTY_STRING);
	}

	/**
	 * Replaces no-break spaces (&nbsp;).
	 */
	protected String replaceNBSP(String text) {
		return text.replace(NBSP, StringTable.SPACE);
	}

	public ParsedPage parsePage(String text, String[] prefixes) throws IOException {
		return parser.parse(CleanWikipedia.clean(replaceNBSP(removeRef(text)), prefixes, true, true));
	}

	public ParsedPage parsePage(String text) throws IOException {
		return parser.parse(replaceNBSP(removeRef(text)));
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option inputOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia file").isRequired().withLongOpt("input").create("i");
			//Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");

			options.addOption(inputOpt);
			//options.addOption(outputDirOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);


			String wikiText = FileUtils.read(new File(line.getOptionValue("input")));

			WikiMarkupParser wikiMarkupParser = WikiMarkupParser.getInstance();
			ParsedPage parsedPage = wikiMarkupParser.parsePage(wikiText);

			for (Section section : parsedPage.getSections()) {
				System.out.println(section.getText());
			}

		} catch (ParseException e) {
			// oops, something went wrong
			System.out.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.xmldump.util.WikiMarkupParser", "\n", options, "\n", true);
		}
	}
}
