package eu.fbk.twm.wiki.wikipedia;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 9/2/13
 * Time: 9:31 AM
 * To change this template use File | Settings | File Templates.
 */
class Counter {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>Counter</code>.
	 */
	static Logger logger = Logger.getLogger(Counter.class.getName());


	int count;

	public Counter(int count) {
		this.count = count;
	}

	public void inc() {
		count++;
	}

	public void inc(int l) {
		count += l;
	}

	public int get() {
		return count;
	}

	public String toString() {
		return Integer.toString(count);
	}
}
