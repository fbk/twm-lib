package eu.fbk.twm.wiki.wikipedia;

import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 15/11/13
 * Time: 10:31
 * To change this template use File | Settings | File Templates.
 */
public class DumpDownloader {
	public static final String[] languages = {"lt", "sq", "be", "fi", "lv", "sr", "bg", "fr", "nl", "sv", "ca", "hr", "no", "tr", "cs", "hu", "pl", "uk", "da", "id", "pt", "de", "is", "ro", "it", "ru", "es", "sk", "et", "sl", "en"};
	public static final String baseUrl = "http://dumps.wikimedia.org/";

	static Logger logger = Logger.getLogger(DumpDownloader.class.getName());

	public static void main(String[] args) {

		CommandLineParser parser = new PosixParser();
		Options options = new Options();

//		options.addOption(OptionBuilder.withDescription("DBpedia BIN file").isRequired().hasArg().withArgName("file").create("d"));
//		options.addOption(OptionBuilder.withDescription("Cross-language schema").isRequired().hasArg().withArgName("folder").create("c"));
//		options.addOption(OptionBuilder.withDescription("Output dir").isRequired().hasArg().withArgName("folder").create("o"));
//		options.addOption(OptionBuilder.withLongOpt("load").withDescription("Load from folder, if exists").create("l"));

		options.addOption(OptionBuilder.withLongOpt("status").withDescription("Status file").isRequired().hasArg().withArgName("folder").create("S"));

		options.addOption(OptionBuilder.withLongOpt("debug").withDescription("Activate debug mode").create("D"));
		options.addOption("H", "help", false, "Print this message");

		CommandLine commandLine = null;

		try {
			commandLine = parser.parse(options, args);
			if (commandLine.hasOption("help")) {
				throw new ParseException("");
			}
		} catch (ParseException exp) {
			System.out.println();
			if (exp.getMessage().length() > 0) {
				System.out.println("ERR: " + exp.getMessage());
				System.out.println();
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -mx4g " + Thread.currentThread().getStackTrace()[1].getClassName(), "\n", options, "\n", true);
			System.out.println();
			System.exit(0);
		}

		Properties defaultProps = new Properties();
		defaultProps.setProperty("log4j.rootLogger", "info,stdout");
		defaultProps.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
		defaultProps.setProperty("log4j.appender.stdout.layout.ConversionPattern", "[%t] %-5p (%F:%L) - %m %n");
		defaultProps.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");

		boolean debug = false;

		if (commandLine.hasOption("debug")) {
			debug = true;
			defaultProps.setProperty("log4j.rootLogger", "trace,stdout");
		}

		PropertyConfigurator.configure(defaultProps);

		// ---

		String statusFile = commandLine.getOptionValue("status");
		File status = new File(statusFile);
		if (!status.exists()) {
			logger.error("Status file does not exist");
			System.exit(1);
		}
	}
}
