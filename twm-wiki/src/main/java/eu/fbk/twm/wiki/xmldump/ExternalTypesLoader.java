package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.index.BigCrossLanguageSearcher;
import eu.fbk.twm.utils.CommandLineWithLogger;
import eu.fbk.twm.utils.ExtendedProperties;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 10/02/14
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */

public class ExternalTypesLoader {

	static Logger logger = Logger.getLogger(ExternalTypesLoader.class.getName());
	static Pattern bncfPattern = Pattern.compile("([0-9]+)$");
	HashMap<Integer, HashSet<Integer>> bncfBroaders = new HashMap<>();
	HashMap<Integer, HashMap<String, HashSet<String>>> wikidataProperties = new HashMap<>();
	BigCrossLanguageSearcher clSearcher = null;
	ExtendedProperties wikidataMappings = null;
	ExtendedProperties bncfMappings = null;

	private static final int MAX_DEPTH = 10;

	public static String multiply(String s, int num) {
		if (num == 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder(num * s.length());
		for (int i = 0; i < num; i++) {
			sb.append(s);
		}
		return sb.toString();
	}

	public static String multiply(char ch, int num) {
		if (num == 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder(num);
		for (int i = 0; i < num; i++) {
			sb.append(ch);
		}
		return sb.toString();
	}


	public void getBncfClass(Integer id, HashSet<String> history) {
		if (logger.isDebugEnabled()) {
			String label = null;
			if (clSearcher != null) {
				Map<String, String> langData = clSearcher.search("wikiID", id.toString());
				label = langData.get("en");
			}
			logger.debug(String.format("BNCF %s [%d]", label, id));
		}

		HashMap<String, HashSet<String>> properties = wikidataProperties.get(id);
		if (properties != null) {
			for (String s : properties.keySet()) {
				if (s.equals("bncf")) {
					for (String val : properties.get(s)) {
						try {
							getBncfClassInside(Integer.parseInt(val), history, 1);
						} catch (Exception e) {
							logger.debug("ERROR: " + e.getMessage());
						}
					}
				}
			}
		}
		else {
			logger.debug(String.format("No BNCF/WikiData properties for [%d]", id));
		}
	}

	public void getBncfClassInside(Integer id, HashSet<String> history, int depth) {
		if (depth > MAX_DEPTH) {
			logger.debug("--- Max depth reached");
			return;
		}
		String tabs = multiply('\t', depth);

		String m = bncfMappings.getProperty(id.toString());
		if (m != null) {
			if (m.length() == 0) {
				logger.debug(String.format("%sSTOPPED!", tabs));
				return;
			}
			else {
				history.add(m);
				logger.debug(String.format("%sMap to: %s", tabs, m));
				return;
			}
		}
		else {
			logger.debug(String.format("%sNo BNCF mappings for [%d]", tabs, id));
		}

		HashSet<Integer> superClasses = bncfBroaders.get(id);
		if (superClasses != null) {
			logger.debug(String.format("%sSuperclasses: %d", tabs, superClasses.size()));
			for (Integer superClass : superClasses) {
				logger.debug(String.format("%ssuper-class %d", tabs, superClass));
				getBncfClassInside(superClass, history, depth + 1);
			}
		}
		else {
			logger.debug(String.format("%sNo more superclasses", tabs));
		}
	}

	public void getWikidataClass(Integer id, HashSet<String> history) {
		getWikidataClass(id, history, 0);
	}

	public void getWikidataClass(Integer id, HashSet<String> history, int depth) {
		if (depth > MAX_DEPTH) {
			logger.debug("--- Max depth reached");
			return;
		}
		String tabs = multiply('\t', depth);

		if (logger.isDebugEnabled()) {
			String label = null;
			if (clSearcher != null) {
				Map<String, String> langData = clSearcher.search("wikiID", id.toString());
				label = langData.get("en");
			}
			logger.debug(String.format("%sWIKIDATA %s [%d]", tabs, label, id));
		}

		if (depth > 0) {
			String m = wikidataMappings.getProperty(id.toString());
			if (m != null) {
				if (m.length() == 0) {
					logger.debug(String.format("%sSTOPPED!", tabs));
					return;
				}
				else {
					history.add(m);
					logger.debug(String.format("%sMap to: %s", tabs, m));
					return;
				}
			}
		}

		HashMap<String, HashSet<String>> properties = wikidataProperties.get(id);
		if (properties != null) {
			for (String s : properties.keySet()) {
				if (s.equals("subclass_of")) {
					logger.debug(String.format("%s%s", tabs, s));
					for (String val : properties.get(s)) {
						getWikidataClass(Integer.parseInt(val), history, depth + 1);
					}
				}
			}
		}
		else {
			logger.debug(String.format("%swP null", tabs));
		}
	}

	public static HashMap<Integer, HashSet<Integer>> getBncf(String bncfFolder) {
		HashMap<Integer, HashSet<Integer>> bncfBroaders = new HashMap<>();
		HashMap<Integer, HashSet<Integer>> collections = new HashMap<>();

		try {
			File bncfFile = new File(bncfFolder);
			File[] xmls = bncfFile.listFiles();
			for (File f : xmls) {
				if (!f.getName().endsWith(".xml")) {
					continue;
				}

				logger.info("Loading file " + f.getName());
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(f);
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("rdf:Description");

				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						Matcher m = bncfPattern.matcher(eElement.getAttribute("rdf:about"));
						if (!m.find()) {
							continue;
						}
						Integer id = Integer.parseInt(m.group(1));
						logger.trace(String.format("About: %d", id));

						if (bncfBroaders.get(id) == null) {
							bncfBroaders.put(id, new HashSet<Integer>());
						}

						boolean isCollection = false;
						NodeList type = eElement.getElementsByTagName("rdf:type");
						for (int j = 0; j < type.getLength(); j++) {
							Node tNode = type.item(j);
							if (tNode.getNodeType() == Node.ELEMENT_NODE) {
								Element tElement = (Element) tNode;
								isCollection = tElement.getAttribute("rdf:resource").equals("http://www.w3.org/2004/02/skos/core#Collection");
							}
						}

						if (isCollection) {
							logger.trace("It is a collection");

							if (collections.get(id) == null) {
								collections.put(id, new HashSet<Integer>());
							}

							NodeList members = eElement.getElementsByTagName("skos:member");
							for (int j = 0; j < members.getLength(); j++) {
								Node mNode = members.item(j);
								if (mNode.getNodeType() == Node.ELEMENT_NODE) {
									Element mElement = (Element) mNode;

									Matcher mM = bncfPattern.matcher(mElement.getAttribute("rdf:resource"));
									if (!mM.find()) {
										continue;
									}
									Integer idM = Integer.parseInt(mM.group(1));
									collections.get(id).add(idM);
									logger.trace(String.format("Member: %d", idM));

								}
							}
						}
						else {
							NodeList broaders = eElement.getElementsByTagName("skos:broader");

							for (int j = 0; j < broaders.getLength(); j++) {
								Node bNode = broaders.item(j);

								if (bNode.getNodeType() == Node.ELEMENT_NODE) {
									Element bElement = (Element) bNode;


									Matcher mB = bncfPattern.matcher(bElement.getAttribute("rdf:resource"));
									if (!mB.find()) {
										continue;
									}
									Integer idB = Integer.parseInt(mB.group(1));
									bncfBroaders.get(id).add(idB);
									logger.trace(String.format("Broader: %d", idB));
								}
							}
						}
					}

				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return bncfBroaders;
	}

	public void init(String wikidataMapFile, BigCrossLanguageSearcher clSearcher, String wikidataFile, String bncfMapFile, String bncfFolder) {
		init(wikidataMapFile, clSearcher, wikidataFile, bncfMapFile, bncfFolder, null);
	}

	public void init(String wikidataMapFile, BigCrossLanguageSearcher clSearcher, String wikidataFile, String bncfMapFile, String bncfFolder, Integer numPages) {

		bncfBroaders = new HashMap<>();
		wikidataProperties = new HashMap<>();

		wikidataMappings = new ExtendedProperties();
		try {
			wikidataMappings = new ExtendedProperties(wikidataMapFile);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		logger.debug(wikidataMappings);

		bncfMappings = new ExtendedProperties();
		try {
			bncfMappings = new ExtendedProperties(bncfMapFile);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		this.clSearcher = clSearcher;
		HashMap<Integer, HashSet<Integer>> collections = new HashMap<>();

		try {
			File bncfFile = new File(bncfFolder);
			File[] xmls = bncfFile.listFiles();
			for (File f : xmls) {
				if (!f.getName().endsWith(".xml")) {
					continue;
				}

				logger.info("Loading file " + f.getName());
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(f);
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("rdf:Description");

				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						Matcher m = bncfPattern.matcher(eElement.getAttribute("rdf:about"));
						if (!m.find()) {
							continue;
						}
						Integer id = Integer.parseInt(m.group(1));
						logger.trace(String.format("About: %d", id));

						if (bncfBroaders.get(id) == null) {
							bncfBroaders.put(id, new HashSet<Integer>());
						}

						boolean isCollection = false;
						NodeList type = eElement.getElementsByTagName("rdf:type");
						for (int j = 0; j < type.getLength(); j++) {
							Node tNode = type.item(j);
							if (tNode.getNodeType() == Node.ELEMENT_NODE) {
								Element tElement = (Element) tNode;
								isCollection = tElement.getAttribute("rdf:resource").equals("http://www.w3.org/2004/02/skos/core#Collection");
							}
						}

						if (isCollection) {
							logger.trace("It is a collection");

							if (collections.get(id) == null) {
								collections.put(id, new HashSet<Integer>());
							}

							NodeList members = eElement.getElementsByTagName("skos:member");
							for (int j = 0; j < members.getLength(); j++) {
								Node mNode = members.item(j);
								if (mNode.getNodeType() == Node.ELEMENT_NODE) {
									Element mElement = (Element) mNode;

									Matcher mM = bncfPattern.matcher(mElement.getAttribute("rdf:resource"));
									if (!mM.find()) {
										continue;
									}
									Integer idM = Integer.parseInt(mM.group(1));
									collections.get(id).add(idM);
									logger.trace(String.format("Member: %d", idM));

								}
							}
						}
						else {
							NodeList broaders = eElement.getElementsByTagName("skos:broader");

							for (int j = 0; j < broaders.getLength(); j++) {
								Node bNode = broaders.item(j);

								if (bNode.getNodeType() == Node.ELEMENT_NODE) {
									Element bElement = (Element) bNode;


									Matcher mB = bncfPattern.matcher(bElement.getAttribute("rdf:resource"));
									if (!mB.find()) {
										continue;
									}
									Integer idB = Integer.parseInt(mB.group(1));
									bncfBroaders.get(id).add(idB);
									logger.trace(String.format("Broader: %d", idB));
								}
							}
						}
					}

				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		for (Integer id : collections.keySet()) {
			String myMapping = (String) bncfMappings.get(id.toString());
			if (myMapping != null) {
				for (Integer idM : collections.get(id)) {
					if (bncfMappings.get(idM.toString()) == null) {
						bncfMappings.put(idM.toString(), myMapping);
						logger.trace(String.format("Mapping added: %d --> %s", idM, myMapping));
					}
				}
			}
		}

		logger.info(String.format("BNCF file(s) loaded: %d entities", bncfBroaders.size()));

		logger.info("Loading WikiData file " + wikidataFile);
		try {
			wikidataProperties = loadWikiDataProperties(wikidataFile, numPages);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info(String.format("WikiData file loaded: %d entities", wikidataProperties.size()));
	}

	static public HashMap<Integer, HashMap<String, HashSet<String>>> loadWikiDataProperties(String wikidataFile) throws IOException {
		return loadWikiDataProperties(wikidataFile, null);
	}

	static public HashMap<Integer, HashMap<String, HashSet<String>>> loadWikiDataProperties(String wikidataFile, Integer numPages) throws IOException {
		HashMap<Integer, HashMap<String, HashSet<String>>> ret = new HashMap<>();

		BufferedReader wikiDataReader = new BufferedReader(new InputStreamReader(new FileInputStream(wikidataFile), "utf-8"));
		String line;
		while ((line = wikiDataReader.readLine()) != null) {
			if (numPages != null && ret.size() >= numPages) {
				break;
			}
			String[] parts = line.split("\t");
			if (parts.length < 3) {
				continue;
			}

			Integer id = Integer.parseInt(parts[0]);
			String propName = parts[1];
			String value = parts[2];

			if (ret.get(id) == null) {
				ret.put(id, new HashMap<String, HashSet<String>>());
			}
			if (ret.get(id).get(propName) == null) {
				ret.get(id).put(propName, new HashSet<String>());
			}
			ret.get(id).get(propName).add(value);
		}
		wikiDataReader.close();

		return ret;
	}

	public HashSet<String> search(Integer wikidataID) {
		HashSet<String> res = new HashSet<String>();

		getWikidataClass(wikidataID, res);
		getBncfClass(wikidataID, res);

		logger.debug(String.format("Found %d --> %s", wikidataID, res));
		return res;
	}

	public void runForAll() {

		for (Integer id : wikidataProperties.keySet()) {

			HashSet<String> res = search(id);

			if (res.size() > 0) {
				String label = null;
				if (clSearcher != null) {
					Map<String, String> langData = clSearcher.search("wikiID", id.toString());
					label = langData.get("en");
				}
				logger.debug(String.format("%s [%s] --> %s", label, id, res));
			}
		}

	}

	public static void main(String[] args) {
		CommandLineWithLogger commandLineWithLogger = new CommandLineWithLogger();

		commandLineWithLogger.addOption(OptionBuilder.withDescription("BNCF folder").isRequired().hasArgs().withArgName("folder").withLongOpt("bncf").create("b"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("BNCF mappings file").isRequired().hasArgs().withArgName("file").withLongOpt("bncf-map").create("n"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("WikiData properties file").isRequired().hasArgs().withArgName("file").withLongOpt("wikidata").create("w"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("WikiData mappings file").isRequired().hasArgs().withArgName("file").withLongOpt("wikidata-map").create("m"));

		commandLineWithLogger.addOption(OptionBuilder.withDescription("WikiData cross-language index").hasArgs().withArgName("folder").withLongOpt("wikidata-cl").create("c"));
		commandLineWithLogger.addOption(OptionBuilder.withDescription("WikiData number of pages").hasArgs().withArgName("num").withLongOpt("pages").create("N"));

		CommandLine commandLine = null;
		try {
			commandLine = commandLineWithLogger.getCommandLine(args);
			PropertyConfigurator.configure(commandLineWithLogger.getLoggerProps());
		} catch (Exception e) {
			System.exit(1);
		}

		String bncfFolder = commandLine.getOptionValue("bncf");
		String wikidataFile = commandLine.getOptionValue("wikidata");
		String wikidataMapFile = commandLine.getOptionValue("wikidata-map");
		String bncfMapFile = commandLine.getOptionValue("bncf-map");
		String wikidataCL = commandLine.getOptionValue("wikidata-cl");

		Integer numPages = null;
		if (commandLine.hasOption("pages")) {
			numPages = Integer.parseInt(commandLine.getOptionValue("pages"));
		}

		try {
			BigCrossLanguageSearcher clSearcher = null;
			if (wikidataCL != null) {
				clSearcher = new BigCrossLanguageSearcher(wikidataCL);
			}

			ExternalTypesLoader externalTypesLoader = new ExternalTypesLoader();
			externalTypesLoader.init(wikidataMapFile, clSearcher, wikidataFile, bncfMapFile, bncfFolder, numPages);
			externalTypesLoader.runForAll();

			if (wikidataCL != null) {
				clSearcher.close();
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
