package eu.fbk.twm.wiki.wikipedia;

/**
 * Created with IntelliJ IDEA.
 * User: alessio
 * Date: 15/11/13
 * Time: 11:22
 * To change this template use File | Settings | File Templates.
 */
public class LanguageStatus {
	private String language;
	private String date;
	private Integer status;

	public LanguageStatus(String language, String date, Integer status) {
		this.language = language;
		this.date = date;
		this.status = status;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "LanguageStatus{" +
				"language='" + language + '\'' +
				", date='" + date + '\'' +
				", status=" + status +
				'}';
	}
}
