/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump.util;

import de.tudarmstadt.ukp.wikipedia.parser.ParsedPage;
import eu.fbk.twm.utils.ParsedPageTitle;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.utils.core.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/8/13
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * This class extracts the type from the text of the page
 */
public class PageTypeExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageTypeExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(PageTypeExtractor.class.getName());

	private boolean nominal;

	Pattern spacePattern = Pattern.compile(StringTable.SPACE);

	public static final double DEFAULT_THRESHOLD = 0.1;

	public PageTypeExtractor(String text, String form) {
		//logger.debug(form);
		String[] tokens = spacePattern.split(form);

		if (tokens.length == 1) {
			//logger.debug("fromPage " + tokens.length);
			//the space is added to allow the regex to match the first word in the text
			nominal = fromPage(" " + text, form);
		}
		else if (tokens.length == 2) {
			//logger.debug("fromForm " + tokens.length);
			if (containsDigits(tokens[1])) {
				nominal = fromPage(" " + text, form);
			} else {
				nominal = fromForm(tokens);
			}

		}
		else {
			//logger.debug("fromForm " + tokens.length);

			nominal = fromForm(tokens);
		}

		//logger.debug(tokens.length + "\t" + form + "\t" + nominal);
	}

	private boolean containsDigits(String token) {
		char ch;
		for (int i = 0; i < token.length(); i++) {
			ch = token.charAt(0);
			if (Character.isDigit(ch)) {
				return true;
			}
		}
		return false;
	}

	private boolean containsUpperCase(String token) {
		char ch;
		for (int i = 0; i < token.length(); i++) {
			ch = token.charAt(0);
			if (Character.isUpperCase(ch)) {
				return true;
			}
		}
		return false;
	}


	public boolean isNominal() {
		return nominal;
	}

	private boolean fromPage(String text, String form) {
		Pattern formPattern;
		try {
			formPattern = Pattern.compile("[\\s'\"«‛“¿](" + form + ")[\\s\\.,!\\?'\"»…:;‟’]", Pattern.CASE_INSENSITIVE);
		} catch (PatternSyntaxException e) {
			return false;
		}

		Matcher matcher = formPattern.matcher(text);
		int lowerCount = 0;
		//int capitalizedCount = 0;
		int totalCount = 0;
		//String s;
		while (matcher.find()) {
			//logger.debug(matcher.start(1)+"\t"+matcher.group(1)+"\t"+matcher.group(0));
			if (Character.isLowerCase(text.charAt(matcher.start(1)))) {
				lowerCount++;
			}
			//else {
			//	capitalizedCount++;
			//}
			totalCount++;

		}
		double ratio = (double) lowerCount / totalCount;
		//logger.debug(lowerCount + "\t" + totalCount + "\t" + ratio);
		//return (lowerCount > capitalizedCount);
		return ratio > DEFAULT_THRESHOLD;
	}

	private boolean fromForm(String[] tokens) {
		//char ch;
		for (int i = 1; i < tokens.length; i++) {
			//ch = tokens[i].charAt(0);
			//logger.debug(tokens[i] + "\t" + ch);
			//if (Character.isLetter(ch) && !Character.isLowerCase(ch)) {
			if (containsUpperCase(tokens[i])) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length != 2) {
			logger.info("java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.xmldump.util.PageTypeExtractor wiki-file title");
			System.exit(1);
		}

		File file = new File(args[0]);
		String text = FileUtils.read(file);
		WikiMarkupParser wikiMarkupParser = WikiMarkupParser.getInstance();
		ParsedPage parsedPage = wikiMarkupParser.parsePage(text);

		String title = args[1];
		ParsedPageTitle parsedPageTitle = new ParsedPageTitle(title);
		logger.debug(title + "\t" + parsedPageTitle);
		PageTypeExtractor pageTypeExtractor = new PageTypeExtractor(parsedPage.getText(), parsedPageTitle.getForm());
		logger.debug(parsedPage.getText());
		logger.info(title + " is " + (pageTypeExtractor.isNominal() ? "nominal" : " not nominal)"));
	}
}
