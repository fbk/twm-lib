/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.Date;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WikipediaPersonInfoExtractor extends AbstractWikipediaExtractor implements WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaPersonInfoExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaPersonInfoExtractor.class.getName());

	private PrintWriter personInfoWriter;

	protected Pattern templatePattern;

	protected Pattern birthDatePattern;

	protected Pattern deathDatePattern;

	protected Pattern namePattern;

	protected Pattern surnamePattern;

	//protected Pattern commentPattern = Pattern.compile("<!--[^-]+-->", Pattern.MULTILINE);

	public WikipediaPersonInfoExtractor(int numThreads, int numPages, Locale locale) throws MissingResourceException {
		super(numThreads, numPages, locale);
		loadResources();

		if (resources.getString("PERSONAL_DATA_TEMPLATE_PATTERN") != null) {
			templatePattern = Pattern.compile(resources.getString("PERSONAL_DATA_TEMPLATE_PATTERN"));
		}

		if (resources.getString("NAME_PATTERN") != null) {
			namePattern = Pattern.compile(resources.getString("NAME_PATTERN"));
		}

		if (resources.getString("SURNAME_PATTERN") != null) {
			surnamePattern = Pattern.compile(resources.getString("SURNAME_PATTERN"));
		}

		if (resources.getString("BIRTH_DATE_PATTERN") != null && resources.getString("BIRTH_DATE_PATTERN").length() != 0) {
			birthDatePattern = Pattern.compile(resources.getString("BIRTH_DATE_PATTERN"));
		}

		if (resources.getString("DEATH_DATE_PATTERN") != null && resources.getString("DEATH_DATE_PATTERN").length() != 0) {
			deathDatePattern = Pattern.compile(resources.getString("DEATH_DATE_PATTERN"));
		}

		logger.info("templatePattern: " + templatePattern);
		logger.info("namePattern: " + namePattern);
		logger.info("surnamePattern: " + surnamePattern);
		logger.info("birthDatePattern: " + birthDatePattern);
		logger.info("deathDatePattern: " + deathDatePattern);
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			personInfoWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaPersonInfoFileName()), "UTF-8")));
		} catch (IOException e) {
			logger.error(e);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	/**
	 * Removes HTML comments
	 */
	/*protected String removeHtmlComments(String text) {
		return commentPattern.matcher(text).replaceAll("");
	}*/

	@Override
	public void contentPage(String text, String title, int wikiID) {
		logger.debug(title);
		String birthYear = null, deathsYear = null;
		String name = null, surname = null;
		String templateText = null;
		Matcher templateMatcher, birthDateMatcher, deathDateMatcher, nameMatcher, surnameMatcher;

		// extract the name
		templateMatcher = templatePattern.matcher(text);
		if (templateMatcher.find()) {
			templateText = templateMatcher.group(1);
			//logger.debug(title + "\t" + templateText);
		}

		if (templateText == null) {
			return;
		}

		templateText = templateText.trim();
		//logger.debug(templateText);

		// extract the name
		nameMatcher = namePattern.matcher(templateText);
		if (nameMatcher.find()) {
			name = nameMatcher.group(1).trim();
			//logger.debug(title + "\t'" + name + "'");
		}

		// extract the surname
		surnameMatcher = surnamePattern.matcher(templateText);
		if (surnameMatcher.find()) {
			surname = surnameMatcher.group(1).trim();
			//logger.debug(title + "\t'" + surname + "'");
		}

		// extract the birth date
		if (birthDatePattern != null) {
			birthDateMatcher = birthDatePattern.matcher(templateText);
			if (birthDateMatcher.find()) {
				birthYear = birthDateMatcher.group(1).trim();
				//logger.debug(title + "\t'" + birthYear + "'");
			}

		}

		if (deathDatePattern != null) {
			deathDateMatcher = deathDatePattern.matcher(templateText);
			if (deathDateMatcher.find()) {
				deathsYear = deathDateMatcher.group(1).trim();
				//logger.debug(title + "\t'" + deathsYear + "'");
			}

		}


		//if (name != null && surname != null && birthYear != null)
		if (name != null && surname != null) {
			synchronized (this) {
				personInfoWriter.print(title.trim());
				personInfoWriter.print("\t");
				personInfoWriter.print(name.trim());
				personInfoWriter.print("\t");
				personInfoWriter.print(surname.trim());
				personInfoWriter.print("\t");
				if (birthYear != null) {
					personInfoWriter.print(birthYear.trim());
				}
				personInfoWriter.print("\t");
				// extract the death date if it exists
				if (deathsYear != null) {
					personInfoWriter.print(deathsYear.trim());
				}
				personInfoWriter.print("\n");
				// personInfoWriter.flush();
			}
		}

	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void endProcess() {
		super.endProcess();
		personInfoWriter.flush();
		personInfoWriter.close();
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER
                    + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");
			Option baseDirOpt = OptionBuilder.withDescription("if set, use the output folder as base dir").withLongOpt("base-dir").create();

			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(notificationPointOpt);

			options.addOption(baseDirOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);


			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			//ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			ExtractorParameters extractorParameters;
			if (line.hasOption("base-dir")) {
				extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"), true);
			}
			else {
				extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			}
			File dest = new File(extractorParameters.getExtractionOutputDirName());
			dest.mkdirs();
			logger.debug(extractorParameters);

			logger.debug("extracting person info (" + extractorParameters.getWikipediaExampleFileName() + ")...");
			WikipediaExtractor wikipediaExtractor = new WikipediaPersonInfoExtractor(numThreads, numPages, extractorParameters.getLocale());
			wikipediaExtractor.setNotificationPoint(notificationPoint);
			wikipediaExtractor.start(extractorParameters);

			logger.info("extraction ended " + new Date());

		} catch (ParseException e) {
			// oops, something went wrong
			System.out.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaPersonInfoExtractor", "\n", options, "\n", true);
		}
	}
}