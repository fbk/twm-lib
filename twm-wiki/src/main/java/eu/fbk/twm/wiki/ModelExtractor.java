/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki;

import eu.fbk.twm.utils.*;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import eu.fbk.utils.lsa.LSM;
import eu.fbk.twm.index.csv.*;
import eu.fbk.twm.index.*;
import eu.fbk.twm.wiki.xmldump.*;
import eu.fbk.twm.wiki.xmldump.util.CollectGoodTemplates;
import eu.fbk.twm.wiki.xmldump.util.PruneTemplates;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

//import org.apache.commons.io.FileUtils;

/**
 * Created with IntelliJ IDEA.
 * User: aprosio
 * Date: 1/14/13
 * Time: 12:22 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * ./cmd/run-all.sh -r --cross-language-dir /data/models/wikidata/20130623/langs/ --file --person-info --abstract --vectors --lsm-dir /data/models/lsa/ --example --incoming-outgoing --one-example-per-sense --airpedia-class-dir /data/models/airpedia/classes/20130624/current/
 */
public class ModelExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>ModelExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(ModelExtractor.class.getName());

	public static final boolean DEFAULT_OVERWRITE = false;

	public static void writeLog(ExtractorParameters extractorParameters, CommandLine commandLine, String[] args, String fileName) throws IOException {
		logger.debug("writing log (" + fileName + ")...");

		PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8")));
		pw.println("date='" + new Date() + "'");
		pw.print("command='java -cp dist/thewikimachine.jar eu.fbk.twm.wiki.ModelExtractor");
		for (int i = 0; i < args.length; i++) {
			pw.print(" ");
			pw.print(args[i]);
		}
		pw.print("'\n");
		pw.println(extractorParameters);
		Iterator<Option> it = commandLine.iterator();
		for (int i = 0; it.hasNext(); i++) {
			Option opt = it.next();
			pw.println(opt.getArgName() + "='" + opt.getValue() + "'");
		}

		pw.close();
	}

	public static void createSymbolicLink(Path link, Path target) {
		logger.debug("index: " + link + "\t" + target);
		try {

			Files.createSymbolicLink(link, target);
		} catch (IOException x) {
			logger.error(x);
		} catch (UnsupportedOperationException x) {
			logger.error(x);
		}
	}

	public static boolean delete(String fileName) throws IOException {
		File file = new File(fileName);
		if (!file.exists()) {
			logger.debug(file + " doesn't exist");
			return false;
		}

		if (file.isDirectory()) {
			logger.debug("removing directory " + file + "...");
			org.apache.commons.io.FileUtils.deleteDirectory(file);
			return true;
		}

		logger.debug("removing file " + file + "...");
		return file.delete();
	}

	public static void main(String args[]) throws Exception {

		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("file from which to read the Wikipedia XML dump").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option exampleOpt = OptionBuilder.withDescription("extract examples").withLongOpt("example").create("e");
			Option preprocessingOpt = OptionBuilder.withDescription("execute pre-processing").withLongOpt("pre-processing").create("r");
			Option compressOpt = OptionBuilder.withDescription("set compression to true (default is " + WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT + ")").withLongOpt("compress").create("c");
			Option vectorOpt = OptionBuilder.withDescription("extract vectors").withLongOpt("vectors").create();
			//Option outgoingOpt = OptionBuilder.withDescription("extract outgoing links").withLongOpt("outgoing").create();
			Option overwriteOpt = OptionBuilder.withDescription("if set overwrite the existing files and directories").withLongOpt("overwrite").create();
			Option incomingOutgoingOpt = OptionBuilder.withDescription("extract incoming/outgoing links").withLongOpt("incoming-outgoing").create("g");
			//Option indexOpt = OptionBuilder.withDescription("execute indexing").withLongOpt("index").create("i");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option minimumFormFreqOpt = OptionBuilder.withArgName("int").hasArg().withDescription("minimum frequency of wanted forms (default is " + WikipediaExtractor.DEFAULT_MINIMUM_FORM_FREQ + ")").withLongOpt("min-freq").create("f");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("maximum number of pages to process (default is all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			Option maximumFormFreqOpt = OptionBuilder.withArgName("int").hasArg().withDescription("maximum frequency of wanted forms (default is " + WikipediaExtractor.DEFAULT_MAXIMUM_FORM_FREQ + ")").withLongOpt("max-freq").create("m");
			Option sortSizeOpt = OptionBuilder.withArgName("int").hasArg().withDescription("maximum number of lines to load in memory when sorting (default is " + Sort.DEFAULT_SIZE + ")").withLongOpt("sort-size").create("z");
			Option ngramSizeOpt = OptionBuilder.withArgName("int").hasArg().withDescription("n-grams length").withLongOpt("n-gram").create("n");
			Option lsaDimOpt = OptionBuilder.withArgName("int").hasArg().withDescription("lsa dimension (default is " + WikipediaExtractor.DEFAULT_LSA_DIM + ")").withLongOpt("lsa-dim").create();
			Option lsaDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("lsa dir").withLongOpt("lsm-dir").create("l");
			Option baseDirOpt = OptionBuilder.withDescription("if set, use the output folder as base dir").withLongOpt("base-dir").create();
			Option templatesOpt = OptionBuilder.hasArgs().withDescription("if set, it extracts good templates and creates indexes after pre-processing").withLongOpt("templates").create();
			Option categoriesOpt = OptionBuilder.hasArgs().withDescription("if set, it creates categories indexes after pre-processing").withLongOpt("categories").create();
			Option sectionsOpt = OptionBuilder.hasArgs().withDescription("if set, it creates section indexes after pre-processing").withLongOpt("sections").create();
			Option abstractOpt = OptionBuilder.withDescription("extract abstracts").withLongOpt("abstract").create();
			Option stopwordsOpt = OptionBuilder.withArgName("folder").hasArg().withDescription("stopwords folder").withLongOpt("stopwords").create();
			Option configurationOpt = OptionBuilder.withArgName("folder").hasArg().withDescription("configuration folder (default configuration)").withLongOpt("configuration").create();
			Option skipUnigramOpt = OptionBuilder.withDescription("skips unigrams").withLongOpt("skip-unigrams").create();
			Option oneExamplePerSenseOpt = OptionBuilder.withDescription("extract one example per sense (--lsm-dir is required, --lsm-dim is optional)").withLongOpt("one-example-per-sense").create();
			Option fileOpt = OptionBuilder.withDescription("extract all file and calculate the md5").withLongOpt("file").create();
			Option airPediaDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("extract airpedia classes (the directory from which to read the mapping between pages and classes is required)").withLongOpt("airpedia-class-dir").create();
			Option personInfoIndexOpt = OptionBuilder.withDescription("extract person information").withLongOpt("person-info").create();
			Option dbpediaParsOpt = OptionBuilder.withArgName("mappingfile-ontologyfile").hasArgs().withDescription("DBpedia mappings and ontology").withLongOpt("dbpedia-pars").create();

			Option normalizedOpt = OptionBuilder.withDescription("normalize vectors (default is " + WikipediaExtractor.DEFAULT_NORMALIZE + ")").withLongOpt("normalized").create();
			Option numFormOpt = OptionBuilder.withArgName("int").hasArg().withDescription("maximum number of forms to process (default is all)").withLongOpt("num-forms").create();

			// Links
			Option crossLanguageDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("root directory from which to read cross language links (csv and index)").withLongOpt("cross-language-dir").create();
			Option topicDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("Topic folder for all languages").withLongOpt("topic-dir").create();
			Option namnomDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("Nam-nom folder for the language").withLongOpt("namnom-dir").create();
			Option airpedia2DirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("Airpedia2 folder for the language").withLongOpt("airpedia2-dir").create();

			Option maxDepthOpt = OptionBuilder.withArgName("int").hasArg().withDescription("recursion maximum category depth (default is " + AbstractCategoryExtractor.DEFAULT_MAX_CATEGORY_DEPTH + ")").withLongOpt("max-depth").create();
			Option categorySimilarityOpt = OptionBuilder.withDescription("extract page similarity based on categories").withLongOpt("category-similarity").create();

			// SSH
			Option removeAddressOpt = OptionBuilder.withArgName("user@address").hasArg().withDescription("remote server in which to write the models").withLongOpt("remote-server").create();

			// Template parameters
			templatesOpt.setOptionalArg(true);
			templatesOpt.setArgName("range min threshold");
			categoriesOpt.setOptionalArg(true);
			categoriesOpt.setArgName("tokens labels");
			sectionsOpt.setOptionalArg(true);
			sectionsOpt.setArgName("tokens labels");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");
			options.addOption(OptionBuilder.withDescription("trace mode").withLongOpt("trace").create());
			options.addOption(OptionBuilder.withDescription("debug mode").withLongOpt("debug").create());

			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(exampleOpt);
			options.addOption(compressOpt);
			options.addOption(preprocessingOpt);
			options.addOption(vectorOpt);
			options.addOption(airPediaDirOpt);
			options.addOption(numPageOpt);
			options.addOption(notificationPointOpt);
			options.addOption(minimumFormFreqOpt);
			options.addOption(maximumFormFreqOpt);
			options.addOption(sortSizeOpt);
			options.addOption(ngramSizeOpt);
			options.addOption(overwriteOpt);
			options.addOption(incomingOutgoingOpt);
			options.addOption(baseDirOpt);
			options.addOption(templatesOpt);
			options.addOption(categoriesOpt);
			options.addOption(sectionsOpt);
			options.addOption(stopwordsOpt);
			options.addOption(configurationOpt);
			options.addOption(normalizedOpt);
			options.addOption(numFormOpt);
			options.addOption(oneExamplePerSenseOpt);
			options.addOption(lsaDirOpt);
			options.addOption(lsaDimOpt);
			options.addOption(skipUnigramOpt);
			options.addOption(fileOpt);
			options.addOption(abstractOpt);
			options.addOption(personInfoIndexOpt);
			options.addOption(crossLanguageDirOpt);
			options.addOption(removeAddressOpt);
			options.addOption(topicDirOpt);
			options.addOption(dbpediaParsOpt);
			options.addOption(namnomDirOpt);
			options.addOption(airpedia2DirOpt);
			options.addOption(categorySimilarityOpt);
			options.addOption(maxDepthOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			String configurationFolder = "configuration/";
			if (line.hasOption("configuration")) {
				configurationFolder = line.getOptionValue("configuration");
				if (!configurationFolder.endsWith(File.separator)) {
					configurationFolder += File.separator;
				}
			}

//			PropertyConfigurator.configure(logConfig);
			Properties defaultProps = new Properties();
			try {
				defaultProps.load(new InputStreamReader(new FileInputStream(logConfig), "UTF-8"));
			} catch (Exception e) {
				defaultProps.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
				defaultProps.setProperty("log4j.appender.stdout.layout.ConversionPattern", "[%t] %-5p (%F:%L) - %m %n");
				defaultProps.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
				defaultProps.setProperty("log4j.appender.stdout.Encoding", "UTF-8");
			}

			if (line.hasOption("trace")) {
				defaultProps.setProperty("log4j.rootLogger", "trace,stdout");
			}
			else if (line.hasOption("debug")) {
				defaultProps.setProperty("log4j.rootLogger", "debug,stdout");
			}
			else {
				if (defaultProps.getProperty("log4j.rootLogger") == null) {
					defaultProps.setProperty("log4j.rootLogger", "info,stdout");
				}
			}
			PropertyConfigurator.configure(defaultProps);


			logger.info("Configuration folder: " + configurationFolder);

			String dbpediaMappingFile = null;
			String ontologyFile = null;
			String[] dbpediaPars = line.getOptionValues("dbpedia-pars");
			if (dbpediaPars != null) {
				if (dbpediaPars.length == 2) {
					dbpediaMappingFile = dbpediaPars[0];
					ontologyFile = dbpediaPars[1];
				}
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			boolean compress = WikipediaExtractor.DEFAULT_COMPRESS_OUTPUT;
			if (line.hasOption("compress")) {
				compress = true;
			}

			boolean normalized = WikipediaExtractor.DEFAULT_NORMALIZE;
			if (line.hasOption("normalized")) {
				normalized = true;
			}

			int sortSize = Sort.DEFAULT_SIZE;
			if (line.hasOption("sort-size")) {
				sortSize = Integer.parseInt(line.getOptionValue("sort-size"));
			}

			ExtractorParameters extractorParameters;
			if (line.hasOption("base-dir")) {
				extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"), true);
			}
			else {
				extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			}
			File dest = new File(extractorParameters.getExtractionOutputDirName());
			dest.mkdirs();

			logger.debug(extractorParameters);

			String stopWordsFolder = null;
			String thisSW = null;
			if (line.hasOption("stopwords")) {
				stopWordsFolder = line.getOptionValue("stopwords");
				if (!stopWordsFolder.endsWith(File.separator)) {
					stopWordsFolder += File.separator;
				}
				File swFile = new File(stopWordsFolder);
				if (!swFile.exists() || !swFile.isDirectory()) {
					stopWordsFolder = null;
				}

				logger.info("Checking stopwords file...");
				if (stopWordsFolder != null) {
					thisSW = stopWordsFolder + "stopwords_" + extractorParameters.getLang() + ".txt";
					File thisSWFile = new File(thisSW);
					if (!thisSWFile.exists()) {
						thisSW = null;
						logger.info("Stopwords file does not exist, it won't be used");
					}
					else {
						logger.info("Stopwords file ok");
					}
				}
			}

			if (line.hasOption("pre-processing")) {
				logger.info("pre-processing...");
				writeLog(extractorParameters, line, args, extractorParameters.getPreprocessingLogFileName());
				WikipediaExtractor wikipediaExtractor = new WikipediaPreprocessing(numThreads, numPages, extractorParameters.getLocale(), configurationFolder);
				wikipediaExtractor.setNotificationPoint(notificationPoint);
				wikipediaExtractor.start(extractorParameters);

				logger.info("Portals and navigation templates...");
				WikipediaPageNavigationTemplateExtractor navExtractor = new WikipediaPageNavigationTemplateExtractor(numThreads, numPages, extractorParameters.getLocale());
				navExtractor.setNotificationPoint(notificationPoint);
				navExtractor.start(extractorParameters);
				WikipediaPagePortalExtractor portalExtractor = new WikipediaPagePortalExtractor(numThreads, numPages, extractorParameters.getLocale());
				portalExtractor.start(extractorParameters);
				PagePortalIndexer pagePortalIndexer = new PagePortalIndexer(extractorParameters.getWikipediaPagePortalIndexName());
				pagePortalIndexer.index(extractorParameters.getWikipediaPagePortalFileName(), compress);
				PageNavigationTemplateIndexer pageNavigationTemplateIndexer = new PageNavigationTemplateIndexer(extractorParameters.getWikipediaPageNavigationTemplateIndexName());
				pageNavigationTemplateIndexer.index(extractorParameters.getWikipediaPageNavigationTemplateFileName(), compress);

				//extract texts
				logger.info("extracting text from " + extractorParameters.getWikipediaXmlFileName() + " (" + extractorParameters.getWikipediaTextFileName() + ")...");
				WikipediaExtractor wikipediaTextExtractor = new WikipediaTextExtractor(numThreads, numPages, extractorParameters.getLocale());
				wikipediaTextExtractor.setNotificationPoint(notificationPoint);
				wikipediaTextExtractor.start(extractorParameters);

				//extract unigrams
				logger.info("extracting unigrams from " + extractorParameters.getWikipediaTextFileName() + " (" + extractorParameters.getWikipediaUnigramFileName() + ")...");
				CSVExtractor unigramExtractor = new UnigramExtractor(numThreads, numPages);
				unigramExtractor.setNotificationPoint(notificationPoint);
				unigramExtractor.start(extractorParameters);

				//todo: use the overwrite option
				delete(extractorParameters.getWikipediaTextIndexName());
				logger.info("indexing text (" + extractorParameters.getWikipediaTextIndexName() + ")...");
				PageTextIndexer pageTextIndexer = new PageTextIndexer(extractorParameters.getWikipediaTextIndexName());
				pageTextIndexer.index(extractorParameters.getWikipediaTextFileName(), compress);
				pageTextIndexer.close();

				//todo: use the overwrite option
				delete(extractorParameters.getWikipediaPageCategoryIndexName());
				logger.info("indexing page categories (" + extractorParameters.getWikipediaPageCategoryIndexName() + ")...");
				PageCategoryIndexer pageCategoryIndexer = new PageCategoryIndexer(extractorParameters.getWikipediaPageCategoryIndexName());
				pageCategoryIndexer.index(extractorParameters.getWikipediaPageCategoryFileName(), compress);
				pageCategoryIndexer.close();

				//todo: use the overwrite option
				delete(extractorParameters.getWikipediaCategorySuperCategoryIndexName());
				logger.info("indexing category super categories (" + extractorParameters.getWikipediaCategorySuperCategoryIndexName() + ")...");
				CategorySuperCategoryIndexer categorySuperCategoryIndexer = new CategorySuperCategoryIndexer(extractorParameters.getWikipediaCategorySuperCategoryIndexName());
				categorySuperCategoryIndexer.index(extractorParameters.getWikipediaCategorySuperCategoryFileName(), compress);
				categorySuperCategoryIndexer.close();

				//todo: use the overwrite option
				logger.info("sorting " + extractorParameters.getWikipediaCategorySuperCategoryFileName() + "...");
				UnixSortWrapper.sort(extractorParameters.getWikipediaCategorySuperCategoryFileName(), extractorParameters.getWikipediaCategorySubCategoryFileName(), 2, numThreads);

				delete(extractorParameters.getWikipediaCategorySubCategoryIndexName());
				logger.info("indexing category sub categories (" + extractorParameters.getWikipediaCategorySubCategoryIndexName() + ")...");
				CategorySubCategoryIndexer categorySubCategoryIndexer = new CategorySubCategoryIndexer(extractorParameters.getWikipediaCategorySubCategoryIndexName());
				categorySubCategoryIndexer.index(extractorParameters.getWikipediaCategorySubCategoryFileName(), compress);
				categorySubCategoryIndexer.close();

				//todo: use the overwrite option
				UnixSortWrapper.sort(extractorParameters.getWikipediaPageCategoryFileName(), extractorParameters.getWikipediaCategoryPageFileName(), 1, numThreads);

				delete(extractorParameters.getWikipediaCategoryPageIndexName());
				logger.info("indexing category page (" + extractorParameters.getWikipediaCategoryPageIndexName() + ")...");
				CategoryPageIndexer categoryPageIndexer = new CategoryPageIndexer(extractorParameters.getWikipediaCategoryPageIndexName());
				categoryPageIndexer.index(extractorParameters.getWikipediaCategoryPageFileName(), compress);
				categoryPageIndexer.close();

				//todo: extract the list of categories
				//time cut -f1 en/enwiki-20141106-category-super-category.csv | uniq | sort > en/enwiki-20141106-category.csv
			}

			if (dbpediaMappingFile != null) {
				logger.info("DBpedia mappings");
				DBpediaClassExtractor dBpediaClassExtractor = new DBpediaClassExtractor(numThreads, extractorParameters.getLocale());
				dBpediaClassExtractor.start(extractorParameters.getWikipediaXmlFileName(), extractorParameters.getWikipediaDBPediaClassesIndexName(), dbpediaMappingFile, ontologyFile);
			}

			if (line.hasOption("templates")) {
				String[] tValues = line.getOptionValues("templates");
				String tplMapRep = extractorParameters.getWikipediaTemplateFileNames().get("map-rep");
				String tplComplete = extractorParameters.getWikipediaTemplateFileNames().get("complete");
				String tplGood = extractorParameters.getWikipediaTemplateFileNames().get("good");
				String tplPruned = extractorParameters.getWikipediaTemplateFileNames().get("pruned");
				String tplPrunedSortedPage = extractorParameters.getWikipediaTemplateFileNames().get("pruned-s-page");
				String tplPrunedSortedTpl = extractorParameters.getWikipediaTemplateFileNames().get("pruned-s-tpl");
				String tplIndexP = extractorParameters.getWikipediaTemplateFileNames().get("index-p2t");
				String tplIndexT = extractorParameters.getWikipediaTemplateFileNames().get("index-t2p");
				String tplIndexID = extractorParameters.getWikipediaTemplateFileNames().get("index-id");
				String tplSimpleGood = extractorParameters.getWikipediaTemplateFileNames().get("infoboxes");

				double range = 1.01;
				int min = 50;
				int threshold = 2;
				if (tValues != null) {
					if (tValues.length > 0) {
						range = Double.parseDouble(tValues[0]);
					}
					if (tValues.length > 1) {
						min = Integer.parseInt(tValues[1]);
					}
					if (tValues.length > 2) {
						threshold = Integer.parseInt(tValues[2]);
					}
				}
				logger.info("Working on templates [min=" + min + " range=" + range + " threshold=" + threshold + "]...");
				logger.debug("min " + min);
				logger.debug("range " + range);
				logger.debug("threshold " + threshold);

				logger.info("Pruning templates...");
				new PruneTemplates(tplMapRep, tplComplete, tplGood, range, min);

				logger.info("Collecting templates...");
				new CollectGoodTemplates(tplMapRep, tplGood, tplPruned, threshold, numThreads, tplSimpleGood);

				logger.info("Sorting template files...");
				FileUtils.sort(tplPruned, tplPrunedSortedPage, 0, sortSize, compress);
				FileUtils.sort(tplPruned, tplPrunedSortedTpl, 1, sortSize, compress);

				logger.info("Indexing templates...");
				tplIndexP = GenericFileUtils.checkWriteableFolder(tplIndexP, true);
				tplIndexT = GenericFileUtils.checkWriteableFolder(tplIndexT, true);
				tplIndexID = GenericFileUtils.checkWriteableFolder(tplIndexID, true);
				if (tplIndexT != null && tplIndexP != null && tplIndexID != null) {
					new GenericValuesIndexer(tplPrunedSortedPage, tplPrunedSortedTpl, tplIndexP, tplIndexT, tplIndexID);
				}

				// java done: cut -f2 itwiki-20130131-template-pruned.csv | sort | uniq --> itwiki-20130131-template-infoboxes.csv
			}

			if (line.hasOption("categories")) {
				logger.info("Working on categories...");
				String[] cValues = line.getOptionValues("categories");
				ArrayList<String> cValuesAL;
				if (cValues != null) {
					cValuesAL = new ArrayList<String>(Arrays.asList(cValues));
				}
				else {
					cValuesAL = new ArrayList<String>();
				}

				String catSortPage = extractorParameters.getWikipediaPageCategoryFileName();

				if (cValuesAL.size() == 0 || cValuesAL.contains("labels")) {
					String catSortCat = extractorParameters.getWikipediaCategoryFileNames().get("s-cat");
					String catIndexP = extractorParameters.getWikipediaCategoryFileNames().get("index-p2c");
					String catIndexC = extractorParameters.getWikipediaCategoryFileNames().get("index-c2p");
					String catIndexID = extractorParameters.getWikipediaCategoryFileNames().get("index-id");

					logger.info("Sorting category files...");
					FileUtils.sort(catSortPage, catSortCat, 1, sortSize, compress);

					logger.info("Indexing categories...");
					catIndexP = GenericFileUtils.checkWriteableFolder(catIndexP, true);
					catIndexC = GenericFileUtils.checkWriteableFolder(catIndexC, true);
					catIndexID = GenericFileUtils.checkWriteableFolder(catIndexID, true);
					if (catIndexC != null && catIndexP != null && catIndexID != null) {
						new GenericValuesIndexer(catSortPage, catSortCat, catIndexP, catIndexC, catIndexID);
					}
				}
				if (cValuesAL.size() == 0 || cValuesAL.contains("tokens")) {
					String catTokens = extractorParameters.getWikipediaCategoryFileNames().get("tokens");
					String catTokOrd = extractorParameters.getWikipediaCategoryFileNames().get("tokens-s-tok");
					String catTokIndexP = extractorParameters.getWikipediaCategoryFileNames().get("tokens-index-p2k");
					String catTokIndexT = extractorParameters.getWikipediaCategoryFileNames().get("tokens-index-k2p");
					String catTokIndexID = extractorParameters.getWikipediaCategoryFileNames().get("tokens-index-id");

					logger.info("Tokenizing categories...");
					new PageCategoryTokenizer(catSortPage, catTokens, thisSW);

					logger.info("Sorting category-token files...");
					FileUtils.sort(catTokens, catTokOrd, 1, sortSize, compress);

					logger.info("Indexing category-tokens...");
					catTokIndexP = GenericFileUtils.checkWriteableFolder(catTokIndexP, true);
					catTokIndexT = GenericFileUtils.checkWriteableFolder(catTokIndexT, true);
					catTokIndexID = GenericFileUtils.checkWriteableFolder(catTokIndexID, true);
					if (catTokIndexP != null && catTokIndexT != null && catTokIndexID != null) {
						new GenericValuesIndexer(catTokens, catTokOrd, catTokIndexP, catTokIndexT, catTokIndexID);
					}
				}
			}

			if (line.hasOption("sections")) {
				logger.info("Working on sections...");
				String[] sValues = line.getOptionValues("categories");
				ArrayList<String> sValuesAL;
				if (sValues != null) {
					sValuesAL = new ArrayList<String>(Arrays.asList(sValues));
				}
				else {
					sValuesAL = new ArrayList<String>();
				}

				String secSortPage = extractorParameters.getWikipediaSectionTitleFileName();

				if (sValuesAL.size() == 0 || sValuesAL.contains("labels")) {
					String secSortSec = extractorParameters.getWikipediaSectionTitleFileNames().get("s-sec");
					String secIndexP = extractorParameters.getWikipediaSectionTitleFileNames().get("index-p2s");
					String secIndexS = extractorParameters.getWikipediaSectionTitleFileNames().get("index-s2p");
					String secIndexID = extractorParameters.getWikipediaSectionTitleFileNames().get("index-id");

					logger.info("Sorting section files...");
					FileUtils.sort(secSortPage, secSortSec, 1, sortSize, compress);

					logger.info("Indexing sections...");
					secIndexP = GenericFileUtils.checkWriteableFolder(secIndexP, true);
					secIndexS = GenericFileUtils.checkWriteableFolder(secIndexS, true);
					secIndexID = GenericFileUtils.checkWriteableFolder(secIndexID, true);
					if (secIndexS != null && secIndexP != null && secIndexID != null) {
						new GenericValuesIndexer(secSortPage, secSortSec, secIndexP, secIndexS, secIndexID);
					}
				}

				if (sValuesAL.size() == 0 || sValuesAL.contains("tokens")) {
					String secTokens = extractorParameters.getWikipediaSectionTitleFileNames().get("tokens");
					String secTokOrd = extractorParameters.getWikipediaSectionTitleFileNames().get("tokens-s-tok");
					String secTokIndexP = extractorParameters.getWikipediaSectionTitleFileNames().get("tokens-index-p2k");
					String secTokIndexT = extractorParameters.getWikipediaSectionTitleFileNames().get("tokens-index-k2p");
					String secTokIndexID = extractorParameters.getWikipediaSectionTitleFileNames().get("tokens-index-id");

					logger.info("Tokenizing sections...");
					new SectionTitleTokenizer(secSortPage, secTokens, thisSW);

					logger.info("Sorting category-token files...");
					FileUtils.sort(secTokens, secTokOrd, 1, sortSize, compress);

					logger.info("Indexing category-tokens...");
					secTokIndexP = GenericFileUtils.checkWriteableFolder(secTokIndexP, true);
					secTokIndexT = GenericFileUtils.checkWriteableFolder(secTokIndexT, true);
					secTokIndexID = GenericFileUtils.checkWriteableFolder(secTokIndexID, true);
					if (secTokIndexP != null && secTokIndexT != null && secTokIndexID != null) {
						new GenericValuesIndexer(secTokens, secTokOrd, secTokIndexP, secTokIndexT, secTokIndexID);
					}
				}
			}

			if (line.hasOption("file")) {
				//if (!(new File(extractorParameters.getWikipediaFileName()).exists()))
				//{
				//moved in pre-processing
				//logger.debug("extracting files (" + extractorParameters.getWikipediaFileName() + ")...");
				//WikipediaFileExtractor wikipediaFileExtractor = new WikipediaFileExtractor(numThreads, numPages, extractorParameters.getLocale());
				//wikipediaFileExtractor.setNotificationPoint(notificationPoint);
				//wikipediaFileExtractor.start(extractorParameters);
				//}
				logger.debug("extracting files from page (" + extractorParameters.getWikipediaFileSourceName() + ")...");
				WikipediaFileSourceExtractor wikipediaFileSourceExtractor = new WikipediaFileSourceExtractor(numThreads, numPages, extractorParameters.getLocale());
				wikipediaFileSourceExtractor.setNotificationPoint(notificationPoint);
				wikipediaFileSourceExtractor.start(extractorParameters);


				delete(extractorParameters.getWikipediaFileSourceIndexName());
				logger.info("indexing files from " + extractorParameters.getWikipediaFileName() + " (" + extractorParameters.getWikipediaFileSourceIndexName() + ")...");
				PageFileIndexer pageFileIndexer = new PageFileIndexer(extractorParameters.getWikipediaFileSourceIndexName());
				pageFileIndexer.index(extractorParameters.getWikipediaFileSourceName(), compress);
				pageFileIndexer.close();

			}

			if (line.hasOption("airpedia-class-dir")) {
				//this should be deprecated and replaced by airpedia2-dir

				//--airpedia-class-dir data/models/airpedia/classes/20130624/current/
				logger.info("extracting airpedia...");
				delete(extractorParameters.getWikipediaPageAirPediaClassIndexName());
				String airPediaClassDirName = line.getOptionValue("airpedia-class-dir");
				if (!airPediaClassDirName.endsWith(File.separator)) {
					airPediaClassDirName += File.separator;
				}
				String airpediaClassFileName = airPediaClassDirName + extractorParameters.getLang() + ".csv";
				logger.info("indexing airpedia from " + airpediaClassFileName + " (" + extractorParameters.getWikipediaPageAirPediaClassIndexName() + ")...");
				//replaced with airperdia
				//PageDBpediaClassIndexer pageDBpediaClassIndexer = new PageDBpediaClassIndexer(extractorParameters.getWikipediaPageAirPediaClassIndexName());
				//pageDBpediaClassIndexer.index(airpediaClassFileName, compress);
				//pageDBpediaClassIndexer.close();
				// @see PageAirpediaClassExtractor
				// e.g., data/models/airpedia/classes/20130617/airpedia/it.csv
				try {
					PageAirpediaClassIndexer pageAirpediaClassIndexer = new PageAirpediaClassIndexer(extractorParameters.getWikipediaPageAirPediaClassIndexName());
					pageAirpediaClassIndexer.index(airpediaClassFileName, compress);
					pageAirpediaClassIndexer.close();
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}

			if (line.hasOption("person-info")) {
				//todo: check why I'm doing it two times: see preprocessing
				logger.debug("extracting first name " + extractorParameters.getWikipediaFirstNameFileName() + "...");
				CSVExtractor firstNameExtractor = new FirstNameExtractor(numThreads);
				firstNameExtractor.setNotificationPoint(notificationPoint);
				firstNameExtractor.start(extractorParameters);

				delete(extractorParameters.getWikipediaFirstNameIndexName());
				logger.info("indexing first name from " + extractorParameters.getWikipediaFirstNameFileName() + "(" + extractorParameters.getWikipediaFirstNameIndexName() + ")...");
				FirstNameIndexer firstNameIndexer = new FirstNameIndexer(extractorParameters.getWikipediaFirstNameIndexName());
				firstNameIndexer.index(extractorParameters.getWikipediaFirstNameFileName());
				firstNameIndexer.close();

				delete(extractorParameters.getWikipediaPersonInfoIndexName());
				logger.info("indexing person information from " + extractorParameters.getWikipediaPersonInfoFileName() + "...");
				PersonInfoIndexer personInfoIndexer = new PersonInfoIndexer(extractorParameters.getWikipediaPersonInfoIndexName());
				personInfoIndexer.index(extractorParameters.getWikipediaPersonInfoFileName());
				personInfoIndexer.close();
			}

			if (line.hasOption("abstract")) {
				//todo: check if it can be moved to preprocessing
				logger.debug("extracting abstracts (" + extractorParameters.getWikipediaAbstractFileName() + ")...");
				WikipediaAbstractExtractor wikipediaAbstractExtractor = new WikipediaAbstractExtractor(numThreads, numPages, extractorParameters.getLocale());
				wikipediaAbstractExtractor.setNotificationPoint(notificationPoint);
				wikipediaAbstractExtractor.start(extractorParameters);

				delete(extractorParameters.getWikipediaAbstractIndexName());
				logger.info("indexing abstract from " + extractorParameters.getWikipediaAbstractFileName() + " (" + extractorParameters.getWikipediaAbstractIndexName() + ")...");
				PageAbstractIndexer pageAbstractIndexer = new PageAbstractIndexer(extractorParameters.getWikipediaAbstractIndexName());
				pageAbstractIndexer.index(extractorParameters.getWikipediaAbstractFileName());
				pageAbstractIndexer.close();
			}

			if (line.hasOption("outgoing")) {
				//not used anymore
				/*logger.info("extracting outgoing links...");
				writeLog(extractorParameters, line, args, extractorParameters.getOutgoingLogFileName());
				WikipediaExtractor wikipediaExtractor = new WikipediaOutgoingLinkExtractor(numThreads, numPages, extractorParameters.getLocale());
				wikipediaExtractor.setNotificationPoint(notificationPoint);
				wikipediaExtractor.start(extractorParameters);

				logger.info("sorting outgoing links...");
				FileUtils.sort(extractorParameters.getWikipediaOutgoingFileName(), extractorParameters.getWikipediaIncomingFileName(), 1, sortSize, compress);

				logger.info("indexing outgoing links...");
				PageOutgoingIndexer pageOutgoingIndexer = new PageOutgoingIndexer(extractorParameters.getWikipediaOutgoingIndexName());
				pageOutgoingIndexer.index(extractorParameters.getWikipediaOutgoingFileName(), compress);
				pageOutgoingIndexer.close();

				logger.info("indexing incoming links...");
				PageIncomingIndexer pageIncomingIndexer = new PageIncomingIndexer(extractorParameters.getWikipediaIncomingIndexName());
				pageIncomingIndexer.index(extractorParameters.getWikipediaIncomingFileName(), compress);
				pageIncomingIndexer.close(); */
			}

			if (line.hasOption("vectors")) {
				if (line.hasOption("lsm-dir")) {
					writeLog(extractorParameters, line, args, extractorParameters.getVectorLogFileName());
					int lsaDim = VectorExtractor.DEFAULT_LSA_DIM;
					if (line.hasOption("lsa-dim")) {
						lsaDim = Integer.parseInt(line.getOptionValue("lsa-dim"));
					}

					logger.info("extracting vectors from " + extractorParameters.getWikipediaTextFileName() + " (" + extractorParameters.getWikipediaVectorFileName() + ")...");
					String lsaDir = line.getOptionValue("lsm-dir") + File.separator + extractorParameters.getLang() + File.separator + "current";
					logger.debug("lsaDir " + lsaDir);
					//if (!(new File(extractorParameters.getWikipediaVectorFileName()).exists()))
					//{
					CSVExtractor csvExtractor = new VectorExtractor(numThreads, numPages, lsaDir, lsaDim, normalized);
					csvExtractor.setNotificationPoint(notificationPoint);
					csvExtractor.start(extractorParameters);

					//}

					//todo: create index
					delete(extractorParameters.getWikipediaVectorIndexName());
					logger.info("indexing vectors from " + extractorParameters.getWikipediaVectorFileName() + " (" + extractorParameters.getWikipediaVectorIndexName() + ")...");
					PageVectorIndexer pageVectorIndexer = new PageVectorIndexer(extractorParameters.getWikipediaVectorIndexName());
					pageVectorIndexer.index(extractorParameters.getWikipediaVectorFileName());
					pageVectorIndexer.close();

				}
				else {
					logger.error("This extractor requires lsm-dir");
				}

			}

			if (line.hasOption("example")) {
				writeLog(extractorParameters, line, args, extractorParameters.getExtractionLogFileName());

				//create examples
				int maximumFormFreq = WikipediaExampleExtractor.DEFAULT_MAXIMUM_FORM_FREQ;
				if (line.hasOption("max-freq")) {
					maximumFormFreq = Integer.parseInt(line.getOptionValue("max-freq"));
				}
				logger.info("filtering examples with frequency higher than " + maximumFormFreq + "...");

				logger.info("extracting examples (" + extractorParameters.getWikipediaExampleFileName() + ")...");
				WikipediaExampleExtractor wikipediaExtractor = new WikipediaExampleExtractor(numThreads, numPages, extractorParameters.getLocale());
				wikipediaExtractor.setCompress(compress);
				wikipediaExtractor.setNotificationPoint(notificationPoint);
				wikipediaExtractor.setMaximumNumberOfExamplesPerPage(maximumFormFreq);
				wikipediaExtractor.start(extractorParameters);

				//sort example
				int minimumFormFreq = WikipediaExtractor.DEFAULT_MINIMUM_FORM_FREQ;
				if (line.hasOption("min-freq")) {
					minimumFormFreq = Integer.parseInt(line.getOptionValue("min-freq"));
				}

				if (minimumFormFreq > 1) {
					logger.info("filtering examples with frequency lower than " + minimumFormFreq + "...");
					FreqSet formFreqSet = new FreqSet();
					formFreqSet.read(new BufferedReader(new FileReader(extractorParameters.getWikipediaFormFreqFileName())));
					FileUtils.filter(extractorParameters.getWikipediaExampleFileName(), extractorParameters.getWikipediaFilteredExampleFileName(), formFreqSet, 0, minimumFormFreq, compress);

					logger.info("sorting " + extractorParameters.getWikipediaExampleFileName() + " (" + sortSize + ")...");
					//FileUtils.sort(extractorParameters.getWikipediaFilteredExampleFileName(), extractorParameters.getWikipediaSortedFormFileName(), 0, sortSize, compress);
					UnixSortWrapper.sort(extractorParameters.getWikipediaFilteredExampleFileName(), extractorParameters.getWikipediaSortedFormFileName(), 1, numThreads);
					//FileUtils.sort(extractorParameters.getWikipediaFilteredExampleFileName(), extractorParameters.getWikipediaSortedPageFileName(), 1, sortSize, compress);
					UnixSortWrapper.sort(extractorParameters.getWikipediaFilteredExampleFileName(), extractorParameters.getWikipediaSortedPageFileName(), "1,2,3,4", 2, numThreads);
				}
				else {
					logger.info("sorting " + extractorParameters.getWikipediaExampleFileName() + "...");
					//FileUtils.sort(extractorParameters.getWikipediaExampleFileName(), extractorParameters.getWikipediaSortedFormFileName(), 0, sortSize, compress);
					UnixSortWrapper.sort(extractorParameters.getWikipediaExampleFileName(), extractorParameters.getWikipediaSortedFormFileName(), 1, numThreads);
					//FileUtils.sort(extractorParameters.getWikipediaExampleFileName(), extractorParameters.getWikipediaSortedPageFileName(), 1, sortSize, compress);
					UnixSortWrapper.sort(extractorParameters.getWikipediaExampleFileName(), extractorParameters.getWikipediaSortedPageFileName(), "1,2,3,4", 2, numThreads);
				}

				//index dictionaries
				logger.info("indexing page/form pairs (" + extractorParameters.getWikipediaSortedPageFileName() + ")...");
				PageFormIndexer pageFormIndexer = new PageFormIndexer(extractorParameters.getWikipediaPageFormIndexName());
				pageFormIndexer.index(extractorParameters.getWikipediaSortedPageFileName(), compress);
				pageFormIndexer.close();

				logger.info("indexing form/page pairs (" + extractorParameters.getWikipediaSortedFormFileName() + ")...");
				FormPageIndexer formPageIndexer = new FormPageIndexer(extractorParameters.getWikipediaFormPageIndexName());
				formPageIndexer.index(extractorParameters.getWikipediaSortedFormFileName(), compress);
				formPageIndexer.close();

				//index page types
				logger.info("indexing types (" + extractorParameters.getWikipediaTypeIndexName() + ")...");
				TypeIndexer typeIndexer = new TypeIndexer(extractorParameters.getWikipediaTypeIndexName());
				typeIndexer.index(extractorParameters.getWikipediaSortedPageFileName());
				typeIndexer.close();

				logger.info("indexing page-freq (" + extractorParameters.getWikipediaPageFreqIndexName() + ")...");
				PageFreqIndexer pageFreqIndexer = new PageFreqIndexer(extractorParameters.getWikipediaPageFreqIndexName());
				pageFreqIndexer.index(extractorParameters.getWikipediaPageFreqFileName());
				pageFreqIndexer.close();

				int ngramSize = PageNGramExtractor.DEFAULT_N_GRAM;
				if (line.hasOption("n-gram")) {
					ngramSize = Integer.parseInt(line.getOptionValue("n-gram"));
				}
				//extract n-grams (uses TypeSearcher extractorParameters.getWikipediaTypeIndexName())
				logger.info("extracting " + ngramSize + "-grams from " + extractorParameters.getWikipediaTextFileName() + " (" + extractorParameters.getWikipediaNGramFileName() + ")...");
				PageNGramExtractor pageNGramExtractor = new PageNGramExtractor(numThreads, numPages, ngramSize);
				pageNGramExtractor.start(extractorParameters);

				//index n-grams
				logger.info("indexing n-grams (" + extractorParameters.getWikipediaNGramFileName() + ")...");
				NGramIndexer ngramIndexer = new NGramIndexer(extractorParameters.getWikipediaNGramIndexName());
				ngramIndexer.index(extractorParameters.getWikipediaNGramFileName());
				ngramIndexer.close();
			}

			if (line.hasOption("incoming-outgoing")) {

				logger.info("extracting incoming/outgoing links...");
				writeLog(extractorParameters, line, args, extractorParameters.getIncomingOutgoingLogFileName());
				WikipediaExtractor wikipediaExtractor = new WikipediaIncomingOutgoingLinkExtractor(numThreads, numPages, extractorParameters.getLocale());
				wikipediaExtractor.setNotificationPoint(notificationPoint);
				wikipediaExtractor.start(extractorParameters);


				logger.info("sorting incoming/outgoing links...");
				//FileUtils.sort(extractorParameters.getWikipediaIncomingOutgoingFileName(), extractorParameters.getWikipediaSortedIncomingOutgoingFileName(), 0, sortSize, compress);
				UnixSortWrapper.sort(extractorParameters.getWikipediaIncomingOutgoingFileName(), extractorParameters.getWikipediaSortedIncomingOutgoingFileName(), 1, numThreads);

				//todo: use the overwrite option
				//todo: deprecate
				//delete(extractorParameters.getWikipediaIncomingOutgoingIndexName());
				//logger.info("indexing incoming/outgoing links...");
				//PageIncomingOutgoingIndexer pageIncomingOutgoingIndexer = new PageIncomingOutgoingIndexer(extractorParameters.getWikipediaIncomingOutgoingIndexName());
				//pageIncomingOutgoingIndexer.index(extractorParameters.getWikipediaSortedIncomingOutgoingFileName(), compress);
				//pageIncomingOutgoingIndexer.close();

				//todo: use the overwrite option
				delete(extractorParameters.getWikipediaIncomingOutgoingWeightedIndexName());
				logger.info("indexing incoming/outgoing weighted links...");
				PageIncomingOutgoingWeightedIndexer pageIncomingOutgoingWeightedIndexer = new PageIncomingOutgoingWeightedIndexer(extractorParameters.getWikipediaIncomingOutgoingWeightedIndexName(), extractorParameters.getWikipediaPageFreqFileName());
				pageIncomingOutgoingWeightedIndexer.index(extractorParameters.getWikipediaSortedIncomingOutgoingFileName(), compress);
				pageIncomingOutgoingWeightedIndexer.close();

			}

			if (line.hasOption("category-similarity")) {
				logger.info("extracting weighted categories...");
				// the weight is the number of pages they contain
				PagePerCategoryCounter pagePerCategoryCounter = new PagePerCategoryCounter(numThreads);
				if (line.hasOption("max-depth")) {
					pagePerCategoryCounter.setMaxDepth(Integer.parseInt(line.getOptionValue("max-depth")));
				}
				pagePerCategoryCounter.setNotificationPoint(notificationPoint);
				pagePerCategoryCounter.start(extractorParameters);

				logger.info("sorting weighted categories...");
				UnixSortWrapper.sort(extractorParameters.getWikipediaPagePerCategoryCountFileName(), extractorParameters.getWikipediaSortedPagePerCategoryCountFileName(), 2, "-nr", numThreads);

				logger.info("extracting page/weighted categories...");
				writeLog(extractorParameters, line, args, extractorParameters.getWikipediaPageAllCategoryFileName());
				PageAllCategoryExtractor pageAllCategoryExtractor = new PageAllCategoryExtractor(numThreads);
				if (line.hasOption("max-depth")) {
					pageAllCategoryExtractor.setMaxDepth(Integer.parseInt(line.getOptionValue("max-depth")));
				}
				pageAllCategoryExtractor.setNotificationPoint(notificationPoint);
				pageAllCategoryExtractor.start(extractorParameters);

				//todo: use the overwrite option
				delete(extractorParameters.getWikipediaPageAllCategoryIndexName());
				logger.info("indexing page/weighted categories...");
				PageAllCategoryIndexer pageAllCategoryIndexer = new PageAllCategoryIndexer(extractorParameters.getWikipediaPageAllCategoryIndexName());
				pageAllCategoryIndexer.index(extractorParameters.getWikipediaPageAllCategoryFileName());
				pageAllCategoryIndexer.close();
			}
			if (line.hasOption("one-example-per-sense")) {
				if (line.hasOption("lsm-dir")) {

					String lsmDirName = line.getOptionValue("lsm-dir");
					if (!lsmDirName.endsWith(File.separator)) {
						lsmDirName += File.separator;
					}
					lsmDirName += extractorParameters.getLang() + File.separator + "current" + File.separator;
					//String lsaDir = line.getOptionValue("lsm-dir") + File.separator + extractorParameters.getLang() + File.separator + "current";
					logger.debug("lsaDir " + lsmDirName);

					File fileUt = new File(lsmDirName + "X-Ut");
					File fileSk = new File(lsmDirName + "X-S");
					File fileR = new File(lsmDirName + "X-row");
					File fileC = new File(lsmDirName + "X-col");
					File fileDf = new File(lsmDirName + "X-df");
					int dim = WikipediaExtractor.DEFAULT_LSA_DIM;
					if (line.hasOption("lsa-dim")) {
						dim = Integer.parseInt(line.getOptionValue("dim"));
					}
					LSM lsm = new LSM(fileUt, fileSk, fileR, fileC, fileDf, dim, true, normalized);
					int numForms = OneExamplePerSenseExtractor.DEFAULT_NUM_FORMS;
					if (line.hasOption("num-forms")) {
						numForms = Integer.parseInt(line.getOptionValue("num-forms"));
					}
					//extract one example per sense
					logger.info("extracting one example per sense (" + extractorParameters.getOneExamplePerSenseFileName() + ")...");
					OneExamplePerSenseExtractor oneExamplePerSenseExtractor = new OneExamplePerSenseExtractor(lsm, extractorParameters.getOneExamplePerSenseFileName(), numThreads);
					oneExamplePerSenseExtractor.setNormalized(normalized);
					oneExamplePerSenseExtractor.setNotificationPoint(notificationPoint);
					oneExamplePerSenseExtractor.setNumForms(numForms);
					oneExamplePerSenseExtractor.extract(extractorParameters.getWikipediaSortedFormFileName());

					//index one example per sense
					if (delete(extractorParameters.getOneExamplePerSenseIndexName())) {
						logger.warn("overwriting an existing index (" + extractorParameters.getOneExamplePerSenseIndexName() + ")...");
					}
					else {
						logger.info("indexing (" + extractorParameters.getOneExamplePerSenseIndexName() + ")...");
					}
					OneExamplePerSenseIndexer oneExamplePerSenseIndexer = new OneExamplePerSenseIndexer(extractorParameters.getOneExamplePerSenseIndexName());
					oneExamplePerSenseIndexer.index(extractorParameters.getOneExamplePerSenseFileName(), compress);
					oneExamplePerSenseIndexer.close();
				}
				else {
					logger.error("This extractor requires lsm-dir");
				}
			}

			/* LINKS */

			if (line.hasOption("cross-language-dir")) {
				String value = line.getOptionValue("cross-language-dir");
				logger.debug(String.format("Linking cross-language to %s", value));

				Path csvLink = Paths.get(extractorParameters.getWikipediaCrossLanguageLinkFileName());
				Path indexLink = Paths.get(extractorParameters.getWikipediaCrossLanguageLinkIndexName());
				Path targetIndex = FileSystems.getDefault().getPath(value, extractorParameters.getLang());
				Path targetCsv = FileSystems.getDefault().getPath(value, extractorParameters.getLang() + ".csv");
				createSymbolicLink(csvLink, targetCsv);
				createSymbolicLink(indexLink, targetIndex);
			}

			if (line.hasOption("topic-dir")) {
				String value = line.getOptionValue("topic-dir");
				logger.debug(String.format("Linking topics to %s", value));

				Path csvLink = Paths.get(extractorParameters.getWikipediaPageTopicsFileName());
				Path indexLink = Paths.get(extractorParameters.getWikipediaPageTopicsIndexName());
				Path targetCsv = FileSystems.getDefault().getPath(value, extractorParameters.getLang() + ".csv");
				Path targetIndex = FileSystems.getDefault().getPath(value, extractorParameters.getLang());
				createSymbolicLink(csvLink, targetCsv);
				createSymbolicLink(indexLink, targetIndex);
			}

			if (line.hasOption("namnom-dir")) {
				String value = line.getOptionValue("namnom-dir");
				logger.debug(String.format("Linking NAM-NOM to %s", value));

				Path indexLink = Paths.get(extractorParameters.getWikipediaNamNomIndexName());
				Path targetIndex = FileSystems.getDefault().getPath(value, extractorParameters.getLang());
				createSymbolicLink(indexLink, targetIndex);
			}

			if (line.hasOption("airpedia2-dir")) {
				String value = line.getOptionValue("airpedia2-dir");
				logger.debug(String.format("Linking Airpedia2 to %s", value));

				Path indexLink = Paths.get(extractorParameters.getWikipediaAirpedia2IndexName());
				Path targetIndex = FileSystems.getDefault().getPath(value, extractorParameters.getLang());
				createSymbolicLink(indexLink, targetIndex);
			}

			/* UPLOAD */

			if (line.hasOption("remote-server")) {

				String dir = extractorParameters.getExtractionOutputDirName();
				String remoteServer = line.getOptionValue("remote-server");
				logger.info("synchronizing " + dir + " with " + remoteServer + ":" + dir + "...");
				//removed: "page-vector-index"
				Map<String, String> resourceMap = GenericFileUtils.searchForFilesInTheSameFolder(dir, "type-index", "page-form-index", "form-page-index", "ngram-index", "page-freq.csv", "form-freq", "cross-lang-index", "ngram.csv", "unigram", "one-example-per-sense-index", "page-file-source-index", "first-name-index", "person-info-index", "airpedia-class-index", "abstract-index", "page-category-index", "category-super-category-index", "incoming-outgoing-weighted-index", "cross-lang.csv", "page-all-category-index");
				Iterator<String> it = resourceMap.keySet().iterator();
				for (int i = 0; it.hasNext(); i++) {
					String s = it.next();
					String v = resourceMap.get(s);
					logger.debug(i + "\t" + remoteServer + ":" + v);
					int exitValue = UnixRsyncWrapper.rsync(v, remoteServer + ":" + dir);
					if (exitValue != 0) {
						logger.error(v + " cannot be synchronized (error " + exitValue + ")");
					}
				}

			}

			logger.info("extraction ended " + new Date());

		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}

			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar eu.fbk.twm.wiki.ModelExtractor", "\n", options, "\n", true);
		}
	}

}
