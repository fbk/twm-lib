/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump.util;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * This class represents a map between Wikipedia pages and NE labels
 * (e.g., PER, ORG, LOC). The map is read from a filePageCounter with format:
 * <p/>
 * <code>(page \t label \n)+</code>
 * <p/>
 * The filePageCounter is created by the class <code>PageLabelMapBuilder</code>
 * and used by the class <code>WikipediaXmlDumpNEExtractor</code>.
 *
 * @author Claudio Giuliano
 * @version 1.0
 * @see PageLabelMapBuilder
 * @since 1.0
 */
public class PageLabelMap {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>PageLabelMap</code>.
	 */
	static Logger logger = Logger.getLogger(PageLabelMap.class.getName());

	//
	protected Map<String, String> map;

	//
	protected static Pattern tabPattern = Pattern.compile("\t");

	//
	protected static Pattern underlinePattern = Pattern.compile("_");

	//
	public PageLabelMap() throws IOException {
		map = new HashMap<String, String>();
	} // end

	//
	public PageLabelMap(File templateFile) throws IOException {
		long begin = System.currentTimeMillis(), end = 0;
		map = read(templateFile);
		end = System.currentTimeMillis();
		logger.info(map.size() + " page/label lines read in " + (end - begin) + " ms");
	} // end 

	//
	public String getLabel(String template) {
		return map.get(template);
	} // end getName

	public Iterator<String> labels() {
		return map.values().iterator();
	} // labels

	//
	private Map<String, String> read(File file) throws IOException {
		Map<String, String> map = new HashMap<String, String>();
		if (!file.exists()) {
			return map;
		}

		LineNumberReader reader = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));

		String line = null;
		int j = 1;

		// read links
		while ((line = reader.readLine()) != null) {
			String[] s = tabPattern.split(line);

			if (s.length == 2) {
				map.put(s[0], s[1]);
			}


			if ((j % 10000) == 0) {
				System.out.print(".");
			}
			j++;
		} // end while
		reader.close();

		System.out.print("\n");
		//logger.info(j + " pages read");
		return map;
	} // end read

	//
	public int size() {
		return map.size();
	} // end size

	//
	public static void main(String[] args) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		if (args.length == 0) {
			logger.info("java -mx1024M org.fbk.cit.hlt.wm.wiki.xmldump.PageLabelMap people-filePageCounter [page]");
			System.exit(1);
		}

		File templateFile = new File(args[0]);
		PageLabelMap map = new PageLabelMap(templateFile);
		logger.info(map.size());
		if (args.length == 1) {
			Iterator<String> it = map.labels();
			while (it.hasNext()) {
				logger.info(it.next());
			}
		}

		for (int i = 1; i < args.length; i++) {
			String p = map.getLabel(args[i]);
			logger.info(i + "\t" + p);
		}
	} // end main

} // end PageLabelMap
