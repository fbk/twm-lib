/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump.util;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WikiTemplate {
	private int    start;
	private int    end;
	private String content;
	private int    templates;
	ArrayList<String> parts;
	private int keyValueParts = 0;
	String firstPart;

	public  boolean                 isRoot   = false;
	private ArrayList<WikiTemplate> children = new ArrayList<WikiTemplate>();

	public void setChildren(ArrayList<WikiTemplate> children) {
		this.children = children;
	}

	public ArrayList<WikiTemplate> getChildren() {
		return children;
	}

	public WikiTemplate() {
		content = "";
	}

	public WikiTemplate(int a, int b, String c, int d) {
		start = a;
		end = b;
		content = c.replaceAll("<!--.*?-->", "");
		templates = d;

        /*
        // Da sistemare
        Pattern p = Pattern.compile("^(.*?)\\|");
        Matcher m = p.matcher(content);
        if (m.find()) {
            firstPart = m.group(1).trim();
        }
        */
	}

	public HashMap<String, String> getHashMapOfParts() {
		return getHashMapOfParts(false, false);
	}
	public HashMap<String, String> getHashMapOfParts(boolean onlyNum) {
		return getHashMapOfParts(onlyNum, false);
	}

	public HashMap<String, String> getHashMapOfParts(boolean onlyNum, boolean lowerCase) {
		Pattern templatePattern = Pattern.compile("([^\\}\\{=]*)(=?)(.*)");
		HashMap<String, String> ret = new HashMap<String, String>();
		keyValueParts = 0;

		for (String p : parts) {
			p = p.trim();
			p = p.replaceAll("\n", " ");
			Matcher templateMatcher = templatePattern.matcher(p);
			if (templateMatcher.find()) {
				String p1 = templateMatcher.group(1).trim();
				if (lowerCase) {
					p1 = p1.toLowerCase();
				}
				String p2 = templateMatcher.group(2).trim();
				String p3 = templateMatcher.group(3).trim();
				keyValueParts += (p2.length() == 0 ? 0 : 1);
				if (p1.length() > 0 && !onlyNum) {
					ret.put(p1, p3);
				}
				// System.out.println(p1 + " --- " + p2 + " --- " + p3);
			}
		}

		return ret;
	}

	public int getNlCount() {
		return StringUtils.countMatches(content, System.getProperty("line.separator"));
	}

	public void setParts(ArrayList<String> p) {
		parts = (ArrayList<String>) p.clone();
		if (parts.size() > 0) {
			firstPart = parts.get(0).trim();
			int n;
			if ((n = firstPart.indexOf(System.getProperty("line.separator"))) != -1) {
				firstPart = firstPart.substring(0, n - 1).trim();
			}
		}
	}

	public int getKeyValueParts() {
		return keyValueParts;
	}

	public String getFirstPart() {
		return firstPart;
	}

	public ArrayList<String> getParts() {
		return parts;
	}

	public int getPartsCount() {
		return parts.size();
	}

	public void appendContent(String c) {
		content = content + c;
	}

	public void setStart(int s) {
		start = s;
	}

	public void setEnd(int e) {
		end = e;
	}

	public void setContent(String c) {
		content = c;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public String getContent() {
		return content;
	}

	public String toString() {
		// return firstPart;
		return firstPart + " -- Start: " + start + " - End: " + end + " - Templates: " + templates + " - Parts: " + parts.size() + " - isRoot: " + isRoot;
	}
}

