/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.log4j.Logger;

import java.util.Locale;

//todo: check that a form matches [a-z][a-z0-9]*
@Deprecated public abstract class WikipediaDictionaryExtractor extends AbstractWikipediaExtractor implements
		WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaDictionaryExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaDictionaryExtractor.class.getName());

	protected WikipediaDictionaryExtractor(int numThreads, int numPages, Locale locale) {
		super(numThreads, numPages, locale);
	}
	/*
	public static final int FORM_INDEX = 0;

	public static final int PAGE_INDEX = 1;

	public static final int SOURCE_INDEX = 2;

	public static final int TYPE_INDEX = 3;

	public static final int ID_INDEX = 4;

	public static final int LEFT_CONTEXT_INDEX = 5;

	public static final int RIGHT_CONTEXT_INDEX = 6;

	public static final int COLUMN_NUMBER = 7;

	private static AtomicInteger entryCounter = new AtomicInteger();

	private PrintWriter dictionaryWriter;

	private PrintWriter pageFormWriter;

	private PrintWriter formPageWriter;

	private PageMap redirectPageMap;

	private PageSet disambiguationPageSet;

	private PageSet contentPageSet;

	private ReversePageMap reverseRedirectPageMap;

	private PersonInfoMap personInformationMap;

	private WikiMarkupParser wikiMarkupParser;

	private Tokenizer tokenizer;

	private Pattern sectionTitleSkipPattern;

	private MultiMap formPageMap;

	private MultiMap pageFormMap;

	private int maximumNumberOfEntry;

	public WikipediaDictionaryExtractor(int numThreads, int numPages, Locale locale) throws IOException {
		super(numThreads, numPages, locale);

		tokenizer = HardTokenizer.getInstance();
		wikiMarkupParser = WikiMarkupParser.getInstance();
		maximumNumberOfEntry = DEFAULT_MAXIMUM_FORM_FREQ;
	}

	public int getMaximumNumberOfEntry() {
		return maximumNumberOfEntry;
	}

	public void setMaximumNumberOfEntry(int maximumNumberOfEntry) {
		this.maximumNumberOfEntry = maximumNumberOfEntry;
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		// String redirectFile, String disambiguationFile, String titleFile, String peopleFile
		try {
			redirectPageMap = new PageMap(new File(extractorParameters.getWikipediaRedirFileName()));
			logger.info(redirectPageMap.size() + " redirect pages");

			reverseRedirectPageMap = new ReversePageMap(new File(extractorParameters.getWikipediaRedirFileName()));
			logger.info(reverseRedirectPageMap.size() + " reverse redirect pages");

			disambiguationPageSet = new PageSet(new File(extractorParameters.getWikipediaDisambiguationFileName()));
			logger.info(disambiguationPageSet.size() + " disambiguation pages");

			contentPageSet = new PageSet(new File(extractorParameters.getWikipediaContentPageFileName()));
			logger.info(contentPageSet.size() + " content pages");

			personInformationMap = new PersonInfoMap(new File(extractorParameters.getWikipediaPersonInfoFileName()));
			logger.info(personInformationMap.size() + " person information");

			logger.info("form/freq file: " + extractorParameters.getWikipediaFormFreqFileName());
			//formPageWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaFormFreqFileName()), "UTF-8")));

			logger.info("page/freq file: " + extractorParameters.getWikipediaPageFreqFileName());
			//pageFormWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaPageFreqFileName()), "UTF-8")));

			pageFormWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaPageFreqFileName()), "UTF-8")));

			formPageMap = new MultiMap();
			pageFormMap = new MultiMap();

		} catch (IOException e) {
			logger.error(e);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		contentPage(text, title, wikiID);
	}

	class MultiMap {
		private Map<String, FreqSet> map;

		public MultiMap() {
			map = new HashMap<String, FreqSet>();
		}

		public synchronized void put(String key, String value) {
			FreqSet freqSet = map.get(key);
			if (freqSet == null) {
				freqSet = new FreqSet();
				map.put(key, freqSet);
			}
			freqSet.add(key);
		}

		public synchronized FreqSet get(String key) {
			return map.get(key);
		}
	}

	@Override
	public void printLog() {
		if (printHeader) {
			logger.info("total\tcontent\tredirect\tdisambiguation\tcategory\tpage\tform\ttime\tdate");
			printHeader = false;
		}
		logger.info(decimalFormat.format(generalCount.intValue()) + "\t" + decimalFormat.format(countPageCounter) + "\t" + decimalFormat.format(redirectPageCounter) + "\t" + decimalFormat.format(disambiguationPageCounter) + "\t" + decimalFormat.format(categoryPageCounter) + "\t" + decimalFormat.format(pageFormMap.size()) + "\t" + "\t" + decimalFormat.format(formPageMap.size()) + "\t" + decimalFormat.format(genEnd.longValue() - genBegin.longValue()) + "\t" + new Date());
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {
		try {
			ParsedPage parsedPage = wikiMarkupParser.parsePage(text);
			ParsedPageTitle parsedPageTitle = new ParsedPageTitle(title);
			addLinkEntry(parsedPage, title);
			addPageEntry(parsedPageTitle);
			addPersonSurnameEntry(title);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void addLinkEntry(ParsedPage parsedPage, String title) {
		String redirectPage;
		List<Link> internalLinks;
		ParsedPageLink parsedPageLink;
		for (Section section : parsedPage.getSections()) {
			internalLinks = section.getLinks(Link.type.INTERNAL);
			for (Link link : internalLinks) {
				try {
					parsedPageLink = new ParsedPageLink(link);
					if (parsedPageLink.isCompliant()) {
						redirectPage = redirectPageMap.get(parsedPageLink.getPage());
						//todo: check multiple redirects
						if (redirectPage != null) {
							parsedPageLink.setPage(redirectPage);
						}
						if (contentPageSet.contains(parsedPageLink.getPage()) && !disambiguationPageSet.contains(parsedPageLink.getPage())) {
							formPageMap.put(parsedPageLink.getForm(), parsedPageLink.getPage());
							pageFormMap.put(parsedPageLink.getPage(), parsedPageLink.getForm());
						}
					}
				} catch (Exception ex) {
					logger.error("Exception adding link entries for page " + title + " (" + entryCounter.intValue() + ")\n" + ex);
				}
			}
		}
	}

	private void addPageEntry(ParsedPageTitle parsedPageTitle) {
		try {
			if (!disambiguationPageSet.contains(parsedPageTitle.getPage())) {
				formPageMap.put(parsedPageTitle.getForm(), parsedPageTitle.getPage());
				pageFormMap.put(parsedPageTitle.getPage(), parsedPageTitle.getForm());
			}
		} catch (Exception ex) {
			logger.error("Exception adding page entry for page " + parsedPageTitle.getPage() + " (" + entryCounter.intValue() + ")\n" + ex);
		}
	}

	private void addPersonSurnameEntry(String title) {
		PersonInfoMap.Person person = personInformationMap.get(title);
		if (person != null) {

			try {
				String surname = person.getSurname();
				if (surname.length() > 0) {
					formPageMap.put(surname, title);
					pageFormMap.put(title, surname);

				}
			} catch (Exception ex) {
				logger.error("Exception adding person info entries (" + entryCounter.intValue() + ")\n" + ex);
			}
		}
	}


	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void endProcess() {
		super.endProcess();
		try {
			logger.info("writing " + formPageMap.size() + " forms...");
			writeSectionTitleFrequency();
			formPageWriter.close();
		} catch (IOException e) {
			logger.error(e);
		}
		try {
			logger.info("writing " + pageFormMap.size() + " forms...");
			pageFormMap.write(pageFormWriter);
			pageFormWriter.close();
		} catch (IOException e) {
			logger.error(e);
		}

		logger.debug("closing the output stream...");
		dictionaryWriter.close();
	}

	public void writeSectionTitleFrequency() throws IOException {
		SortedMap<AtomicInteger, List<String>> sortedMap = formPageMap.getSortedMap();
		Iterator<AtomicInteger> it = sortedMap.keySet().iterator();
		AtomicInteger i;
		for (; it.hasNext(); ) {
			i = it.next();
			List<String> list = sortedMap.get(i);
			for (int j = 0; j < list.size(); j++) {
				formPageWriter.print(i.toString());
				formPageWriter.print(CharacterTable.HORIZONTAL_TABULATION);
				formPageWriter.println(tokenizer.tokenizedString(list.get(j).toString()));
			}
		}
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			Option maximumFormFreqOpt = OptionBuilder.withArgName("max-freq").hasArg().withDescription("maximum frequency of wanted forms (default is " + WikipediaExtractor.DEFAULT_MAXIMUM_FORM_FREQ + ")").withLongOpt("max-freq").create("m");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");


			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(notificationPointOpt);
			options.addOption(maximumFormFreqOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);


			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			logger.debug(extractorParameters);
			int maximumFormFreq = WikipediaDictionaryExtractor.DEFAULT_MAXIMUM_FORM_FREQ;
			if (line.hasOption("max-freq")) {
				maximumFormFreq = Integer.parseInt(line.getOptionValue("max-freq"));
			}
			logger.debug("filtering entries with frequency higher than " + maximumFormFreq + "...");

			logger.debug("extracting entries (" + extractorParameters.getWikipediaEntryFileName() + ")...");
			WikipediaDictionaryExtractor wikipediaExtractor = new WikipediaDictionaryExtractor(numThreads, numPages, extractorParameters.getLocale());
			wikipediaExtractor.setNotificationPoint(notificationPoint);
			wikipediaExtractor.setMaximumNumberOfEntry(maximumFormFreq);
			wikipediaExtractor.start(extractorParameters);

			logger.info("extraction ended " + new Date());

		} catch (ParseException e) {
			// oops, something went wrong
			System.out.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaDictionaryExtractor", "\n", options, "\n", true);
		}
	} */
}