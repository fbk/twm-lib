/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import de.tudarmstadt.ukp.wikipedia.parser.Content;
import de.tudarmstadt.ukp.wikipedia.parser.ParsedPage;
import de.tudarmstadt.ukp.wikipedia.parser.Section;
import eu.fbk.twm.utils.*;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import eu.fbk.twm.wiki.xmldump.util.PageTypeExtractor;
import eu.fbk.twm.wiki.xmldump.util.ReversePageMap;
import eu.fbk.twm.wiki.xmldump.util.WikiMarkupParser;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.*;

public class WikipediaTextExtractor extends AbstractWikipediaExtractor implements WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaTextExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaTextExtractor.class.getName());

	private PrintWriter textWriter;

	private ReversePageMap reverseRedirectPageMap;

	//private PageMap redirectPageMap;

	public WikipediaTextExtractor(int numThreads, int numPages, Locale locale) {
		super(numThreads, numPages, locale);

	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			//redirectPageMap = new PageMap(new File(extractorParameters.getWikipediaRedirFileName()));
			//logger.info(redirectPageMap.size() + " redirect pages");

			reverseRedirectPageMap = new ReversePageMap(new File(extractorParameters.getWikipediaRedirFileName()));
			logger.info(reverseRedirectPageMap.size() + " reverse redirect pages");

			textWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaTextFileName()), "UTF-8")));
		} catch (IOException e) {
			logger.error(e);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {

		try {
			WikiMarkupParser wikiMarkupParser = WikiMarkupParser.getInstance();
			String[] prefixes = {imagePrefix, filePrefix};
			ParsedPage parsedPage = wikiMarkupParser.parsePage(text, prefixes);

			ParsedPageTitle parsedPageTitle = new ParsedPageTitle(title);
			PageTypeExtractor pageTypeExtractor = new PageTypeExtractor(text, parsedPageTitle.getForm());
			boolean nominal = pageTypeExtractor.isNominal();

			String page = tokenizedText(parsedPage, title);
			String redirect = tokenizedRedirectForms(title, nominal);
			synchronized (this) {
				textWriter.print(page);
				textWriter.println(redirect);
			}
		} catch (Exception e) {
			logger.error("Error processing page " + title + " (" + wikiID + ")");
		}
	}

	private String tokenizedRedirectForms(String title, boolean nominal) {
		StringBuilder sb = new StringBuilder();
		Set<String> redirectSet = reverseRedirectPageMap.get(title);
		if (redirectSet == null) {
			return sb.toString();
		}

		Tokenizer tokenizer = HardTokenizer.getInstance();
		Iterator<String> it = redirectSet.iterator();
		for (int i = 0; it.hasNext(); i++) {
			sb.append(CharacterTable.SPACE);

			String redirectPage = it.next();
			ParsedPageTitle parsedRedirectPage = new ParsedPageTitle(redirectPage);
			String tokenizedParsedRedirectPage = tokenizer.tokenizedString(parsedRedirectPage.getForm());
			if (nominal) {
				sb.append(tokenizedParsedRedirectPage.toLowerCase());
				sb.append(CharacterTable.SPACE);
			}
			sb.append(tokenizedParsedRedirectPage);
		}

		return sb.toString();
	}

	/**
	 * Returns the whole content of the page tokenized in a single line.
	 * The first token is the page title (with underscores)
	 */
	private String tokenizedText(ParsedPage parsedPage, String title) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(title);
		sb.append(CharacterTable.SPACE);
		Tokenizer tokenizer = HardTokenizer.getInstance();
		String tokenizedTitle = tokenizer.tokenizedString(title.replace(CharacterTable.LOW_LINE, CharacterTable.SPACE));
		sb.append(tokenizedTitle);

		String rawContent;
		String tokenizedContent;
		List<Content> list;
		for (Section section : parsedPage.getSections()) {
			list = section.getContentList();
			for (int i = 0; i < list.size(); i++) {
				rawContent = list.get(i).getText();
				if (rawContent.length() > 0) {
					tokenizedContent = tokenizer.tokenizedString(rawContent);
					if (tokenizedContent.length() > 0) {
						sb.append(CharacterTable.SPACE);
						sb.append(tokenizedContent);
					}

				}
			}
		}
		return sb.toString();
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	public void endProcess() {
		super.endProcess();
		textWriter.flush();
		textWriter.close();
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER
                    + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT
                    + ")").withLongOpt("notification-point").create("b");

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");


			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(notificationPointOpt);
			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);
			logger.debug(line);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			WikipediaExtractor wikipediaPageParser = new WikipediaTextExtractor(numThreads, numPages, extractorParameters.getLocale());
			wikipediaPageParser.setNotificationPoint(notificationPoint);
			wikipediaPageParser.start(extractorParameters);

			logger.info("extraction ended " + new Date());

		} catch (ParseException e) {
			// oops, something went wrong
			System.out.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaTextExtractor", "\n", options, "\n", true);
		}
	}

}