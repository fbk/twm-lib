/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.WikipediaExtractor;
import eu.fbk.twm.wiki.xmldump.util.WikiTemplate;
import eu.fbk.twm.wiki.xmldump.util.WikiTemplateParser;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;


/**
 * User: aprosio
 * This class extracts the list of pairs page/navigation template
 *
 * @see WikipediaNavigationTemplateExtractor
 */
public class WikipediaPageNavigationTemplateExtractor extends AbstractWikipediaExtractor implements WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaTemplateExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaPageNavigationTemplateExtractor.class.getName());

	private PrintWriter pageNavigationTemplateWriter;
	private HashSet<String> navigationTemplate = new HashSet<String>();
	private String navTpl;

	public WikipediaPageNavigationTemplateExtractor(int numThreads, int numPages, Locale locale) {
		super(numThreads, numPages, locale);
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		// String prefix = extractorParameters.getWikipediaTemplateFilePrefixName();
		try {
			navTpl = resources.getString("NAVBOX_PTEMPLATE");
			if (navTpl != null) {
				logger.info("NAVBOX_PTEMPLATE is " + navTpl);
			}
			pageNavigationTemplateWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaPageNavigationTemplateFileName()), "UTF-8")));
			BufferedReader reader = new BufferedReader(new FileReader(extractorParameters.getWikipediaTemplateFileNames().get("navigation")));
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0) {
					continue;
				}
				navigationTemplate.add(line.toLowerCase());
			}
		} catch (IOException e) {
			logger.error(e);
		}

		startProcess(extractorParameters.getWikipediaXmlFileName());

	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}


	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {
		ArrayList<WikiTemplate> listOfTemplates = WikiTemplateParser.parse(text, false);

		for (WikiTemplate t : listOfTemplates) {

			String firstPart = t.getFirstPart();
			if (firstPart == null) {
				continue;
			}

			// Template complete e.g. http://fr.wikipedia.org/wiki/Mod%C3%A8le:Palette_Therion
			if (navigationTemplate.contains(firstPart.toLowerCase())) {
				StringBuilder buff = new StringBuilder();
				buff.append(title);
				buff.append(CharacterTable.HORIZONTAL_TABULATION);
				buff.append(normalizePageName(firstPart.replace(CharacterTable.SPACE, CharacterTable.LOW_LINE)));
				buff.append(CharacterTable.LINE_FEED);

				synchronized (this) {
					pageNavigationTemplateWriter.append(buff);
				}
			}

			// Template with '|' e.g. http://fr.wikipedia.org/wiki/Mod%C3%A8le:Palette
			if (navTpl == null) {
				continue;
			}

			if (firstPart.toLowerCase().equals(navTpl.toLowerCase())) {
				Set<String> parts = t.getHashMapOfParts().keySet();
				for (String p : parts) {
					if (p.toLowerCase().equals(navTpl.toLowerCase())) {
						continue;
					}

					StringBuilder buff = new StringBuilder();
					buff.append(title);
					buff.append(CharacterTable.HORIZONTAL_TABULATION);
					buff.append(navTpl);
					buff.append(CharacterTable.LOW_LINE);
					buff.append(normalizePageName(p.replace(CharacterTable.SPACE, CharacterTable.LOW_LINE)));
					buff.append(CharacterTable.LINE_FEED);

					synchronized (this) {
						pageNavigationTemplateWriter.append(buff);
					}
				}
			}

		}
	}

	@Override
	public void endProcess() {
		super.endProcess();
		pageNavigationTemplateWriter.flush();
		pageNavigationTemplateWriter.close();
	}

	public static void main(String[] args) {
		String xmlFileName = args[0];
		String baseDir = args[1];

		String configurationFolder = "configuration/";
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = configurationFolder + "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		ExtractorParameters extractorParameters = new ExtractorParameters(xmlFileName, baseDir, true);
		WikipediaPageNavigationTemplateExtractor w = new WikipediaPageNavigationTemplateExtractor(12, Integer.MAX_VALUE, extractorParameters.getLocale());
		w.start(extractorParameters);
	}

}