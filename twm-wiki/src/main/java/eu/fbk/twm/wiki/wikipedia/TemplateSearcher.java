/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.wikipedia;

import eu.fbk.twm.index.PageFormIndexer;
import eu.fbk.twm.index.util.SimpleStringSearcher;
import eu.fbk.twm.utils.Defaults;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 1/22/13
 * Time: 6:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class TemplateSearcher extends SimpleStringSearcher {
	static Logger logger = Logger.getLogger(TemplateSearcher.class.getName());

	public TemplateSearcher(String indexName) throws IOException {
		super(indexName, PageFormIndexer.PAGE_FIELD_NAME, PageFormIndexer.ENTRY_FIELD_NAME);
//		logger.trace(toString(10));
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = OptionBuilder.withArgName("index").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").create("i");
			Option interactiveModeOpt = OptionBuilder.withArgName("interactive-mode").withDescription("enter in the interactive mode").withLongOpt("interactive-mode").create("t");
			Option searchOpt = OptionBuilder.withArgName("search").hasArg().withDescription("search for the specified key").withLongOpt("search").create("s");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default is " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("b");
			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(searchOpt);
			options.addOption(notificationPointOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			TemplateSearcher pageFormSearcher = new TemplateSearcher(line.getOptionValue("index"));
			pageFormSearcher.setNotificationPoint(notificationPoint);
			pageFormSearcher.setKeyFieldName("page");
			pageFormSearcher.setValueFieldName("xml");

			if (line.hasOption("search")) {
				logger.debug("searching " + line.getOptionValue("search") + "...");
				String result = pageFormSearcher.search(line.getOptionValue("search"));
				System.out.println(result);
			}
			if (line.hasOption("interactive-mode")) {
				pageFormSearcher.interactive();
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.airpedia.PagesSearcher", "\n", options, "\n", true);
		}
	}

}
