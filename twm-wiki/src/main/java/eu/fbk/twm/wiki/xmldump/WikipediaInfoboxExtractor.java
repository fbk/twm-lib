/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.regex.Matcher;

public class WikipediaInfoboxExtractor extends AbstractWikipediaExtractor implements WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaInfoboxExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaInfoboxExtractor.class.getName());

	private PrintWriter infoBoxWriter;
	private boolean     shortenTemplate;

	private HashSet<String> infoboxTemplates = new HashSet<String>();
	private HashSet<String> infoboxes        = new HashSet<String>();
	private String inCS;

	public WikipediaInfoboxExtractor(int numThreads, int numPages, Locale locale, String inCS, boolean shortenTemplate) {
		super(numThreads, numPages, locale);
		this.inCS = inCS;
		this.shortenTemplate = shortenTemplate;

	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		HashMap<String, HashSet<String>> sons = new HashMap<String, HashSet<String>>();
		try {
			logger.info("reading " + inCS + "...");
			BufferedReader br = new BufferedReader(new FileReader(inCS));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parts = line.split("\\t");
				if (parts.length < 2) {
					continue;
				}

				HashSet<String> content;
				if ((content = sons.get(parts[1])) == null) {
					content = new HashSet<String>();
					sons.put(parts[1], content);
				}
				content.add(parts[0]);
			}

			goToDeepCategory(sons, infoboxTemplates, infoboxRootCategory);

			infoBoxWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaInfoboxFileName()), "UTF-8")));
		} catch (IOException e) {
			logger.error(e);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	private void goToDeepCategory(HashMap<String, HashSet<String>> s, HashSet<String> i, String thisLabel) {
		i.add(thisLabel.toLowerCase());
		if (s.get(thisLabel) == null) {
			return;
		}
		for (String son : s.get(thisLabel)) {
			goToDeepCategory(s, i, son);
		}
	}

	void init(String out) throws IOException {
		HashMap<String, HashSet<String>> sons = new HashMap<String, HashSet<String>>();

		logger.info("reading " + inCS + "...");
		BufferedReader in = new BufferedReader(new FileReader(inCS));
		String line;
		while ((line = in.readLine()) != null) {
			String[] parts = line.split("\\t");
			if (parts.length < 2) {
				continue;
			}

			HashSet<String> content;
			if ((content = sons.get(parts[1])) == null) {
				content = new HashSet<String>();
				sons.put(parts[1], content);
			}
			content.add(parts[0]);
		}

		goToDeepCategory(sons, infoboxTemplates, infoboxRootCategory);

		infoBoxWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(out), "UTF-8")));

	}

	@Override
	public void contentPage(String text, String title, int wikiID) {
		Matcher mt = simpleTemplatePattern.matcher(title);
		if (mt.find()) {
			title = mt.group(1).toLowerCase();
			Matcher m = categoryPattern.matcher(text);

			while (m.find()) {
				int index = 2;
				int s = m.start(index);
				int e = m.end(index);

				String category = text.substring(s, e).replace(' ', '_');
				int j = category.indexOf("|");
				if (j != -1) {
					category = category.substring(0, j);
				}

				if (!infoboxTemplates.contains(category.toLowerCase())) {
					continue;
				}

				if (!infoboxes.contains(title)) {
					synchronized (this) {
						infoBoxWriter.print(title + "\n");
						infoboxes.add(title);
					}
				}
				break;
			}
		}
	}

	@Override
	public void endProcess() {
		super.endProcess();
		infoBoxWriter.flush();
		infoBoxWriter.close();

	}

	/*public static void main(String argv[]) throws IOException
	{
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) logConfig = "configuration/log-config.txt";

		PropertyConfigurator.configure(logConfig);

		if (argv.length < 3) {
			System.out.println("");
			System.out.println("USAGE:");
			System.out.println("");
			System.out.println("java -mx6G org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaInfoboxExtractor\n" +
							" in-wiki-xml -- Input file\n" +
							" in-cat-subcat -- Input Category-Subcategory file\n" +
							" root-template-files -- Output template file\n" +
							" locale -- Locale (used for resources)\n" +
							" [threads=1] -- Number of threads to use\n" +
							" [shorten-template=false] -- Shorten template name");
			System.out.println("");
			System.exit(1);
		}

		int parID = 0;

		String xin = argv[parID++];
		String inCatSubcat = argv[parID++];
		String xout = argv[parID++];
		Locale locale = new Locale(argv[parID++]);

		int threads = 1;
		if (argv.length > parID) {
			threads = Integer.parseInt(argv[parID++]);
		}

		boolean shortenTemplate = false;
		if (argv.length > parID) {
			shortenTemplate = Boolean.parseBoolean(argv[parID++]);
		}

		int size = 0;
		if (argv.length > parID) {
			size = Integer.parseInt(argv[parID++]);
		}
		WikipediaInfoboxExtractor infoBoxWriter = new WikipediaInfoboxExtractor(threads, size, locale, inCatSubcat, shortenTemplate);
		infoBoxWriter.start(xin, xout);
	} */

}