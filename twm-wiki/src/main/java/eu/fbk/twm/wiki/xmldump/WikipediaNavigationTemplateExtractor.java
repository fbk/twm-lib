/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import eu.fbk.twm.utils.CharacterTable;
import eu.fbk.twm.utils.ExtractorParameters;
import eu.fbk.twm.utils.WikipediaExtractor;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.Locale;
import java.util.regex.Matcher;

/**
 * User: aprosio
 * This class extracts the list of navigation template
 *
 * @see WikipediaPageNavigationTemplateExtractor
 */
public class WikipediaNavigationTemplateExtractor extends AbstractWikipediaExtractor implements WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaTemplateExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaNavigationTemplateExtractor.class.getName());

	private PrintWriter templateNavigationWriter;

	public WikipediaNavigationTemplateExtractor(int numThreads, int numPages, Locale locale) {
		super(numThreads, numPages, locale);
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		// String prefix = extractorParameters.getWikipediaTemplateFilePrefixName();

		if (navigationTemplatePattern == null) {
			logger.error("Invalid navigation template pattern");
			System.exit(1);
		}
		try {
			templateNavigationWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaTemplateFileNames().get("navigation")), "UTF-8")));
		} catch (IOException e) {
			logger.error(e.getMessage());
			System.exit(1);
		}
		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}


	@Override
	public void templatePage(String text, String title, int wikiID) {

		Matcher m;

		// Extract categories
		//todo: make uppercase the first letter
		m = categoryPattern.matcher(text);
		int index = 2;
		while (m.find()) {
			Matcher m2;
			int s = m.start(index);
			int e = m.end(index);
			String category = text.substring(s, e).replace(CharacterTable.SPACE, CharacterTable.LOW_LINE);

			if (navigationTemplatePattern != null) {
				m2 = navigationTemplatePattern.matcher(category);
				if (m2.find()) {
					if (simpleTemplatePattern != null) {
						m2 = simpleTemplatePattern.matcher(title);
						if (m2.find()) {
							String simpleTemplate = m2.group(1);
							synchronized (this) {
								templateNavigationWriter.append(simpleTemplate);
								templateNavigationWriter.append(CharacterTable.LINE_FEED);
							}
						}
					}
				}
			}
		}

	}

	@Override
	public void contentPage(String text, String title, int wikiID) {
//		ArrayList<WikiTemplate> listOfTemplates = WikiTemplateParser.parse(text, false);
//
//		for (WikiTemplate t : listOfTemplates) {
//
//			//todo: Write it into properties file: this works only for IT!
//			String navigationCategory = "Template_di_navigazione";
//			if (t.getHashMapOfParts().size() > 1) {
//				continue;
//			}
//			String firstPart = t.getFirstPart();
//
//			synchronized (this) {
//				System.out.println(firstPart);
//			}
//		}
	}

	@Override
	public void endProcess() {
		super.endProcess();
		templateNavigationWriter.flush();
		templateNavigationWriter.close();
	}

	public static void main(String[] args) {
		String xmlFileName = args[0];
		String baseDir = args[1];

		String configurationFolder = "configuration/";
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = configurationFolder + "log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		ExtractorParameters extractorParameters = new ExtractorParameters(xmlFileName, baseDir, true);
		logger.debug("Locale: " + extractorParameters.getLocale());
		WikipediaNavigationTemplateExtractor w = new WikipediaNavigationTemplateExtractor(12, Integer.MAX_VALUE, extractorParameters.getLocale());
		w.start(extractorParameters);
	}

}