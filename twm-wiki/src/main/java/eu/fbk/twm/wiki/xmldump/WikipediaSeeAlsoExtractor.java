/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki.xmldump;

import de.tudarmstadt.ukp.wikipedia.parser.Link;
import de.tudarmstadt.ukp.wikipedia.parser.ParsedPage;
import de.tudarmstadt.ukp.wikipedia.parser.Section;
import eu.fbk.twm.utils.*;
import eu.fbk.twm.wiki.xmldump.util.WikiMarkupParser;
import org.apache.commons.cli.*;
import org.apache.commons.cli.OptionBuilder;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/21/13
 * Time: 2:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class WikipediaSeeAlsoExtractor extends AbstractWikipediaExtractor implements WikipediaExtractor {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>WikipediaSeeAlsoExtractor</code>.
	 */
	static Logger logger = Logger.getLogger(WikipediaSeeAlsoExtractor.class.getName());

	private PrintWriter seeAlsoWriter;

	private Pattern seeAlsoPattern;

	private Pattern sectionPattern;

	private PageMap redirectPagesMap;

	private PageSet disambiguationPagesSet;

	public final static String NUM_SIGN = "#";

	public WikipediaSeeAlsoExtractor(int numThreads, int numPages, Locale locale) {
		super(numThreads, numPages, locale);
		if (resources.getString("SEE_ALSO_LABEL") != null) {
			seeAlsoPattern = Pattern.compile("==\\s*" + resources.getString("SEE_ALSO_LABEL") + "\\s*==", Pattern.CASE_INSENSITIVE);
			sectionPattern = Pattern.compile("== ?[^=]+ ?==", Pattern.CASE_INSENSITIVE);
		}
		logger.debug("SEE_ALSO_LABEL: " + seeAlsoPattern);
	}

	@Override
	public void start(ExtractorParameters extractorParameters) {
		try {
			redirectPagesMap = new PageMap(new File(extractorParameters.getWikipediaRedirFileName()));
			logger.info(redirectPagesMap.size() + " redirect pages");
			disambiguationPagesSet = new PageSet(new File(extractorParameters.getWikipediaDisambiguationFileName()));
			logger.info(disambiguationPagesSet.size() + " disambiguation pages");

			seeAlsoWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(extractorParameters.getWikipediaSeeAlsoFileName()), "UTF-8")));
		} catch (IOException e) {
			logger.error(e);
		}

		startProcess(extractorParameters.getWikipediaXmlFileName());
	}

	@Override
	public void filePage(String text, String title, int wikiID) {
		//logger.debug(title);
	}

	@Override
	public void disambiguationPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void categoryPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void templatePage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void redirectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void portalPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void projectPage(String text, String title, int wikiID) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	/**
	 * Returns the outgoing links. From this...
	 * links contained within templates are missing?
	 *
	 * @see WikipediaOutgoingLinkExtractor
	 */
	private String outgoingInternalLinks(String text, String title) throws IOException {
		StringBuilder sb = new StringBuilder();

		WikiMarkupParser wikiMarkupParser = WikiMarkupParser.getInstance();
		ParsedPage pp = wikiMarkupParser.parsePage(text);
		String target;
		String redirect;
		if (pp != null) {
			List<Section> sections = pp.getSections();
			if (sections != null) {
				for (Section section : sections) {
					List<Link> internalLinks = section.getLinks(Link.type.INTERNAL);
					for (Link link : internalLinks) {
						target = normalizePageName(link.getTarget());
						redirect = redirectPagesMap.get(target);
						if (redirect != null) {
							target = redirect;
						}
						else if (target.startsWith(NUM_SIGN)) {
						}
						else if (disambiguationPagesSet.contains(target)) {
						}
						else {
							// outgoing
							sb.append(title);
							sb.append(CharacterTable.HORIZONTAL_TABULATION);
							sb.append(target);
							sb.append(CharacterTable.LINE_FEED);
						}
					}
				}
			}
		}
		return sb.toString();
	}

	@Override
	public void contentPage(String text, String title, int wikiID) {
		//logger.debug(title + "\t" + text.length());

		Matcher matcher1 = seeAlsoPattern.matcher(text);
		if (matcher1.find()) {
			int start1 = matcher1.start();
			int end1 = matcher1.end();
			//logger.debug(text.substring(start1, end1));

			String rest = text.substring(end1, text.length());
			//logger.debug(rest);
			Matcher matcher2 = sectionPattern.matcher(rest);
			//logger.debug(matcher2);
			int start2 = rest.length();
			if (matcher2.find()) {
				start2 = matcher2.start();
			}
			//logger.debug(start2);
			//logger.debug(rest.substring(0, start2));
			String seeAlsoText = rest.substring(0, start2);
			//remove templates tags
			seeAlsoText = seeAlsoText.replace("{{", "");
			seeAlsoText = seeAlsoText.replace("}}", "");
			try {
				String s = outgoingInternalLinks(seeAlsoText, title);
				//s += title + "\t" + seeAlsoText.replace('\n', ' ') + "\n";
				synchronized (this) {
					seeAlsoWriter.print(s);
				}
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}


	@Override
	public void endProcess() {
		super.endProcess();
		seeAlsoWriter.close();
	}

	public static void main(String args[]) throws IOException {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);

		Options options = new Options();
		try {
			Option wikipediaDumpOpt = OptionBuilder.withArgName("file").hasArg().withDescription("wikipedia xml dump file").isRequired().withLongOpt("wikipedia-dump").create("d");
			Option outputDirOpt = OptionBuilder.withArgName("dir").hasArg().withDescription("output directory in which to store output files").isRequired().withLongOpt("output-dir").create("o");
			Option numThreadOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of threads (default " + Defaults.DEFAULT_THREADS_NUMBER + ")").withLongOpt("num-threads").create("t");
			Option numPageOpt = OptionBuilder.withArgName("int").hasArg().withDescription("number of pages to process (default all)").withLongOpt("num-pages").create("p");
			Option notificationPointOpt = OptionBuilder.withArgName("int").hasArg().withDescription("receive notification every n pages (default " + Defaults.DEFAULT_NOTIFICATION_POINT + ")").withLongOpt("notification-point").create("n");
			Option baseDirOpt = OptionBuilder.withDescription("if set, use the output folder as base dir").withLongOpt("base-dir").create();

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");


			options.addOption(wikipediaDumpOpt);
			options.addOption(outputDirOpt);
			options.addOption(numThreadOpt);
			options.addOption(numPageOpt);
			options.addOption(notificationPointOpt);
			options.addOption(baseDirOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);


			int numThreads = Defaults.DEFAULT_THREADS_NUMBER;
			if (line.hasOption("num-threads")) {
				numThreads = Integer.parseInt(line.getOptionValue("num-threads"));
			}

			int numPages = Defaults.DEFAULT_NUM_PAGES;
			if (line.hasOption("num-pages")) {
				numPages = Integer.parseInt(line.getOptionValue("num-pages"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			//ExtractorParameters extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			ExtractorParameters extractorParameters;
			if (line.hasOption("base-dir")) {
				extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"), true);
			}
			else {
				extractorParameters = new ExtractorParameters(line.getOptionValue("wikipedia-dump"), line.getOptionValue("output-dir"));
			}
			File dest = new File(extractorParameters.getExtractionOutputDirName());

			if (dest.mkdirs()) {
				logger.info(dest + " created");
			}
			logger.debug(extractorParameters);

			logger.debug("extracting see also links (" + extractorParameters.getWikipediaSeeAlsoFileName() + ")...");
			WikipediaExtractor wikipediaExtractor = new WikipediaSeeAlsoExtractor(numThreads, numPages, extractorParameters.getLocale());
			wikipediaExtractor.setNotificationPoint(notificationPoint);
			wikipediaExtractor.start(extractorParameters);

			logger.info("extraction ended " + new Date());

		} catch (ParseException e) {
			// oops, something went wrong
			System.out.println("Parsing failed: " + e.getMessage() + "\n");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaSeeAlsoExtractor", "\n", options, "\n", true);
		}
	}
}