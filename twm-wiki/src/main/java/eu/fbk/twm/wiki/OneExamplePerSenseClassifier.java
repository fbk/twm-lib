/*
 * Copyright (2013) Fondazione Bruno Kessler (http://www.fbk.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.fbk.twm.wiki;

import eu.fbk.twm.index.OneExamplePerSenseSearcher;
import eu.fbk.twm.utils.Defaults;
import eu.fbk.twm.utils.OptionBuilder;
import eu.fbk.twm.utils.StringTable;
import eu.fbk.twm.utils.WikipediaExtractor;
import eu.fbk.twm.utils.analysis.HardTokenizer;
import eu.fbk.twm.utils.analysis.Tokenizer;
import eu.fbk.utils.lsa.BOW;
import eu.fbk.utils.lsa.LSM;
import eu.fbk.utils.math.Node;
import eu.fbk.utils.math.Vector;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xerial.snappy.SnappyInputStream;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: giuliano
 * Date: 2/6/13
 * Time: 11:52 AM
 * To change this template use File | Settings | File Templates.
 * @deprecated
 */
@Deprecated public class OneExamplePerSenseClassifier {
	/**
	 * Define a static logger variable so that it references the
	 * Logger instance named <code>OneExamplePerSenseClassifier</code>.
	 */
	static Logger logger = Logger.getLogger(OneExamplePerSenseClassifier.class.getName());

	LSM lsm;

	OneExamplePerSenseSearcher oneExamplePerSenseSearcher;

	private boolean normalized;

	protected static DecimalFormat rf = new DecimalFormat("###,###,##0.000000");

	private static Pattern tabPattern = Pattern.compile(StringTable.HORIZONTAL_TABULATION);

	protected static DecimalFormat df = new DecimalFormat("###,###,###,###");

	protected static DecimalFormat tf = new DecimalFormat("000,000,000.#");


	public OneExamplePerSenseClassifier(LSM lsm, OneExamplePerSenseSearcher oneExamplePerSenseSearcher, boolean normalized) {
		this.lsm = lsm;
		this.oneExamplePerSenseSearcher = oneExamplePerSenseSearcher;
		this.normalized = normalized;
	}

	public class Sense {
		private double bow;

		private double ls;

		private double prior;

		private double combo;

		public String page;

		Sense(String page, double bow, double ls, double prior) {
			this.page = page;
			this.bow = bow;
			this.ls = ls;
			this.prior = prior;
			//combo = (bow + ls) / 2;
			combo = bow;
		}

		public String getPage() {
			return page;
		}

		public double getCombo() {
			return combo;
		}

		public double getBow() {
			return bow;
		}

		public double getLs() {
			return ls;
		}

		public double getPrior() {
			return prior;
		}


	}

	public void classify(File f, boolean compress) throws IOException {
		logger.info("classifying " + f);
		long begin = System.currentTimeMillis(), end = 0;
		LineNumberReader lnr = null;
		if (compress) {
			lnr = new LineNumberReader(new InputStreamReader(new SnappyInputStream(new FileInputStream(f)), "UTF-8"));
		}
		else {
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
		}

		Tokenizer tokenizer = HardTokenizer.getInstance();
		String line;
		String[] s;
		int tot = 0;
		Node[][] nodes;
		logger.info("totalFreq\tsize\ttime (ms)\tdate");
		int tp = 0, fp = 0, fn = 0;
		while ((line = lnr.readLine()) != null) {
			s = tabPattern.split(line);
			nodes = mapInstance(s);
			Sense[] senses = classify(s, nodes);
			String page = "";
			if (senses.length > 0)
			{
				if (senses[0].getCombo() == 0)
				{
					page = "Null_result";
				}
				else
				{
					page = senses[0].getPage();
				}

				/*logger.info("i\tprior\tbow\tls\tcombo\tpage");
				for (int i = 0; i < senses.length; i++) {
					logger.info(i + "\t" + rf.format(senses[i].getPrior()) + "\t" + rf.format(senses[i].getBow()) + "\t" + rf.format(senses[i].getLs()) + "\t" + rf.format(senses[i].getCombo()) + "\t" + senses[i].getPage());
				}*/
			}

			if (s[0].equals(page))
			{
				tp++;
			}
			else
			{
				fp++;
				fn++;
			}
			logger.debug(tot + "\t" + tp + "\t" + fp + "\t" + fn + "\t" + s[0] + "\t" + s[1] + "\t" + page);

			tot++;
		}
		lnr.close();
		end = System.currentTimeMillis();

		double precision = (double) tp / (tp + fp);
		double recall = (double) tp / (tp + fn);
		double f1 = (2 * precision * recall) / (precision + recall);
		logger.debug(tot + "\t" + tp + "\t" + fp + "\t" + fn + "\t" + rf.format(precision) + "\t" + rf.format(recall) + "\t" + rf.format(f1) );

		logger.info(df.format(tot) + "\t" + df.format(end - begin) + "\t" + new Date());

		logger.info("ending the process " + new Date() + "...");
	}

	//todo: use the PageIncomingOutgoingSearcher strategy
	private Sense[] classify(String[] s, Node[][] nodes)
	{
		long begin = System.nanoTime();
		OneExamplePerSenseSearcher.Entry[] entries = oneExamplePerSenseSearcher.search(s[3]);
		Sense[] senses = new Sense[entries.length];
		long end = System.nanoTime();
		for (int i = 0; i < entries.length; i++) {
			//logger.debug(i + "\t" + Node.toString(entries[i].getBowVector()));
			//logger.debug(i + "\t" + Node.toString(entries[i].getLsVector()));
			if (normalized)
			{
				Node.normalize(entries[i].getBowVector());
				Node.normalize(entries[i].getLsVector());
			}

			double bowKernel = Node.dot(nodes[0], entries[i].getBowVector());
			double lsKernel = Node.dot(nodes[1], entries[i].getLsVector());

			//double bowKernel = Node.dot(nodes[0], entries[i].getBowVector()) / Math.sqrt(Node.dot(nodes[0], nodes[0]) * Node.dot(entries[i].getBowVector(), entries[i].getBowVector()));
			//double lsKernel = Node.dot(nodes[1], entries[i].getLsVector()) / Math.sqrt(Node.dot(nodes[1], nodes[1]) * Node.dot(entries[i].getLsVector(), entries[i].getLsVector()));

			logger.debug(i + "\t" + entries[i].getPage() + "\t" + rf.format(bowKernel) + "\t" + rf.format(lsKernel) + "\t" + rf.format(entries[i].getFreq()));
			senses[i] = new Sense(entries[i].getPage(), bowKernel, lsKernel, entries[i].getFreq());
		}
		Arrays.sort(senses, new Comparator<Sense>() {
			@Override
			public int compare(Sense sense, Sense sense2) {
				double diff = sense.getCombo() - sense2.getCombo();
				//double diff = sense.getBow() - sense2.getBow();
				if (diff > 0) {
					return -1;
				}
				else if (diff < 0) {
					return 1;
				}
				return 0;
			}
		});
		return senses;
	}

	private Node[][] mapInstance(String[] s) {
		Tokenizer tokenizer = HardTokenizer.getInstance();

		BOW bow = new BOW();
		String[] left = tokenizer.stringArray(s[2].toLowerCase());
		bow.addAll(left);
		if (s.length == 5) {
			String[] right = tokenizer.stringArray(s[4].toLowerCase());
			bow.addAll(right);
		}
		logger.debug(bow);
		Vector bowVector = lsm.mapDocument(bow);
		Vector lsVector = lsm.mapPseudoDocument(bowVector);

		if (normalized) {
			bowVector.normalize();
			lsVector.normalize();
		}
		logger.debug("bow\t" + bowVector);
		//logger.debug("lsi\t" + lsVector);

		Node[][] nodes = new Node[2][];
		nodes[0] = bowVector.toNodeArray();
		nodes[1] = lsVector.toNodeArray();
		return nodes;
	}

	public static void main(String args[]) throws Exception {
		String logConfig = System.getProperty("log-config");
		if (logConfig == null) {
			logConfig = "configuration/log-config.txt";
		}

		PropertyConfigurator.configure(logConfig);
		Options options = new Options();
		try {
			Option indexNameOpt = new OptionBuilder().withArgName("dir").hasArg().withDescription("open an index with the specified name").isRequired().withLongOpt("index").toOption("i");
			Option interactiveModeOpt = new OptionBuilder().withDescription("enter in the interactive mode").withLongOpt("interactive-mode").toOption("t");
			Option instanceFileOpt = new OptionBuilder().withArgName("file").hasArg().withDescription("read the instances to classify from the specified file").withLongOpt("instance-file").toOption("f");
			Option lsmDirOpt = new OptionBuilder().withArgName("dir").hasArg().withDescription("lsm dir").isRequired().withLongOpt("lsm").toOption("l");
			Option lsmDimOpt = new OptionBuilder().withArgName("int").hasArg().withDescription("lsm dim").withLongOpt("dim").toOption("d");
			Option normalizedOpt = new OptionBuilder().withDescription("normalize vectors (default is " + WikipediaExtractor.DEFAULT_NORMALIZE + ")").withLongOpt("normalized").toOption();

			options.addOption("h", "help", false, "print this message");
			options.addOption("v", "version", false, "output version information and exit");

			options.addOption(indexNameOpt);
			options.addOption(interactiveModeOpt);
			options.addOption(instanceFileOpt);
			options.addOption(lsmDirOpt);
			options.addOption(lsmDimOpt);
			options.addOption(normalizedOpt);

			CommandLineParser parser = new PosixParser();
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help") || line.hasOption("version")) {
				throw new ParseException("");
			}

			int minFreq = OneExamplePerSenseSearcher.DEFAULT_MIN_FREQ;
			if (line.hasOption("minimum-freq")) {
				minFreq = Integer.parseInt(line.getOptionValue("minimum-freq"));
			}

			int notificationPoint = Defaults.DEFAULT_NOTIFICATION_POINT;
			if (line.hasOption("notification-point")) {
				notificationPoint = Integer.parseInt(line.getOptionValue("notification-point"));
			}

			String lsmDirName = line.getOptionValue("lsm");
			if (!lsmDirName.endsWith(File.separator)) {
				lsmDirName += File.separator;
			}

			boolean normalized =  WikipediaExtractor.DEFAULT_NORMALIZE ;
			if (line.hasOption("normalized")) {
				normalized = true;
			}

			File fileUt = new File(lsmDirName + "X-Ut");
			File fileSk = new File(lsmDirName + "X-S");
			File fileR = new File(lsmDirName + "X-row");
			File fileC = new File(lsmDirName + "X-col");
			File fileDf = new File(lsmDirName + "X-df");
			int dim = 100;
			if (line.hasOption("dim")) {
				dim = Integer.parseInt(line.getOptionValue("dim"));
			}
			logger.debug(line.getOptionValue("lsm") + "\t" + line.getOptionValue("dim"));

			LSM lsm = new LSM(fileUt, fileSk, fileR, fileC, fileDf, dim, true, normalized);
			OneExamplePerSenseSearcher oneExamplePerSenseSearcher = new OneExamplePerSenseSearcher(line.getOptionValue("index"));
			oneExamplePerSenseSearcher.setNotificationPoint(notificationPoint);

			if (line.hasOption("interactive-mode")) {
				//oneExamplePerSenseSearcher.interactive(lsm);
			}

			if (line.hasOption("instance-file")) {
				OneExamplePerSenseClassifier oneExamplePerSenseClassifier = new OneExamplePerSenseClassifier(lsm, oneExamplePerSenseSearcher, normalized);
				oneExamplePerSenseClassifier.classify(new File(line.getOptionValue("instance-file")), false);
			}
		} catch (ParseException e) {
			// oops, something went wrong
			if (e.getMessage().length() > 0) {
				System.out.println("Parsing failed: " + e.getMessage() + "\n");
			}
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp(400, "java -cp dist/thewikimachine.jar eu.fbk.twm.wiki.OneExamplePerSenseClassifier", "\n", options, "\n", true);
		}
	}
}
