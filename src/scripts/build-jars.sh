#!/bin/sh

set -e

JARORIG=`ls -1 target/thewikimachine-*-jar-with-dependencies.jar | tail -1`
if [ -z "$JARORIG" ]; then
    exit 1;
fi
JAR=${JARORIG%.*}
JAR=$JAR-only.jar
echo $JAR

cp $JARORIG $JAR
zip -d $JAR org/fbk/cit/hlt/thewikimachine/*
