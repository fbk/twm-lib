Lingue:
be  bg  ca  cs  da  de  en  es  et  fi  fr  hr  hu  id  is  it  lt  lv  nl  no  pl  pt  ro  ru  sk  sl  sq  sr  sv  tr  uk
nl ru sl tr

---

export CLASSPATH=thewikimachine.jar:twm-deps.jar

# ESTRAE TUTTO
java eu.fbk.twm.wiki.ModelExtractor

# ESTRAE L'INDICE DI TPL-LABEL-VALUE
java org.fbk.cit.hlt.thewikimachine.dbpedia.TemplateLabelValueIndexCreator \
        -i /data/corpora/wikipedia/ptwiki-20130125-pages-articles.xml \
    -o data-new/pt/20130125/ \
    -t 12

# CREA IL BIN (non necessario)
java -cp thewikimachine.jar -Xmx6G org.fbk.cit.hlt.thewikimachine.dbpedia.BuildUniqueTemplateLabelArchive \
    -i temp/ptwiki-20130125-template-properties-index/ \
    -o temp/ptwiki-20130125-infobox-label.bin

# CREA L'INDICE DEI TEMPLATE GREZZI
java org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaTemplateContentExtractor \
    -i /data/corpora/wikipedia/ptwiki-20130125-pages-articles.xml \
    -o data/ \
    -t 12

# INDICE DBPEDIA PROPERTIES
/data/models/dbpedia/properties-index-complete/
(language, page, property, value, lang-page, lang-page-property)
org.fbk.cit.hlt.thewikimachine.dbpedia.ExtractDBpediaDumpsProperties

# MEGA CROSS-LANGUAGE SCHEMA
/data/models/dbpedia/20130301/clschema


TUTTO

java eu.fbk.twm.wiki.ModelExtractor -d /data/corpora/wikipedia/itwiki-20130131-pages-articles.xml -r --templates 2 5 2 -o data/ -t 12 --base-dir
java org.fbk.cit.hlt.thewikimachine.dbpedia.TemplateLabelValueIndexCreator -i /data/corpora/wikipedia/itwiki-20130131-pages-articles.xml -o data/it/20130131/ -t 12
java org.fbk.cit.hlt.thewikimachine.xmldump.WikipediaTemplateContentExtractor -i /data/corpora/wikipedia/itwiki-20130131-pages-articles.xml -o data/ -t 12
java org.fbk.cit.hlt.thewikimachine.xmldump.DBpediaMappingsPropertiesExtractor mappings/Mappings_it.xml it-mappings.txt it

java -Xmx50G org.fbk.cit.hlt.thewikimachine.airpedia.CompareDBpediaWikipediaML -D /data/models/dbpedia/properties-index-complete/ -C configuration/ -L /data/models/dbpedia/20130301/clschema/ -O /data/corpora/dbpedia/ontology.owl -B data/ -l it -t 12 -p birthDate -n 1000 -T

Cosa linka qui: http://en.wikipedia.org/wiki/Special:WhatLinksHere/


####

Nel portoghese il mapping "occupation" dell'artista musicale e' toppato.
http://pt.wikipedia.org/w/index.php?title=John_Lennon&action=edit
http://pt.wikipedia.org/w/index.php?title=John_Lennon&action=edit
http://mappings.dbpedia.org/index.php/Mapping_pt:Info/M%C3%BAsica/artista


Nel paper [Rinser et al., 2012] viene fatto il mapping tra pagine, infobox e proprieta'.
Limitazioni:
- Vengono mappati campi tra infobox che in varie lingue significano la medesima cosa
- Viene calcolato uno score di somiglianza tra attributi della infobox

Il nostro sistema corregge anche gli errori
- Vengono eliminate una alla volta le altre lingue, cosi' da identificare eventuali mapping sbagliati
- Il tipo di dato viene controllato in modo che sia coerente con l'ontologia (uso di Airpedia)

TODO:
- Ripescare successivamente i possibili candidati


time java -Xmx50G org.fbk.cit.hlt.thewikimachine.airpedia.CompareDBpediaWikipediaML -D /data/models/dbpedia/properties-index-complete/ -C configuration/ -L /data/models/dbpedia/20130301/clschema/ -O /data/corpora/dbpedia/ontology.owl -B data/ -l en -t 12 -T -r experiments/20130504-1800/en.bin

Differenza parsando o no i template.
23s [main] INFO  (CompareDBpediaWikipediaML.java:1402) - {res=1.0, rank=1.0, value=0.4842364961003439, prop_birthPlace=1.0}
38s [main] INFO  (CompareDBpediaWikipediaML.java:1402) - {res=1.0, rank=1.0, value=0.48989184213308196, prop_birthPlace=1.0}

time java -Xmx50G org.fbk.cit.hlt.thewikimachine.airpedia.CompareDBpediaWikipediaML -D /data/models/dbpedia/properties-index-complete/ -C configuration/ -L /data/models/dbpedia/20130301/clschema/ -O /data/corpora/dbpedia/ontology.owl -B data/ -l fr -t 12 -T -r experiments/20130504-1800/fr.bin > experiments/20130504-1800/log-fr.txt


FOLDER=$1
MEM=6000
CORES=8

echo ${FOLDER}
export OMP_NUM_THREADS=${CORES}
lib/libsvm-3.12/svm-train -t 0 -b 1 -m ${MEM} data/svm/${FOLDER}/test.txt data/svm/${FOLDER}/model-test.bin


real    67m38.317s
user    212m54.034s
sys     28m5.869s

real    74m0.607s
user    211m51.586s
sys     85m34.057s

real    102m0.147s
user    320m46.075s
sys     78m35.235s

real    49m59.258s
user    120m33.532s
sys     31m24.862s

real    106m45.194s
user    345m31.252s
sys     102m43.389s

real    485m59.553s
user    2716m20.634s
sys     315m53.397s


java -Xmx50G org.fbk.cit.hlt.thewikimachine.airpedia.CompareDBpediaWikipediaML \
    -D /data/models/dbpedia/properties-index-complete/ -C configuration/ \
    -L /data/models/dbpedia/20130301/clschema/ -O /data/corpora/dbpedia/ontology.owl -B data/ \
    -l it -t 12 -p birthDate


[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/artista_musicale/didascalia/birthDate: {res=0.0, rank=1.0, value=0.0022, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/artista_musicale/nota_genere/birthDate: {res=0.0, rank=0.5, value=0.0014666666666666667, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/artista_musicale/nota_genere7/birthDate: {res=0.0, rank=0.3333333333333333, value=7.333333333333333E-4, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/bio/giornomesenascita/birthDate: {res=1.0, rank=1.0, value=0.6266911206677802, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/bio/annonascita/birthDate: {res=0.0, rank=0.5, value=0.32440677966101655, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/bio/giornomesemorte/birthDate: {res=0.0, rank=0.3333333333333333, value=0.018783202630913154, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/bio/fineincipit/birthDate: {res=0.0, rank=0.25, value=0.002342524664811536, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/bio/epoca/birthDate: {res=0.0, rank=0.2, value=0.001419175309891222, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/bio/immagine/birthDate: {res=0.0, rank=0.16666666666666666, value=0.0011687326081457122, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/bio/notenascita/birthDate: {res=0.0, rank=0.14285714285714285, value=4.1740450290918296E-4, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/bio/didascalia/birthDate: {res=0.0, rank=0.125, value=2.5044270174550976E-4, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/bio/luogonascita/birthDate: {res=0.0, rank=0.1111111111111111, value=1.6696180116367317E-4, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/box_successione/periodo/birthDate: {res=0.0, rank=1.0, value=0.07275080906148848, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/box_successione/precedente/birthDate: {res=0.0, rank=0.5, value=0.0026699029126213596, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/box_successione/immagine/birthDate: {res=0.0, rank=0.3333333333333333, value=0.0010679611650485437, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/box_successione/periodo3/birthDate: {res=0.0, rank=0.25, value=5.339805825242719E-4, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/sportivo/aggiornato/birthDate: {res=0.0, rank=1.0, value=0.004615384615384616, prop_birthDate=1.0}
[main] INFO  (CompareDBpediaWikipediaML.java:1496) - it/sportivo/rigavuota/birthDate: {res=0.0, rank=0.5, value=0.002307692307692308, prop_birthDate=1.0}
