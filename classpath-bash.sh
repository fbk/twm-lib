export CLASSPATH=dist/thewikimachine-alone.jar
export CLASSPATH=${CLASSPATH}:dist/thewikimachine-lib.jar

export CLASSPATH=${CLASSPATH}:properties/

export CLASSPATH=${CLASSPATH}:lib/bliki-core-3.0.19.jar
export CLASSPATH=${CLASSPATH}:lib/de.tudarmstadt.ukp.wikipedia.parser-0.9.2.jar
export CLASSPATH=${CLASSPATH}:lib/jcore-alone.jar